#include "Level.h"

void Level::LoadMap( string filename )
{
	ifstream infile;
	infile.open( filename.c_str() );
	if ( infile.bad() )
	{
		cerr<<"Error loading game map "<<filename<<endl;
	}
	string temp;
	infile>>temp;       //x
    infile>>temp;       //160
    infile>>temp;       //y
    infile>>temp;       //60
    infile>>temp;       //bgimage
    infile>>temp;       //background, none in this game.
    
    //BOTTOM LAYER
    for ( int j=0; j<TILEY; j++ )
    {
		for ( int i=0; i<TILEX; i++ )
		{
			infile>>temp;
			//Basic filmstrip coordinates, world coordinates
			tile[0][i][j].fx = atoi( temp.c_str() ) * TILEWH;
			tile[0][i][j].x = i * TILEWH;
			tile[0][i][j].y = j * TILEWH;
			//Collision? (rects, yay!)
		}
	}
	//MIDDLE LAYER
    for ( int j=0; j<TILEY; j++ )
    {
		for ( int i=0; i<TILEX; i++ )
		{
			infile>>temp;
			//Basic filmstrip coordinates, world coordinates
			tile[1][i][j].fx = atoi( temp.c_str() ) * TILEWH;
			tile[1][i][j].x = i * TILEWH;
			tile[1][i][j].y = j * TILEWH;
			//Collision?
		}
	}
	//TOP LAYER
    for ( int j=0; j<TILEY; j++ )
    {
		for ( int i=0; i<TILEX; i++ )
		{
			infile>>temp;
			//Basic filmstrip coordinates, world coordinates
			tile[2][i][j].fx = atoi( temp.c_str() ) * TILEWH;
			tile[2][i][j].x = i * TILEWH;
			tile[2][i][j].y = j * TILEWH;
			//Collision?
		}
	}
	infile.close();
}

void Level::DrawBottomLayer( SDL_Surface *buffer, SDL_Surface *tileset, float offsetX, float offsetY )
{
	SDL_Rect clipCoord;
	SDL_Rect tileCoord;
	
	clipCoord.w = TILEWH;
	clipCoord.h = TILEWH;
	tileCoord.w = TILEWH;
	tileCoord.h = TILEWH;
	for ( int i=0; i<TILEX; i++ )
	{	
		for ( int j=0; j<TILEY; j++ )
		{
			if ( tile[0][i][j].fx != 0 )
			{
				//Draw
				clipCoord.x = tile[0][i][j].fx;
				clipCoord.y = tile[0][i][j].fy;
				tileCoord.x = (int)(tile[0][i][j].x - offsetX);
				tileCoord.y = (int)(tile[0][i][j].y - offsetY);
				
				SDL_BlitSurface( tileset, &clipCoord, buffer, &tileCoord );
			}
			if ( tile[1][i][j].fx != 0 )
			{
				//Draw
				clipCoord.x = tile[1][i][j].fx;
				clipCoord.y = tile[1][i][j].fy;
				tileCoord.x = (int)(tile[1][i][j].x - offsetX);
				tileCoord.y = (int)(tile[1][i][j].y - offsetY);
				SDL_BlitSurface( tileset, &clipCoord, buffer, &tileCoord );
			}
		}
	}
}

void Level::DrawTopLayer( SDL_Surface *buffer, SDL_Surface *tileset, float offsetX, float offsetY )
{
	SDL_Rect clipCoord;
	SDL_Rect tileCoord;
	
	clipCoord.w = TILEWH;
	clipCoord.h = TILEWH;
	tileCoord.w = TILEWH;
	tileCoord.h = TILEWH;
	for ( int i=0; i<TILEX; i++ )
	{
		for ( int j=0; j<TILEY; j++ )
		{
			if ( tile[2][i][j].fx != 0 )
			{
				//Draw
				clipCoord.x = tile[2][i][j].fx;
				clipCoord.y = tile[2][i][j].fy;
				tileCoord.x = (int)(tile[2][i][j].x - offsetX);
				tileCoord.y = (int)(tile[2][i][j].y - offsetY);
				
				SDL_BlitSurface( tileset, &clipCoord, buffer, &tileCoord );
			}
		}
	}
}








