#pragma once
// ~ function from LazyFoo's tutorial
class Timer
{
	private:
		int startTicks;
		int pausedTicks;
		bool paused;
		bool started;
	public:
		Timer();
		void Start();
		void Stop();
		void Pause();
		void Unpause();
		int GetTicks();
		bool IsStarted();
		bool IsPaused();
};



