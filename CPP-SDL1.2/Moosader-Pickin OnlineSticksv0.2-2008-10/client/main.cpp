#include "SDL/SDL.h"
#include <iostream>
#include <fstream>
#include "Game.h"
#include "Timer.h"
#include "Player.h"
#include "Input.h"
#include "Level.h"
#include "SDL_net.h"

/*
Linker:
-lmingw32 -lSDLmain -lSDL  -mwindows
../../../../0Apps/DevCPP/lib/libws2_32.a
*/

enum GameState { MENU, INGAME };
enum Key { KEYNONE, KEYUP, KEYDOWN, KEYLEFT, KEYRIGHT };

void Draw( SDL_Surface *destination, SDL_Surface *source, int x, int y );
SDL_Surface *LoadImage( string filename );
const int CHARSIZE = 1024;

void TrySend( string message, TCPsocket *socket )
{
    cout<<"send message: "<<message<<endl;
	if ( SDLNet_TCP_Send( *socket, message.c_str(), message.size() ) < message.size() )
    {
        cerr<<"SDLNet_TCP_Send: "<<SDLNet_GetError()<<endl;
    }
}

void ReadConfig( string *ip, Uint16 *port )
{
    ifstream infile;
    infile.open( "config.txt" );
    if ( infile.bad() ) 
    {
        cerr<<"Error loading config.txt!"<<endl;
        exit(10);
    }
    string temp;
    infile>>temp;       //"host:"
    infile>>*ip;       //IP address
    infile>>temp;       //"port:"
    infile>>*port;
    
    infile.close();
}

int main( int argc, char *args[] )
{
	Game posGame;
	SDL_Event event;
	//internet stuff
	IPaddress ipaddress;
	string szIP;
	TCPsocket socket;
	char messageBuffer[CHARSIZE];
	int len, result;
	Uint16 port = 666;
	bool connected = false;
	if ( SDLNet_Init() != 0 )
	{
        cerr<<"SDLNet_Init(): "<<SDLNet_GetError()<<endl;
        exit(10);
    }
    
    ReadConfig( &szIP, &port );
    cout<<"connecting to "<<szIP<<",\t"<<port<<endl;
    
    if ( !SDLNet_ResolveHost( &ipaddress, szIP.c_str(), port ) )
    {
        //open socket
        socket = SDLNet_TCP_Open( &ipaddress );
        if ( !socket )
        {
            cerr<<"SDLNet_TCP_Open: "<<SDLNet_GetError()<<endl;
            exit(10);
        }
        else
        {
            connected = true;
        }
    }
    
	bool done = false;
	float gameCounter = 0.0f;
	float offsetX = 0, offsetY = 0;
	int frame = 0;
	Timer fps;
	Input input;
	Key key = KEYNONE;
	GameState state = INGAME;
	SDL_Surface *playerBase = NULL;
	SDL_Surface *hairBase = NULL;
	SDL_Surface *clothesBase = NULL;
	SDL_Surface *tileset = NULL;
	playerBase = LoadImage( "playerbase.bmp" );
	hairBase = LoadImage( "hairbase.bmp" );
	clothesBase = LoadImage( "clothesbase.bmp" );
	tileset = LoadImage( "tiles.bmp" );
	string strMessageBuffer;
	
	Level yardLevel;
	yardLevel.LoadMap( "yard.map" );
	Player localPlayer;
	localPlayer.Setup( 640, 120, 64, 96, 0, 0, 0 );
	
	while ( !done )
	{
		fps.Start();
		while ( SDL_PollEvent( &event ) )
		{
			if ( event.type == SDL_QUIT ) { done = true; }
		}
		if ( input.GetKey( 27 ) ) { done = true; }
		
		//UPDATES----------------------------------------------
		//Menu
		if ( state == MENU )
		{
		} // end menu
		//Game
		else if ( state == INGAME && connected )
		{
			//PLAYER MOVEMENT
			if ( input.GetKey( SDLK_UP ) )
			{
				if ( offsetY - 5 > 0 )
					offsetY -= 5;
				key = KEYUP;
			}
			else if ( input.GetKey( SDLK_DOWN ) )
			{
				offsetY += 5;
				key = KEYDOWN;
			}
			else if ( input.GetKey( SDLK_LEFT ) )
			{
				if ( offsetX - 5 > 0 )
					offsetX -= 5;
				key = KEYLEFT;
			}
			else if ( input.GetKey( SDLK_RIGHT ) )
			{
				offsetX += 5;			
				key = KEYRIGHT;
			}
			else
			{
                if      ( key == KEYUP ) { TrySend( "KEY PRESS - UP   ", &socket ); }
                else if ( key == KEYDOWN ) { TrySend( "KEY PRESS - DOWN ", &socket ); }
                else if ( key == KEYRIGHT ) { TrySend( "KEY PRESS - RIGHT", &socket ); }
                else if ( key == KEYLEFT ) { TrySend( "KEY PRESS - LEFT ", &socket ); }
                key = KEYNONE;
            }
			if ( input.GetKey( SDLK_F2 ) )
			{
                TrySend( "greets server", &socket );
            }
            
            if ( SDLNet_SocketReady( socket ) )
            {
                if ( SDLNet_TCP_Recv( socket, messageBuffer, CHARSIZE ) > 0 )
                {
                    cout<<"received message: "<<messageBuffer<<endl;
                }
            }
            
		} // end in game
		
		//DRAW---------------------------------------------------
		yardLevel.DrawBottomLayer( posGame.buffer, tileset, offsetX, offsetY );
		localPlayer.Draw( posGame.buffer, playerBase, clothesBase, hairBase, offsetX, offsetY );
		yardLevel.DrawTopLayer( posGame.buffer, tileset, offsetX, offsetY );
		posGame.Update();
		frame++;
		if ( fps.GetTicks() < 1000 / posGame.FPS() )
		{
			SDL_Delay( ( 1000 / posGame.FPS() ) - fps.GetTicks() );
		}
	}	
	SDLNet_TCP_Close( socket );
	SDL_FreeSurface( clothesBase );
	SDL_FreeSurface( hairBase );
	SDL_FreeSurface( playerBase );
	SDL_FreeSurface( tileset );
	SDLNet_Quit();
	
    return 0;
}

void Draw( SDL_Surface *destination, SDL_Surface *source, int x, int y )
{
	SDL_Rect offset;
	offset.x = x;		offset.y = y;
	SDL_BlitSurface( source, NULL, destination, &offset );
}

SDL_Surface *LoadImage( string filename )
{
	SDL_Surface *loadedImage = NULL;
	SDL_Surface *optimizedImage = NULL;
	loadedImage = SDL_LoadBMP( filename.c_str() );
	if ( loadedImage != NULL )
	{
		optimizedImage = SDL_DisplayFormat( loadedImage );
		SDL_FreeSurface( loadedImage );
		if ( optimizedImage != NULL )
		{
			//make that pink color transparent
			Uint32 colorkey = SDL_MapRGB( optimizedImage->format, 0xFF, 0, 0xFF );
			SDL_SetColorKey( optimizedImage, SDL_SRCCOLORKEY, colorkey );
		}
	}
	else
	{
		cerr<<"Error loading image "<<filename<<endl;
	}
	return optimizedImage;
}


