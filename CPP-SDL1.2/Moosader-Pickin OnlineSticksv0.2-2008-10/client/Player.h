#pragma once
#include "SDL/SDL.h"
#include "GenericObject.h"
#include <iostream>
using namespace std;

enum Direction { UP = 1, DOWN = 0, LEFT = 2, RIGHT = 3 };



class Player : public GenericObject
{
	private:
		int hairImg, clothesImg, skinImg, frame;
		float speed;
		Direction direction;
	public:
		float Speed() { return speed; }
		void Speed( float val ) { speed = val; }
		int Hair() { return hairImg; }
		void Hair( int val ) { hairImg = val; }
		int Clothes() { return clothesImg; }
		void Clothes( int val ) { clothesImg = val; }
		int Skin() { return skinImg; }
		void Skin( int val ) { skinImg = val; }
		Player();
		void Draw( SDL_Surface *buffer, SDL_Surface *base, SDL_Surface *clothes, SDL_Surface *hair, float offsetX, float offsetY );
		void Setup( int X, int Y, int W, int H, int Hair, int Clothes, int Skin );
		void Move( Direction );
};
