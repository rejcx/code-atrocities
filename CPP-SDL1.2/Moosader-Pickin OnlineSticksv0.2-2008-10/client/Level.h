#pragma once
#include "Tile.h"
#include "SDL/SDL.h"
#include <string>
#include <iostream>
#include <fstream>
using namespace std;

const int TILEX = 64;
const int TILEY = 48;
const int TILEWH = 64;		//tiles are square, w = h = 64

class Level
{
	private:
	public:
		Tile tile[3][TILEX][TILEY];
		void LoadMap( string filename );
		void DrawTopLayer( SDL_Surface *buffer, SDL_Surface *tileset, float, float );		
		void DrawBottomLayer( SDL_Surface *buffer, SDL_Surface *tileset, float, float );
};
