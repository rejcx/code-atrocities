#include "Timer.h"
#include "SDL/SDL.h"

Timer::Timer()
{
	startTicks = pausedTicks = 0;
	paused = started = false;
}

void Timer::Start()
{
	started = true;
	paused = false;
	startTicks = SDL_GetTicks();
}

void Timer::Stop()
{
	started = false;
	paused = false;
}

void Timer::Pause()
{
	if ( ( started ) && ( paused ) )
	{
		paused = true;
		pausedTicks = SDL_GetTicks() - startTicks;
	}
}

void Timer::Unpause()
{
	if ( paused )
	{
		paused = false;
		startTicks = SDL_GetTicks() - pausedTicks;
		pausedTicks = 0;
	}
}

int Timer::GetTicks()
{
	if ( started )
	{
		if ( paused )
		{
			return pausedTicks;
		}
		else
		{
			return SDL_GetTicks() - startTicks;
		}
	}
	return 0;	//If the timer isn't running
}

bool Timer::IsStarted() { return started; }

bool Timer::IsPaused() { return paused; }

