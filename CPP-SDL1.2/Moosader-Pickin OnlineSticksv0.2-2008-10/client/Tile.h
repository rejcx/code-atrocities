#pragma once
#include "SDL/SDL.h"

class Tile
{
    public:
        int x, y, w, h, fx, fy;
        SDL_Rect region;
        bool solid;
        Tile()
        {
            x = y = fx = fy = 0;
            w = h = 64;
            solid = false;
            region.x = 0;
            region.y = 0;
            region.w = 64;
            region.h = 64;
        }
};
