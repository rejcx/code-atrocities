#pragma once
#include "SDL/SDL.h"
#include <string>
#include <iostream>
using namespace std;

class Game
{
	private:
		int screenWidth, screenHeight, screenBPP, maxFPS;
	public:
		SDL_Surface *buffer;
		Game();
		~Game();

		int ScreenWidth();
		int ScreenHeight();
		int ScreenBPP();
		int FPS();

		void SetWindowCaption( string caption );

		void Update();
};
