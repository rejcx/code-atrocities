#include "Player.h"

void Player::Draw( SDL_Surface *buffer, SDL_Surface *base, SDL_Surface *clothes, SDL_Surface *hair, float offsetX, float offsetY )
{
	SDL_Rect clipCoord;
	SDL_Rect tileCoord;
	
	clipCoord.w = 64;
	clipCoord.h = 96;
	tileCoord.w = 64;
	tileCoord.h = 96;
	
	clipCoord.x = (int)frame*64;
	clipCoord.y = 0;
	tileCoord.x = (int)(x - offsetX);
	tileCoord.y = (int)(y - offsetY);

	/*
	For some reason, blitting clothes and hair to the "buffer" after the base
	results in it not... scrolling off the screen... o_o;  Like sticking to the
	(0, 0) coordinates.  NO IDEA WHY.  I AM CONFUSED. TT_TT
	--Rachie
	*/
	SDL_BlitSurface( clothes, 	&clipCoord, base, &clipCoord );
	SDL_BlitSurface( hair, 		&clipCoord, base, &clipCoord );
	
	SDL_BlitSurface( base, 		&clipCoord, buffer, &tileCoord );
}

void Player::Setup( int X, int Y, int W, int H, int Hair, int Clothes, int Skin )
{
	x = X;
	y = Y;
	w = W;
	h = H;
	hairImg = Hair;
	clothesImg = Clothes;
	skinImg = Skin;
	frame = 1;
}

Player::Player()
{
	x = y = 0;
	w = 64;
	y = 96;
	hairImg = clothesImg = skinImg = 0;
	frame = 0;
	speed = 5.0;
}

void Player::Move( Direction dir )
{
	if ( dir == UP )
		y = (int)( y - speed );
	else if ( dir == DOWN )
		y = (int)( y + speed );
	else if ( dir == LEFT )
		x = (int)( x - speed );
	else if ( dir == RIGHT )
		x = (int)( x + speed );
}
