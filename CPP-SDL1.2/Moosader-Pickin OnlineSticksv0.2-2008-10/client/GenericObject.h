#pragma once
#include "SDL/SDL.h"
#include <iostream>
#include <string>
using namespace std;

class GenericObject
{
    protected:
        int x, y, w, h, fx, fy;
        string name;
    public:
        GenericObject();
        int X() { return x; }
		void X( int val ) { x = val; }
		int Y() { return y; }
		void Y( int val ) { y = val; }
		int W() { return w; }
		void W( int val ) { w = val; }
		int H() { return h; }
		void H( int val ) { h = val; }
		void Setup( int X, int Y, int W, int H, int FX, int FY );
		void Draw( SDL_Surface *buffer, SDL_Surface *source, float offsetX, float offsetY );
};

