#include <iostream>
#include <string>
#include "SDL_net.h"
#include "SDL/SDL.h"
#include "SDL/SDL_ttf.h"
#include "Input.h"
#include "SDL/SDL_thread.h"
using namespace std;

/*
-lmingw32 -lSDLmain -lSDL 

../../../../0Apps/DevCPP/lib/libws2_32.a
*/

SDL_Surface *LoadImage( string filename );
void Draw( SDL_Surface *destination, SDL_Surface *source, int x, int y );

const int SCRW = 480;
const int SCRH = 240;
const int SCRBPP = 32;
const int TXTLINES = 32;
const int CHARSIZE = 1024;

SDL_Surface *screen = NULL;

TTF_Font *font = NULL;
SDL_Color colorNormal = { 255, 255, 255 };
SDL_Color colorWarning = { 255, 0, 0 };
SDL_Color colorEvent = { 0, 255, 0 };
SDL_Color colorConnection = { 0, 0, 255 };

SDL_Thread *thread = NULL;
bool done = false;
bool connected = false;
TCPsocket sockServer, sockClient;
IPaddress ipServer, *ipClient;
SDL_Surface *text[TXTLINES];
string szBuffer;
char messageBuffer[CHARSIZE];
int line = 2;

int ThreadFunction( void *data )
{
    while ( !done )
    {
        while ( connected )
        {
            if ( SDLNet_TCP_Recv( sockClient, messageBuffer, 1024 ) > 0 )
            {
                cout<<"receiving message: "<<messageBuffer<<endl;
                text[line++] = TTF_RenderText_Solid( font, messageBuffer, colorEvent );
            }
            cerr<<"thread: "<<SDLNet_GetError()<<endl;
        }
    }
    return 0;
}

void Init();
	
int main(int argc, char **argv)
{
    Init();
    thread = SDL_CreateThread( ThreadFunction, NULL );
    Input input;
	
	Uint16 port = 666;
	int len;
	Uint32 ipaddr;
	if ( SDLNet_Init() != 0 )
	{
        cerr<<"SDLNet_Init(): Error initializing SDLNet"<<endl;
        cerr<<" "<<SDLNet_GetError()<<endl;
        exit(10);
    }
	SDL_Event event;
	font = TTF_OpenFont( "cour.ttf", 13 );
	for ( int i=0; i<TXTLINES; i++ )
	{
        text[i] = NULL;
    }
    
    if ( SDLNet_ResolveHost( &ipServer, NULL, port ) != 0 )
    {
        cerr<<"SDLNet_ResolveHost: "<<SDLNet_GetError()<<endl;
        exit(10);
    }
    sockServer = SDLNet_TCP_Open( &ipServer );
	
	text[0] = TTF_RenderText_Solid( font, "Pickin' Online Sticks - Server", colorNormal );
	text[1] = TTF_RenderText_Solid( font, "(c) Moosader Games / Rachel J. Morris, 2008", colorNormal );
    text[30] = TTF_RenderText_Solid( font, "----------------------------------------------------------------------------------------", colorNormal );
    text[31] = TTF_RenderText_Solid( font, "ESC - Quit", colorNormal );
    
    //Initialize
    szBuffer = "Listening on port ";
    itoa( port, messageBuffer, 10 );
    szBuffer += messageBuffer;
    text[line++] = TTF_RenderText_Solid( font, szBuffer.c_str(), colorEvent );
    
    cout<<"begin hosting"<<endl;
    	
	while( !done )
	{
        while ( SDL_PollEvent( &event ) )
        {
            if ( event.type == SDL_QUIT )
            {
                SDLNet_TCP_Close( sockClient );
                done = true;
            }
            if ( input.GetKey( 27 ) ) { SDLNet_TCP_Close( sockClient ); done = true; }
        }
        if ( !connected )
        {
            sockClient = SDLNet_TCP_Accept( sockServer );        
            if ( sockClient )
            {
                cout<<"client was accepted"<<endl;
                ipClient = SDLNet_TCP_GetPeerAddress( sockClient );
                cout<<"ipClient = "<<ipClient<<endl;
                if ( ipClient )
                {
                    ipaddr = SDL_SwapBE32( ipClient->host );
                    szBuffer = "Client ";
                    itoa( ipaddr, messageBuffer, 10 );
                    szBuffer += messageBuffer;
                    szBuffer += " joined on port";
                    text[line++] = TTF_RenderText_Solid( font, szBuffer.c_str(), colorConnection );
                    
                    szBuffer = "Welcome";
                    SDLNet_TCP_Send( sockClient, szBuffer.c_str(), szBuffer.size() );
                    cout<<"sending stuff: "<<szBuffer<<endl;   
                    connected = true;         
                }
            }
        }
   
        //Draw
        for ( int i=0; i<TXTLINES; i++ )
        {
            Draw( screen, text[i], 0, i*15 );
        }
    	
    	SDL_Flip( screen );
    }
	cout<<"Exit"<<endl;
	//Clean up
	for ( int i=0; i<TXTLINES; i++ )
	{
        SDL_FreeSurface( text[i] );
    }
	TTF_CloseFont( font );
	TTF_Quit();
	SDL_KillThread( thread );
	SDLNet_Quit();
	SDL_Quit();
	return 0;
}

SDL_Surface *LoadImage( string filename )
{
	SDL_Surface *loadedImage = NULL;
	SDL_Surface *optimizedImage = NULL;
	loadedImage = SDL_LoadBMP( filename.c_str() );
	if ( loadedImage != NULL )
	{
		optimizedImage = SDL_DisplayFormat( loadedImage );
		SDL_FreeSurface( loadedImage );
		if ( optimizedImage != NULL )
		{
			//make that pink color transparent
			Uint32 colorkey = SDL_MapRGB( optimizedImage->format, 0xFF, 0, 0xFF );
			SDL_SetColorKey( optimizedImage, SDL_SRCCOLORKEY, colorkey );
		}
	}
	else
	{
		cerr<<"Error loading image "<<filename<<endl;
	}
	return optimizedImage;
}

void Draw( SDL_Surface *destination, SDL_Surface *source, int x, int y )
{
	SDL_Rect offset;
	offset.x = x;		offset.y = y;
	SDL_BlitSurface( source, NULL, destination, &offset );
}

void Init()
{
    //Init
    SDL_Init( SDL_INIT_EVERYTHING );
    screen = SDL_SetVideoMode( SCRW, SCRH, SCRBPP, SDL_SWSURFACE );
    TTF_Init();
    SDL_WM_SetCaption( "Pickin' Online Sticks Server v0.0", NULL );
}
