#include "Inventory.h"
#include "LuaManager.h"
//
void InventoryClass::AddItem(string name,LuaManager *luaScript ,int num)
{
    //PRE: the name of the item, a luamanager pointer, the number of items to add
    //POST: the function will add the item to the inventory or update the quantity
    //      of the existing item in the inventory by the num specified
    int index;
    index = SearchInventory(name);
    if( index >= 0)
    {
        Inventory[index].quantity += num;
        return;
    }
    else
    {
        ItemCount ++;
        Inventory[ItemCount].name = name;
        Inventory[ItemCount].quantity = num;
        Inventory[ItemCount].type = luaScript->GetItemInfoStr(name.c_str(), "type");
    }
}

void InventoryClass::AddItem(string name,LuaManager *luaScript)
{
    //PRE: the name of the item, a luamanager pointer
    //POST: the function will add the item to the inventory or update the quantity
    //      of the existing item in the inventory by one
    int index;
    index = SearchInventory(name);
    if( index >= 0)
    {
        Inventory[index].quantity += 1;
        return;
    }
    else
    {
        ItemCount ++;
        Inventory[ItemCount].name = name;
        Inventory[ItemCount].quantity = 1;
        Inventory[ItemCount].type = luaScript->GetItemInfoStr(name.c_str(), "type");
    }
}


void InventoryClass::PrintInventory(ostream &out, bool printinvisible)
{
    //PRE: an inventory list with at least one item
    //POST: this outputs the names of each item in the inventory
    out << "Inventory List" << endl;
    for(int i = 1; i <= ItemCount; i++)
    {
        if(printinvisible == true)
            out << Inventory[i].name << endl;
        else
        {
            if(Inventory[i].type != "invisible")
                out << Inventory[i].name << endl;
        }
    }
}

void InventoryClass::PrintInventoryDetails(ostream &out, bool printinvisible)
{
    //PRE: an inventory list with at least one item
    //POST: this outputs the name, quantity, and type of each
    //      item in the inventory
    out << "Inventory Details" << endl;
    for(int i = 1; i <= ItemCount; i++)
    {
        if(printinvisible == true)
        {
            out << Inventory[i].name << endl;
            out << Inventory[i].quantity << endl;
            out << Inventory[i].type << endl;
            out << endl;
        }
        else
        {
            if(Inventory[i].type != "invisible")
            {
                out << Inventory[i].name << endl;
                out << Inventory[i].quantity << endl;
                out << Inventory[i].type << endl;
                out << endl;
            }
        }
    }
}

int InventoryClass::SearchInventory(string name)
{
    //PRE: the name of the item to search for.
    //POST: this will return the index of the item in the inventory array
    for(int i = 1; i <= ItemCount; i++)
    {
        if (Inventory[i].name == name)
            return i;
    }
    return -1;
}

void InventoryClass::RemoveItem(string name, int num)
{
    //PRE: the name of the item and the number to remove
    //POST: this removes up to the number of items specified, or the
    //      actual number of items in the inventory, whichever is lower.
    int index;
    index = SearchInventory(name);
    if(index >= 0)
    {
        if(Inventory[index].quantity < num)
            num = Inventory[index].quantity;
        Inventory[index].quantity -= num;
    }
}

bool InventoryClass::UseItemNonCombat(string name, string target, LuaManager *luaScript)
{
    //PRE: NON COMBAT!! The name of the item, the target to use it on, and the luamanager
    //POST: This will use the item if there is one in the inventory to use.  It returns
    //      true if it's used, false if it's not.
    int index;
    index = SearchInventory(name);
    if(index < 0)
        return false;
    else
    {
        string type;
        type = luaScript->GetItemInfoStr(name.c_str(), "type");
        if(type == "consumable")
        {
            if(Inventory[index].quantity <= 0)
            {
                return false;
            }
            else
            {
                RemoveItem(name,1);
                luaScript->UseItem(name, target);
            }
        return true;
        }
        else
            return false;
    }
}

