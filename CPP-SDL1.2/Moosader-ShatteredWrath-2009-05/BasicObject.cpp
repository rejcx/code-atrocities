#include "BasicObject.h"

const int DEFAULT_VAL = -1;

void BasicObject::Setup( int tx, int ty )
{
    Setup( tx, ty, DEFAULT_VAL, DEFAULT_VAL );
}

void BasicObject::Setup( int tx, int ty, int tw, int th )
{
    coord.x = tx;
    coord.y = ty;
    coord.w = tw;
    coord.h = th;
}

BasicObject::BasicObject()
{
    Setup( DEFAULT_VAL, DEFAULT_VAL, DEFAULT_VAL, DEFAULT_VAL );
}

BasicObject::BasicObject( int tx, int ty )
{
    Setup( tx, ty, DEFAULT_VAL, DEFAULT_VAL );
}

BasicObject::BasicObject( int tx, int ty, int tw, int th )
{
    Setup( tx, ty, tw, th );
}

int BasicObject::X()
{
    return coord.x;
}
void BasicObject::X( int val )
{
    coord.x = val;
}
int BasicObject::Y()
{
    return coord.y;
}
void BasicObject::Y( int val )
{
    coord.y = val;
}
int BasicObject::W()
{
    return coord.w;
}
void BasicObject::W( int val )
{
    coord.w = val;
}
int BasicObject::H()
{
    return coord.h;
}
void BasicObject::H( int val )
{
    coord.h = val;
}
int BasicObject::X1()
{
    return coord.x;
}
void BasicObject::X1( int val )
{
    coord.x = val;
}
int BasicObject::X2()
{
    return coord.x + coord.w;
}
void BasicObject::X2( int val )
{
    coord.w = val - coord.x;
}
int BasicObject::Y1()
{
    return coord.y;
}
void BasicObject::Y1( int val )
{
    coord.y = val;
}
int BasicObject::Y2()
{
    return coord.y + coord.h;
}
void BasicObject::Y2( int val )
{
    coord.h = val - coord.y;
}

bool IsCollision( BasicObject *objA , BasicObject *objB )
{
    /*
    There is a collision with an object if
    Object 1's left side is less than Object 2's right side,
    Object 1's right side is greater than Object 2's left side,
    Object 1's top side is less than Object 2's bottom side,
    Object 1's bottom side is greater than Object 2's top side.

    The LEFT side is just the x coordinate
    the RIGHT side is the x coordinate plus the width
    the TOP side is just the y coordinate
    the BOTTOM side is the y coordinate plus the height.
    */
    if (    ( objA->colRegion.x                 < objB->colRegion.x + objB->colRegion.w ) &&
            ( objA->colRegion.x + objA->colRegion.w > objB->colRegion.x ) &&
            ( objA->colRegion.y                 < objB->colRegion.y + objB->colRegion.h ) &&
            ( objA->colRegion.y + objA->colRegion.h > objB->colRegion.y ) )
    {
        return true;
    }
    return false;
}



