#pragma once
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "Level.h"
#include "NpcManager.h"
#include "ImageManager.h"
#include "Character.h"
#include "LuaManager.h"

/*
Stores all the levels for the games, makes it easier
to shift between them, rather than doing it in main.
*/

const int LEVELAMT = 11;

class LevelManager
{
    private:
        SDL_Rect warpCoord[5];
    public:
        Level level[LEVELAMT];
        int current;

        LevelManager();
        void LoadInLevels();
        void DrawLevel( SDL_Surface *buffer, ImageManager *img, int offsetX, int offsetY, bool bottom );
        void DrawLevelCollide( SDL_Surface *buffer, int offsetX, int offsetY, int x, int y );
        Level CurrentLevel() { return level[current]; }
        int CurrentLevelIndex() { return current; }
        void NextLevel();
        void PrevLevel();
        void Warp( LuaManager *luaScript, int warpToIndex );

        void CheckForWarpTile( LuaManager *luaScript, int px, int py );
        void UpdateWarpCoords();
};
