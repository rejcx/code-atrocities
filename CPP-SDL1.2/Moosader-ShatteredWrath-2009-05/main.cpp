#include "SDL/SDL.h"
#include "Character.h"
#include "Game.h"
#include "Input.h"
#include "Level.h"
#include "Timer.h"
#include "BasicObject.h"
#include "ImageManager.h"
#include "LevelManager.h"
#include "LuaManager.h"
#include "NpcManager.h"
#include "Dialog.h"
#include "MenuManager.h"
#include "SoundManager.h"
#include "BattleSystem.h"
#include "Inventory.h"
#include "Cutscene.h"
#include "Text.h"

/*
CS451 SOFTWARE ENGINEERING
Project
Jan 15th to ...
*/

void Draw( SDL_Surface *destination, SDL_Surface *source, int x, int y );
void UpdateOffset( int *offsetX, int *offsetY, Character *player, Game *game );

LuaManager luaScript;

int main( int argc, char *args[] )
{
    DebugOutput( "> Game" );
    Game game;
    DebugOutput("> Timer" );
    Timer fps;
    DebugOutput("> Input" );
    Input input;
    DebugOutput("> ImageManager" );
    ImageManager img;
    DebugOutput("> LevelManager" );
    LevelManager lev;
    DebugOutput("> NpcManager" );
    NpcManager npc( &luaScript );
    DebugOutput("> SDL_Event" );
    SDL_Event event;
    DebugOutput("> Character" );
    Character player;
    DebugOutput("> Dialog" );
    Dialog dialogBox;
    DebugOutput("> MenuManager" );
    MenuManager menu;
    DebugOutput("> SoundManager" );
    SoundManager sound;
    DebugOutput("> BattleSystem" );
    BattleSystem battleSys;

    int batY = 0;
    string batOutcome;

    int offsetX = 0, offsetY = 0, frame = 0;    //Offsets are for screen scrolling.


    sound.StartSong( lev.CurrentLevel().songID );
    while ( game.Done() == false )
    {
        fps.Start();
        /* USER INPUT */
        while ( SDL_PollEvent( &event ) )
        {
            if ( event.type == SDL_QUIT )
                game.Done( true );
        }

        //Take care of keyboard input
        input.HandleEventPlz( &game, &player, &lev, &npc, &dialogBox, &luaScript, &sound, &menu, &battleSys );

        //Update battle
        if ( game.State() == BATTLE )
        {
            battleSys.Battle( &luaScript );
        }

        //Updates screen scrolling
        UpdateOffset( &offsetX, &offsetY, &player, &game );

        //Update npcs
        if ( dialogBox.DialogShown() == false )
            npc.Update( &lev.CurrentLevel(), &luaScript );

        /* DRAW */
        if ( game.State() == TITLE ) //Title
		{
		    Draw( game.buffer, img.title, 0, 0 );
		    game.Update();      // extra?
		} // End Title
		else if ( game.State() == MENU )
		{
		    menu.Draw( game.buffer, &img );
		} // End menu
		else if ( game.State() == INGAME )
		{
            //Draw to the screen
            //Draw bg
            SDL_Rect coord;
		    coord.x = 0;
		    coord.y = 0;
		    coord.w = 640;
		    coord.h = 480;
		    SDL_FillRect( game.buffer, &coord, SDL_MapRGB( game.buffer->format, 0, 0, 0 ) );

            lev.DrawLevel( game.buffer, &img, offsetX, offsetY, true );
            if ( player.Debug() )
            {
                //void DrawLevelCollide( SDL_Surface *buffer, int offsetX, int offsetY, int x, int y );
                lev.DrawLevelCollide( game.buffer, offsetX, offsetY, player.RegX(), player.RegY() );
            }
            player.Draw( game.buffer, img.player, offsetX, offsetY );
            npc.DrawNpcs( game.buffer, &img, offsetX, offsetY, lev.CurrentLevelIndex() );
            lev.DrawLevel( game.buffer, &img, offsetX, offsetY, false );
            dialogBox.Draw( game.buffer, img.menugfx );

            //Draw HUD
            img.DrawHud( game.buffer );

            frame++;
		}
		else if ( game.State() == BATTLE )
		{
		    SDL_Rect coord;
		    coord.x = 0;
		    coord.y = 0;
		    coord.w = 640;
		    coord.h = 480;
		    SDL_FillRect( game.buffer, &coord, SDL_MapRGB( game.buffer->format, 0, 0, 0 ) );
		    battleSys.Draw( game.buffer, &img );
		}
		//Draws the buffer to the screen, and any necessary game updates
		game.Update();

        //Keeps the frame rate constant
        fps.CheckRate( game.FPS() );
    }

    SDL_Quit();
    return 0;
}

void Draw( SDL_Surface *destination, SDL_Surface *source, int x, int y )
{
    //Back up draw function, in case we have an image to output that
    //doesn't belong to a class that has it's own draw function.
	SDL_Rect offset;
	offset.x = x;
    offset.y = y;
	SDL_BlitSurface( source, NULL, destination, &offset );
}

void UpdateOffset( int *offsetX, int *offsetY, Character *player, Game *game )
{
    // Player stays in center of the screen, the screen scrolls with the movement.
    if ( player->X() - (int)( 0.5 * player->W() ) - (int)( 0.5 * game->ScreenWidth() ) >= 0 )
    {
        *offsetX = player->X() - (int)( 0.5 * player->W() ) - (int)( 0.5 * game->ScreenWidth() );
    }
    if ( player->Y() - (int)( 0.5 * player->H() ) - (int)( 0.5 * game->ScreenHeight() ) >= 0 )
    {
        *offsetY = player->Y() - (int)( 0.5 * player->H() ) - (int)( 0.5 * game->ScreenHeight() );
    }
}




