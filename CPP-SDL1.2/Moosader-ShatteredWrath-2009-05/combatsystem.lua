--===============================================================================================================================
--===============================================================================================================================
--==========================================GLOBAL VARIABLES=====================================================================
--===============================================================================================================================
--===============================================================================================================================

	--a global counter variable, so as not to reset it to 0.
	--having coutner=counter+1 did not work, as apparently you
	--can't add 1 to nil and get a value
	count = 0
	npccount = 0
	--the seed value for the random number generator
	math.randomseed( os.time() )
	--a list of all the npc's in the entity list
	npclist = {}
	--a list for each result of combat to send to c++
	combatresultslist = {}
	--a counter for the results to be sent
	combatresultscount = 0
	--a counter for what round of combat it is
	combatround = 0
	--a list for possible use later - for multi round affects
	combatroundlist = {}
	--the list of actors in initiative order
	combatinitlist = {}
	--which index in the combatinitlist should be called next
	combatinitturn = 1
	--an iterator to move through the combat results when being called from c++
	combatresultsiterator = 0
	--iterator for initial items
	getinitialitemscounter = 0
	--sammich counters
	flipper = 0
	poesammichtype = "sammich"
	sammichcount = 0
	flsammichcount = 0
	gbsammichcount = 0


--===============================================================================================================================
--===============================================================================================================================
--===========================================INFO FUNCTIONS======================================================================
--===============================================================================================================================
--===============================================================================================================================

function DeepCopy(Src, Seen)
--this function was copied directly from Beginning Lua Programming by Kurt Jung and Aaron Brown
	local Dest
	if Seen then
		--this will only set Dest if Src has been seen before:
		Dest = Seen[Src]
	else
		--Top-level call; create the Seen table:
		Seen = {}
	end
	--If Src is new, copy it into Dest:
	if not Dest then
		--Make a fresh table and record it as seen:
		Dest = {}
		Seen[Src] = Dest
		for Key, Val in pairs(Src) do
			Key = type(Key) == "table" and DeepCopy(Key, Seen) or Key
			Val = type(Val) == "table" and DeepCopy(Val, Seen) or Val
			Dest[Key] = Val
		end
	end
	return Dest
end

function adduniquenpc(uniqueid, model)
	--table.insert(entity, "uniqueid")
	entity[uniqueid] = DeepCopy(entity[model])
	--entity[uniqueid] = entity[model]
end


function generictext(npc)
	--this function returns a random generic text
	answer = math.random(1,#generictexttbl)
	return(generictexttbl[answer])

	--return ( entity[npc]["region"])
end

function poetext()
	if flipper == 0 then
		flipper = 1
		return "Anything I can do to help"
	elseif flipper == 1 then
		flipper = 0
		if poesammichtype == "sammich" then
			if sammichcount <= 10 then
				sammichcount = sammichcount + 1
				return "ITEM::sammich"
			else
				return "Sorry, I'm out of sammiches"
			end
		elseif poesammichtype == "flsammich" then
			if flsammichcount <= 10 then
				flsammichcount = flsammichcount +1
				return "ITEM::footlongsammich"
			else
				return "Sorry, I'm out of foot long sammiches"
			end
		elseif poesammichtype == "gbsammich" then
			if gbsammichcount <= 10 then
				gbsammichcount = gbsammichcount + 1
				return "ITEM::gutbustersammich"
			else
				return "Sorry, I'm out of gut buster sammiches"
			end
		end
	end
end

function getinfo(npc, info)
	--this function is a generic function to get most info
	-- from the entity list

	--if the info requested is the NPC list, then generate the npc lists
	if info == "npclist" then
		-- increment the count and return the next npc in the list or null if it's at the end
		npccount = npccount + 1
		returnvalnilcheck = npclist[npccount]
		if returnvalnilcheck == nil then
			returnvalnilcheck = "NOVALUE"
		end
		return (returnvalnilcheck)
	--if info is npc count then generate the npc list and return the count.
	elseif info == "npccount" then
		npclist = {}
		funcnpclist()
		npccount = 0
		return (#npclist)
	--if the value we are looking for is a funciton
	elseif type(entity[npc][info]) == "function" then
	--then call the funcion and return it's value
		returnvalnilcheck = entity[npc][info](npc)
		if returnvalnilcheck == nil then
			returnvalnilcheck = "NOVALUE"
		end
		return (returnvalnilcheck)
	else
		--otherwise just return the value.
		returnvalnilcheck = entity[npc][info]
		if returnvalnilcheck == nil then
			returnvalnilcheck = "NOVALUE"
		end
		return returnvalnilcheck
	end
end

function getplayerinfo(info)
	returnvalnilcheck = player[info]
	if returnvalnilcheck == nil then
			returnvalnilcheck = "NOVALUE"
		end
	return returnvalnilcheck
end

function getplayerequippedinfo(info)
	figureallstats()
	returnvalnilcheck = playerequippedvals[info]
	if returnvalnilcheck == nil then
			returnvalnilcheck = "NOVALUE"
		end
	return returnvalnilcheck
end

function getiteminfo(item, info)
	if info == "attack" or info == "defense" or
	info == "armor" or info == "damage" or info == "health" then
		returnvalnilcheck = items[item]["effect"][info]
		if returnvalnilcheck == nil then
			returnvalnilcheck = "NOVALUE"
		end
		return returnvalnilcheck
	else
		returnvalnilcheck = items[item][info]
		if returnvalnilcheck == nil then
			returnvalnilcheck = "NOVALUE"
		end
		return returnvalnilcheck
	end
end

function getequippedinfo(location, info)
	returnvalnilcheck = equipped[location][info]
		if returnvalnilcheck == nil then
			returnvalnilcheck = "NOVALUE"
		end
	return returnvalnilcheck
end


function patroliterate(npc)
	--this function will be used to itterate through the movement of
	--the npc's on patrol.
	entity[npc]["patrolcount"] = entity[npc]["patrolcount"] + 1
	if entity[npc]["patrolcount"] > (#entity[npc]["patrollist"]) then
		entity[npc]["patrolcount"] = 1
	end
	returnvalnilcheck = (entity[npc]["patrollist"][(entity[npc]["patrolcount"])])
		if returnvalnilcheck == nil then
			returnvalnilcheck = "NOVALUE"
		end
	return returnvalnilcheck
end

function ranmove(npc)
	--this function loads the list of probabilities of movement and randomly
	-- chooses and returns one
	rannum = math.random(1,#entity[npc]["randommovelist"])
	return (entity[npc]["randommovelist"][rannum])
end

function funcnpclist()
	tempcounter = 0
	for i, ent in pairs(entity) do
		tempcounter = tempcounter + 1
		npclist[tempcounter] = i
	end
end

function getinitialitems()
	getinitialitemscounter = getinitialitemscounter + 1
	if getinitialitemscounter <= #initialitems then
		return initialitems[getinitialitemscounter]
	else
		return "END::INITITEMLIST"
	end
end

--===============================================================================================================================
--===============================================================================================================================
--=============================================COMBAT SYSTEM=====================================================================
--===============================================================================================================================
--===============================================================================================================================
debugcombat = true

function combatsystem(enemy)
	--figureallstats()
	--make sure to not go past out of bounds on the initiative list
--	checkhealth(enemy)
	if #combatinitlist > 0 and combatinitturn > #combatinitlist then
		--==========debug===============
		if debugcombat then
			addresult(("=====#combatinitlist===::" .. #combatinitlist))
			addresult(("=====combatinitturn====::" .. combatinitturn))
			addresult(("=====combatround=======::" .. combatround))
		end
		--==========end debug===========
		combatround = combatround + 1
		combatinitturn = 1
		--==========debug===============
		if debugcombat then
			addresult(("=====after update======::"))
			addresult(("=====#combatinitlist===::" .. #combatinitlist))
			addresult(("COMBATINITTURN::" .. combatinitturn))
		end
		--==========end debug===========
		addresult(("COMBATROUND::" .. combatround))

	end
	--if new call determine initiative and make initiative list
	if combatround == 0 then
		figureallstats()
		playerinit = dieroller(10)
		monsterinit = dieroller(10)
		if playerinit >= monsterinit then
			combatinitlist[1] = "player"
			combatinitlist[2] = enemy
			addresult(("INITIATIVEWINNER::player"))
		else
			combatinitlist[1] = enemy
			combatinitlist[2] = "player"
			addresult(("INITIATIVEWINNER::" .. enemy))
		end
		addresult(("NEW_COMBAT::"))
		addresult(("PLAYERINIT::" .. playerinit))
		addresult(("MONSTERINIT::" .. monsterinit))
		addresult(("INIT_LIST::" .. combatinitlist[1] .. ", " .. combatinitlist[2]))
		combatround = 1
	end
	checkhealth(enemy)
	--for the next entity in the list
	if combatinitlist[combatinitturn] == "player" then
		--increment the combat initiative counter
		combatinitturn = combatinitturn + 1
		--add input request to the results list
		addresult("TURN::Player")
		addresult("COMBAT::INPUT")
		--combatresultscount = combatresultscount + 1
		--combatresultslist[combatresultscount] = "COMBAT::INPUT"
		return
	else
		addresult(("TURN::" .. enemy))
		--Increment the initiative counter
		combatinitturn = combatinitturn + 1
		--call enemy ai function
		combatsystemenemyai(enemy)
	end
end

function combatsystemplayerinput(input, target)
	--figureallstats()
	--combatinitturn = combatinitturn + 1
	if input == "ATTACK" then
		addresult(("ACTION::ATTACK"))
		--roll a d10 and add player attack rating
		attackroll = dieroller(10)
		addresult(("DIEROLL::" .. attackroll))
		addresult(("PLAYER_ATTACK::" .. playerequippedvals["attack"]))
		attackroll = attackroll + playerequippedvals["attack"]
		addresult(("TOTAL_ATTACK::" .. attackroll))
		addresult(("TARGET_DEFENSE::" .. entity[target]["defense"]))
		--if attack roll is greater than the targets defense, it hit
		if attackroll > entity[target]["defense"] then
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = "ANIMATION::HIT"
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = ("TARGET::" .. target)
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = "BY::PLAYER"
			--roll d10 and add player damage then subtract enemy armor rating
			damageroll = dieroller(10)
			addresult(("DIE_ROLL::" .. damageroll))
			addresult(("PLAYER_DAMAGE::" .. playerequippedvals["damage"]))
			addresult(("TARGET_ARMOR::" .. entity[target]["armor"]))
			damageroll = damageroll + playerequippedvals["damage"] - entity[target]["armor"]
			addresult(("TOTAL_DAMAGE::" .. damageroll))
			--if damage is 0 or less, make it 1
			if damageroll <= 0 then
				damageroll = 1
			end
			addresult(("TARGET_HEALTH::" .. entity[target]["health"]))
			addresult(("DAMAGE_ROLL::" .. damageroll))
			entity[target]["health"] = entity[target]["health"] - damageroll
			addresult(("TARGET_HEALTH::" .. entity[target]["health"]))
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = "ANIMATION::DAMAGE"
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = ("TARGET::" .. target)
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = ("AMOUNT::" .. damageroll)
			if entity[target]["health"] <= 0 then
				combatresultscount = combatresultscount + 1
				combatresultslist[combatresultscount] = "ANIMATION::DIE"
				combatresultscount = combatresultscount + 1
				combatresultslist[combatresultscount] = ("TARGET::" .. target)
				combatresultscount = combatresultscount + 1
				combatresultslist[combatresultscount] = "COMBAT::END"
				return
			end

		else
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = "ANIMATION::MISS"
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = ("TARGET::" .. target)
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = "BY::PLAYER"
		end
	elseif input == "ITEM" then
		--input is the item name, target is either "player" for the player or the specific enemy name for the enemy.
		useitem(input, target)
		addresult("ACTION::ITEM")
	elseif input == "WAIT" then
		addresult("ACTION::WAIT")
		--==========debug===============
		if debugcombat then
			addresult(("=====waiting=========="))
		end
		--==========end debug===========

	elseif input == "RUN" then
		--if the player chose to try to run
		--roll a die and see if he can escape
		addresult("ACTION::RUN")
		runroll = dieroller(10)
		addresult(("DIEROLL" .. runroll))
		if runroll >= 3 then
			--==========debug===============
			if debugcombat then
				addresult(("=====" .. dieroll .. " >= 3======"))
			end
			--==========end debug===========
			addresult("RUN::SUCCESS")
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = "ANIMATION::RUN"
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = "BY::PLAYER"
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = "COMBAT::END"
			return
		else
			--==========debug===============
			if debugcombat then
				addresult(("=====" .. runroll .. " < 3======="))
			end
			--==========end debug===========
			addresult("RUN::FAIL")
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = "ANIMATION::RUNFAIL"
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = "BY::PLAYER"
		end
	end
end

function combatsystemenemyai(enemy)
	--figureallstats()
	checkhealth(enemy)
	--set choice to default choice of attack
	choice = "ATTACK"
	choicefrom = ""
	--if the enemy has a player health trigger, set the choice to that
	if entity[enemy]["combatplayerhealthtrigger"]  then
		lowrange = entity[enemy]["combatplayerhealthtrigger"] - entity[enemy]["combatplayerhealthtriggerrange"]
		highrange = entity[enemy]["combatplayerhealthtrigger"] + entity[enemy]["combatplayerhealthtriggerrange"]
		health = playerequippedvals["health"]
		--==========debug===============
		if debugcombat then
			addresult(("=====playerhealth trig::" .. entity[enemy]["combatplayerhealthtrigger"] .. " +/- " .. entity[enemy]["combatplayerhealthtriggerrange"]))
			addresult(("=====low range=======::" .. lowrange))
			addresult(("=====high range======::" .. highrange))
			addresult(("=====player health===::" .. playerequippedvals["health"]))
		end
		--==========end debug===========
		if lowrange <= health and health <= highrange then
			choice = entity[enemy]["combatplayerhealtheffect"]
			choicefrom = "combatplayerhealth"
			--==========debug===============
			if debugcombat then
				addresult(("=====choice======::" .. choice))
			end
			--==========end debug===========
		end
	end
	--if the enemy has a self health trigger, set the choice to that
	if entity[enemy]["combatselfhealthtrigger"] then
		lowrange = entity[enemy]["combatselfhealthtrigger"] - entity[enemy]["combatselfhealthtriggerrange"]
		highrange = entity[enemy]["combatselfhealthtrigger"] + entity[enemy]["combatselfhealthtriggerrange"]
		health = entity[enemy]["health"]
		--==========debug===============
		if debugcombat then
			addresult(("=====self health trig::" .. entity[enemy]["combatselfhealthtrigger"] .. " +/- " .. entity[enemy]["combatselfhealthtriggerrange"]))
			addresult(("=====low range=======::" .. lowrange))
			addresult(("=====high range======::" .. highrange))
			addresult(("=====self health=====::" .. entity[enemy]["health"]))
		end
		--==========end debug===========
		if lowrange <= health and health <= highrange then
			choice = entity[enemy]["combatselfhealtheffect"]
			choicefrom = "combatselfhealth"
			--==========debug===============
			if debugcombat then
				addresult(("=====choice======::" .. choice))
			end
			--==========end debug===========
		end
	end
	-- if the enemy has a round trigger, set the choice to that
	if entity[enemy]["combatroundtrigger"] then
		if combatround == entity[enemy]["combatroundtrigger"] then
			choice = entity[enemy]["combatroundeffect"]
			choicefrom = "combatround"
			--==========debug===============
			if debugcombat then
				addresult(("=====round trigger===::" .. entity[enemy]["combatroundtrigger"]))
				addresult(("=====combatroundeffect==::" .. entity[enemy]["combatroundeffect"]))
				addresult(("=====choice==========::" .. choice))
			end
			--==========end debug===========
		end
	end
	if choice == "ATTACK" then
		addresult("ACTION::ATTACK")
		--roll a d10 and add player attack rating
		attackroll = dieroller(10)
		addresult(("DIE_ROLL::" .. attackroll))
		addresult(("ENEMY_ATTACK::" .. entity[enemy]["attack"]))
		attackroll = attackroll + entity[enemy]["attack"]
		addresult(("TOTAL_ATTACK::" .. attackroll))
		addresult(("PLAYER_DEFENSE::" .. playerequippedvals["defense"]))
		--if attack roll is greater than the targets defense, it hit
		if attackroll > playerequippedvals["defense"] then
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = "ANIMATION::HIT"
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = "TARGET::PLAYER"
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = ("BY::" .. enemy)
			--roll d10 and add player damage then subtract enemy armor rating
			damageroll = dieroller(10)
			addresult(("DIEROLL::" .. damageroll))
			addresult(("ENEMY_DAMAGE::" .. entity[enemy]["damage"]))
			addresult(("PLAYER_ARMOR::" .. playerequippedvals["armor"]))
			damageroll = damageroll + entity[enemy]["damage"] - playerequippedvals["armor"]
			--if damage is 0 or less, make it 1
			if damageroll <= 0 then
				damageroll = 1
			end
			addresult(("PLAYER_HEALTH::" .. playerequippedvals["health"]))
			addresult(("TOTAL_DAMAGE::" .. damageroll))
			playerequippedvals["health"] = playerequippedvals["health"] - damageroll
			addresult(("PLAYER_HEALTH::" .. playerequippedvals["health"]))
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = "ANIMATION::DAMAGE"
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = ("TARGET::PLAYER")
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = ("AMOUNT::" .. damageroll)
			if playerequippedvals["health"] <= 0 then
				combatresultscount = combatresultscount + 1
				combatresultslist[combatresultscount] = "ANIMATION::DIE"
				combatresultscount = combatresultscount + 1
				combatresultslist[combatresultscount] = ("TARGET::PLAYER")
				combatresultscount = combatresultscount + 1
				combatresultslist[combatresultscount] = "COMBAT::END"
				return
			end

		else
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = "ANIMATION::MISS"
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = ("TARGET::PLAYER")
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = ("BY::" .. enemy)
		end
	elseif choice == "ITEM" then
		addresult("ACTION::ITEM")
		if (entity[enemy][(choicefrom .. "target")]) == "self" then
			itemtarget = enemy
		end
		if entity[enemy][(choicefrom .. "target")] == "player" then
			itemtarget = "player"
		end
		itemused = entity[enemy][(choicefrom .. "item")]
		combatuseitem((entity[enemy][(choicefrom .. "item")]) , itemtarget)


	elseif choice == "RUN" then
		addresult("ACTION::RUN")
		--roll a die and see if enemy can escape
		runroll = dieroller(10)
		addresult(("DIEROLL::" .. runroll))
		if runroll <= entity[enemy]["runchance"] then
			--==========debug===============
			if debugcombat then
				addresult(("=====" .. runroll .. " <= " .. entity[enemy]["runchance"] .. "======="))
			end
			--==========end debug===========
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = "ANIMATION::RUN"
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = ("BY::" .. enemy)
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = "COMBAT::END"
			return
		else
			--==========debug===============
			if debugcombat then
				addresult(("=====" .. runroll .. " > " .. entity[enemy]["runchance"] .."========"))
			end
			--==========end debug===========
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = "ANIMATION::RUNFAIL"
			combatresultscount = combatresultscount + 1
			combatresultslist[combatresultscount] = ("BY::" .. enemy)
		end
	end
end

function addresult(text)
	combatresultscount = combatresultscount + 1
	combatresultslist[combatresultscount] = text
end

function getcombatresults()
	combatresultsiterator = combatresultsiterator + 1
	if combatresultsiterator > #combatresultslist then
		results = "COMBAT::CONTINUE"
		combatresultsiterator = combatresultsiterator - 1
	else
		results =combatresultslist[combatresultsiterator]
	end
	return (results)
end

function dieroller(die)
	dieroll = math.random(1, die)
	return dieroll
end

function combatcleanup()
	combatresultslist = {}
	combatresultscount = 0
	combatround = 0
	combatroundlist = {}
	combatinitlist = {}
	combatinitturn = 1
	combatresultsiterator = 0
	--==========debug===============
	if debugcombat then
		addresult(("=====Debug so player returned to full health========"))
		playerequippedvals["health"] = playerequippedvals["maxhealth"]
	end
	--==========end debug===========
end


--===============================================================================================================================
--===============================================================================================================================
--========================================ITEM AND EQUIPMENT FUNCTIONS===========================================================
--===============================================================================================================================
--===============================================================================================================================

function figurestat(stat)
	if stat == "maxhealth" then
		total = (player[stat]
				 + equipped["head"]["health"]
				 + equipped["chest"]["health"]
				 + equipped["arms"]["health"]
				 + equipped["legs"]["health"]
				 + equipped["feet"]["health"]
				 + equipped["hands"]["health"])
	elseif stat == "health" then
		if player[stat] < player["maxhealth"] then
			healtdiff = player["maxhealth"] - player[stat]
		else
			healthdiff = 0
		end
		total = (player[stat]
				+ equipped["head"][stat]
				+ equipped["chest"][stat]
				+ equipped["arms"][stat]
				+ equipped["legs"][stat]
				+ equipped["feet"][stat]
				+ equipped["hands"][stat]
				- healthdiff)
	else
		total = (player[stat]
				+ equipped["head"][stat]
				+ equipped["chest"][stat]
				+ equipped["arms"][stat]
				+ equipped["legs"][stat]
				+ equipped["feet"][stat]
				+ equipped["hands"][stat])
	end
	playerequippedvals[(stat)] = total
	return total
end

function figureallstats()
	figurestat("health")
	figurestat("maxhealth")
	figurestat("attack")
	figurestat("defense")
	figurestat("damage")
	figurestat("armor")
end

function equip(item)
	if items[item]["type"] == "equipment" then
		location = items[item]["location"]
		equipped[location]["name"] = items[item]["name"]
		equipped[location]["attack"] = items[item]["effect"]["attack"]
		equipped[location]["defense"] = items[item]["effect"]["defense"]
		equipped[location]["damage"] = items[item]["effect"]["damage"]
		equipped[location]["armor"] = items[item]["effect"]["armor"]
		equipped[location]["health"] = items[item]["effect"]["health"]
		figureallstats()
	end
end

function useitem(item, target)
	if target == "player" then
		holder = player
	else
		holder = entity[target]
	end
	for curstat, curamount in pairs(items[item]["effect"]) do
		holder[curstat] = holder[curstat] + curamount
		if curstat == "health" then
			if holder[curstat] > holder["max" .. curstat] then
				holder[curstat] = holder["max" .. curstat]
			end
		end
	end
end

function combatuseitem(item, target)
	if target == "player" then
		holder = playerequippedvals
	else
		holder = entity[target]
	end
	addresult("ACTION::ITEM")
	addresult("ITEMUSED::" .. item)
	addresult("TARGET::" .. target)
	for curstat, curamount in pairs(items[item]["effect"]) do
		addresult("STATEFFECTED::" .. curstat)
		addresult("AMOUNTEFFECTED::" .. curamount)
		addresult("ORIGINALAMOUNT::" .. holder[curstat])
		holder[curstat] = holder[curstat] + curamount
		if curstat == "health" then
			print(holder)
			print("curstat " .. curstat)
			print ("holder[curstat] " .. holder[curstat])
			print("holder [max .. curstat] " .. holder["max" .. curstat])
			if holder[curstat] > holder["max" .. curstat] then
				holder[curstat] = holder["max" .. curstat]
			end
		end
		addresult("CHANGEDAMOUNT::" .. holder[curstat])
	end
end

function checkhealth(enemy)
	if playerequippedvals["health"] <= 0 then
		addresult("ANIMATION::DIE")
		addresult("TARGET::PLAYER")
		addresult("COMBAT::END")
	end
	if entity[enemy]["health"] <= 0 then
		addresult("ANIMATION::DIE")
		addresult("TARGET::" .. enemy)
		addresult("COMBAT::END")
	end
end

--===============================================================================================================================
--===============================================================================================================================
--===============================================CUT SCENES======================================================================
--===============================================================================================================================
--===============================================================================================================================

function checkforcutscene(thingtype, thing)
	if thingtype == "NPC" then
		thingholder = entity[thing]
	elseif thingtype == "MAP" then
		thingholder = map[thing]
	end
	if thingholder["cutsceneitems"] == nil then
		return "NOCUTSCENE"
	else
		local i = 1
		local returnvalue = "CUTSCENEITEMS"
		while thingholder["cutsceneitems"][i] do
			returnvalue = (returnvalue .. "::" .. thingholder["cutsceneitems"][i])
			i = i + 1
		end
		return returnvalue
	end

end

function getcutscene(scenename)
	if scenename == "quest1complete" then
		poesammichtype = "flsammich"
	elseif scenename == "quest2complete" then
		poesammichtype = "gbsammich"
	end
	csholder = cutscenelist[scenename]["scene"]
	if cutscenelist[scenename]["iter"] > #csholder then
		return "CUTSCENE::END"
	else
		rholder = csholder[cutscenelist[scenename]["iter"]]
		cutscenelist[scenename]["iter"] = cutscenelist[scenename]["iter"] + 1
		return rholder
	end
end

cutscenelist = {["intro"] = {["scene"] = {"SPAWN::player",
										  "INITIALLOCATIONX::11",
										  "INITIALLOCATIONY::8",
										  "INITIALFACING::U",
										  "SPAWN::fredrick",
										  "INITIALLOCATIONX::12",
										  "INITIALLOCATIONY::22",
										  "INITIALFACING::U",
										  "TALK::Fredrick",
										  "TEXT::My, you�re a hard man to track down.",
										  "CHANGEFACING::player",
										  "NEWFACING::D",
										  "TALK::player",
										  "TEXT::What do you want and how much are you offering?",
										  "TALK::Fredrick",
										  "TEXT::Straight to the point, I like that in a business relationship.",
										  "TALK::Fredrick",
										  "TEXT::I won�t bore you with trivial details but two of my former partners decided to nudge me out.",
										  "TALK::Fredrick",
										  "TEXT::I�d like your help with a �hostile take-over� of their assets.",
										  "TALK::player",
										  "TEXT::<rather annoyed> You still haven�t made an offer.",
										  "TALK::Fredrick",
										  "TEXT::5,000 gold pieces for each one, payable on return of the pedant each of them wear.",
										  "TALK::player",
										  "TEXT::Who are the marks?",
										  "TALK::Fredrick",
										  "TEXT::We�ll discuss that at the Pub across from the church to the west if you accept.",
										  "MOVE::fredrick",
										  "LIST::D",
										  "DESPAWN::fredrick",
										  "END::CUTSCENE"},
									   ["iter"] = 1
									  },
				["slumintro"] = {["scene"] = {"SPAWN::player",
											  "INITIALLOCATIONY:26",
											  "INITIALLOCATIONX::12",
											  "INITIALFACING::U",
											  "SPAWN::thug",
											  "INITIALLOCATIONY::19",
											  "INITIALLOCATIONX::13",
											  "INITIALFACING::D",
											  "TALK::Thug",
											  "TEXT::Move and I�ll cut you. Give me you pouch.",
											  "TALK::player",
											  "TEXT::Sigh...",
											  "COMBAT::START",
											  "ENEMY::thug" ,
											  "DESPAWN::thug",
											  "SPAWN::poe",
											  "INITIALLOCATIONY::11",
											  "INITIALLOCATIONX::13",
											  "INITIALFACING::D",
											  "TALK::Poe",
											  "TEXT::Are you all right boy?",
											  "TALK::player",
											  "TEXT::I think you should be asking him that.",
											  "TALK::Poe",
											  "TEXT::Perhaps.  But he has chosen his path where yours is still undecided�",
											  "TALK::player",
											  "TEXT::What�s that Sup�",
											  "TALK::Poe",
											  "TEXT::Take this and remember the church will always help those in need.",
											  "ITEM::sammich",
											  "MOVE::poe",
											  "LIST::U",
											  "DESPAWN::poe",
											  "END::CUTSCENE"},
									   ["iter"] = 1
									  },
				["rigleyspubintro"] = {["scene"] = {"SPAWN::player",
													"INITIALLOCATIONY::0", "INITIALLOCATIONX::0",
													"INITIALFACING::U",
													"SPAWN::rigley",
													"INITIALLOCATIONY::23",
													"INITIALLOCATIONX::11",
													"INITIALFACING::D",
													"MOVE::rigley",
													"LIST::DDDDD",
													"TALK::Rigley",
													"TEXT::Hey Marcus! Some guy came in here, bought a room an said he was gonna wait for ya.",
													"TALK::player",
													"TEXT::Real rich looking guy in a top hat?",
													"TALK::Rigley",
													"TEXT::Yep, that�s him.  Second door on the right.",
													"TALK::player",
													"TEXT::Thanks",
													"END::CUTSCENE"},
									   ["iter"] = 1
									  },
				["fredrickpubfirsttalk"] = {["scene"] = {"TALK::Fredrick",
														 "TEXT::So, you accept then?",
														 "TALK::player",
														 "TEXT::I�ll listen a little more. Now who are the marks?",
														 "TALK::Fredrick",
														 "TEXT::Fine, the marks are Nehir Tigmet and Kenner Baben.",
														 "TALK::player",
														 "TEXT::�Hostile takeover� huh? Then I want five percent of everything you take.",
														 "TALK::Fredrick",
														 "TEXT::Sigh� Deal. But then I�ll need you to bring me something personally from both.",
														 "TALK::Fredrick",
														 "TEXT::A small pendant that each of them wear�s, bring me that after you�ve �bought them out� and we will be in business.",
														 "TALK::player",
														 "TEXT::Where�s the best place to find them.",
														 "TALK::Fredrick",
														 "TEXT::Nehir Tigmet is likely to be in his factory just west of here.",
														 "TALK::Fredrick",
														 "TEXT::I�d suggest you use the basement to get in then work your way up.",
														 "TALK::player",
														 "TEXT::What about the Kenner?",
														 "TALK::Fredrick",
														 "TEXT::He�s working at his bank just to the north in the financial district.",
														 "TALK::player",
														 "TEXT::I�ll hit him first, Breaking in to financial district with everyone looking for me wouldn�t be easy.",
														 "TALK::Fredrick",
														 "TEXT::Well then, there not secret way in.",
														 "TALK::player",
														 "TEXT::Guess I�m using the front door then.",
														 "END::CUTSCENE"},
									   ["iter"] = 1
									  },
				["bankintroday"] = {["scene"] = {"TALK::General Manager",
												 "TEXT::Sir, Sir.  I�m afraid I�ll have to ask you to leave.",
												 "TALK::player",
												 "TEXT::What? I just came to make a withdrawal",
												 "TALK::General Manager",
												 "TEXT::I doubt that.  Your kind isn�t served here.  Now, either leave or I�ll have you thrown out.",
												 "TALK::player",
												 "TEXT::Try it, I dare you.",
												 "TALK::General Manager",
												 "TEXT::Very well� GUARDS!",
												 "COMBAT::START",
												 "ENEMY::securityguard",
												 "DESPAWN::general manager",
												 "END::CUTSCENE"},
									   ["iter"] = 1
								   },
				["bankgmencounter"] = {["scene"] = {"TALK::General Manager",
													"TEXT::DON�T COME ANY CLOSER!, he says, brandishing an office chair.",
													"TALK::player", "TEXT::What are you doing?",
													"TALK::General Manager",
													"TEXT::I won�t let you go any further.  I�ll stop you my self.  Rarrr�",
													"TALK::player",
													"TEXT::�",
													"COMBAT::START",
													"ENEMY::generalmanager",
													"TALK::player",
													"TEXT::Do you yield?",
													"TALK::General Manager",
													"TEXT::PLEASE, don�t kill me�",
													"TALK::player",
													"TEXT::Get out of my face.",
													"MOVE::general manager",
													"LIST::UUUUUU",
													"END::CUTSCENE"},
									   ["iter"] = 1
									  },
				["bankroof"] = {["scene"] = {"TALK::Kenner",
											 "TEXT::I know why your here.  Just take it and let me go.  I�ll disappear, he�ll never know.",
											 "TALK::player",
											 "TEXT::After all that trouble you cause me you expect me to just let you go?  I don�t think so.",
											 "TALK::Kenner",
											 "TEXT::Listen you don�t understand.  He�s using you, as soon as you give him both amulets half�s he�ll kill you!",
											 "TALK::player",
											 "TEXT::How about you let me worry about that.",
											 "TALK::Kenner",
											 "TEXT::He�ll betray you just like he did to us.",
											 "TALK::player",
											 "TEXT::I�m going to take that amulet now.",
											 "TALK::Kenner",
											 "TEXT::No, stay back! I won�t let you!",
											 "MOVE::kenner",
											 "LIST::U",
											 "MOVE::player",
											 "LIST::UUU",
											 "TALK::player",
											 "TEXT::You�re not getting away that easy.",
											 "COMBAT::START",
											 "ENEMY::kenner",
											 "LOADMAP::slumhouse",
											 "ITEM::quest1complete",
											 "END::CUTSCENE"},
									   ["iter"] = 1
									  },
				["quest1complete"] = {["scene"] = {"TALK::player",
												   "TEXT::Here�s your amulet.",
												   "TALK::Fredrick",
												   "TEXT::And here�s your payment.  Now you still have Nehir to deal with�",
												   "ITEM::gold",
												   "AMOUNT::50000",
												   "TALK::player",
												   "TEXT:;I�m working on it, you just make sure to have my payment ready.",
												   "TALK::Fredrick",
												   "TEXT::Don�t get overconfident, Nihir knows your coming. He�ll be ready for you.  I�d suggest you head west now before he can prepare much more.",
												   "TALK::player",
												   "TEXT::What ever you say�",
												   "END::CUTSCENE"},
									   ["iter"] = 1
									  },
				["factoryfloorday"] = {["scene"] = {"TALK::Foreman",
													"TEXT::Look what we have here� You lost boy?",
													"TALK::player",
													"TEXT::Nope, I�m right where I�m supposed to be.",
													"TALK::Foreman",
													"TEXT::Think ya best be gettin on outta here, less you be looking for trouble�",
													"TALK::player",
													"TEXT::I like trouble.",
													"TALK::Foreman",
													"TEXT::Heh, oh then we�ll show ya some big trouble.  BOYS!!",
													"COMBAT::START",
													"ENEMY::thug",
													"DESPAWN::foreman",
													"END::CUTSCENE"},
									   ["iter"] = 1
									  },
				["factoryfloordaynearend"] = {["scene"] = {"TALK::Foreman",
														   "TEXT::Well, like my pa always said �Want somethin done right, do it yer self�.",
														   "TALK::player",
														   "TEXT::This should be fun.",
														   "TALK::Foreman",
														   "TEXT::Oh yeah, we�re gonna have a real good time�",
														   "COMBAT::START",
														   "ENEMY::shopforeman",
														   "END::CUTSCENE"},
									   ["iter"] = 1
									  },
				["factoryoffice"] = {["scene"] = {"TALK::Niher",
												  "TEXT::So, you�ve finally arrived.  I hope you know I won�t go down as easy as the other you�ve fallen.",
												  "TALK::player",
												  "TEXT::Oh? And why is that? You have some kind of secret weapon?",
												  "TALK::Nither",
												  "TEXT::Not exactly, I am a master swordsman.  6 generations of my family.",
												  "TALK::player",
												  "TEXT::Good, I�ve been rather disappointed up to now with the quality of your lackies.",
												  "TALK::Nither",
												  "TEXT::Then I�ll waste no more time. On Guard!",
												  "COMBAT::START",
												  "ENEMY::nither",
												  "ITEM::quest2complete",
												  "LOADMAP::slumhouse",
												  "END::CUTSCENE"},
									   ["iter"] = 1
									  },
				["quest2complete"] = {["scene"] = {"TALK::Fredrick",
												   "TEXT::Do you have it?",
												   "TALK::player",
												   "TEXT::Yes, right here.",
												   "TALK::Fredrick",
												   "TEXT::Give it to me.  Ahh� finally, ahahaha�",
												   "TALK::player",
												   "TEXT::My payment?",
												   "TALK::Fredrick",
												   "TEXT::Oh yes, of course.  Kill him he of no use to me now.",
												   "TALK::player",
												   "TEXT::What! You�",
												   "ITEM::finalbattle",
												   "COMBAT::START",
												   "ENEMY::thug",
												   "DESPAWN::fredrick",
												   "SPAWN::poe",
												   "INITIALLOCATIONX::5",
												   "INITIALLOCAITNY::8",
												   "TALK::Poe",
												   "TEXT::Here take this my son.",
												   "ITEM::gutbustersammich",
												   "TALK::player",
												   "TEXT::Where is he?",
												   "TALK::Poe",
												   "TEXT::He�s fled in to the catacombs under the church, likely to restore the amulet you�ve given him.",
												   "TALK::player",
												   "TEXT::Then I�m going after him.",
												   "TALK::Poe",
												   "TEXT::Be careful with the amulet he�s more powerful then any being the world has ever seen.  Take this you�ll need it.",
												   "ITEM::magicalpigsticker",
												   "EQUIP::magicalpigsticker",
												   "END::CUTSCENE"},
									   ["iter"] = 1
									  },
				["finalbattle"] = {["scene"] = {"TALK::Fredrick",
											    "TEXT::You think you can stop me peasant?",
												"TALK::player",
												"TEXT::You couldn�t just stick to the deal.",
												"TALK::Fredrick",
												"TEXT::I don�t make deals with slime like you.  I rule over you and with this I�ll take back what�s mine and more!",
												"TALK::Fredrick",
												"TEXT::Feel my POWER!",
												"COMBAT::START",
												"ENEMY::fredrick",
												"LOADMAP::epilogue",
												"ITEM::epilogue",
												"END::CUTSCENE"},
									   ["iter"] = 1
									  },
				["epilogue"] = {["scene"] = {"TALK::Mysterious Voice",
											 "TEXT::With Fredrick defeated and the amulet lost Marcus return to his routine.  Stealing from the rich and giving to himself.  But while the amulet was lost it was not gone.  It still lingered waiting to be rediscovered and reconstruct, its shattered form the embodiment of its very nature, to destroy.",
											 "TALK::Mysterious Voice",
											 "TEXT::Game Over",
											 "END::CUTSCENE"},
									   ["iter"] = 1
									  }
			   }

--===============================================================================================================================
--===============================================================================================================================
--===============================================SAVE FUNCTIONS==================================================================
--===============================================================================================================================
--===============================================================================================================================

function savegame()
	return playerequippedvals["armor"], playerequippedvals["damage"], playerequippedvals["defense"], playerequippedvals["attack"], playerequippedvals["maxhealth"], playerequippedvals["health"], equipped["hands"]["name"], equipped["feet"]["name"], equipped["legs"]["name"], equipped["arms"]["name"], equipped["chest"]["name"], equipped["head"]["name"], poesammichtype, gbsammichcount, flsammichcount, sammichcount, flipper, getinitialitemscounter, count
end

function loadgame(armor, damage, defense, attack, maxhealth, health, hands, feet, legs, arms, chest, head, psamtype, gbsam, flsam, sam, flip, getinit, countit)
	playerequippedvals["armor"] = armor
	playerequippedvals["damage"] = damage
	playerequippedvals["defense"] = defense
	playerequippedvals["attack"] = attack
	playerequippedvals["maxhealth"] = maxhealth
	playerequippedvals["health"] = health
	equipped["hands"]["name"] = hands
	equipped["feet"]["name"] = feet
	equipped["legs"]["name"] = legs
	equipped["arms"]["name"] = arms
	equipped["chest"]["name"] = chest
	equipped["head"]["name"] = head
	poesammichtype = psamtype
	gbsammichcount = gbsam
	flsammichcount = flsam
	sammichcount = sam
	flipper = flip
	getinitialitemscounter = getinit
	count = countit
end

--===============================================================================================================================
--===============================================================================================================================
--===============================================DATA LISTS======================================================================
--===============================================================================================================================
--===============================================================================================================================


--this list contains the lists of map info - currently only cutscenes
map = {["slum"] = {["cutsceneitems"] = {"slumintro"}},
	   ["pub"] = {["cutsceneitems"] = {"rigleyspubintro"}},
	   ["house"] = {["cutsceneitems"] = {"intro"}},
	   ["fac_office"] = {["cutsceneitems"] = {"factoryoffice"}},
	   ["fac_main"] = {["cutsceneitems"] = {"factoryfloorday"}},
	   ["fac_base"] = {["cutsceneitems"] = nil},
	   ["church"] = {["cutsceneitems"] = nil},
	   ["catacomb"] = {["cutsceneitems"] = nil},
	   ["bank_vault"] = {["cutsceneitems"] = nil},
	   ["bank_roof"] = {["cutsceneitems"] = {"bankroof"}},
	   ["bank"] = {["cutsceneitems"] = {"bankintroday"}},
	   ["epilogue"] = {["cutsceneitems"] = {"epilogue"}}
	  }

--this is a list of items useable by the player and possibly npc's of the game
items = {["minor healing potion"] = {["name"] = "Minor Healing Potion",
									 ["give"] = "ITEM::Minor Healing Potion",
									 ["type"] = "consumable",
									 ["target"] = "self",
									 ["location"] = nil,
									 ["effect"] = {["health"] = 10},
									 ["cutsceneitems"] = nil
							  },
		 ["sammich"] = {["name"] = "Sammich",
									 ["give"] = "ITEM::sammich",
									 ["type"] = "consumable",
									 ["target"] = "self",
									 ["location"] = nil,
									 ["effect"] = {["health"] = 10},
									 ["cutsceneitems"] = nil
							  },
		 ["footlongsammich"] = {["name"] = "Foot Long Sammich",
									 ["give"] = "footlongsammich",
									 ["type"] = "consumable",
									 ["target"] = "self",
									 ["location"] = nil,
									 ["effect"] = {["health"] = 30},
									 ["cutsceneitems"] = nil
							  },
		 ["gutbustersammich"] = {["name"] = "Gut Buster Sammich",
									 ["give"] = "ITEM::gutbustersammich",
									 ["type"] = "consumable",
									 ["target"] = "self",
									 ["location"] = nil,
									 ["effect"] = {["health"] = 50},
									 ["cutsceneitems"] = nil
							  },
		 ["map"] = {["name"] = "Map",
					["give"] = "ITEM::Map",
					["type"] = "fluff",
					["target"] = "none",
					["location"] = nil,
					["effect"] = nil,
					["cutsceneitems"] = {"csitem5"}
				   },
		 ["hat"] = {["name"] = "Hat",
					["give"] = "ITEM::Hat",
					["type"] = "equipment",
					["target"] = "none",
					["location"] = "head",
					["effect"] = {["attack"] = 0,
								  ["defense"] = 0,
								  ["damage"] = 0,
								  ["armor"] = 1,
								  ["health"] = 5
								 },
					["cutsceneitems"] = nil
				   },
		 ["helmet"] = {["name"] = "Helmet",
					   ["give"] = "ITEM::Helmet",
					   ["type"] = "equipment",
					   ["target"] = "none",
					   ["location"] = "head",
					   ["effect"] = {["attack"] = 0,
								     ["defense"] = 0,
								     ["damage"] = 0,
								     ["armor"] = 15,
								     ["health"] = 0
								 },
						["cutsceneitems"] = nil
				   },
		 ["magicalpigsticker"] = {["name"] = "Magical Pig Sticker",
					   ["give"] = "ITEM::magicalpigsticker",
					   ["type"] = "equipment",
					   ["target"] = "none",
					   ["location"] = "hands",
					   ["effect"] = {["attack"] = 40,
								     ["defense"] = 40,
								     ["damage"] = 40,
								     ["armor"] = 40,
								     ["health"] = 50
								 },
						["cutsceneitems"] = nil
				   },
		 ["rock"] = {["name"] = "Rock",
					["give"] = "ITEM::Rock",
					["type"] = "consumable",
					["target"] = "opponent",
					["location"] = nil,
					["effect"] = {["health"] = -5},
					["cutsceneitems"] = nil
				   },
		 ["intro"] = {["name"] = "intro",
						["give"] = "ITEM::intro",
						["type"] = "invisible",
						["target"] = nil,
						["location"] = nil,
						["effect"] = nil,
						["cutsceneitems"] = nil,
						["cutscene"] = "intro"
					   },
		 ["slumintro"] = {["name"] = "slumintro",
						["give"] = "ITEM::slumintro",
						["type"] = "invisible",
						["target"] = nil,
						["location"] = nil,
						["effect"] = nil,
						["cutsceneitems"] = nil,
						["cutscene"] = "slumintro"
					   },
		 ["rigleyspubintro"] = {["name"] = "rigleyspubintro",
						["give"] = "ITEM::rigleyspubintro",
						["type"] = "invisible",
						["target"] = nil,
						["location"] = nil,
						["effect"] = nil,
						["cutsceneitems"] = nil,
						["cutscene"] = "rigleyspubintro"
					   },
		 ["fredrickpubfirsttalk"] = {["name"] = "fredrickpubfirsttalk",
						["give"] = "ITEM::fredrickpubfirsttalk",
						["type"] = "invisible",
						["target"] = nil,
						["location"] = nil,
						["effect"] = nil,
						["cutsceneitems"] = nil,
						["cutscene"] = "fredrickpubfirsttalk"
					   },
		 ["bankintroday"] = {["name"] = "bankintroday",
						["give"] = "ITEM::bankintroday",
						["type"] = "invisible",
						["target"] = nil,
						["location"] = nil,
						["effect"] = nil,
						["cutsceneitems"] = nil,
						["cutscene"] = "bankintroday"
					   },
		 ["bankgmencounter"] = {["name"] = "bankgmencounter",
						["give"] = "ITEM::bankgmencounter",
						["type"] = "invisible",
						["target"] = nil,
						["location"] = nil,
						["effect"] = nil,
						["cutsceneitems"] = nil,
						["cutscene"] = "bankgmencounter"
					   },
		 ["bankroof"] = {["name"] = "bankroof",
						["give"] = "ITEM::bankroof",
						["type"] = "invisible",
						["target"] = nil,
						["location"] = nil,
						["effect"] = nil,
						["cutsceneitems"] = nil,
						["cutscene"] = "bankroof"
					   },
		 ["quest1complete"] = {["name"] = "quest1complete",
						["give"] = "ITEM::quest1complete",
						["type"] = "invisible",
						["target"] = nil,
						["location"] = nil,
						["effect"] = nil,
						["cutsceneitems"] = nil,
						["cutscene"] = "quest1complete"
					   },
		 ["factoryfloorday"] = {["name"] = "factoryfloorday",
						["give"] = "ITEM::factoryfloorday",
						["type"] = "invisible",
						["target"] = nil,
						["location"] = nil,
						["effect"] = nil,
						["cutsceneitems"] = nil,
						["cutscene"] = "factoryfloorday"
					   },
		 ["factoryfloordaynearend"] = {["name"] = "factoryfloordaynearend",
						["give"] = "ITEM::factoryfloordaynearend",
						["type"] = "invisible",
						["target"] = nil,
						["location"] = nil,
						["effect"] = nil,
						["cutsceneitems"] = nil,
						["cutscene"] = "factoryfloordaynearend"
					   },
		 ["factoryoffice"] = {["name"] = "factoryoffice",
						["give"] = "ITEM::factoryoffice",
						["type"] = "invisible",
						["target"] = nil,
						["location"] = nil,
						["effect"] = nil,
						["cutsceneitems"] = nil,
						["cutscene"] = "factoryoffice"
					   },
		 ["quest2complete"] = {["name"] = "quest2complete",
						["give"] = "ITEM::quest2complete",
						["type"] = "invisible",
						["target"] = nil,
						["location"] = nil,
						["effect"] = nil,
						["cutsceneitems"] = nil,
						["cutscene"] = "quest2complete"
					   },
		 ["finalbattle"] = {["name"] = "finalbattle",
						["give"] = "ITEM::finalbattle",
						["type"] = "invisible",
						["target"] = nil,
						["location"] = nil,
						["effect"] = nil,
						["cutsceneitems"] = nil,
						["cutscene"] = "finalbattle"
					   },
		 ["epilogue"] = {["name"] = "epilogue",
						["give"] = "ITEM::epilogue",
						["type"] = "invisible",
						["target"] = nil,
						["location"] = nil,
						["effect"] = nil,
						["cutsceneitems"] = nil,
						["cutscene"] = "epilogue"
					   }
		}

--this is a list of the equipment the player is wearing and it's effects
equipped = {["head"] = {["name"] = "nothing",
						["attack"] = 0,
						["defense"]= 0,
						["damage"] = 0,
						["armor"] = 0,
						["health"] = 0,
					   },
			["chest"] = {["name"] = "nothing",
						 ["attack"] = 0,
						 ["defense"]= 0,
						 ["damage"] = 0,
						 ["armor"] = 0,
						 ["health"] = 0,
					   },
			["arms"] = {["name"] = "nothing",
						["attack"] = 0,
						["defense"]= 0,
						["damage"] = 0,
						["armor"] = 0,
						["health"] = 0,
					   },
			["legs"] = {["name"] = "nothing",
						["attack"] = 0,
						["defense"]= 0,
						["damage"] = 0,
						["armor"] = 0,
						["health"] = 0,
					   },
			["feet"] = {["name"] = "nothing",
						["attack"] = 0,
						["defense"]= 0,
						["damage"] = 0,
						["armor"] = 0,
						["health"] = 0,
					   },
			["hands"] = {["name"] = "nothing",
						 ["attack"] = 0,
						 ["defense"]= 0,
						 ["damage"] = 0,
						 ["armor"] = 0,
						 ["health"] = 0,
					   }
		   }

--this is a list of entities in the game.  Each entity is a list on it's own
-- and may contain lists even further down.
entity = {
--test npc's - not in the actual game
		  ["orc"] = {["name"] = "Orc",
					 ["imagename"] = "orc.bmp",
					 ["region"] = "Region 1",
					 ["txtgeneric"] =  generictext,
					 ["txtspecific"] = "I'm an orc who enjoys eating people like you!",
					 ["patrol"] = patroliterate,
					 ["patrolcount"] = 0,
					 ["patrollist"] = {"L","L","U","U","R","R","D","D"},
					 ["randommove"] = ranmove,
					 --the multiple ups in this list increase the chance of the character moving up
					 ["randommovelist"] = {"U","U","U", "U", "U", "R"},
					 ["combatroundtrigger"] = 2,
					 ["combatroundeffect"] = "RUN",
					 ["combatplayerhealthtrigger"] = 9,
					 ["combatplayerhealthtriggerrange"] = 1,
					 ["combatplayerhealtheffect"] = "ITEM",
					 ["combatplayerhealthitem"] = "Rock",
					 ["combatplayerhealthtarget"] = "player",
					 ["combatselfhealthtrigger"] = 4,
					 ["combatselfhealthtriggerrange"] = 2,
					 ["combatselfhealtheffect"] = "ITEM",
					 ["combatselfhealthitem"] = "sammich",
					 ["combatselfhealthtarget"] = "self",
					 ["defense"] = 2,
					 ["armor"] = 2,
					 ["attack"] = 5,
					 ["damage"] = 5,
					 ["health"] = 10,
					 ["maxhealth"] = 10,
					 ["runchance"] = 3,
					 ["cutsceneitems"] = nil
					}
		  ,["kobold"] = {["name"] = "kobold",
						 ["imagename"] = "kobold.bmp",
						 ["region"] = "Region 2",
						 ["txtgeneric"] = "Hiya!  I'm a kobold",
						 ["txtspecific"] = "I'm a kobold, run away!",
						 ["patrol"] = patroliterate,
						 ["patrolcount"] = 0,
						 ["patrollist"] = {"U","U","L","L","D","D","R","R"},
						 ["randommove"] = ranmove,
						 ["randommovelist"] = {"U", "D", "L", "R"},
						 ["combatroundtrigger"] = nil,
						 ["combatroundeffect"] = nil,
						 ["combatplayerhealthtrigger"] = nil,
						 ["combatplayerhealthtriggerrange"] = 0,
						 ["combatplayerhealtheffect"] = nil,
						 ["combatselfhealthtrigger"] = nil,
						 ["combatselfhealthtriggerrange"] = 0,
						 ["combatselfhealtheffect"] = nil,
						 ["defense"] = 5,
						 ["armor"] = 2,
						 ["attack"] = 5,
						 ["damage"] = 5,
						 ["health"] = 10,
						 ["maxhealth"] = 10,
						 ["runchance"] = 3,
						 ["cutsceneitems"] = nil
					   }
		  ,["item giver"] = {["name"] = "item giver",
							["imagename"] = "itemgiver.bmp",
							["region"] = "Region 2",
							["txtgeneric"] = items["sammich"]["give"],
							["txtspecific"] = "I love to give items!",
							["patrol"] = patroliterate,
							["patrolcount"] = 0,
							["patrollist"] = {"U","U","L","L","D","D","R","R"},
							["randommove"] = ranmove,
							["randommovelist"] = {"U", "D", "L", "R"},
							["combatroundtrigger"] = nil,
							["combatroundeffect"] = nil,
							["combatplayerhealthtrigger"] = nil,
							["combatplayerhealthtriggerrange"] = 0,
							["combatplayerhealtheffect"] = nil,
							["combatselfhealthtrigger"] = nil,
							["combatselfhealthtriggerrange"] = 0,
							["combatselfhealtheffect"] = nil,
							["defense"] = 5,
							["armor"] = 2,
							["attack"] = 5,
							["damage"] = 5,
							["health"] = 10,
							["maxhealth"] = 10,
							["runchance"] = 3,
							["cutsceneitems"] = nil
					   }
--non combat npc's
			,["poe"] = {["name"] = "Poe",
					 ["imagename"] = "poe.bmp",
					 ["region"] = "Region 1",
					 ["txtgeneric"] =  poetext,
					 ["txtspecific"] = "Welcome, anything that I can do to help.",
					 ["patrol"] = nil,
					 ["patrolcount"] = nil,
					 ["patrollist"] = nil,
					 ["randommove"] = nil,
					 --the multiple ups in this list increase the chance of the character moving up
					 ["randommovelist"] = nil,
					 ["combatroundtrigger"] = nil,
					 ["combatroundeffect"] = nil,
					 ["combatplayerhealthtrigger"] = nil,
					 ["combatplayerhealthtriggerrange"] = nil,
					 ["combatplayerhealtheffect"] = nil,
					 ["combatplayerhealthitem"] = nil,
					 ["combatplayerhealthtarget"] = nil,
					 ["combatselfhealthtrigger"] = nil,
					 ["combatselfhealthtriggerrange"] = nil,
					 ["combatselfhealtheffect"] = nil,
					 ["combatselfhealthitem"] = nil,
					 ["combatselfhealthtarget"] = nil,
					 ["defense"] = 2,
					 ["armor"] = 2,
					 ["attack"] = 5,
					 ["damage"] = 5,
					 ["health"] = 10,
					 ["maxhealth"] = 10,
					 ["runchance"] = 3,
					 ["cutsceneitems"] = nil
					}
			,["rigley"] = {["name"] = "Rigley",
					 ["imagename"] = "rigley.bmp",
					 ["region"] = "Region 1",
					 ["txtgeneric"] =  "Need a beer to wash down that sammich?",
					 ["txtspecific"] = "Need a beer to wash down that sammich?",
					 ["patrol"] = nil,
					 ["patrolcount"] = nil,
					 ["patrollist"] = nil,
					 ["randommove"] = nil,
					 --the multiple ups in this list increase the chance of the character moving up
					 ["randommovelist"] = nil,
					 ["combatroundtrigger"] = nil,
					 ["combatroundeffect"] = nil,
					 ["combatplayerhealthtrigger"] = nil,
					 ["combatplayerhealthtriggerrange"] = nil,
					 ["combatplayerhealtheffect"] = nil,
					 ["combatplayerhealthitem"] = nil,
					 ["combatplayerhealthtarget"] = nil,
					 ["combatselfhealthtrigger"] = nil,
					 ["combatselfhealthtriggerrange"] = nil,
					 ["combatselfhealtheffect"] = nil,
					 ["combatselfhealthitem"] = nil,
					 ["combatselfhealthtarget"] = nil,
					 ["defense"] = 2,
					 ["armor"] = 2,
					 ["attack"] = 5,
					 ["damage"] = 5,
					 ["health"] = 10,
					 ["maxhealth"] = 10,
					 ["runchance"] = 3,
					 ["cutsceneitems"] = nil
					}
--boss npc's
			,["fredrick"] = {["name"] = "Fredrick",
					 ["imagename"] = "fredrick.bmp",
					 ["region"] = "Region 1",
					 ["txtgeneric"] =  "Don't you have work to do?",
					 ["txtspecific"] = "Don't you have work to do?",
					 ["patrol"] = nil,
					 ["patrolcount"] = nil,
					 ["patrollist"] = nil,
					 ["randommove"] = nil,
					 --the multiple ups in this list increase the chance of the character moving up
					 ["randommovelist"] = nil,
					 ["combatroundtrigger"] = nil,
					 ["combatroundeffect"] = nil,
					 ["combatplayerhealthtrigger"] = nil,
					 ["combatplayerhealthtriggerrange"] = nil,
					 ["combatplayerhealtheffect"] = nil,
					 ["combatplayerhealthitem"] = nil,
					 ["combatplayerhealthtarget"] = nil,
					 ["combatselfhealthtrigger"] = nil,
					 ["combatselfhealthtriggerrange"] = nil,
					 ["combatselfhealtheffect"] = nil,
					 ["combatselfhealthitem"] = nil,
					 ["combatselfhealthtarget"] = nil,
					 ["defense"] = 50,
					 ["armor"] = 50,
					 ["attack"] = 50,
					 ["damage"] = 50,
					 ["health"] = 50,
					 ["maxhealth"] = 50,
					 ["runchance"] = 3,
					 ["cutsceneitems"] =  {"fredrickpubfirsttalk", "quest1complete", "quest2complete"}
					}
			,["nehir"] = {["name"] = "Nehir Tignet",
					 ["imagename"] = "nehir.bmp",
					 ["region"] = "Region 1",
					 ["txtgeneric"] =  "En Guard!",
					 ["txtspecific"] = "Prepare to taste my blade!",
					 ["patrol"] = nil,
					 ["patrolcount"] = nil,
					 ["patrollist"] = nil,
					 ["randommove"] = nil,
					 --the multiple ups in this list increase the chance of the character moving up
					 ["randommovelist"] = nil,
					 ["combatroundtrigger"] = nil,
					 ["combatroundeffect"] = nil,
					 ["combatplayerhealthtrigger"] = nil,
					 ["combatplayerhealthtriggerrange"] = nil,
					 ["combatplayerhealtheffect"] = nil,
					 ["combatplayerhealthitem"] = nil,
					 ["combatplayerhealthtarget"] = nil,
					 ["combatselfhealthtrigger"] = nil,
					 ["combatselfhealthtriggerrange"] = nil,
					 ["combatselfhealtheffect"] = nil,
					 ["combatselfhealthitem"] = nil,
					 ["combatselfhealthtarget"] = nil,
					 ["defense"] = 10,
					 ["armor"] = 5,
					 ["attack"] = 15,
					 ["damage"] = 10,
					 ["health"] = 35,
					 ["maxhealth"] = 35,
					 ["runchance"] = 3,
					 ["cutsceneitems"] =  nil
					}
			,["kenner"] = {["name"] = "Kenner Baben",
					 ["imagename"] = "kenner.bmp",
					 ["region"] = "Region 1",
					 ["txtgeneric"] =  "I could have just dissapeared!",
					 ["txtspecific"] = "You must know he'll betray you!",
					 ["patrol"] = nil,
					 ["patrolcount"] = nil,
					 ["patrollist"] = nil,
					 ["randommove"] = nil,
					 --the multiple ups in this list increase the chance of the character moving up
					 ["randommovelist"] = nil,
					 ["combatroundtrigger"] = nil,
					 ["combatroundeffect"] = nil,
					 ["combatplayerhealthtrigger"] = nil,
					 ["combatplayerhealthtriggerrange"] = nil,
					 ["combatplayerhealtheffect"] = nil,
					 ["combatplayerhealthitem"] = nil,
					 ["combatplayerhealthtarget"] = nil,
					 ["combatselfhealthtrigger"] = nil,
					 ["combatselfhealthtriggerrange"] = nil,
					 ["combatselfhealtheffect"] = nil,
					 ["combatselfhealthitem"] = nil,
					 ["combatselfhealthtarget"] = nil,
					 ["defense"] = 10,
					 ["armor"] = 10,
					 ["attack"] = 10,
					 ["damage"] = 15,
					 ["health"] = 35,
					 ["maxhealth"] = 35,
					 ["runchance"] = 3,
					 ["cutsceneitems"] =  nil
					}
--specific combat npc's
			,["shopforeman"] = {["name"] = "Shop Foreman",
					 ["imagename"] = "shopforeman.bmp",
					 ["region"] = "Region 1",
					 ["txtgeneric"] =  "You've ravaged your own factories, you'll not ravage mine!",
					 ["txtspecific"] = "You've ravaged your own factories, you'll not ravage mine!",
					 ["patrol"] = nil,
					 ["patrolcount"] = nil,
					 ["patrollist"] = nil,
					 ["randommove"] = nil,
					 --the multiple ups in this list increase the chance of the character moving up
					 ["randommovelist"] = nil,
					 ["combatroundtrigger"] = nil,
					 ["combatroundeffect"] = nil,
					 ["combatplayerhealthtrigger"] = nil,
					 ["combatplayerhealthtriggerrange"] = nil,
					 ["combatplayerhealtheffect"] = nil,
					 ["combatplayerhealthitem"] = nil,
					 ["combatplayerhealthtarget"] = nil,
					 ["combatselfhealthtrigger"] = nil,
					 ["combatselfhealthtriggerrange"] = nil,
					 ["combatselfhealtheffect"] = nil,
					 ["combatselfhealthitem"] = nil,
					 ["combatselfhealthtarget"] = nil,
					 ["defense"] = 10,
					 ["armor"] = 10,
					 ["attack"] = 10,
					 ["damage"] = 15,
					 ["health"] = 35,
					 ["maxhealth"] = 35,
					 ["runchance"] = 3,
					 ["cutsceneitems"] =  {"factoryfloordaynearend"}
					}
			,["generalmanager"] = {["name"] = "General Manager",
					 ["imagename"] = "generalmanager.bmp",
					 ["region"] = "Region 1",
					 ["txtgeneric"] =  "Oh no, oh bother!",
					 ["txtspecific"] = "I must stop you!",
					 ["patrol"] = nil,
					 ["patrolcount"] = nil,
					 ["patrollist"] = nil,
					 ["randommove"] = nil,
					 --the multiple ups in this list increase the chance of the character moving up
					 ["randommovelist"] = nil,
					 ["combatroundtrigger"] = nil,
					 ["combatroundeffect"] = nil,
					 ["combatplayerhealthtrigger"] = nil,
					 ["combatplayerhealthtriggerrange"] = nil,
					 ["combatplayerhealtheffect"] = nil,
					 ["combatplayerhealthitem"] = nil,
					 ["combatplayerhealthtarget"] = nil,
					 ["combatselfhealthtrigger"] = 8,
					 ["combatselfhealthtriggerrange"] = 8,
					 ["combatselfhealtheffect"] = "RUN",
					 ["combatselfhealthitem"] = nil,
					 ["combatselfhealthtarget"] = nil,
					 ["defense"] = 7,
					 ["armor"] = 7,
					 ["attack"] = 7,
					 ["damage"] = 7,
					 ["health"] = 30,
					 ["maxhealth"] = 30,
					 ["runchance"] = 10,
					 ["cutsceneitems"] =  {"bankgmencounter"}
					}
--generic combat npc's
			,["thug"] = {["name"] = "Thug",
					 ["imagename"] = "thug.bmp",
					 ["region"] = "Region 1",
					 ["txtgeneric"] =  "I'll cut you, fool!",
					 ["txtspecific"] = "Gimme your money, or your life!",
					 ["patrol"] = nil,
					 ["patrolcount"] = nil,
					 ["patrollist"] = nil,
					 ["randommove"] = nil,
					 --the multiple ups in this list increase the chance of the character moving up
					 ["randommovelist"] = nil,
					 ["combatroundtrigger"] = nil,
					 ["combatroundeffect"] = nil,
					 ["combatplayerhealthtrigger"] = nil,
					 ["combatplayerhealthtriggerrange"] = nil,
					 ["combatplayerhealtheffect"] = nil,
					 ["combatplayerhealthitem"] = nil,
					 ["combatplayerhealthtarget"] = nil,
					 ["combatselfhealthtrigger"] = nil,
					 ["combatselfhealthtriggerrange"] = nil,
					 ["combatselfhealtheffect"] = nil,
					 ["combatselfhealthitem"] = nil,
					 ["combatselfhealthtarget"] = nil,
					 ["defense"] = 2,
					 ["armor"] = 2,
					 ["attack"] = 5,
					 ["damage"] = 5,
					 ["health"] = 10,
					 ["maxhealth"] = 10,
					 ["runchance"] = 3,
					 ["cutsceneitems"] =  nil
					}
			,["janitor"] = {["name"] = "Janitor",
					 ["imagename"] = "janitor.bmp",
					 ["region"] = "Region 1",
					 ["txtgeneric"] =  "You scuffed my floor, DIE!",
					 ["txtspecific"] = "I'll mop the floor with you!",
					 ["patrol"] = nil,
					 ["patrolcount"] = nil,
					 ["patrollist"] = nil,
					 ["randommove"] = nil,
					 --the multiple ups in this list increase the chance of the character moving up
					 ["randommovelist"] = nil,
					 ["combatroundtrigger"] = nil,
					 ["combatroundeffect"] = nil,
					 ["combatplayerhealthtrigger"] = nil,
					 ["combatplayerhealthtriggerrange"] = nil,
					 ["combatplayerhealtheffect"] = nil,
					 ["combatplayerhealthitem"] = nil,
					 ["combatplayerhealthtarget"] = nil,
					 ["combatselfhealthtrigger"] = nil,
					 ["combatselfhealthtriggerrange"] = nil,
					 ["combatselfhealtheffect"] = nil,
					 ["combatselfhealthitem"] = nil,
					 ["combatselfhealthtarget"] = nil,
					 ["defense"] = 4,
					 ["armor"] = 4,
					 ["attack"] = 6,
					 ["damage"] = 6,
					 ["health"] = 15,
					 ["maxhealth"] = 15,
					 ["runchance"] = 3,
					 ["cutsceneitems"] =  nil
					}
			,["securityguard"] = {["name"] = "Janitor",
					 ["imagename"] = "janitor.bmp",
					 ["region"] = "Region 1",
					 ["txtgeneric"] =  "Nothing to see here, except your ass being handed to you!",
					 ["txtspecific"] = "Move along... to your death!",
					 ["patrol"] = nil,
					 ["patrolcount"] = nil,
					 ["patrollist"] = nil,
					 ["randommove"] = nil,
					 --the multiple ups in this list increase the chance of the character moving up
					 ["randommovelist"] = nil,
					 ["combatroundtrigger"] = nil,
					 ["combatroundeffect"] = nil,
					 ["combatplayerhealthtrigger"] = nil,
					 ["combatplayerhealthtriggerrange"] = nil,
					 ["combatplayerhealtheffect"] = nil,
					 ["combatplayerhealthitem"] = nil,
					 ["combatplayerhealthtarget"] = nil,
					 ["combatselfhealthtrigger"] = nil,
					 ["combatselfhealthtriggerrange"] = nil,
					 ["combatselfhealtheffect"] = nil,
					 ["combatselfhealthitem"] = nil,
					 ["combatselfhealthtarget"] = nil,
					 ["defense"] = 5,
					 ["armor"] = 5,
					 ["attack"] = 7,
					 ["damage"] = 7,
					 ["health"] = 20,
					 ["maxhealth"] = 20,
					 ["runchance"] = 3,
					 ["cutsceneitems"] =  nil
					}
			,["factoryworker"] = {["name"] = "Factory Worker",
					 ["imagename"] = "factoryworker.bmp",
					 ["region"] = "Region 1",
					 ["txtgeneric"] =  "I'm going to assemble your face to my liking!",
					 ["txtspecific"] = "Snap, Twist, Pop, next piece",
					 ["patrol"] = nil,
					 ["patrolcount"] = nil,
					 ["patrollist"] = nil,
					 ["randommove"] = nil,
					 --the multiple ups in this list increase the chance of the character moving up
					 ["randommovelist"] = nil,
					 ["combatroundtrigger"] = nil,
					 ["combatroundeffect"] = nil,
					 ["combatplayerhealthtrigger"] = nil,
					 ["combatplayerhealthtriggerrange"] = nil,
					 ["combatplayerhealtheffect"] = nil,
					 ["combatplayerhealthitem"] = nil,
					 ["combatplayerhealthtarget"] = nil,
					 ["combatselfhealthtrigger"] = nil,
					 ["combatselfhealthtriggerrange"] = nil,
					 ["combatselfhealtheffect"] = nil,
					 ["combatselfhealthitem"] = nil,
					 ["combatselfhealthtarget"] = nil,
					 ["defense"] = 6,
					 ["armor"] = 6,
					 ["attack"] = 8,
					 ["damage"] = 8,
					 ["health"] = 25,
					 ["maxhealth"] = 25,
					 ["runchance"] = 3,
					 ["cutsceneitems"] =  nil
					}
			,["teller"] = {["name"] = "Teller",
					 ["imagename"] = "teller.bmp",
					 ["region"] = "Region 1",
					 ["txtgeneric"] =  "My fist has a deposit to make in your face!",
					 ["txtspecific"] = "Sorry, your in the wrong line!",
					 ["patrol"] = nil,
					 ["patrolcount"] = nil,
					 ["patrollist"] = nil,
					 ["randommove"] = nil,
					 --the multiple ups in this list increase the chance of the character moving up
					 ["randommovelist"] = nil,
					 ["combatroundtrigger"] = nil,
					 ["combatroundeffect"] = nil,
					 ["combatplayerhealthtrigger"] = nil,
					 ["combatplayerhealthtriggerrange"] = nil,
					 ["combatplayerhealtheffect"] = nil,
					 ["combatplayerhealthitem"] = nil,
					 ["combatplayerhealthtarget"] = nil,
					 ["combatselfhealthtrigger"] = nil,
					 ["combatselfhealthtriggerrange"] = nil,
					 ["combatselfhealtheffect"] = nil,
					 ["combatselfhealthitem"] = nil,
					 ["combatselfhealthtarget"] = nil,
					 ["defense"] = 4,
					 ["armor"] = 4,
					 ["attack"] = 6,
					 ["damage"] = 6,
					 ["health"] = 15,
					 ["maxhealth"] = 15,
					 ["runchance"] = 3,
					 ["cutsceneitems"] =  nil
					}
			,["regulator"] = {["name"] = "Regulator",
					 ["imagename"] = "regulator.bmp",
					 ["region"] = "Region 1",
					 ["txtgeneric"] =  "Out of my vault",
					 ["txtspecific"] = "No admittance without an escort!",
					 ["patrol"] = nil,
					 ["patrolcount"] = nil,
					 ["patrollist"] = nil,
					 ["randommove"] = nil,
					 --the multiple ups in this list increase the chance of the character moving up
					 ["randommovelist"] = nil,
					 ["combatroundtrigger"] = nil,
					 ["combatroundeffect"] = nil,
					 ["combatplayerhealthtrigger"] = nil,
					 ["combatplayerhealthtriggerrange"] = nil,
					 ["combatplayerhealtheffect"] = nil,
					 ["combatplayerhealthitem"] = nil,
					 ["combatplayerhealthtarget"] = nil,
					 ["combatselfhealthtrigger"] = nil,
					 ["combatselfhealthtriggerrange"] = nil,
					 ["combatselfhealtheffect"] = nil,
					 ["combatselfhealthitem"] = nil,
					 ["combatselfhealthtarget"] = nil,
					 ["defense"] = 6,
					 ["armor"] = 6,
					 ["attack"] = 8,
					 ["damage"] = 8,
					 ["health"] = 25,
					 ["maxhealth"] = 25,
					 ["runchance"] = 3,
					 ["cutsceneitems"] =  nil
					}
		 }


--this is a list containing generic text for the game
generictexttbl = {"Hi, I love living in Cinnaka!  How about you?",
			   "How do you do, I'm heading to the Church of the Shattered to get some food!",
			   "Cinnaka is my home, and talking to strangers is my game!"
		      }


player = {["health"] = 50,
		  ["maxhealth"] = 50,
		  ["attack"] = 5,
		  ["defense"] = 5,
		  ["damage"] = 5,
		  ["armor"] = 5
		 }

playerequippedvals = {["health"] = (player["health"]
									   + equipped["head"]["health"]
									   + equipped["chest"]["health"]
									   + equipped["arms"]["health"]
									   + equipped["legs"]["health"]
									   + equipped["feet"]["health"]
									   + equipped["hands"]["health"]),
		  ["maxhealth"] = (player["maxhealth"]
							 + equipped["head"]["health"]
							 + equipped["chest"]["health"]
							 + equipped["arms"]["health"]
							 + equipped["legs"]["health"]
							 + equipped["feet"]["health"]
							 + equipped["hands"]["health"]),
		  ["attack"] = (player["attack"]
							+ equipped["head"]["attack"]
							+ equipped["chest"]["attack"]
							+ equipped["arms"]["attack"]
							+ equipped["legs"]["attack"]
							+ equipped["feet"]["attack"]
							+ equipped["hands"]["attack"]),
		  ["defense"] = (player["defense"]
							+ equipped["head"]["defense"]
							+ equipped["chest"]["defense"]
							+ equipped["arms"]["defense"]
							+ equipped["legs"]["defense"]
							+ equipped["feet"]["defense"]
							+ equipped["hands"]["defense"]),
		  ["damage"] = (player["damage"]
							+ equipped["head"]["damage"]
							+ equipped["chest"]["damage"]
							+ equipped["arms"]["damage"]
							+ equipped["legs"]["damage"]
							+ equipped["feet"]["damage"]
							+ equipped["hands"]["damage"]),
		  ["armor"] = (player["armor"]
							+ equipped["head"]["armor"]
							+ equipped["chest"]["armor"]
							+ equipped["arms"]["armor"]
							+ equipped["legs"]["armor"]
							+ equipped["feet"]["armor"]
							+ equipped["hands"]["armor"])
					}

initialitems = {"intro","slumintro","rigleyspubintro", "bankintroday", "bankroof", "factoryfloorday", "factoryoffice"}

