#include "Enemy.h"

void Enemy::LoadMove( LuaManager *luaScript )
{
    DebugOutput( "Enemy::LoadMove()" );
    // Load patrol movement
    string moveDir;
    DebugOutput( "Enemy::LoadMove() > GetInfoStr for patrol", type );
    moveDir = luaScript->GetInfoStr( type.c_str(), "patrol" ); //get next NPC movement
    DebugOutput( "Enemy::LoadMove() - moveDir", moveDir );

    if ( moveDir == "U" )
    {
        direction = UP;
        moveCounter = 0;
        moving = true;
    }
    else if ( moveDir == "D" )
    {
        direction = DOWN;
        moveCounter = 0;
        moving = true;
    }
    else if ( moveDir == "R" )
    {
        direction = RIGHT;
        moveCounter = 0;
        moving = true;
    }
    else if ( moveDir == "L" )
    {
        direction = LEFT;
        moveCounter = 0;
        moving = true;
    }
    else
    {
        //no movement
        moveCounter = -1;
        moving = false;
    }
}

void Enemy::Move()
{
    DebugOutput( "Enemy::Move()" );
    // In the middle of a movement
    moveCounter++;
    if ( moveCounter >= 32 )
    {
        moveCounter = -1;
        moving = false;
    }
    IncrementFrame();
    Correct( direction );
}

Direction Enemy::Update( LuaManager *luaScript )
{
    DebugOutput( "Enemy::Update()" );
    if ( !moving )
    {
        DebugOutput( "Enemy::Update() - not moving > LoadMove" );
        LoadMove( luaScript );
    }
    else
    {
        DebugOutput( "Enemy::Update() - moving > Move" );
        Move();
    }

    return direction;
}

Enemy::Enemy()
{
    enemyNPC = true;
    active = false;
}

Enemy::~Enemy()
{
}

string Enemy::GetDialog( LuaManager *luaScript )
{
    return luaScript->GetInfoStr( name.c_str(), "txtgeneric" );
}

void Enemy::DrawNpc( SDL_Surface *destination, SDL_Surface *source, int offsetX, int offsetY, int currentLevel )
{
    if ( currentLevel == startCoordinates.mapId )
    {
        Draw( destination, source, offsetX, offsetY );
    }
}

void Enemy::Setup( int index, LuaManager *luaScript )
{
    moving = false;
    moveCounter = -1;
    frame = IDLE;
    speed = 1.0f;
    direction = RIGHT;
    action = WALKING;
    FRAMEMAX = 4;
    W( 32 );
    H( 48 );

    this->index = index;

    switch( index )
    {
        case 0:
            // Npc NAME
            startCoordinates.Setup( 1, 10, HOUSE );
            type = "janitor";
            image = 4;
        break;
        case 1:
            // Npc NAME
            startCoordinates.Setup( 1, 5, SLUM );
            type = "janitor";
            image = 3;
        break;
        case 2:
            // Npc NAME
            startCoordinates.Setup( 5, 15, SLUM );
            type = "poe";
            image = 7;
        break;
        case 3:
            // Npc NAME
            startCoordinates.Setup( 10, 5, PUB );
            type = "rigley";
            image = 9;
        break;
        case 4:
            // Npc NAME
            startCoordinates.Setup( 12, 5, HOUSE );
            type = "shopforeman";
            image = 2;
        break;
        case 5:
            // Npc NAME
            startCoordinates.Setup( 12, 10, HOUSE );
            type = "nehir";
            image = 6;
        break;
        case 6:
            type = "kenner";
            image = 5;
        break;
        case 7:
            // Npc NAME
            startCoordinates.Setup( 12, 22, HOUSE );
            type = "janitor";
            image = 4;
        break;
        case 8:
            // Npc NAME
            startCoordinates.Setup( 12, 22, HOUSE );
            type = "securityguard";
            image = 10;
        break;
        case 9:
            // Npc NAME
            startCoordinates.Setup( 12, 22, HOUSE );
            type = "factoryworker";
            image = 1;
        break;
        case 10:
            // Npc NAME
            startCoordinates.Setup( 12, 22, HOUSE );
            type = "teller";
            image = 11;
        break;
        default:
            image = 0;
            type = "orc";
        break;
    }
    DebugOutput( "Setup > Type", type );
    name = luaScript->GetInfoStr( type.c_str(), "name" );
    DebugOutput( "Setup > Name", name );
    X( startCoordinates.x * 32 );
    Y( startCoordinates.y * 32 );
    DebugOutput( "Setup - x", X() );
    DebugOutput( "Setup - y", Y() );
}


