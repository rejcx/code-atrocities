#include "ImageManager.h"

SDL_Surface *LoadImage( string filename )
{
    SDL_Surface *loadedImage = NULL;
    SDL_Surface *optimizedImage = NULL;
    loadedImage = SDL_LoadBMP( filename.c_str() );
    if ( loadedImage != NULL )
    {
        optimizedImage = SDL_DisplayFormat( loadedImage );
        SDL_FreeSurface( loadedImage );
        if ( optimizedImage != NULL )
        {
            //make that pink color transparent
            Uint32 colorkey = SDL_MapRGB( optimizedImage->format, 0xFF, 0, 0xFF );
            SDL_SetColorKey( optimizedImage, SDL_SRCCOLORKEY, colorkey );
        }
    }
    else
    {
        cerr<<"Error loading image "<<filename<<endl;
    }
    return optimizedImage;
}

void ImageManager::LoadInGraphics()
{
    tileset = LoadImage( "data/tileset.bmp" );
    player = LoadImage( "data/marcus.bmp" );
    title = LoadImage( "data/title.bmp" );
    menugfx = LoadImage( "data/menugraphic.bmp" );
    hudgfx = LoadImage( "data/hud.bmp" );

    /*
    SDL_Surface *enemy[13];
    SDL_Surface *enemyBattle[13];
    */
          enemy[ 0 ]  = LoadImage( "data/char_bankmanager.bmp" );
    enemyBattle[ 0 ]  = LoadImage( "data/char_bankmanager_bat.bmp" );

          enemy[ 1 ]  = LoadImage( "data/char_factoryworker.bmp" );
    enemyBattle[ 1 ]  = LoadImage( "data/char_factoryworker_bat.bmp" );

          enemy[ 2 ]  = LoadImage( "data/char_foreman.bmp" );
    enemyBattle[ 2 ]  = LoadImage( "data/char_foreman_bat.bmp" );

          enemy[ 3 ]  = LoadImage( "data/char_frederick.bmp" );
    enemyBattle[ 3 ]  = LoadImage( "data/char_frederick_bat.bmp" );

          enemy[ 4 ]  = LoadImage( "data/char_janitor.bmp" );
    enemyBattle[ 4 ]  = LoadImage( "data/char_janitor_bat.bmp" );

          enemy[ 5 ]  = LoadImage( "data/char_kenner.bmp" );
    enemyBattle[ 5 ]  = LoadImage( "data/char_kenner_bat.bmp" );

          enemy[ 6 ]  = LoadImage( "data/char_nehir.bmp" );
    enemyBattle[ 6 ]  = LoadImage( "data/char_nehir_bat.bmp" );

          enemy[ 7 ]  = LoadImage( "data/char_poe.bmp" );
    enemyBattle[ 7 ]  = LoadImage( "data/char_poe_bat.bmp" );

          enemy[ 8 ]  = LoadImage( "data/char_regulator.bmp" );
    enemyBattle[ 8 ]  = LoadImage( "data/char_regulator_bat.bmp" );

          enemy[ 9 ]  = LoadImage( "data/char_rigley.bmp" );
    enemyBattle[ 9 ]  = LoadImage( "data/char_rigley_bat.bmp" );

          enemy[ 10 ]  = LoadImage( "data/char_securityguard.bmp" );
    enemyBattle[ 10 ]  = LoadImage( "data/char_securityguard_bat.bmp" );

          enemy[ 11 ]  = LoadImage( "data/char_teller.bmp" );
    enemyBattle[ 11 ]  = LoadImage( "data/char_teller_bat.bmp" );

          enemy[ 12 ]  = LoadImage( "data/char_thug.bmp" );
    enemyBattle[ 12 ]  = LoadImage( "data/char_thug_bat.bmp" );

    // Check for errors

    for ( int i=0; i<12; i++ )
    {
        if ( enemy[i] == NULL )
            cerr<<"Error loading image for enemy "<<i<<endl;
        if ( enemyBattle[i] == NULL )
            cerr<<"Error loading image for enemyBattle "<<i<<endl;
    }
}

ImageManager::ImageManager()
{
    LoadInGraphics();
}

ImageManager::~ImageManager()
{
    SDL_FreeSurface( tileset );
    SDL_FreeSurface( player );
    SDL_FreeSurface( enemy[0] );
    SDL_FreeSurface( enemy[1] );
    SDL_FreeSurface( title );
    SDL_FreeSurface( menugfx );
    SDL_FreeSurface( hudgfx );
}

void ImageManager::DrawHud( SDL_Surface *destination )
{
    SDL_Rect coordinates;
    SDL_Rect clipCoord;

    clipCoord.x = 0;
    clipCoord.y = 0;
    clipCoord.w = 153;
    clipCoord.h = 38;
    coordinates.x = 640 - clipCoord.w - 2;
    coordinates.y = 2;
    coordinates.w = clipCoord.w;
    coordinates.h = clipCoord.h;
    SDL_BlitSurface( hudgfx, &clipCoord, destination, &coordinates );
}





