#include "BattleSystem.h"

BattleSystem::BattleSystem()
{
    active = false;

    /* Setup things to help parse returns from script */
    str.combat = "COMBAT::";
    str.animation = "ANIMATION::";
    str.by = "BY::";
    str.amount = "AMOUNT::";
    str.target = "TARGET::";
    str.turn = "TURN::";
    str.action == "ACTION::";
    str.itemused == "ITEMUSED::";
    currLine = 0;
    enemy.battleImage = 0;
}

void BattleSystem::SetupBattle( string npcId, LuaManager *luaScript, Character *pla )
{
    enemyGraphicCoord.x = 640/2 - 125;
    enemyGraphicCoord.y = 480/2 - 200;

    active = true;
    enemy.idName = npcId;
    enemy.name = npcId;
    player.name = pla->Name();
    UpdateHealth( luaScript );
    awaitingInput = false;

    for ( int i=0; i<LINEAMT; i++ )
        textLine[i] = "";
    currLine = 0;

    // set enemy graphic
    cout<<enemy.idName<<endl;
    if ( enemy.idName == "bankmanager" )            { enemy.battleImage = 0; }
    else if ( enemy.idName == "factoryworker" )     { enemy.battleImage = 1; }
    else if ( enemy.idName == "foreman" )           { enemy.battleImage = 2; }
    else if ( enemy.idName == "frederick" )         { enemy.battleImage = 3; }
    else if ( enemy.idName == "janitor" )           { enemy.battleImage = 4; }
    else if ( enemy.idName == "kenner" )            { enemy.battleImage = 5; }
    else if ( enemy.idName == "nehir" )             { enemy.battleImage = 6; }
    else if ( enemy.idName == "poe" )               { enemy.battleImage = 7; }
    else if ( enemy.idName == "regulator" )         { enemy.battleImage = 8; }
    else if ( enemy.idName == "rigley" )            { enemy.battleImage = 9; }
    else if ( enemy.idName == "securityguard" )     { enemy.battleImage = 10; }
    else if ( enemy.idName == "teller" )            { enemy.battleImage = 11; }
    else if ( enemy.idName == "thug" )              { enemy.battleImage = 12; }
    else { enemy.battleImage = 0; } // orc, kobold
}

void BattleSystem::UpdateHealth( LuaManager *luaScript )
{
    enemy.HP = luaScript->GetInfoInt( enemy.name.c_str(), "health" );
    enemy.maxHP = luaScript->GetInfoInt( enemy.name.c_str(), "maxhealth" );

    player.HP = luaScript->GetPlayerInfo( "health" );
    player.maxHP = luaScript->GetPlayerInfo( "maxhealth" );
}

void BattleSystem::GetBattleCommand( LuaManager *luaScript, string action )
{
    if ( awaitingInput )
    {
//        cout<<action<<endl;
        string temp;
        temp = "> ";
        temp += action;
        AddTextLine( temp );
        AddTextLine( ATTACK );
        luaScript->CombatSystemInput( action, enemy.name.c_str() );
        awaitingInput = false;
        Battle( luaScript );            // NEW
    }
}

void BattleSystem::AddTextLine( TextType textType )
{
    string strLine;

    if ( textType == DIE )
    {
        strLine = target; //?
        strLine += " died! ";
    }
    else if ( textType == ATTACK )
    {
        strLine = turn;
        strLine += " attacks!";
    }
    else if ( textType == RUN )
    {
        strLine = turn;
        strLine += " ran away! ";
    }
    else if ( textType == TURN )
    {
        strLine = turn;
        strLine += "'s turn";
    }
    else if ( textType == USE_ROCK )
    {
        strLine = turn;
        strLine += " threw rock!";
    }
    else if ( textType == STRING )
    {
//        cout<<"string"<<endl;       //temp
    }
    else
    {
//        cout<<"categoryless"<<endl;     //temp
    }

    AddTextLine( strLine );
}

void BattleSystem::AddTextLine( string text )
{
    DebugOutput( "AddTextLine() > Display ", text );
    textLine[currLine] = text;
    currLine++;
}

void BattleSystem::AddTextLine( TextType textType, int damage )
{
    DebugOutput( "AddTextLineDamage()" );
    string strLine;
    char buff[16];

    strLine = turn;
    strLine += " hits ";
    strLine += target;
    strLine += " for ";
    itoa( damage, buff, 10 );
    strLine += buff;
    strLine += " damage!";

    DebugOutput( "AddTextLineDamage() > Display", strLine );

    AddTextLine( strLine );
}

void BattleSystem::AddTextLineStatic( string text )
{
    textLine[currLine] = text;
}

void BattleSystem::Battle( LuaManager *luaScript )
{
     if ( !awaitingInput )
    {
        string result, action, substring, buffer;
        int cutoff;

        luaScript->CombatSystem( enemy.name.c_str() );

        if( active )
        {
            result = luaScript->CombatResults();
            DebugOutput( "Battle > Lua Input", result );

            if ( result[0] != '=' )
            {
                if ( result.compare( 0, 6, str.turn ) == 0 )    // TURN::
                {
//                    cout<<">> TURN::"<<endl;
                    cutoff = 6;
                    substring = result.substr( cutoff, result.size() - cutoff );
                    turn = substring;
                    AddTextLine( TURN );
                }
                else if ( result.compare( 0, 8, str.action ) == 0 ) // ACTION::
                {
//                    cout<<">> ACTION::"<<endl;
                    cutoff = 8;
                    substring = result.substr( cutoff, result.size() - cutoff );
                }
                else if ( result.compare( 0, 10, str.itemused ) == 0 )
                {
                    cutoff = 10;
                    substring = result.substr( cutoff, result.size() - cutoff );
//                    cout<<">> ITEMUSED::"<<endl;
                    if ( substring == "Rock" )
                    {
                        AddTextLine( USE_ROCK );
                    }
                }
                else if ( result.compare( 0, 8, str.amount ) == 0 )              // AMOUNT::
                {
                    cout<<">> AMOUNT::"<<endl;
                    cutoff = 8;
                    substring = result.substr( cutoff, result.size() - cutoff );
                    int hitDamage;
                    hitDamage = atoi( substring.c_str() );
//                    cout<<"damage"<<endl;

                    cout<<"AddTextLine(ATTACK, damage)"<<endl;
                    AddTextLine( ATTACK, hitDamage );
                }
                else if ( result.compare( 0, 11, str.animation ) == 0 )     // ANIMATION::
                {
//                    cout<<">> ANIMATION::"<<endl;
                    cutoff = 11;
                    substring = result.substr( cutoff, result.size() - cutoff );
                    if ( substring == "HIT" )
                    {
                    }
                    else if ( substring == "DAMAGE" )
                    {
                    }
                    else if ( substring == "DIE" )
                    {
                        active = false;
                    }
                    else if ( substring == "RUN" )
                    {
                        AddTextLine( RUN );

                        active = false;
                    }
                    else if ( substring == "RUNFAIL" )
                    {
                        AddTextLine( "Failed to run away!" );
                    }
                }
                else if ( result.compare( 0, 4, str.by ) == 0 )             // BY::
                {
                    cout<<">> BY::"<<endl;
                    cutoff = 4;
                    substring = result.substr( cutoff, result.size() - cutoff );
                    turn = substring;
                }
                else if ( result.compare( 0, 8, str.combat ) == 0 )         // COMBAT::
                {
//                    cout<<">> COMBAT::"<<endl;
                    cutoff = 8;
                    substring = result.substr( cutoff, result.size() - cutoff );
                    /* CONTINUE, INPUT, or END */
                    if ( substring == "CONTINUE" )
                    {
                        luaScript->CombatSystem( enemy.name.c_str() );
                    }
                    else if ( substring == "INPUT" )
                    {
                        // Do nothing until enemy input
                        AddTextLine( "Enter your command" );
                        awaitingInput = true;
                    }
                    else if ( substring == "END" )
                    {
                        active = false;
                    }
                }
                else if ( result.compare( 0, 8, str.target ) == 0 )         // TARGET::
                {
//                    cout<<">> TARGET::"<<endl;
                    cutoff = 8;
                    substring = result.substr( cutoff, result.size() - cutoff );
                    target = substring;
                }
            }
        }

        if ( active == false )
        {
            luaScript->CombatSysClean();
            buffer = "Press ESC to close";
            textLine[currLine] = buffer;
        }
    }//if ( awaitingInput )

    UpdateHealth( luaScript );
}

void BattleSystem::Draw( SDL_Surface *destination, ImageManager *img )
{
    SDL_Color white = {255, 255, 255};
    int size = 14;
    int x = 200;
    int y = 400;

    if ( currLine >= LINEAMT )
    {
        currLine = 0;
        x += 10;
    }
    for ( int i=0; i<LINEAMT; i++ )
    {
        if ( textLine[i] != "" )
        {
            DrawText( destination, textLine[i], x, y, 0, 0, size, white );
            y += size;
        }
        if ( (i+1) % (380/size) == 0 )
        {
            x += 200;
            y = 0;
        }
    }

    int tempX = 8, tempY = 388;
    char buff[16];
    string tempStr;

    //Draw bottom area - battle stats
    //PLAYER
    size = 20;
    tempStr = player.name;
    tempStr += ": ";
    itoa( player.HP, buff, 10 );
    tempStr += buff;
    tempStr += '/';
    itoa( player.maxHP, buff, 10 );
    tempStr += buff;
    DrawText( destination, tempStr, tempX, tempY, 0, 0, size, white );

    //Commands
    size = 14;
    tempY = 416;
    tempX = 16;
    DrawText( destination, "A - Attack", tempX, tempY, 0, 0, size, white );
    tempY += 32;
    DrawText( destination, "I - Item", tempX, tempY, 0, 0, size, white );
    tempY = 416;
    tempX += 100;
    DrawText( destination, "R - Run", tempX, tempY, 0, 0, size, white );
    tempY += 32;
    DrawText( destination, "W - Wait", tempX, tempY, 0, 0, size, white );

    //Draw enemy on screen
    DrawEnemyGraphic( destination, img );
}

void BattleSystem::DrawEnemyGraphic( SDL_Surface *destination, ImageManager *img )
{
    SDL_BlitSurface( img->enemyBattle[ enemy.battleImage ], NULL, destination, &enemyGraphicCoord );
}



