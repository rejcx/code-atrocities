#pragma once
#include "SDL/SDL.h"
#include "BasicObject.h"


class Tile : public BasicObject
{
    public:
        bool solid;
        int imgX, imgY; // Tileset x and y coordinates.
        Tile()
        {
            coord.x = coord.y = imgX = imgY = 0;
            coord.w = coord.h = 32;

            colRegion = coord;
            solid = false;
        }
};
