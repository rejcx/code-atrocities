#pragma once
#include "SDL/SDL.h"
#include <iostream>
#include <string>
#include "LuaManager.h"
#include "Character.h"
#include "Text.h"
#include "ImageManager.h"
using namespace std;

const int LINEAMT = 20; // screenHeight / fontHeight
const int LINESHOWN = 5;

enum TextType { STRING, TURN, ATTACK, DIE, RUN, USE_ROCK, DAMAGED };

struct StringPrefix
{
    string combat;
    string animation;
    string by;
    string amount;
    string target;
    string turn;
    string action;
    string itemused;
};

struct BattleEnemy
{
    string name, idName;
    int HP, maxHP;
    int battleImage;
};

struct BattlePlayer
{
    string name;
    int HP, maxHP;
};

class BattleSystem
{
    private:
        BattleEnemy enemy;
        BattlePlayer player;
        StringPrefix str;
        bool active;
        string target, turn;
        string textLine[LINEAMT];
        int currLine;
        bool awaitingInput;
        SDL_Rect enemyGraphicCoord;

    public:
        BattleSystem();
        void SetupBattle( string npcId, LuaManager *luaScript, Character *player );
        void Battle( LuaManager *luaScript );
        bool Active() { return active; }
        void Draw( SDL_Surface *destination, ImageManager *img );
        void UpdateHealth( LuaManager *luaScript );
        void GetBattleCommand( LuaManager *luaScript, string action );
        void AddTextLine( TextType textType );
        void AddTextLine( string text );
        void AddTextLine( TextType textType, int damage );
        void AddTextLineStatic( string text );
        void DrawEnemyGraphic( SDL_Surface *destination, ImageManager *img );
};

