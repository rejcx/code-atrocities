#pragma once
#include "Game.h"
#include "Character.h"
#include "LevelManager.h"
#include "Dialog.h"
#include "SoundManager.h"
#include "MenuManager.h"
#include "BattleSystem.h"

#define BTNAMT 6

class Input
{
	private:
		Uint8 *keys;
		int mousex, mousey;
		bool mouseButtons[BTNAMT];
		float keyTimer;
	public:
		Input();
		~Input();
		void Refresh();
		//int GetMouseX();
		//int GetMouseY();
		bool GetKey(int);
		//bool GetMouseButton(int);
		void HandleEventPlz( Game *game, Character *Player, LevelManager *lev, NpcManager *npc, Dialog *dialogBox, LuaManager *luaScript, SoundManager *sound, MenuManager *menu, BattleSystem *battleSys );
};

