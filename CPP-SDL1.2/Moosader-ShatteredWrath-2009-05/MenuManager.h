#pragma once
#include "SDL/SDL.h"
#include "ImageManager.h"
#include "Text.h"
#include "Menu.h"
#include <string>
using namespace std;

const int MENUAMT = 5;

class MenuManager
{
    private:
        bool active;
        int currentMenu;
        Menu menu[MENUAMT];
    public:
        MenuManager();
        void Draw( SDL_Surface *destination, ImageManager *img );
        void Setup();
        void Draw( SDL_Surface *destination, SDL_Surface *menugfx );
};

