#include "Text.h"

void DrawText( SDL_Surface *destination, string msg, float x, float y, float offsetX, float offsetY, int size, SDL_Color color )
{
    SDL_Color shadow;
    shadow.r = shadow.g = shadow.b = 0;

    TTF_Font *font;
    font = TTF_OpenFont( "data/georgia.ttf", size );
    if ( !font )
        cerr<<"Error loading georgia.ttf"<<endl;

    SDL_Rect coordinates;
    coordinates.x = (int)(x - offsetX);
    coordinates.y = (int)(y - offsetY);

    SDL_Rect shadowOffset;
    shadowOffset.x = coordinates.x - 1;    shadowOffset.y = coordinates.y - 1;

    SDL_Surface *message = NULL;
    SDL_Surface *shadowtxt = NULL;
    message = TTF_RenderText_Solid( font, msg.c_str(), color );
    shadowtxt = TTF_RenderText_Solid( font, msg.c_str(), shadow );


    SDL_BlitSurface( shadowtxt, NULL, destination, &shadowOffset );
    shadowOffset.x = coordinates.x - 2;    shadowOffset.y = coordinates.y - 2;
    SDL_BlitSurface( shadowtxt, NULL, destination, &shadowOffset );
    SDL_BlitSurface( message, NULL, destination, &coordinates );


    TTF_CloseFont( font );
    SDL_FreeSurface( message );
    SDL_FreeSurface( shadowtxt );
}

void DrawText( SDL_Surface *destination, string msg, float x, float y, float offsetX, float offsetY, int size, int r, int g, int b )
{
    SDL_Color color;
    color.r = r;
    color.g = g;
    color.b = b;
    DrawText( destination, msg, x, y, offsetX, offsetY, size, color );
}

void DebugOutput( string message )
{
    bool DEBUG = true;
    if ( DEBUG )
        cout<<message<<endl;
}

void DebugOutput( string message, int index )
{
    char tempBuf[5];
    itoa( index, tempBuf, 10 );
    message += ": ";
    message += tempBuf;
    DebugOutput( message );
}

void DebugOutput( string type, string value )
{
    type += ": ";
    type += value;
    DebugOutput( type );
}
