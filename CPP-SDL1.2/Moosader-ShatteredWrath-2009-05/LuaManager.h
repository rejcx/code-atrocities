#pragma once
#include <string>
#include <iostream>
#include "Text.h"

using namespace std;

extern "C"
{
    #include "lua.h"
    #include "lualib.h"
    #include "lauxlib.h"
}
using namespace std;

class LuaManager
{
    private:
        lua_State *VM;
    public:
        LuaManager();
        ~LuaManager();
        string GetInfoStr( const char *npc, string info );
        int GetInfoInt( const char *npc, string info );
        int GetPlayerInfo( string info );
        void AddUniqueNpc( const char*, const char* );
        void CombatSysClean();
        string CombatResults();
        void CombatSystemInput( string, const char* );
        void CombatSystem( const char* );
        string GetEquippedStatsInfoStr(string stat);
        string GetItemInfoStr(const char *item, string info);
        string GetEquippedInfoStr(const char *location, string info);
        string GetCutSceneStr(string name);
        void EquipItem(const char *item);
        string GetInitialItemsStr();
        string CheckForCutsceneStr( const char *typeofobject, string objectname );

        struct LuaSaveGameDataType
        {
            int count;
	        int getinitialitemscounter;
	        int flipper;
	        int sammichcount;
	        int flsammichcount;
	        int gbsammichcount;
	        string poesammichtype;
	        //playerequipped
            string head;
            string chest;
            string arms;
            string legs;
            string feet;
            string hands;
	        //playerequippedvals
            int health;
            int maxhealth;
            int attack;
            int defense;
            int damage;
            int armor;
        };
        LuaSaveGameDataType LuaSaveGameData;
        void LuaSaveGame();
        void LuaLoadGame();
        void PlayCutscene( string name );
        string GetCutscene( string name );
        void UseItem(string name, string target);
};


