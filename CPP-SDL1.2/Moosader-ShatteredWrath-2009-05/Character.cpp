#include "Character.h"

Character::Character()
{
    // basic setup
    X ( 8*32 );
    Y ( 10*32 );
    W ( 32 );
    H ( 48 );
    frame = IDLE;
    speed = 2.0f;
    direction = RIGHT;
    action = WALKING;
    FRAMEMAX = 4;
    hp = 100;
    name = "marcus";
//    txtName.Setup( PLAYER );
//    txtMessage.Setup( DIALOG );
    run = false;
    enemyNPC = false;
    debug = false;
}

Character::~Character()
{
}

void Character::TakeDamage( int amt )
{
    hp -= amt;
    if ( hp < 0 )
        hp = 0;
}

void Character::Draw( SDL_Surface *destination, SDL_Surface *source, int offsetX, int offsetY )
{
    /*
        clipCoord is the part of the bitmap it's drawing from;
        it is the frame multiplied by the width (frame 1, 2, 3, etc. at pixel 1*32, 2*32...)

        tileCoord is where on the screen the character will be drawn, which is
        their x coordinate subtracted by the screen offset. (generally will
        stay in the middle of the screen, unless at the edge of the map).

        Not required, but there to sort of self-document and keep the blitting code clean.
    */
    SDL_Rect clipCoord;
    SDL_Rect tileCoord;
    clipCoord.w = W();
    clipCoord.h = H();
    tileCoord.w = W();
    tileCoord.h = H();

    clipCoord.x = (int)frame * W();
    clipCoord.y = (int)direction * H();
    tileCoord.x = (int)(X() - offsetX);
    tileCoord.y = (int)(Y() - offsetY);

    //Draw name above head
    SDL_Color col = {255, 75, 255};
    if ( enemyNPC ) { col.b = 75; }
    else { col.r = 75; }

    string temp;
    if ( debug && !enemyNPC )
        temp = "GOD";
    else
        temp = name;

    DrawText( destination, temp.c_str(), (X() + W()/2)-(name.size()*3), Y() - H()/3, offsetX, offsetY, 12, col );

    if ( debug )
    {
        //Temp draw collision region
        SetupCollisionRegion();
        SDL_Rect tempRegion;
        tempRegion = colRegion;
        tempRegion.x -= offsetX;
        tempRegion.y -= offsetY;
        SDL_FillRect( destination, &tempRegion, SDL_MapRGB( destination->format, 0, 255, 100 ) );
    }

    SDL_BlitSurface( source, &clipCoord, destination, &tileCoord );
}

void Character::Correct( Direction dir )
{
    float tempSpeed = speed;
    if ( run )
        tempSpeed += 5;
    if ( dir == LEFT )
    {
        coord.x = (int)(coord.x - tempSpeed);
    }
    else if ( dir == RIGHT )
    {
        coord.x = (int)(coord.x + tempSpeed);
    }

    else if ( dir == UP )
    {
        coord.y = (int)(coord.y - tempSpeed);
    }
    else if ( dir == DOWN )
    {
        coord.y = (int)(coord.y + tempSpeed);
    }
}

void Character::Move( Direction dir )
{
    direction = dir;
    IncrementFrame();
    Correct( dir );
}

void Character::IncrementFrame()
{
    /*
    Frame is a float, but when the player is outputted we use
    (int)frame.  This way, the player's frame isn't incremented by 1
    every cycle, which would make him walk way too fast.
    */
    frame += 0.15f;
    if ( frame >= FRAMEMAX )
        frame = 0.0f;
}

void Character::SetupCollisionRegion()
{
    colRegion.x = coord.x+8;
    colRegion.y = coord.y + coord.h - 16;
    colRegion.w = coord.w-16;
    colRegion.h = 16;
}

/*
FRIEND FUNCTIONS
*/

bool IsCollision( Character *chara, Level *level )
{
    chara->SetupCollisionRegion();
    int origX = chara->X(), origY = chara->Y();
    int x, y;
    int mapWidth = level->Width();
    int mapHeight = level->Height();

    // Temporary files (the four surrounding the player)
    // To keep the code easier to read.
    Tile temp[4];
    bool solid[4];

//    cout<<"player x "<<chara->X()<<endl;
//    cout<<"player y "<<chara->Y()<<endl<<endl;

    x = ((origX / TILEWH) % (mapWidth * TILEWH));
    y = (int)(origY / TILEWH) * mapWidth;
    temp[0] = level->collideLayer[x+y];
    solid[0] = level->collideLayer[x+y].solid;

    x = (((origX+32) / TILEWH) % (mapWidth * TILEWH));
    y = (int)(origY / TILEWH) * mapWidth;
    temp[1] = level->collideLayer[x+y];
    solid[1] = level->collideLayer[x+y].solid;

    x = (((origX+32) / TILEWH) % (mapWidth * TILEWH));
    y = (int)((origY+32) / TILEWH) * mapWidth;
    temp[2] = level->collideLayer[x+y];
    solid[2] = level->collideLayer[x+y].solid;

    x = (((origX) / TILEWH) % (mapWidth * TILEWH));
    y = (int)((origY+32) / TILEWH) * mapWidth;
    temp[3] = level->collideLayer[x+y];
    solid[3] = level->collideLayer[x+y].solid;

    for ( int i=0; i<4; i++ )
    {
        if ( solid[i] == true && IsCollision( chara, &temp[i] ) )
        {
            return true;
        }
        else
        {
        }
    }

    return false;
}

