#pragma once
#include <iostream>
#include <string>
#include "LuaManager.h"
using namespace std;

class InventoryClass
{
    private:
        struct InventoryItemType
        {
            string name;
            int quantity;
            string type;
        };
        InventoryItemType Inventory[100];
        int ItemCount;
    public:
        InventoryClass(){ItemCount = 0;};
        void AddItem(string name, LuaManager *luaScript , int num);
        void AddItem(string name, LuaManager *luaScript);
        void PrintInventory(ostream &out, bool printinvisible);
        void PrintInventoryDetails(ostream &out, bool printinvisible);
        int SearchInventory(string name);
        void RemoveItem(string name, int num);
        bool UseItemNonCombat(string name, string target, LuaManager *luaScript);
};