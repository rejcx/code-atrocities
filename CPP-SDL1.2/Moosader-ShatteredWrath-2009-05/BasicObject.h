#pragma once
#include "SDL/SDL.h"

/*
    Game tiles, items, npcs, and the player all inherit from this
    class.  This is to make collision detection easier.
*/

class BasicObject
{
    public:
        SDL_Rect coord;
        SDL_Rect colRegion;
        int X();
        void X( int val );
        int Y();
        void Y( int val );
        int W();
        void W( int val );
        int H();
        void H( int val );
        int X1();
        void X1( int val );
        int X2();
        void X2( int val );
        int Y1();
        void Y1( int val );
        int Y2();
        void Y2( int val );
        void Setup( int tx, int ty );
        void Setup( int tx, int ty, int tw, int th );
        BasicObject();
        BasicObject( int tx, int ty );
        BasicObject( int tx, int ty, int tw, int th );
        friend bool IsCollision( BasicObject*, BasicObject* );
        void SetupCollisionRegion()
        {
            colRegion = coord;
        }
};

