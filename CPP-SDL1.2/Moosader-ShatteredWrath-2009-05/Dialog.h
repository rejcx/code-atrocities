#pragma once
#include "SDL/SDL.h"
#include "Text.h"
#include "Character.h"
#include <string>
#include <iostream>
#include <fstream>
using namespace std;

class Dialog
{
    protected:
        bool visible;
        /* Window coordinates */
        SDL_Rect bgCoord;
        string message;
        SDL_Rect cursorCoord;
    public:
        Dialog();
        ~Dialog();

        void Draw( SDL_Surface *destination, SDL_Surface *menugfx );
        void DrawGradient( SDL_Surface *destination, SDL_Surface *menugfx );
        void DrawBorder( SDL_Surface *destination, SDL_Surface *menugfx );
        void SetCurrentDialog( string val );
        void DrawMessage( SDL_Surface *destination );
        void ShowDialog( bool val ) { visible = val; }
        bool DialogShown() { return visible; }
};


