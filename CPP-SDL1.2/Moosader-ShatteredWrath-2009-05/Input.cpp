#include "SDL/SDL.h"
#include "SDL/SDL_opengl.h"
#include "Input.h"

Input::Input()
{
	keys = NULL;
	Refresh();
	keyTimer = 0;
}

Input::~Input()
{
}

void Input::Refresh()
{
	SDL_PumpEvents();
	keys = SDL_GetKeyState(NULL);
	SDL_GetMouseState(&mousex, &mousey);
	for (int i=0; i<BTNAMT; i++)
	{
		SDL_GetMouseState(NULL, NULL) &SDL_BUTTON(i);
	}
}

bool Input::GetKey(int key)
{
	return keys[key];
}

void Input::HandleEventPlz( Game *game, Character *player, LevelManager *lev, NpcManager *npc, Dialog *dialogBox, LuaManager *luaScript, SoundManager *sound, MenuManager *menu, BattleSystem *battleSys )
{
    /*
        TEMP DEBUG
                            */
    if ( GetKey( SDLK_b ) && keyTimer <= 0 )        //enter battle
    {
        battleSys->SetupBattle( "orc", luaScript, player );
        game->State( BATTLE );
        keyTimer = 10;
    }
    else if ( game->State() == BATTLE && GetKey( 27 ) && keyTimer <= 0 )   //exit battle
    {
        game->State( INGAME );
        keyTimer = 10;
    }

    if ( GetKey( SDLK_1 ) && keyTimer <= 0 )         //enable clipping mode
    {
        if ( player->Debug() )
            player->Debug( false );
        else
            player->Debug( true );

        keyTimer = 10;
    }
    if ( GetKey( SDLK_9 ) && keyTimer <= 0 )            //go to next map
    {
        lev->NextLevel();
        sound->StartSong( lev->CurrentLevel().songID );
        keyTimer = 10;
    }
    else if ( GetKey( SDLK_0 ) && keyTimer <= 0 )       //go to next map
    {
        lev->PrevLevel();
        sound->StartSong( lev->CurrentLevel().songID );
        keyTimer = 10;
    }

    /*
        BATTLE COMMANDS
                            */
    if ( game->State() == BATTLE )
    {
        if ( GetKey( SDLK_a ) )         //attack
        {
            battleSys->GetBattleCommand( luaScript, "ATTACK" );
        }
        else if ( GetKey( SDLK_w ) )      //wait
        {
            battleSys->GetBattleCommand( luaScript, "WAIT" );
        }
        else if ( GetKey( SDLK_i ) )     //item
        {
            battleSys->GetBattleCommand( luaScript, "ITEM" );
        }
        else if ( GetKey( SDLK_r ) )     //run
        {
            battleSys->GetBattleCommand( luaScript, "RUN" );
        }
        else if ( GetKey( 27 ) )
        {
            game->State( INGAME );
        }
    } //if ( game->State() == BATTLE )

    /*
        UNIVERSAL COMMANDS
                            */
    if ( GetKey( SDLK_F4 ) || GetKey( SDLK_q ) )    // quit game
    {
        game->Done( true );
    }
    if ( GetKey( SDLK_F6 ) )                        // toggle fullscreen
    {
        game->ToggleFullscreen();
    }

    /*
        TITLE SCREEN COMMANDS
                                */
    if ( game->State() == TITLE )
    {
        if ( GetKey( SDLK_RETURN ) )
        {
            game->State( INGAME );
        }
    } //if ( game->State() == TITLE )

    /*
        MENU COMMANDS
                        */
    else if ( game->State() == MENU )
    {
        if ( GetKey( 27 ) && keyTimer <= 0 )     //ESC
        {
            //back to game
            Mix_PlayChannel( -1, sound->errorSound, 0 );  //channel (-1 = first unused one), chunk (sample), loop (-1 = infinite)
            game->State( INGAME );
            keyTimer = 10;
        }
        if ( GetKey( SDLK_UP ) && keyTimer <= 0 )
        {
            //Navigate through options
//            menu->MoveCursor( UP );
            keyTimer = 10;
            Mix_PlayChannel( -1, sound->cursorSound, 0 );  //channel (-1 = first unused one), chunk (sample), loop (-1 = infinite)
        }
        else if ( GetKey( SDLK_DOWN ) && keyTimer <= 0 )
        {
            //Navigate through options
//            menu->MoveCursor( DOWN );
            keyTimer = 10;
            Mix_PlayChannel( -1, sound->cursorSound, 0 );  //channel (-1 = first unused one), chunk (sample), loop (-1 = infinite)
        }
        else if ( GetKey( SDLK_RETURN ) && keyTimer <= 0 )
        {
            //Select option
            keyTimer = 10;
            Mix_PlayChannel( -1, sound->confirmSound, 0 );  //channel (-1 = first unused one), chunk (sample), loop (-1 = infinite)

            /* to be moved into other area */
//            if ( menu->CursorY() == 440 )
//            {
//                game->
//                Done( true );
//            }
        }
        else if ( GetKey( 27 ) && keyTimer <= 0 )
        {
            //Go back an option
            keyTimer = 10;
        }
    } //if ( game->State() == MENU )

    /*
        INGAME COMMANDS
                        */
    else if ( game->State() == INGAME )
    {
        //SYSTEM KEYS
        if ( GetKey( 27 ) && keyTimer <= 0 )     //ESC
        {
            //play noise
            Mix_PlayChannel( -1, sound->confirmSound, 0 );  //channel (-1 = first unused one), chunk (sample), loop (-1 = infinite)
            game->State( MENU );
            keyTimer = 10;
//            menu->CursorY( 40 );
        }
        //PLAYER KEYS
        if ( GetKey( SDLK_RSHIFT ) || GetKey( SDLK_LSHIFT ) )   // Run
        {
            player->Run( true );
        }
        else
        {
            player->Run( false );
        }
        if ( dialogBox->DialogShown() == false )        // can't move while talking.
        {
            if ( GetKey( SDLK_UP ) )  // Move up
            {
                player->Move( UP );
                if ( player->Debug() == false && IsCollision( player, &lev->CurrentLevel() ) )
                    player->Correct( DOWN );
            }
            else if ( GetKey( SDLK_DOWN ) )   // Move down
            {
                player->Move( DOWN );
                if ( player->Debug() == false && IsCollision( player, &lev->CurrentLevel() ) )
                    player->Correct( UP );
            }
            else if ( GetKey( SDLK_LEFT ) )    // Move left
            {
                player->Move( LEFT );
                if ( player->Debug() == false && IsCollision( player, &lev->CurrentLevel() ) )
                    player->Correct( RIGHT );
            }
            else if ( GetKey( SDLK_RIGHT ) )  // Move right
            {
                player->Move( RIGHT );
                if ( player->Debug() == false && IsCollision( player, &lev->CurrentLevel() ) )
                    player->Correct( LEFT );
            }

            // Check for a warp point
//            lev->CheckForWarpTile( luaScript, player->X(), player->Y() );
        }

        if ( GetKey( SDLK_RETURN ) )
        {
            if ( keyTimer <= 0 )
            {
                if ( dialogBox->DialogShown() == false )
                {
                    // Open dialog box if there's something to say
                    string message = npc->TryInteraction( player, luaScript );
                    // interact with NPC or object
                    if ( message != "NULL" )
                    {
                        Mix_PlayChannel( -1, sound->talkSound, 0 );  //channel (-1 = first unused one), chunk (sample), loop (-1 = infinite)
                        dialogBox->SetCurrentDialog( message );
                        keyTimer = 10;
                        dialogBox->ShowDialog( true );
                    }
                }
                else
                {
                    // Close dialog box
                    dialogBox->ShowDialog( false );
                    keyTimer = 10;
                }
            }
        }
    } //if ( game->State() == INGAME )

    if ( keyTimer > 0 )
        keyTimer -= 1;
}



