#include "Level.h"

Level::Level()
{
    mapWidth = mapHeight = 0;
    layers = 0;
    songID = 0;
    index = -1;
}

void Level::CheckAndStartCutscene( LuaManager *luaScript )
{
    DebugOutput( "CheckAndStartCutscene()" );
    if ( cutscene[0].played == false )
    {
        luaScript->PlayCutscene( cutscene[0].name );
    }
}

void Level::LoadMap( string filename )
{
    DebugOutput( "LoadMap() - ", index );
    cout<<"Loading map "<<filename<<endl;
	ifstream infile;
	infile.open( filename.c_str() );

	name = filename;
	DebugOutput( "LoadMap() - name", filename );
	if ( infile.bad() )
	{
		cerr<<"Error loading game map "<<filename<<endl;
	}

	for ( int i=0; i<5; i++ )
	{
	    cutscene[i].name = "NONE";
	    cutscene[i].played = false;
    }

//	 Cutscene name thingy thigny

	if ( index == 0 )           // Bank
	{
	    cutscene[0].name = "bankintroday";
	    cutscene[1].name = "bankmencounter";
	}
	else if ( index == 1 )      // Bank Roof
	{
	    cutscene[0].name = "bankroof";
	}
	else if ( index == 2 )      // Bank Vault
	{
	}
	else if ( index == 3 )      // Catacomb
	{
	    cutscene[0].name = "finalbattle";
	    cutscene[1].name = "epilogue";
	}
	else if ( index == 4 )      // Church
	{
	}
	else if ( index == 5 )      // Factory Base
	{
	}
	else if ( index == 6 )      // Factory Main
	{
	    cutscene[0].name = "factoryfloorday";
	    cutscene[1].name = "factoryfloordaynearend";
	}
	else if ( index == 7 )      // Factory Office
	{
	    cutscene[0].name = "factoryoffice";
	}
	else if ( index == 8 )      // House
	{
	    cutscene[0].name = "intro";
	}
	else if ( index == 9 )      // Pub
	{
	    cutscene[0].name = "rigleyspubintro";
	    cutscene[1].name = "quest1complete";
	    cutscene[2].name = "quest2complete";
	}
	else if ( index == 10 )      // Slum
	{
	    cutscene[0].name = "slumintro";
	    cutscene[1].name = "fredrickpubfirsttalk";
	}

    //free tiles incase they have something stored
    FreeTiles();

	string temp;
	infile>>temp;       //w
    infile>>mapWidth;   //width
    infile>>temp;       //h
    infile>>mapHeight;  //height
    infile>>temp;       //bgimage
    infile>>temp;       //background, none in this game.

    layers = 3;

    warpAmt = 0;

    int t; // temp

    // Create map tile array
    bottomLayer = new Tile[mapWidth*mapHeight];
    middleLayer = new Tile[mapWidth*mapHeight];
    topLayer = new Tile[mapWidth*mapHeight];
    collideLayer = new Tile[mapWidth*mapHeight];

    //BOTTOM LAYER
    for ( int i=0; i<mapWidth*mapHeight; i++ )
    {
        infile>>temp;
        //Basic filmstrip coordinates, world coordinates
        bottomLayer[i].imgX = atoi( temp.c_str() ) * TILEWH;
        bottomLayer[i].imgY = 0;
        bottomLayer[i].X( (i % mapWidth) * TILEWH );
        bottomLayer[i].Y( (int)(i / mapWidth) * TILEWH );
    }
    //MIDDLE LAYER
    for ( int i=0; i<mapWidth*mapHeight; i++ )
    {
        infile>>temp;
        //Basic filmstrip coordinates, world coordinates
        middleLayer[i].imgX = atoi( temp.c_str() ) * TILEWH;
        middleLayer[i].imgY = 0;
        middleLayer[i].X( (i % mapWidth) * TILEWH );
        middleLayer[i].Y( (int)(i / mapWidth) * TILEWH );
    }
    //TOP LAYER
    for ( int i=0; i<mapWidth*mapHeight; i++ )
    {
        infile>>temp;
        //Basic filmstrip coordinates, world coordinates
        topLayer[i].imgX = atoi( temp.c_str() ) * TILEWH;
        topLayer[i].imgY = 0;
        topLayer[i].X( (i % mapWidth) * TILEWH );
        topLayer[i].Y( (int)(i / mapWidth) * TILEWH );
    }
    //COLLISION LAYER
    for ( int i=0; i<mapWidth*mapHeight; i++ )
    {
        infile>>temp;
        //Basic filmstrip coordinates, world coordinates
        collideLayer[i].imgX = atoi( temp.c_str() ) * TILEWH;
        collideLayer[i].imgY = 0;
        collideLayer[i].X( (i % mapWidth) * TILEWH );
        collideLayer[i].Y( (int)(i / mapWidth) * TILEWH );

        //Look for solid tiles
        if ( collideLayer[i].imgX >= 1 )
            collideLayer[i].solid = true;
        else
            collideLayer[i].solid = false;

        collideLayer[i].SetupCollisionRegion();
    }

    // Load in extra info at end of file
    int loopMax = 1000, loopCtr = 0;
    bool doneLoading = false;
    while ( !doneLoading )
    {
        infile>>temp;
        loopCtr++;
//        cout<<loopCtr<<"\tLoaded\t"<<temp<<endl;

        if ( loopCtr >= loopMax )
        {
            cerr<<"Exited due to infinite loop in LoadMap"<<endl;
            cerr<<"Please make sure that map has 'END MAP' at the end of the file"<<endl;
            exit (1);
        }
        if ( temp == "END" )        // End Map
        {
            doneLoading = true;
        }
        else if ( temp == "bgsong" )
        {
            infile>>temp;       // song filename
//            cout<<"\tsong "<<temp<<endl;
            if ( temp == "apoc.ogg" )
                songID = 0;
            else if ( temp == "celest.ogg" )
                songID = 1;
            else if ( temp == "chess.ogg" )
                songID = 2;
            else if ( temp == "daydream.ogg" )
                songID = 3;
            else if ( temp == "dejavu.ogg" )
                songID = 4;
            else if ( temp == "closing.ogg" )
                songID = 5;
            else if ( temp == "glow.ogg" )
                songID = 6;
            else if ( temp == "walk.ogg" )
                songID = 7;
            else
                songID = -1;
        }
        else if ( temp == "end warp" )
        {
            warpAmt++;
//            cout<<"\twarpAmt = "<<warpAmt<<endl;
        }

        else if ( temp == "warpfrom_x" )
        {
            infile>>warpTile[warpAmt].locX;
//            cout<<"\tLoaded "<<warpTile[warpAmt].locX<<endl;
        }
        else if ( temp == "warpfrom_y" )
        {
            infile>>warpTile[warpAmt].locY;
//            cout<<"\tLoaded "<<warpTile[warpAmt].locY<<endl;
        }
        else if ( temp == "map" )
        {
            infile>>temp;
            int warp = -1;
            if ( temp == "bank" ) { warp = 0; }
            else if ( temp == "bank_roof" ) { warp = 1; }
            else if ( temp == "bank_vault" ) { warp = 2; }
            else if ( temp == "catacomb" ) { warp = 3; }
            else if ( temp == "church" ) { warp = 4; }
            else if ( temp == "fac_base" ) { warp = 5; }
            else if ( temp == "fac_main" ) { warp = 6; }
            else if ( temp == "fac_office" ) { warp = 7; }
            else if ( temp == "house" ) { warp = 8; }
            else if ( temp == "pub" ) { warp = 9; }
            else if ( temp == "slum" ) { warp = 10; }

            warpTile[warpAmt].warpToIndex = warp;
//            cout<<"\tLoaded "<<temp<<endl;
//            cout<<"\tSet warp to "<<warpTile[warpAmt].warpToIndex<<endl;
        }
        else if ( temp == "warpto_x" )
        {
            infile>>warpTile[warpAmt].warpToX;
//            cout<<"\tLoaded "<<warpTile[warpAmt].warpToX<<endl;
        }
        else if ( temp == "warpto_y" )
        {
            infile>>warpTile[warpAmt].warpToY;
//            cout<<"\tLoaded "<<warpTile[warpAmt].warpToY<<endl;
        }
    }
	infile.close();
}

void Level::Draw( SDL_Surface *buffer, SDL_Surface *tileset, float offsetX, float offsetY, int layer )
{
    DebugOutput( "Draw()" );
	SDL_Rect clipCoord;
	SDL_Rect tileCoord;

	clipCoord.w = TILEWH;
	clipCoord.h = TILEWH;
	tileCoord.w = TILEWH;
	tileCoord.h = TILEWH;

	int tempX, tempY;

	for ( int i=0; i<mapWidth*mapHeight; i++ )
    {
        if ( layer == BOTTOM )
        {
            tempX = bottomLayer[i].X();
            tempY = bottomLayer[i].Y();
            if ( (( tempX < offsetX + 640 )     // Don't draw tiles past the right side of the screen
                && ( tempX > offsetX - 32 )     // Don't draw tiles past the left side of the screen
                && ( tempY < offsetY + 480 )
                && ( tempY > offsetY - 32 ) )
                && ( bottomLayer[i].imgX != 0 ) )      //As long as tile isn't 0 (empty tile) draw...
            {
                //Draw
                clipCoord.x = bottomLayer[i].imgX;
                clipCoord.y = bottomLayer[i].imgY;
                tileCoord.x = (int)(bottomLayer[i].X() - offsetX);
                tileCoord.y = (int)(bottomLayer[i].Y() - offsetY);

                SDL_BlitSurface( tileset, &clipCoord, buffer, &tileCoord );
            }
        }
        else if ( layer == MIDDLE )
        {
            tempX = middleLayer[i].X();
            tempY = middleLayer[i].Y();
            if ( (( tempX < offsetX + 640 )     // Don't draw tiles past the right side of the screen
                && ( tempX > offsetX - 32 )     // Don't draw tiles past the left side of the screen
                && ( tempY < offsetY + 480 )
                && ( tempY > offsetY - 32 ) )
                && ( middleLayer[i].imgX != 0 ) )      //As long as tile isn't 0 (empty tile) draw...
            {
                //Draw
                clipCoord.x = middleLayer[i].imgX;
                clipCoord.y = middleLayer[i].imgY;
                tileCoord.x = (int)(middleLayer[i].X() - offsetX);
                tileCoord.y = (int)(middleLayer[i].Y() - offsetY);

                SDL_BlitSurface( tileset, &clipCoord, buffer, &tileCoord );
            }
        }
        else if ( layer == TOP )
        {
            tempX = topLayer[i].X();
            tempY = topLayer[i].Y();
            if ( (( tempX < offsetX + 640 )     // Don't draw tiles past the right side of the screen
                && ( tempX > offsetX - 32 )     // Don't draw tiles past the left side of the screen
                && ( tempY < offsetY + 480 )
                && ( tempY > offsetY - 32 ) )
                && ( topLayer[i].imgX != 0 ) )      //As long as tile isn't 0 (empty tile) draw...
            {
                //Draw
                clipCoord.x = topLayer[i].imgX;
                clipCoord.y = topLayer[i].imgY;
                tileCoord.x = (int)(topLayer[i].X() - offsetX);
                tileCoord.y = (int)(topLayer[i].Y() - offsetY);

                SDL_BlitSurface( tileset, &clipCoord, buffer, &tileCoord );
            }
        }
    }
}

void Level::DrawCollision( SDL_Surface *buffer, float offsetX, float offsetY, int x, int y )
{
    DebugOutput( "DrawCollision()" );
    SDL_Rect coordinates;

    //draw
    int tempX;
    int tempY;
    for ( int i=0; i<mapWidth*mapHeight; i++ )
    {
        tempX = collideLayer[i].X();
        tempY = collideLayer[i].Y();
        if ( (( tempX < offsetX + 640 )     // Don't draw tiles past the right side of the screen
            && ( tempX > offsetX - 32 )     // Don't draw tiles past the left side of the screen
            && ( tempY < offsetY + 480 )
            && ( tempY > offsetY - 32 ) )
            && ( collideLayer[i].imgX != 0 ) )      //As long as tile isn't 0 (empty tile) draw...
        {
            coordinates.x = (int)(collideLayer[i].X() - offsetX);
            coordinates.y = (int)(collideLayer[i].Y() - offsetY);
            coordinates.w = TILEWH;
            coordinates.h = TILEWH;
            SDL_FillRect( buffer, &coordinates, SDL_MapRGB( buffer->format, 255, 0, 0 ) );
        }
    }

    //Draw warps
    for ( int i=0; i<warpAmt; i++ )
    {
        coordinates.x = (int)(warpTile[i].locX - offsetX);
        coordinates.y = (int)(warpTile[i].locY - offsetY);
        coordinates.w = coordinates.h = 32;
        SDL_FillRect( buffer, &coordinates, SDL_MapRGB( buffer->format, 0, 0, 255 ) );
    }
}

void Level::DrawRegion( SDL_Surface *buffer, float offsetX, float offsetY, int x, int y )
{
    DebugOutput( "DrawRegion()" );
    // Draw a rectangle over the player's region, to show
    // where it checks for collision
    int origX = x, origY = y;

    x = y = 0;

    // Temporary files (the four surrounding the player)
    // To keep the code easier to read.
    Tile temp[4];
    bool solid[4];

    x = (origX / TILEWH) % (mapWidth * TILEWH);
//    y = (int)(origY / TILEWH) * (TILEWH);
    y = (int)(origY / TILEWH) * mapWidth;
    temp[0] = collideLayer[ x+y ];
    solid[0] = true;

    x = ((origX+32) / TILEWH) % (mapWidth * TILEWH);
    y = (int)(origY / TILEWH) * mapWidth;
    temp[1] = collideLayer[ x+y ];
    solid[1] = true;

    x = ((origX+32) / TILEWH) % (mapWidth * TILEWH);
    y = (int)((origY+32) / TILEWH) * mapWidth;
    temp[2] = collideLayer[ x+y ];
    solid[2] = true;

    x = ((origX) / TILEWH) % (mapWidth * TILEWH);
    y = (int)((origY+32) / TILEWH) * mapWidth;
    temp[3] = collideLayer[ x+y ];
    solid[3] = true;

    SDL_Rect coordinates;
    for ( int i=0; i<4; i++ )
    {
        coordinates.x = (int)(temp[i].X() - offsetX);
        coordinates.y = (int)(temp[i].Y() - offsetY);
        coordinates.w = TILEWH;
        coordinates.h = TILEWH;

        //draw
        SDL_FillRect( buffer, &coordinates, SDL_MapRGB( buffer->format, 255, 255, 0 ) );
    }
}

Level::~Level()
{
    FreeTiles();
}

void Level::FreeTiles()
{
    DebugOutput( "FreeTiles()" );
    if ( bottomLayer == NULL )
        delete [] bottomLayer;
    if ( middleLayer == NULL )
        delete [] middleLayer;
    if ( topLayer == NULL )
        delete [] topLayer;
}



