#include "LevelManager.h"

void LevelManager::LoadInLevels()
{
    for ( int i=0; i<LEVELAMT; i++ )
    {
        level[i].Index( i );
    }

    level[0].LoadMap( "data/levels/bank.map" );
    level[1].LoadMap( "data/levels/bank_roof.map" );
    level[2].LoadMap( "data/levels/bank_vault.map" );
    level[3].LoadMap( "data/levels/catacomb.map" );
    level[4].LoadMap( "data/levels/church.map" );
    level[5].LoadMap( "data/levels/fac_base.map" );
    level[6].LoadMap( "data/levels/fac_main.map" );
    level[7].LoadMap( "data/levels/fac_office.map" );
    level[8].LoadMap( "data/levels/house.map" );
    level[9].LoadMap( "data/levels/pub.map" );
    level[10].LoadMap( "data/levels/slum.map" );
}

LevelManager::LevelManager()
{
    LoadInLevels();
    current = 8;
    UpdateWarpCoords();
}

void LevelManager::NextLevel()
{
    current++;
    if ( current > LEVELAMT-1 )
        current = 0;
    DebugOutput( "NextLevel - ", current );
    DebugOutput( "NextLevel - ", level[current].Name() );
}

void LevelManager::PrevLevel()
{
    current--;
    if ( current < 0 )
        current = LEVELAMT-1;
    DebugOutput( "PrevLevel - ", current );
    DebugOutput( "PrevLevel - ", level[current].Name() );
}

void LevelManager::DrawLevelCollide( SDL_Surface *buffer, int offsetX, int offsetY, int x, int y )
{
    level[current].DrawCollision( buffer, offsetX, offsetY, x, y );
    level[current].DrawRegion( buffer, offsetX, offsetY, x, y );

    //Draw map ID
    string mapInfo;
    char tempBuf[5];
    mapInfo = "Map ID ";
    itoa( current, tempBuf, 10 );
    mapInfo += tempBuf;
    mapInfo += ", ";
    mapInfo += level[current].Name();

    DrawText( buffer, mapInfo, 0, 0, 0, 0, 14, 255, 255, 255 );
}

void LevelManager::DrawLevel( SDL_Surface *buffer, ImageManager *img, int offsetX, int offsetY, bool bottom )
{
    if ( bottom )
    {
        level[current].Draw( buffer, img->tileset, offsetX, offsetY, BOTTOM );
        level[current].Draw( buffer, img->tileset, offsetX, offsetY, MIDDLE );
    }
    else
    {
        level[current].Draw( buffer, img->tileset, offsetX, offsetY, TOP );
    }
}

void LevelManager::Warp( LuaManager *luaScript, int warpToIndex )
{
    cout<<"Warp to map "<<warpToIndex<<", ";
    cout<<level[warpToIndex].Name()<<endl;
    current = warpToIndex;

    //Reload warp coordinates
    for ( int i=0; i<5; i++ )
    {
        warpCoord[i].x = level[current].warpTile[i].locX;
        warpCoord[i].y = level[current].warpTile[i].locY;
    }

    level[current].CheckAndStartCutscene( luaScript );
}

void LevelManager::CheckForWarpTile( LuaManager *luaScript, int px, int py )
{
    BasicObject tile[5], player;
    tile[0].coord = warpCoord[0];
    tile[1].coord = warpCoord[1];
    tile[2].coord = warpCoord[2];
    tile[3].coord = warpCoord[3];
    tile[4].coord = warpCoord[4];

    player.coord.x = px;
    player.coord.y = py;
    for ( int i=0; i<5; i++ )
    {
        cout<<"\nTILE "<<i<<"\t"<<tile[i].coord.x<<", "<<tile[i].coord.y<<endl;
        cout<<"PLAYER \t"<<player.coord.x<<", "<<player.coord.y<<endl;
        if ( IsCollision( &tile[i], &player ) )
        {
            cout<<"COLLISION"<<endl;
            Warp( luaScript, i );
        }
    }
}

void LevelManager::UpdateWarpCoords()
{
    for ( int i=0; i<5; i++ )
    {
        warpCoord[i].x = level[current].warpTile[i].locX;
        warpCoord[i].y = level[current].warpTile[i].locY;
    }
}





