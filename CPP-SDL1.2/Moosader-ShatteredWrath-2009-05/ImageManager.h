#pragma once
#include <string>
#include <iostream> //for debugging
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
using namespace std;

SDL_Surface *LoadImage( string filename );

class ImageManager
{
    public:
        //Game images
        SDL_Surface *tileset;
        SDL_Surface *player;
        SDL_Surface *enemy[13];
        SDL_Surface *enemyBattle[13];
        SDL_Surface *title;
        SDL_Surface *menugfx;
        SDL_Surface *hudgfx;

        friend SDL_Surface *LoadImage( string filename );
        void LoadInGraphics();
        void DrawHud( SDL_Surface *destination );
        ImageManager();
        ~ImageManager();
};

