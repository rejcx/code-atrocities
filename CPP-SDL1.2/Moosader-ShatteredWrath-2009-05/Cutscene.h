#pragma once
#include "SDL/SDL.h"
#include "LuaManager.h"
#include "Text.h"
#include "Menu.h"
#include "Item.h"
#include "NpcManager.h"
#include "Character.h"
#include "Inventory.h"
#include <string>
using namespace std;

/*
Cutscene accesses the enemies and characters, as
well as the item/inventory class
*/

class Cutscene
{
    private:
    public:
        Cutscene();
//        void CutsceneRunner( LuaManager *luaScript, NpcManager *npcs, Character *player, Inventory *inv );
        string GetNextInstruction( LuaManager *luaScript );
};
