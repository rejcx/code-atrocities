#pragma once
#include "SDL/SDL.h"
#include "BasicObject.h"
#include <iostream>
#include <string>
#include "LuaManager.h"
#include "Text.h"
#include "Level.h"
using namespace std;

enum Direction { DOWN = 0, UP = 1, LEFT = 2, RIGHT = 3 };
enum Action { WALKING = 0, ATTACKING = 1 };

#define IDLE 1;

class Character : public BasicObject
{
    protected:
        int FRAMEMAX;
        Direction direction;
        Action action;
        float speed, frame;
        string name, message;
        bool debug;
        int hp;
        bool enemyNPC;
        bool run;
    public:
        Character();
        ~Character();
        void Draw( SDL_Surface *destination, SDL_Surface *source, int offsetX, int offsetY );
        void Move( Direction dir );
        void Correct( Direction dir );
        Direction Dir() { return direction; }
        void IncrementFrame();
        void Run( bool val ) { run = val; }
        int HP() { return hp; }
        void TakeDamage( int amt );
        string Name() { return name; }
        void Debug( bool val ) { debug = val; }
        bool Debug() { return debug; }
        void SetupCollisionRegion();
        int RegX() { return colRegion.x; }
        int RegY() { return colRegion.y; }

        friend bool IsCollision( Character *chara, Level *level );
};



