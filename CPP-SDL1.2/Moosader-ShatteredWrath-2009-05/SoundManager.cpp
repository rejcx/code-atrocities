#include "SoundManager.h"

SoundManager::SoundManager()
{
    confirmSound = Mix_LoadWAV( "data/confirm.wav" );
    if ( confirmSound == NULL ) { cerr<<"Error loading data/confirm.wav"<<endl; }

    errorSound = Mix_LoadWAV( "data/error.wav" );
    if ( errorSound == NULL ) { cerr<<"Error loading data/error.wav"<<endl; }

    cursorSound = Mix_LoadWAV( "data/cursor.wav" );
    if ( cursorSound == NULL ) { cerr<<"Error loading data/cursor.wav"<<endl; }

    talkSound = Mix_LoadWAV( "data/talk.wav" );
    if ( talkSound == NULL ) { cerr<<"Error loading data/talk.wav"<<endl; }

    test = Mix_LoadMUS( "data/music/apoc.ogg" );
    if ( test == NULL ) { cerr<<"Error loading data/music/apoc.ogg: "<<Mix_GetError()<<endl; }

    song[0] = Mix_LoadMUS( "data/music/apoc.ogg" );
    if ( song[0] == NULL ) { cerr<<"Error loading data/music/apoc.ogg: "<<Mix_GetError()<<endl; }

    song[1] = Mix_LoadMUS( "data/music/celest.ogg" );
    if ( song[1] == NULL ) { cerr<<"Error loading data/music/celest.ogg: "<<Mix_GetError()<<endl; }

    song[2] = Mix_LoadMUS( "data/music/chess.ogg" );
    if ( song[2] == NULL ) { cerr<<"Error loading data/music/chess.ogg: "<<Mix_GetError()<<endl; }

    song[3] = Mix_LoadMUS( "data/music/daydream.ogg" );
    if ( song[3] == NULL ) { cerr<<"Error loading data/music/daydream.ogg: "<<Mix_GetError()<<endl; }

    song[4] = Mix_LoadMUS( "data/music/dejavu.ogg" );
    if ( song[4] == NULL ) { cerr<<"Error loading data/music/dejavu.ogg: "<<Mix_GetError()<<endl; }

    song[5] = Mix_LoadMUS( "data/music/closing.ogg" );
    if ( song[5] == NULL ) { cerr<<"Error loading data/music/closing.ogg: "<<Mix_GetError()<<endl; }

    song[6] = Mix_LoadMUS( "data/music/glow.ogg" );
    if ( song[6] == NULL ) { cerr<<"Error loading data/music/glow.ogg: "<<Mix_GetError()<<endl; }

    song[7] = Mix_LoadMUS( "data/music/walk.ogg" );
    if ( song[7] == NULL ) { cerr<<"Error loading data/music/walk.ogg: "<<Mix_GetError()<<endl; }
}

void SoundManager::StartSong( int id )
{
    Mix_HaltMusic();
    SDL_Delay(10);
    Mix_PlayMusic(song[id], 0);
}

SoundManager::~SoundManager()
{
    Mix_FreeChunk ( confirmSound );
    Mix_FreeChunk ( errorSound );
    Mix_FreeChunk ( cursorSound );
    Mix_FreeChunk ( talkSound );

    Mix_HaltMusic();
    for ( int i=0; i<8; i++ )
    {
        Mix_FreeMusic( song[i] );
    }
}
