#include "LuaManager.h"

LuaManager::LuaManager()
{
    VM = lua_open();
    luaL_openlibs( VM );
    luaL_dofile( VM, "combatsystem.lua" );
}

LuaManager::~LuaManager()
{
    lua_close( VM );
}

void LuaManager::AddUniqueNpc( const char *uniqueID, const char *model )
{
    //PRE:  a char pointer with the name of the entity we are gathering information on,
    //      and a string containing the info we are requesting
    //POST: this function will return the information requested

    //get the function name from lua
    lua_getglobal( VM, "adduniquenpc" );

    //push the first argument (entity name)
    lua_pushstring( VM, uniqueID );

    //push the second argument (info to get)
    lua_pushstring( VM, model );

    //call the function, currently with 2 arguments and 1 result
    lua_call( VM, 2, 1 );
}

string LuaManager::GetInfoStr( const char *npc, string info )      // changed to const char so I could pass a string.c_str()
{
    //PRE:  a char pointer with the name of the entity we are gathering information on,
    //      and a string containing the info we are requesting
    //POST: this function will return the information requested

    //string to store the restults
    string results;

    //get the function name from lua
    lua_getglobal( VM, "getinfo" );

    //push the first argument (entity name)
    lua_pushstring( VM, npc );

    //push the second argument (info to get)
    lua_pushstring( VM, info.c_str() );

    //call the function, currently with 2 arguments and 1 result
    lua_call( VM, 2, 1 );

    //get the results from the stack (it's at the top of the stack, hence -1)
    results = (string)lua_tostring( VM, -1 );

    //clear that result off of the stack
    lua_pop( VM, 1 );

    return results;
}

int LuaManager::GetInfoInt( const char *npc, string info )
{
    //PRE:  a char pointer with the name of the entity we are gathering information on,
    //      and a string containing the info we are requesting
    //POST: this function will return the information requested

    //string to store the restults
    int results;

    //get the function name from lua
    lua_getglobal( VM, "getinfo" );

    //push the first argument (entity name)
    lua_pushstring( VM, npc );

    //push the second argument (info to get)
    lua_pushstring( VM, info.c_str() );

    //call the function, currently with 2 arguments and 1 result
    lua_call( VM, 2, 1 );

    //get the results from the stack (it's at the top of the stack, hence -1)
    results = (int)lua_tonumber( VM, -1) ;

    //clear that result off of the stack
    lua_pop( VM, 1 );

    return results;
}

int LuaManager::GetPlayerInfo( string info )
{
    int result;

    lua_getglobal( VM, "getplayerinfo" );
    lua_pushstring( VM, info.c_str() );
    lua_call( VM, 1, 1 );
    result = (int)lua_tonumber( VM, -1 );
    lua_pop( VM, 1 );

    return result;
}

void LuaManager::CombatSystem( const char* npc )
{
    //PRE:  a char pointer with the name of the entity we are fighting
    //POST: this function will do a call to combatsystem

    //get the function name from lua
    lua_getglobal( VM, "combatsystem" );

    //push the first argument (entity name)
    lua_pushstring( VM, npc );

    //call the function, currently with 1 arguments and 0 result
    lua_call( VM, 1, 0 );
}

void LuaManager::CombatSystemInput( string action, const char* npc )
{
    //get function name from lua
    lua_getglobal( VM, "combatsystemplayerinput" );

    //push first argument (action choosen)
    lua_pushstring( VM, action.c_str() );

    //push second argument (npc were fighting)
    lua_pushstring( VM, npc );

    //call the function with 2 arguments and 0 results)
    lua_call( VM, 2, 0 );
}

string LuaManager::CombatResults()
{
    string results;

    //get function name
    lua_getglobal( VM, "getcombatresults" );

    //call the function with 0 arguments and 1 results
    lua_call( VM, 0, 1 );

    //get the results from the stack (it's at the top of the stack, hence -1)
    results = (string)lua_tostring( VM, -1 );

    //clear that result off of the stack
    lua_pop( VM, 1 );

    return results;
}

void LuaManager::CombatSysClean()
{
    //get function name
    lua_getglobal( VM, "combatcleanup" );

    //call the function with 0 arguments and 0 results
    lua_call( VM, 0,0 );
}


string LuaManager::GetEquippedStatsInfoStr(string stat)      // changed to const char so I could pass a string.c_str()
{
    //PRE:  a string containing the stat we are requesting
    //POST: this function will return the statistic requested including bonuses from equipment

    //string to store the restults
    string results;

    //get the function name from lua
    lua_getglobal( VM, "getplayerequippedinfo" );

    //push the first argument (info to get)
    lua_pushstring( VM, stat.c_str() );

    //call the function, currently with 1 arguments and 1 result
    lua_call( VM, 1, 1 );

    //get the results from the stack (it's at the top of the stack, hence -1)
    results = (string)lua_tostring( VM, -1 );

    //clear that result off of the stack
    lua_pop( VM, 1 );

    return results;
}

string LuaManager::GetItemInfoStr(const char *item, string info)      // changed to const char so I could pass a string.c_str()
{
    //PRE:  a char pointer with the name of the item we are gathering information on,
    //      and a string containing the info we are requesting
    //POST: this function will return the information requested

    //string to store the restults
    string results;

    //get the function name from lua
    lua_getglobal( VM, "getiteminfo" );

    //push the first argument (item name)
    lua_pushstring( VM, item);

    //push the second argument (info to get)
    lua_pushstring( VM, info.c_str() );

    //call the function, currently with 2 arguments and 1 result
    lua_call( VM, 2, 1 );

    //get the results from the stack (it's at the top of the stack, hence -1)
    results = (string)lua_tostring( VM, -1 );

    //clear that result off of the stack
    lua_pop( VM, 1 );

    return results;
}

string LuaManager::GetEquippedInfoStr(const char *location, string info)      // changed to const char so I could pass a string.c_str()
{
    //PRE:  a char pointer with the name of the body location we are gathering information on,
    //      and a string containing the info we are requesting
    //POST: this function will retuturn the requested info

    //string to store the restults
    string results;

    //get the function name from lua
    lua_getglobal( VM, "getequippedinfo" );

    //push the first argument (item name)
    lua_pushstring( VM, location);

    //push the second argument (info to get)
    lua_pushstring( VM, info.c_str() );

    //call the function, currently with 2 arguments and 1 result
    lua_call( VM, 2, 1 );

    //get the results from the stack (it's at the top of the stack, hence -1)
    results = (string)lua_tostring( VM, -1 );

    //clear that result off of the stack
    lua_pop( VM, 1 );

    return results;
}

void LuaManager::EquipItem(const char *item)      // changed to const char so I could pass a string.c_str()
{
    //PRE:  a char pointer with the name of the item we are equipping
    //POST: this function will equip the item to the appropriate body part,
    //      if it is equipment, otherwise it will do nothing.

    //string to store the restults
    string results;

    //get the function name from lua
    lua_getglobal( VM, "equip" );

    //push the first argument (item name)
    lua_pushstring( VM, item);

    //call the function, currently with 1 arguments and 1 result
    lua_call( VM, 1, 0 );

}

string LuaManager::GetCutSceneStr(string name)      // changed to const char so I could pass a string.c_str()
{
    //PRE:  a string containing the cut scene we are requesting
    //POST: this function will retuturn the next line in the cut scene

    //string to store the restults
    string results;

    //get the function name from lua
    lua_getglobal( VM, "getcutscene" );

    //push the first argument (item name)
    lua_pushstring( VM, name.c_str());

    //call the function, currently with 2 arguments and 1 result
    lua_call( VM, 1, 1 );

    //get the results from the stack (it's at the top of the stack, hence -1)
    results = (string)lua_tostring( VM, -1 );

    //clear that result off of the stack
    lua_pop( VM, 1 );

    return results;
}

string LuaManager::GetInitialItemsStr()      // changed to const char so I could pass a string.c_str()
{
    //PRE:  none
    //POST: this function will return the next item in the initial items list, or END::INITITEMLIST

    //string to store the restults
    string results;

    //get the function name from lua
    lua_getglobal( VM, "getinitialitems" );

    //call the function, currently with 0 arguments and 1 result
    lua_call( VM, 0, 1 );

    //get the results from the stack (it's at the top of the stack, hence -1)
    results = (string)lua_tostring( VM, -1 );

    //clear that result off of the stack
    lua_pop( VM, 1 );

    return results;
}

string LuaManager::CheckForCutsceneStr( const char *typeofobject, string objectname )      // changed to const char so I could pass a string.c_str()
{
    //PRE:  A const char with the type of object being checked (either "map" or "npc")
    //POST: this function will return either NOCUTSCENE or a list of the items with this format
    //      CUTSCENEITEMS::item1::item2::item3 etc.

    //string to store the restults
    string results;

    //get the function name from lua
    lua_getglobal( VM, "checkforcutscene" );

    //push the first argument (entity name)
    lua_pushstring( VM, typeofobject );

    //push the second argument (info to get)
    lua_pushstring( VM, objectname.c_str() );

    //call the function, currently with 2 arguments and 1 result
    lua_call( VM, 2, 1 );

    //get the results from the stack (it's at the top of the stack, hence -1)
    results = (string)lua_tostring( VM, -1 );

    //clear that result off of the stack
    lua_pop( VM, 1 );

    return results;
}


void LuaManager::LuaSaveGame()      // changed to const char so I could pass a string.c_str()
{
    //PRE::none
    //POST::this stores the data that will be written to the save game file in the luasavegamedata struct

    //LuaSaveGameDataType LuaSaveGameData;

    string results;

    //get the function name from lua
    lua_getglobal( VM, "savegame" );

    //call the function, currently with 0 arguments and 19 result
    lua_call( VM, 0, 19 );

    LuaSaveGameData.count = (int)lua_tonumber(VM, -1);
    lua_pop( VM, 1 );
    LuaSaveGameData.getinitialitemscounter = (int)lua_tonumber(VM, -1);
    lua_pop( VM, 1 );
    LuaSaveGameData.flipper = (int)lua_tonumber(VM, -1);
    lua_pop( VM, 1 );
    LuaSaveGameData.sammichcount = (int)lua_tonumber(VM, -1);
    lua_pop( VM, 1 );
    LuaSaveGameData.flsammichcount = (int)lua_tonumber(VM, -1);
    lua_pop( VM, 1 );
    LuaSaveGameData.gbsammichcount = (int)lua_tonumber(VM, -1);
    lua_pop( VM, 1 );
    LuaSaveGameData.poesammichtype = (string)lua_tostring( VM, -1 );
    lua_pop( VM, 1 );
    LuaSaveGameData.head = (string)lua_tostring( VM, -1 );
    lua_pop( VM, 1 );
    LuaSaveGameData.chest = (string)lua_tostring( VM, -1 );
    lua_pop( VM, 1 );
    LuaSaveGameData.arms = (string)lua_tostring( VM, -1 );
    lua_pop( VM, 1 );
    LuaSaveGameData.legs = (string)lua_tostring( VM, -1 );
    lua_pop( VM, 1 );
    LuaSaveGameData.feet = (string)lua_tostring( VM, -1 );
    lua_pop( VM, 1 );
    LuaSaveGameData.hands = (string)lua_tostring( VM, -1 );
    lua_pop( VM, 1 );
    LuaSaveGameData.health = (int)lua_tonumber(VM, -1);
    lua_pop( VM, 1 );
    LuaSaveGameData.maxhealth = (int)lua_tonumber(VM, -1);
    lua_pop( VM, 1 );
    LuaSaveGameData.attack = (int)lua_tonumber(VM, -1);
    lua_pop( VM, 1 );
    LuaSaveGameData.defense = (int)lua_tonumber(VM, -1);
    lua_pop( VM, 1 );
    LuaSaveGameData.damage = (int)lua_tonumber(VM, -1);
    lua_pop( VM, 1 );
    LuaSaveGameData.armor = (int)lua_tonumber(VM, -1);
    lua_pop( VM, 1 );
}

void LuaManager::LuaLoadGame()
{
    //PRE: save game data, stored in the luasavegamedata struct
    //POST: loads this data into the lua script.
    //get the function name from lua
    lua_getglobal( VM, "loadgame" );

    //call the function, currently with 0 arguments and 19 result
    lua_call( VM, 0, 19 );

    lua_pushnumber(VM, LuaSaveGameData.count );
    lua_pushnumber(VM, LuaSaveGameData.getinitialitemscounter );
    lua_pushnumber(VM, LuaSaveGameData.flipper );
    lua_pushnumber(VM, LuaSaveGameData.sammichcount);
    lua_pushnumber(VM, LuaSaveGameData.flsammichcount);
    lua_pushnumber(VM, LuaSaveGameData.gbsammichcount);
    lua_pushstring( VM, LuaSaveGameData.poesammichtype.c_str());
    //lua_pushstring( VM, LuaSaveGameData.head.c_str() );
    //lua_pushstring( VM, LuaSaveGameData.chest.c_str() );
    //lua_pushstring( VM, LuaSaveGameData.arms.c_str() );
    //lua_pushstring( VM, LuaSaveGameData.legs.c_str() );
    //lua_pushstring( VM, LuaSaveGameData.feet.c_str() );
    //lua_pushstring( VM, LuaSaveGameData.hands.c_str() );
    EquipItem(LuaSaveGameData.head.c_str());
    EquipItem(LuaSaveGameData.chest.c_str());
    EquipItem(LuaSaveGameData.arms.c_str());
    EquipItem(LuaSaveGameData.legs.c_str());
    EquipItem(LuaSaveGameData.feet.c_str());
    EquipItem(LuaSaveGameData.hands.c_str());
    lua_pushnumber(VM, LuaSaveGameData.health );
    lua_pushnumber(VM, LuaSaveGameData.maxhealth);
    lua_pushnumber(VM, LuaSaveGameData.attack );
    lua_pushnumber(VM, LuaSaveGameData.defense );
    lua_pushnumber(VM, LuaSaveGameData.damage );
    lua_pushnumber(VM, LuaSaveGameData.armor );

}


void LuaManager::UseItem(string name, string target)      // changed to const char so I could pass a string.c_str()
{
    //PRE:  A string with the name of the item to use (non combat)
    //POST: This function uses the item on the target

    //string to store the restults
    string results;

    //get the function name from lua
    lua_getglobal( VM, "useitem" );

    //push the first argument (entity name)
    lua_pushstring( VM, name.c_str() );

    //push the second argument (info to get)
    lua_pushstring( VM, target.c_str() );

    //call the function, currently with 2 arguments and 0 results
    lua_call( VM, 2,0 );
}

string LuaManager::GetCutscene(string name)
{
    string results;
    //get the function name from lua
    lua_getglobal(VM, "getcutscene");

    //push the first argument (entity name)
    lua_pushstring(VM, name.c_str());

    //call the function, currently with 1 arguments and 1 result
    lua_call(VM, 1, 1);

    //get the results from the stack (it's at the top of the stack, hence -1)
    results = (string)lua_tostring(VM, -1);

    //clear that result off of the stack
    lua_pop(VM, 1);

    return results;
}

void LuaManager::PlayCutscene(string name)
{
    string results;
    results = GetCutscene(name);
    cout << results << endl;
    while(results != "END::CUTSCENE")
    {
        results = GetCutscene(name);
        cout << results << endl;
    }
}

