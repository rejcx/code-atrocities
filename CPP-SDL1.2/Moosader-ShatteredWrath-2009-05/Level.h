#pragma once
#include "Tile.h"
#include "SDL/SDL.h"
#include "Text.h"
#include "LuaManager.h"
#include <string>
#include <iostream>
#include <fstream>
using namespace std;

const int TILEWH = 32;		//tiles are square, w = h = 32

#define BOTTOM 0
#define MIDDLE 1
#define TOP 2
#define COLLISION 3

struct Warp
{
    int warpToIndex;
    int warpToX;
    int warpToY;

    int locX;
    int locY;
};

struct CutsceneInfo
{
    string name;
    bool played;
};

class Level
{
	private:
        int layers, mapWidth, mapHeight, index;
        int warpAmt;
        string name;
        CutsceneInfo cutscene[5];
	public:
        /*
        */
        Warp warpTile[5];
		Tile *bottomLayer;
		Tile *middleLayer;
		Tile *topLayer;
		Tile *collideLayer;

		void LoadMap( string filename );
		int songID;
		void Draw( SDL_Surface *buffer, SDL_Surface *tileset, float, float, int layer );
		void DrawCollision( SDL_Surface *buffer, float offsetX, float offsetY, int x, int y );
		void DrawRegion( SDL_Surface *buffer, float offsetX, float offsetY, int x, int y );
		~Level();
		Level();
		void FreeTiles();
		void Index( int val ) { index = val; }
		int Width() { return mapWidth; }
		int Height() { return mapHeight; }
		string Name() { return name; }
		void CheckAndStartCutscene( LuaManager *luaScript );
};
