/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

GameSys.h
*/

#pragma once
#include <iostream>
#include <fstream>
#include <ctime>
#include <cstdlib>

#include "_ErrorCodes.h"
#include "_Global.h"

#include "ImageManager.h"

using namespace std;

enum GameState { EXIT=-1, TITLE=0, HELP=1, OPTIONS=2, INGAME=3, GAMEOVER=4 };

class GameSys
{
    private:
        // Singleton instance
        static GameSys *inst;
        GameSys();

        // Private members
        GameState state;                        // Current GameSyste
        bool fullscreen;                        // Whether it's fullscreen
        SDL_Rect screenRes;                     // The screen's dimensions
        int screenBPP, maxFPS;
        float gameTime;                         // Counts length of time game has been running

        // Other classes will use these
        string gfxDir, sndDir, lvlDir, fontDir; // Directories for various resourcse
    public:
        // Singleton functions
        static GameSys& GetInst();
        static GameSys* InstPtr();

        // Initialization / Constructors / Destructors
        void Init();
        void ReadConfig();
        ~GameSys();

        // Get / Set & other accessor functions
        void ToggleFullscreen();
        GameState State();
        void State( GameState val );
        void SetResolution( int w, int h );
        SDL_Rect ScreenRect();
        int ScreenWidth();
        int ScreenHeight();
        int MaxFPS();
        float GameTime();
        void IncGameTime();

        string GfxDir();
        string& GfxDirPtr();
        string SndDir();
        string& SndDirPtr();
        string LvlDir();
        string& LvlDirPtr();
        string FontDir();
        string& FontDirPtr();

        // Misc functions
        void Update();

        // Class variables
};
