/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

Image.cpp
*/

#include "Image.h"

Image::~Image()
{
    // Clear up space if it has been allocated
    if ( img )
        SDL_FreeSurface( img );
}

void Image::Init( int index, string& gfxDir )
{
    // Load in image, based on it's index #.
    scrolling = false;
    iID = index;
    switch ( iID )
    {
        case 0:             // Background image
            szID = "bg_00";
            scrolling = true;
        break;

        case 1:             // Enemy 00 image
            szID = "enemy_00";
        break;

        case 2:             // Enemy 01 image
            szID = "enemy_01";
        break;

        case 3:             // Title image
            szID = "title";
        break;

        case 4:             // Unicorn princess image
            szID = "uniprincess";
        break;

        case 5:             // Bullet sheet
            szID = "bullets";
        break;

        case 6:             // Another background images
            szID = "bg_01";
            scrolling = true;
        break;

        case 7:
            szID = "level_names";
        break;

        case 8:
            szID = "pickup_items";
        break;

        default:
            szID = "ERROR";
        break;
    }

    // LoadImage loads the image file based on the string ID
    if ( szID != "ERROR" )
    {
        LoadImage( gfxDir );
    }
    else
    {
        cerr<<"Error "<<INDEX_ERROR<<": Error loading image: "<<iID<<", out of bounds."<<endl;
    }
}

void Image::LoadImage( string& gfxDir )
{
    // Set up the path for loading from, plus image name & file type extention
    string filepath = gfxDir;
    filepath += szID;
    filepath += ".bmp";

    img = SDL_LoadBMP( filepath.c_str() );

    if ( img != NULL )
    {
        // Loaded successfully
        // Set the transparent color (magenta: #FF00FF)
        Uint32 colorkey = SDL_MapRGB( img->format, 0xFF, 0, 0xFF );
        SDL_SetColorKey( img, SDL_SRCCOLORKEY, colorkey );
        cout<<"Loaded image "<<filepath<<" successfully"<<endl;
    }
    else
    {
        cerr<<"Error "<<FILEPATH_ERROR<<": Error loading in image # "<<iID<<" at "<<filepath<<endl;
    }
}

string Image::StrID() { return szID; }

int Image::IntID() { return iID; }

int Image::W() { return img->w; }

int Image::H() { return img->h; }

bool Image::Scrolling() { return scrolling; }

void Image::Scrolling( bool val ) { scrolling = val; }




