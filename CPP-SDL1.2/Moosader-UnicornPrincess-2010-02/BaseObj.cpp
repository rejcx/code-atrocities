/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

BaseObj.cpp
*/

#include "BaseObj.h"

BaseObj::BaseObj()
{
    // Setting default values so it's easier to track if something's wrong
    SetCoordinates( -1, -1, 1, 1 );
    SetColReg( -1, -1, 1, 1 );
    active = false;
    dying = false;
}

void BaseObj::Init()
{
    active = true;
}

bool BaseObj::Active()
{
    return active;
}

int BaseObj::X() { return coord.x; }

int BaseObj::Y() { return coord.y; }

int BaseObj::W() { return coord.w; }

int BaseObj::H() { return coord.h; }

float BaseObj::Speed() { return speed; }

SDL_Rect BaseObj::Coord() { return coord; }

SDL_Rect& BaseObj::CoordPtr() { return coord; }

SDL_Rect BaseObj::ColReg() { return colReg; }

SDL_Rect BaseObj::CollisionCoord()
{
    /*
        Coord() returns the player's (x, y) coordinate rectangle,
        ColReg() returns the player's collision region, from 0 to width and
            0 to height. It is with respect to the actual image drawn, and not
            where the player currently is on the screen.
        CollisionCoord() will return the coordinates plus the collision region,
            giving the area that will be solid on the game screen.  This will be
            what is given to the function that checks for collision between two
            things, based on two SDL_Rect objects given.
     */

     SDL_Rect temp;

     if ( active && !dying )
     {
         temp = coord;
         temp.x += colReg.x;
         temp.y += colReg.y;
         temp.w -= colReg.w;
         temp.h -= colReg.h;
     }
     else
     {
         temp.x = temp.y = temp.w = temp.h = -1;
     }

     return temp;
}

void BaseObj::Move( Direction direction )
{
    // Move a character in a given direction, based on it's speed
    if ( direction == UP )
    {
        coord.y = (int)(coord.y - speed);
    }
    else if ( direction == DOWN )
    {
        coord.y = (int)(coord.y + speed);
    }
    else if ( direction == LEFT )
    {
        coord.x = (int)(coord.x - speed );
    }
    else if ( direction == RIGHT )
    {
        coord.x = (int)(coord.x + speed );
    }
}

void BaseObj::Draw()
{
    // Draw entire image out by default
    ImageManager::GetInst().DrawImage( coord, imgId );
}

void BaseObj::Update()
{
}

float BaseObj::DistanceTo( SDL_Rect& coord )
{
    // return distance between THIS and the passed rectangle.
    float ans = sqrt( (this->coord.x - coord.x)*(this->coord.x - coord.x) + (this->coord.y - coord.y)*(this->coord.y - coord.y) );

    if ( ans < 0 )      // Make it positive
        ans = -ans;

    return ans;
}

void BaseObj::DisplayStats()
{
    // Mainly for debuggin'
    cout<<"Coord: x "<<coord.x<<", y "<<coord.y<<", w "<<coord.w<<", h "<<coord.h<<", Colreg: x "<<colReg.x<<", y "<<colReg.y<<", w "<<colReg.w<<", h "<<colReg.h<<", Active: "<<active<<endl;
}

void BaseObj::SetCoordinates( int x, int y, int w, int h )
{
    // Set the user's coordinates and dimensions
    // Temp: Add error checking
    coord.x = x;
    coord.y = y;
    coord.w = w;
    coord.h = h;
}

void BaseObj::SetCoordinates( int x, int y )
{
    // Set the user's coordinates and dimensions
    // Temp: Add error checking
    coord.x = x;
    coord.y = y;
}

void BaseObj::SetColReg( int x, int y, int w, int h )
{
    // Set the collision region -- ColReg is based on the relative position on the image, between (0, 0) and (128, 128) for Characters.
    // Temp: Add error checking
    colReg.x = x;
    colReg.y = y;
    colReg.w = w;
    colReg.h = h;
}
