/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

Timer.h
*/

#pragma once

#include "_ErrorCodes.h"
#include "_Global.h"

class Timer
{
    public:
        // Initialization / Constructors / Destructors
        Timer();

        // Get / Set & other accessor functions
        int GetTicks();

        // Misc functions
        void CheckRate( float fps );
        void Start();

        // Class variables
        int startTicks;
        bool running;
};
