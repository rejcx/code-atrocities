/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

_Global.cpp
*/

#include "_Global.h"

bool IsCollision( SDL_Rect& obj1, SDL_Rect& obj2 )
{
    if ( obj1.x == -1 || obj2.x == -1 ) // one of these is inactive.
        return false;

    // left1 < right2 && right1 > left2 && top1 < bottom2 && bottom1 > top2
    if (    obj1.x            <   obj2.x + obj2.w &&
            obj1.x + obj1.w   >   obj2.x &&
            obj1.y            <   obj2.y + obj2.h &&
            obj1.y + obj1.h   >   obj2.y )
    {
//        cout<<"\n\n\tCollision between ("<<obj1.x<<", "<<obj1.y<<", "<<obj1.x+obj1.w<<", "<<obj1.y+obj1.h<<") && ("<<obj2.x<<", "<<obj2.y<<", "<<obj2.x+obj2.w<<", "<<obj2.y+obj2.h<<")"<<endl;

        // Collision
        return true;
    }

    return false;
}
