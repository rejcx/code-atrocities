/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

GameSys.cpp
*/

#include "GameSys.h"

// Singleton intialization accessors
GameSys& GameSys::GetInst()
{
    if ( !inst )
        inst = new GameSys();

    return *inst;
}

GameSys* GameSys::InstPtr()
{

    if ( !inst )
        inst = new GameSys();

    return inst;
}

// Initialization / Constructors / Destructors
GameSys::GameSys()
{
    ;
}

void GameSys::ReadConfig()
{
    ifstream infile;
    infile.open( "config.txt" );

    // Set defaults in case something doesn't load in right...:
    screenRes.w = 1024;
    screenRes.h = 768;
    fullscreen = false;

    /* Read config file and load in basic system options */
    if ( infile.fail() )
    {
        infile.close();
        // Use default values if cannot find config file
        // Also, create a config file.
        cerr<<"Error "<<FILEPATH_ERROR<<": Error opening 'config.txt'"<<endl;
        cerr<<"Using default screen res: "<<screenRes.w<<"x"<<screenRes.h<<endl;

        // Write replacement config file (you know, in case someone actually wants to modify it later)
        ofstream outfile;
        outfile.open( "config.txt" );
        outfile<<"screenWidth "<<screenRes.w<<endl;
        outfile<<"screenHeight "<<screenRes.h<<endl;
        outfile<<"fullscreen false"<<endl;
        outfile<<"lvl_dir \\data\\lvl\\"<<endl;
        outfile<<"gfx_dir \\data\\gfx\\"<<endl;
        outfile<<"snd_dir \\data\\snd\\"<<endl;
        outfile.close();
    }
    else
    {
        string temp;
        cout<<"Values from config file: "<<endl;
        while ( infile>>temp )
        {
            // Read through entire config file, and set values based on the line tags
            if ( temp == "screenWidth" )
            {
                infile>>screenRes.w;

                if ( screenRes.w > 1280 || screenRes.w < 320 )   // Make sure it's a decent value
                {
                    cerr<<"Error "<<RES_DIMENSION_BAD<<": Bad resolution dimensions in config.txt.  Setting to default"<<endl;
                    screenRes.w = 1024;
                }

                cout<<"Screen width "<<screenRes.w<<endl;
            }
            else if ( temp == "screenHeight" )
            {
                infile>>screenRes.h;
                if ( screenRes.h > 1024 || screenRes.w < 240 )   // Make sure it's a decent value
                {
                    cerr<<"Error "<<RES_DIMENSION_BAD<<": Bad resolution dimensions in config.txt.  Setting to default"<<endl;
                    screenRes.h = 768;
                }

                cout<<"Screen height "<<screenRes.h<<endl;
            }
            else if ( temp == "fullscreen" )
            {
                infile>>temp;
                if ( temp == "false" )
                {
                    fullscreen = false;
                    cout<<"Fullscreen false"<<endl;
                }
                else
                {
                    fullscreen = true;
                    cout<<"Fullscreen true"<<endl;
                }
            }
            else if ( temp == "lvl_dir" )
            {
                infile>>lvlDir;
            }
            else if ( temp == "snd_dir" )
            {
                infile>>sndDir;
            }
            else if ( temp == "gfx_dir" )
            {
                infile>>gfxDir;
            }
            else if ( temp == "font_dir" )
            {
                infile>>fontDir;
            }
        }

        infile.close();
    }//if ( infile.fail() )
}

void GameSys::Init()
{
    /*
        No inputs, no returns
        Initializes SDL and other game "system" variables.
        Open config file for values like screen res.
    */

    gameTime = 0;
    srand( unsigned( time( NULL ) ) );

    ReadConfig();

    /* Initialize SDL and system variables */
    state = INGAME;
    screenBPP = 32;
    maxFPS = 60;

    // This calls the SDL_Init function, and enters
    // the if statement if the return value is -1.
    if ( SDL_Init( SDL_INIT_EVERYTHING ) == -1 )
    {
        cerr<<"Error "<<SDL_INIT_ERROR<<": Error initializing SDL -- SDL_Init( SDL_INIT_EVERYTHING )"<<endl;
        exit( SDL_INIT_ERROR );
    }

    // Set window caption
    SDL_WM_SetCaption( "Unicorn Princess", NULL );

    if ( fullscreen )
    {
        ImageManager::GetInst().buffer = SDL_SetVideoMode( screenRes.w, screenRes.h, screenBPP, SDL_SWSURFACE | SDL_FULLSCREEN  );
    }
    else
    {
        ImageManager::GetInst().buffer = SDL_SetVideoMode( screenRes.w, screenRes.h, screenBPP, SDL_SWSURFACE );
    }

    // Check to see if buffer was initialized correctly
    if ( ImageManager::GetInst().buffer == NULL )
    {
        cerr<<"Error "<<BUFFER_INIT_ERROR<<": Error initializing the buffer"<<endl;
        exit( BUFFER_INIT_ERROR );
    }

    // Init sound
    if ( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 )
    {
        cerr<<"Error "<<SOUND_INIT_ERROR<<": Error initializing sound"<<endl;
        exit( SOUND_INIT_ERROR );
    }
}

GameSys::~GameSys()
{
    if ( inst )
        delete inst;

    SDL_Quit();
}

// Get / Set & other accessor functions

void GameSys::ToggleFullscreen()
{
    if ( fullscreen == false )
    {
        ImageManager::GetInst().buffer = SDL_SetVideoMode( screenRes.w, screenRes.h, screenBPP, SDL_SWSURFACE | SDL_FULLSCREEN);
        fullscreen = true;
    }
    else
    {
        ImageManager::GetInst().buffer = SDL_SetVideoMode( screenRes.w, screenRes.h, screenBPP, SDL_SWSURFACE );
        fullscreen = false;
    }
}

GameState GameSys::State()
{
    return state;
}

void GameSys::State( GameState val )
{
    state = val;
}

void GameSys::SetResolution( int w, int h )
{
    screenRes.w = w;
    screenRes.h = h;
}

SDL_Rect GameSys::ScreenRect()
{
    return screenRes;
}

int GameSys::ScreenWidth()
{
    return screenRes.w;
}

int GameSys::ScreenHeight()
{
    return screenRes.h;
}

int GameSys::MaxFPS()
{
    return maxFPS;
}

float GameSys::GameTime()
{
    return gameTime;
}

void GameSys::IncGameTime()
{
    gameTime += 0.1;
}

string GameSys::GfxDir()
{
    return gfxDir;
}

string& GameSys::GfxDirPtr()
{
    return gfxDir;
}

string GameSys::SndDir()
{
    return sndDir;
}

string& GameSys::SndDirPtr()
{
    return sndDir;
}

string GameSys::LvlDir()
{
    return lvlDir;
}

string& GameSys::LvlDirPtr()
{
    return lvlDir;
}

string GameSys::FontDir()
{
    return lvlDir;
}

string& GameSys::FontDirPtr()
{
    return lvlDir;
}

void GameSys::Update()
{
    IncGameTime();
}




