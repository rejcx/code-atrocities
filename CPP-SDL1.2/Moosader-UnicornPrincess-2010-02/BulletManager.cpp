/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

BulletManager.cpp
*/

#include "BulletManager.h"

// Singleton intialization accessors
BulletManager& BulletManager::GetInst()
{
    if ( !inst )
        inst = new BulletManager();

    return *inst;
}

BulletManager* BulletManager::InstPtr()
{
    if ( !inst )
        inst = new BulletManager();

    return inst;
}

void BulletManager::Init()
{
    for ( int i=0; i<MAX_BULLETS; i++ )
    {
        bullet[i].Init();
    }
}

void BulletManager::UpdateBullets()
{
    for ( int i=0; i<MAX_BULLETS; i++ )
    {
        bullet[i].Update();
    }
}

void BulletManager::DrawBullets()
{
    for ( int i=0; i<MAX_BULLETS; i++ )
    {
        if ( bullet[i].Active() == true )
        {
            bullet[i].Draw();
        }
    }
}

void BulletManager::Shoot( int x, int y, Direction dir, BulletType type, BulletDamage damage )
{
    // Check for an available bullet and use it
    int index = -1;
    for ( int i=0; i<MAX_BULLETS; i++ )
    {
        if ( bullet[i].Active() == false )
        {
            index = i;
            break;
        }
    }

    if ( index != -1 )
    {
        // Temp: Clean this up

        bullet[index].Init();
        bullet[index].Shoot( x, y, dir, type, damage );
        if ( type == bSMALLBULLET || type == bBULLET )
        {
            // Enemy bullet
            bullet[index].SetMovementType( LINE );
            bullet[index].SetSpeed( 10 );
        }
        else if ( type == bSPARKLE )
        {
            bullet[index].SetMovementType( LINE );
            bullet[index].SetSpeed( -7 );
        }
        else if ( type == bGEM )
        {
            // Player bullet
            bullet[index].SetMovementType( LINE );
            bullet[index].SetSpeed( -10 );
        }
        else if ( type == bFLOWER )
        {
            bullet[index].SetMovementType( WAVE );
            bullet[index].SetSpeed( -5 );
        }
        else if ( type == bHEART )
        {
            bullet[index].SetMovementType( LINE );
            bullet[index].SetSpeed( -7 );
        }
    }
}


