/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

Bullet.h
*/

#pragma once

#include "_ErrorCodes.h"
#include "_Global.h"

#include "GameSys.h"
#include "ImageManager.h"

#include "BaseObj.h"

using namespace std;

class Bullet : public BaseObj
{
    private:
        BulletType bulletType;          // The type of bullet it is (See: _Global)
        BulletDamage bulletDamage;      // If it damages the player, enemy, or everybody (See: _Global)
        MovementType movementType;      // The way it moves (See: _Global)
        float defSpeed;                 // "Default Speed". What it resets to.  Temp: Fix
    public:
        // Initialization / Constructors / Destructors
        Bullet();
        void Init();

        // Get/Set
        void Shoot( int x, int y, Direction dir, BulletType type, BulletDamage damage  );
        void SetBulletType( EnemyType enemyType );
        void SetSpeed( float spd );
        BulletDamage GetBulletDamage();
        void SetMovementType( MovementType movement );

        // Misc functions
        void Draw();
        void Update();
        void Explode();
};
