/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

Timer.cpp
*/

#include "Timer.h"

// Initialization / Constructors / Destructors
Timer::Timer()
{
    startTicks = 0;
    running = false;
}

// Get / Set & other accessor functions
int Timer::GetTicks()
{
    if ( running )
    {
        return SDL_GetTicks() - startTicks;
    }
    else
    {
        return -1;
    }
}

// Misc functions
void Timer::CheckRate( float fps )
{
    if ( GetTicks() < 1000 / fps )
    {
        SDL_Delay( (int)( (1000/fps) - GetTicks() ) );
    }
}

void Timer::Start()
{
    running = true;
    startTicks = SDL_GetTicks();
}
