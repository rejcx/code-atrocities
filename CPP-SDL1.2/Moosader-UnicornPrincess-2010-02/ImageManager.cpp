/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

ImageManager.cpp
*/

#include "ImageManager.h"

// Singleton intialization accessors
ImageManager& ImageManager::GetInst()
{
    if ( !inst )
        inst = new ImageManager();

    return *inst;
}

ImageManager* ImageManager::InstPtr()
{
    if ( !inst )
        inst = new ImageManager();

    return inst;
}

// Initialization / Constructors / Destructors
ImageManager::ImageManager()
{
    ;
}

void ImageManager::Init( string& gfxDir )
{
    currentBgImg = 0; // bg_00

    /* For each Image (class) type, initialize based on it's index */
    for ( int i=0; i<IMGAMT; i++ )
    {
        image[i].Init( i, gfxDir );
    }

    bgScroll = 0;
    absScroll = 0;
}

// Get/set

void ImageManager::SetBackgroundImage( int val )
{
    if ( val >= 0 && val < IMGAMT )
        currentBgImg = val;
}

void ImageManager::SetBackgroundImage( string val )
{
    int i = ReturnIntId( val );
    if ( i == -1 ) // couldn't find
    {
        cerr<<"Error "<<FILEPATH_ERROR<<": Could not load in background image \""<<val<<"\" from the level file.  Setting to default"<<endl;
        i = 0;
    }
    SetBackgroundImage( i );
}

int ImageManager::BgScroll() { return bgScroll; }

int ImageManager::AbsScroll() { return absScroll; }

// Misc functions

void ImageManager::DrawBackground()
{
    // Set up coordinate and crop rectangles
    SDL_Rect cD, cC;
    cD.x = 0;
    cD.y = 0;
    cC.x = cC.y = 0;
    cC.w = image[currentBgImg].img->w;
    cC.h = image[currentBgImg].img->h;

    DrawImage( cD, cC, currentBgImg );
}

void ImageManager::DrawLevelName()
{
    if ( levelNameTimer > 0 )
    {
        // Crop rectangle
        SDL_Rect coordDestination, coordCrop;

        coordDestination.x = 1024 / 2;
        coordDestination.y = 768 / 2;
        coordDestination.w = coordCrop.w = 100;
        coordDestination.h = coordCrop.h = 50;

        coordCrop.x = 0;
        coordCrop.y = currentLevel * coordDestination.h;

        DrawImage( coordDestination, coordCrop, 7 );
        levelNameTimer -= 1;
    }
}

void ImageManager::ShowTitle( int lvl )
{
    currentLevel = lvl;
    levelNameTimer = 60;
}

void ImageManager::DrawImage( const SDL_Rect& coordDestination, const SDL_Rect& coordCrop, int iID )
{
    /* SDL_BlitSurface doesn't accept const SDL_Rects.  Find out how to pass these from the
        parameters, if possible. */
    SDL_Rect tCoord, tCrop;
    tCoord = coordDestination;
    tCrop = coordCrop;

    if ( image[ iID ].Scrolling() == true ) // Background image
    {
        // Offset it
        tCoord.x += bgScroll;
        SDL_BlitSurface( image[ iID ].img, &tCrop, buffer, &tCoord );

        // Draw the image again at the end of the previous one,
        // so that it loops cleanly
        // ?
        tCoord.x += image[ iID ].W() + bgScroll;
        SDL_BlitSurface( image[ iID ].img, &tCrop, buffer, &tCoord );
    }
    else
    {
        SDL_BlitSurface( image[ iID ].img, &tCrop, buffer, &tCoord );
    }
}

void ImageManager::DrawImage( const SDL_Rect& coordDestination, int iID )
{
    // Draw entire image; no cropping.
    SDL_Rect tCrop;
    tCrop.x = tCrop.y = 0;
    tCrop.w = coordDestination.w;
    tCrop.h = coordDestination.h;

    DrawImage( coordDestination, tCrop, iID );
}

void ImageManager::DrawImage( const SDL_Rect& coordDestination, const SDL_Rect& coordCrop, const string& szID )
{
    int i = ReturnIntId( szID );

    if ( i == -1 )
    {
        cout<<"Error "<<SZID_ERROR<<": Could not find image '"<<szID<<"'.  Skipping draw."<<endl;
    }

    DrawImage( coordDestination, coordCrop, i );
}

void ImageManager::DrawImage( int x, int y, const char* szID )
{
    int i = ReturnIntId( szID );

    if ( i == -1 )
    {
        cout<<"Error "<<SZID_ERROR<<": Could not find image '"<<szID<<"'.  Skipping draw."<<endl;
    }

    // Set up coordinate and crop rectangles
    SDL_Rect cD, cC;
    cD.x = x;
    cD.y = y;
    cC.x = cC.y = 0;
    cC.w = image[i].img->w;
    cC.h = image[i].img->h;

    DrawImage( cD, cC, i );
}

void ImageManager::DrawToScreen()
{
    SDL_Flip( buffer );
}

void ImageManager::ResetLevelVariables()
{
    bgScroll = 0;
    absScroll = 0;
}

void ImageManager::ClearBgRect( SDL_Rect* coordinates )
{
    SDL_FillRect( buffer, coordinates, SDL_MapRGB( buffer->format, 0x00, 0x00, 0x00 ) );
}

// Scrolley functions
void ImageManager::ScrollScreen()
{
    bgScroll -= 1;
    absScroll += 1;
    if ( bgScroll <= -image[ 0 ].W() )  // Compare to background width
    {
        bgScroll = 0;
    }
}

int ImageManager::ReturnIntId( string szID )
{
    int i = 0;
    bool searching = true;

    /* Search for image integer IDs with same string ID as what was given */
    while ( i < IMGAMT && searching )
    {
        if ( image[i].StrID() == szID )
            searching = false;
        else
            i++;
    }

    if ( searching == true )    // it wasn't found
        i = -1;

    return i;
}




