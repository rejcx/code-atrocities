/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

Character.cpp
*/

#include "Character.h"


Character::Character()
{
    Init(); // temp
}

void Character::Init()
{
    SetCoordinates( -4, -4, 4, 4 );
    SetColReg( -4, -4, 4, 4 );
    shootCooldownLeft = 0;
    shootCooldownTime = 10;
    bulletType = bBULLET;
    active = false;
    dying = false;
}

void Character::Draw()
{
    /*
        Set up varaibles to call ImageManager's draw function.
        - coordinates
        - frame
        - action

        frame and action determine the coordCrop variable (Where to crop on the actual bitmap image):
            Frame is the horizontal, will be (frame_number) * (player_width)
            Action is the vertical, will be (action_number) * (player_height)

        coordinates determine coordDestination
    */

    // void DrawImage( const SDL_Rect *coordDestination, const SDL_Rect *coordCrop, int iID );

    if ( active )
    {
        SDL_Rect coordCrop;
        coordCrop.x = (int)frame * coord.w;
        coordCrop.y = (int)action * coord.h;
        coordCrop.w = coord.w;
        coordCrop.h = coord.h;

        ImageManager::GetInst().DrawImage( coord, coordCrop, imgId );
    }
}

void Character::IncrementFrame()
{
    frame += 0.2;
    if ( frame > MAXFRAME )
    {
        frame = 0;

        if ( dying )
        {
            active = false;
            dying = false;
        }

        if ( action == SHOOTING )
        {
            action = FLYING;
        }
    }
}

void Character::Update()
{
    if ( active )
    {
        IncrementFrame();

        if ( shootCooldownLeft > 0 )
        {
            shootCooldownLeft -= 1;
        }
    }
}

void Character::Shoot()
{
    if ( active && !dying )
    {
        if ( shootCooldownLeft <= 0 )   // Throttle the rate character can shoot bullets.
        {
            action = SHOOTING;
            frame = 0;
            shootCooldownLeft = shootCooldownTime;

            // Temp: Clean this up

            if ( bulletType == bHEART )
            {
                BulletManager::GetInst().Shoot( coord.x + (coord.w/2), coord.y + (coord.h/2) + 16, dir, bulletType, gunDamage );
                BulletManager::GetInst().Shoot( coord.x + (coord.w/2), coord.y + (coord.h/2) - 16, dir, bulletType, gunDamage );
            }
            else if ( bulletType == bGEM )
            {
                BulletManager::GetInst().Shoot( coord.x + (coord.w/2), coord.y + (coord.h/2), dir, bulletType, gunDamage );
                BulletManager::GetInst().Shoot( coord.x + (coord.w/2) - 16, coord.y + (coord.h/2), dir, bulletType, gunDamage );
            }
            else if ( bulletType == bBULLET )
            {
                BulletManager::GetInst().Shoot( coord.x + (coord.w/2), coord.y + (coord.h/2), dir, bulletType, gunDamage );
                SoundManager::GetInst().PlaySound( "MachineGun" );
            }
            else if ( bulletType == bSMALLBULLET )
            {
                BulletManager::GetInst().Shoot( coord.x + (coord.w/2), coord.y + (coord.h/2), dir, bulletType, gunDamage );
                SoundManager::GetInst().PlaySound( "Pistol" );
            }
            else
            {
                BulletManager::GetInst().Shoot( coord.x + (coord.w/2), coord.y + (coord.h/2), dir, bulletType, gunDamage );
            }
        }
    }
}

void Character::Die()
{
    if ( active && !dying )
    {
        frame = 0.0;
        dying = true;
        action = DYING;
    }
}


