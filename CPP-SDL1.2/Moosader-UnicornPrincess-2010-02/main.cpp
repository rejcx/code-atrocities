/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

Main.cpp
*/

#include "SDL/SDL.h"

#include "BulletManager.h"
#include "CharacterManager.h"
#include "ImageManager.h"
#include "GameSys.h"
#include "ItemManager.h"
#include "LevelManager.h"
#include "SoundManager.h"

#include "Input.h"
#include "Text.h"
#include "Timer.h"

BulletManager* BulletManager::inst = 0;
CharacterManager* CharacterManager::inst = 0;
GameSys* GameSys::inst = 0;
ImageManager* ImageManager::inst = 0;
ItemManager* ItemManager::inst = 0;
LevelManager* LevelManager::inst = 0;
SoundManager* SoundManager::inst = 0;

void InitManagers()
{
    /*
        Call all the manager's Initialization functions so that
        they're ready.
    */
    GameSys::GetInst().Init();
    ImageManager::GetInst().Init( GameSys::GetInst().GfxDirPtr() );
    CharacterManager::GetInst().Init();
    BulletManager::GetInst().Init();
    SoundManager::GetInst().Init( GameSys::GetInst().SndDirPtr() );
    ItemManager::GetInst().Init();
    LevelManager::GetInst().Init( GameSys::GetInst().LvlDirPtr() );
}

int main( int argc, char *args[] )
{
    InitManagers();

    SDL_Event event;

    // Game system tools
    Input input;
    Timer timer;

//    InitFont( "arial.ttf", 16 );


    SoundManager::GetInst().PlayMusic( "Melody" );

    // Begin game loop
    while ( GameSys::GetInst().State() != EXIT )
    {
        timer.Start(); // Refresh timer for this iteration
        while ( SDL_PollEvent( &event ) )
        {
            if ( event.type == SDL_QUIT )
                GameSys::GetInst().State( EXIT );
        }

        // Handle input
        input.GetOptionCommands();
        input.GetPlayerCommands();

        if ( GameSys::GetInst().State() == INGAME )
        {
            // Update game logic
            ImageManager::GetInst().ScrollScreen();
            CharacterManager::GetInst().UpdateCharacters();
            BulletManager::GetInst().UpdateBullets();
            GameSys::GetInst().Update();
            ItemManager::GetInst().UpdateItems();
            LevelManager::GetInst().CheckIfWarpReached();

            // Display an image
            ImageManager::GetInst().DrawBackground();
            ItemManager::GetInst().DrawItems();
            CharacterManager::GetInst().DrawCharacters();
            BulletManager::GetInst().DrawBullets();
            ImageManager::GetInst().DrawLevelName();
            ImageManager::GetInst().DrawToScreen();
        }

        // Check game speed and keep it at a constant rate
        timer.CheckRate( GameSys::GetInst().MaxFPS() );
    }

    return 0;

}
