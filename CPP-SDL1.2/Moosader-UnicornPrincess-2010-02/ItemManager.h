/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

ItemManager.h
*/

#pragma once

#include <iostream>
#include <fstream>

#include "_ErrorCodes.h"
#include "_Global.h"

#include "Item.h"

using namespace std;

class ItemManager
{
    private:
        // Singleton instance
        static ItemManager* inst;
        ItemManager();

        int itemCount;
    public:
        // Singleton functions
        static ItemManager& GetInst();
        static ItemManager* InstPtr();

        // Initialization / Constructors / Destructors
        void Init();

        // Get / Set & other accessor functions
        int ItemCount();

        // Misc functions
        void CreateItem( int x, int y, BulletType bullet );
        void UpdateItems();
        void AddOneToItemArray();
        void DrawItems();

        // Class objects
        Item* item;
};



