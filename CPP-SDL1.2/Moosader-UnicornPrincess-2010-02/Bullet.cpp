/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

Bullet.cpp
*/

#include "Bullet.h"

Bullet::Bullet()
{
    SetCoordinates( -2, -2, 2, 2 );
    SetColReg( -2, -2, 2, 2 );
    active = false;
    imgId = ImageManager::GetInst().ReturnIntId( "bullets" );
    bulletType = bSPARKLE;
}

void Bullet::Init()
{
    movementType = LINE;
    active = false;
    speed = 10;
    bulletType = bSPARKLE;
    coord.w = 16;
    coord.h = 16;
    imgId = ImageManager::GetInst().ReturnIntId( "bullets" );
    defSpeed = 10;
}
void Bullet::Shoot( int x, int y, Direction dir, BulletType type, BulletDamage damage  )
{
    active = true;
    SetCoordinates( x, y, 16, 16 );
    SetColReg( 0, 0, 16, 16 );
    this->dir = dir;
    bulletType = type;
    bulletDamage = damage;
}

void Bullet::SetSpeed( float spd )
{
    defSpeed = speed = spd;
}

void Bullet::SetBulletType( EnemyType enemyType )
{
    bulletType = (BulletType)(enemyType + 1);
}

BulletDamage Bullet::GetBulletDamage()
{
    return bulletDamage;
}

void Bullet::SetMovementType( MovementType movement )
{
    movementType = movement;
}

void Bullet::Draw()
{
    /*
        Set up variables for ImageManager's draw function.
        Bullet drawn depends on bulletType.
    */

    // void DrawImage( const SDL_Rect *coordDestination, const SDL_Rect *coordCrop, int iID );

    SDL_Rect coordCrop;
    coordCrop.x = 0;
    coordCrop.y = (int)bulletType * coord.h;
    coordCrop.w = coord.w;
    coordCrop.h = coord.h;

    ImageManager::GetInst().DrawImage( coord, coordCrop, imgId );
}

void Bullet::Update()
{
    if ( active )
    {
        speed = defSpeed;
        Move( LEFT );

        switch ( movementType )
        {
            case LINE:
                // No Y Movement
            break;
            case WAVE:
                speed = (int)(GameSys::GetInst().GameTime()) % 5;
                if ( (int)(GameSys::GetInst().GameTime()) % 10 < 5 )
                {
                    Move( DOWN );
                }
                else
                {
                    Move( UP );
                }
            break;
            default:
                // No Y Movement
            break;
        }

        if ( coord.x < 0 || coord.x - coord.w > GameSys::GetInst().ScreenWidth() ) // off-screen
        {
            active = false;
        }
    }
}

void Bullet::Explode()
{
    if ( active )
    {
        active = false;
    }
}

