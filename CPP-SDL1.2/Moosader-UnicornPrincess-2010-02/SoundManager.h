/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

SoundManager.h
*/

#pragma once
#include <iostream>
#include <fstream>

#include "SDL/SDL.h"

#include "_ErrorCodes.h"
#include "_Global.h"

#include "Sound.h"
#include "Music.h"

using namespace std;

class SoundManager
{
    private:
        // Singleton instance
        static SoundManager *inst;
        SoundManager() { ; }

        int bgScroll;
    public:
        // Singleton functions
        static SoundManager& GetInst();
        static SoundManager* InstPtr();

        // Initialization / Constructors / Destructors
        void Init( string& sndDir );
        ~SoundManager();

        // Get / Set & other accessor functions
        // Misc functions
            // Sound functions
        void PlaySound( int iID );
        void PlaySound( const string& szID );
        void PlayMusic( int iID );
        void PlayMusic( const string& szID );
        void PauseMusic();
        void ResumeMusic();
        void StopMusic();

            // Other
        int ReturnIntId( string szID, bool music );

        // Class variables
        Sound sound[SNDAMT];      // Temp: Change to private
        Music music[MSCAMT];
};

