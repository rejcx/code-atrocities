/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

_Global.h
*/

#pragma once

#include <iostream>

#include "SDL/SDL.h"
#include "SDL/SDL_mixer.h"
#include "SDL/SDL_ttf.h"

using namespace std;

enum Action { FLYING=0, SHOOTING=1, DYING=2 };
enum BulletType { bSPARKLE=0, bFLOWER=1, bGEM=2, bHEART=3, bBULLET=4, bSMALLBULLET=5 };
enum ItemType { iFLOWER=0, iGEM=1, iHEART=2, iONEUP=3 };
enum Direction { LEFT, RIGHT, UP, DOWN };
enum EnemyType { SCOUT=1, TROOPER=2 };
enum MovementType { LINE, TOWARDS_PLAYER, AWAY_PLAYER, WAVE };
enum BulletDamage { PLAYER, ENEMY, ALL };

const int MAX_BULLETS = 10;
const int IMGAMT = 9;
const int SNDAMT = 3;
const int MSCAMT = 2;

bool IsCollision( SDL_Rect& obj1, SDL_Rect& obj2 );

