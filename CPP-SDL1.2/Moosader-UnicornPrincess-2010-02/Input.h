/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

Input.h
*/

#pragma once

#include "_ErrorCodes.h"
#include "_Global.h"

#include "CharacterManager.h"
#include "GameSys.h"
#include "LevelManager.h"

#include "Player.h"

using namespace std;

class Input
{
    private:
        Uint8 *keys;
    public:
        // Initialization / Constructors / Destructors
        Input();

        // Misc functions
        void Refresh();
        bool GetKey( int key );
        void GetOptionCommands();
        void GetPlayerCommands();
};
