/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

Text.cpp
*/

#include "Text.h"
#include "GameSys.h"

TTF_Font *font;

// Not used in Iteration 1.  Code from Shattered Wrath, needs to be updated.

void InitFont( string filename, int size )
{
    string filepath = GameSys::GetInst().FontDir();
    filepath += filename;

    font = TTF_OpenFont( filepath.c_str(), size );

    if ( !font )
    {
        cerr<<"Error "<<FILEPATH_ERROR<<": Error loading font "<<filepath<<endl;
    }
}

void DrawText( SDL_Surface* destination, string& msg, float x, float y, float offsetX, float offsetY, int size, SDL_Color color )
{
    SDL_Color shadow;
    shadow.r = shadow.g = shadow.b = 0;

    SDL_Rect coordinates;
    coordinates.x = (int)(x - offsetX);
    coordinates.y = (int)(y - offsetY);

    SDL_Rect shadowOffset;
    shadowOffset.x = coordinates.x - 1;    shadowOffset.y = coordinates.y - 1;

    SDL_Surface *message = NULL;
    SDL_Surface *shadowtxt = NULL;
    message = TTF_RenderText_Solid( font, msg.c_str(), color );
    shadowtxt = TTF_RenderText_Solid( font, msg.c_str(), shadow );


    SDL_BlitSurface( shadowtxt, NULL, destination, &shadowOffset );
    shadowOffset.x = coordinates.x - 2;    shadowOffset.y = coordinates.y - 2;
    SDL_BlitSurface( shadowtxt, NULL, destination, &shadowOffset );
    SDL_BlitSurface( message, NULL, destination, &coordinates );


    TTF_CloseFont( font );
    SDL_FreeSurface( message );
    SDL_FreeSurface( shadowtxt );
}

void DrawText( SDL_Surface* destination, string& msg, float x, float y, float offsetX, float offsetY, int size, int r, int g, int b )
{
    SDL_Color color;
    color.r = r;
    color.g = g;
    color.b = b;
    DrawText( destination, msg, x, y, offsetX, offsetY, size, color );
}

void DrawText( SDL_Surface* destination, char* message, float x, float y, float offsetX, float offsetY, int size, int r, int g, int b )
{
    string temp( message ); // init

    DrawText( destination, temp, x, y, offsetX, offsetY, size, r, g, b );
}
