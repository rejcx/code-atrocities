/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

Input.cpp
*/

#include "Input.h"

// Initialization / Constructors / Destructors
Input::Input()
{
    keys = NULL;
    Refresh();
}

// Misc functions

void Input::Refresh()
{
    SDL_PumpEvents();
    keys = SDL_GetKeyState( NULL );
}

bool Input::GetKey( int key )
{
    return keys[key];
}

void Input::GetOptionCommands()
{
    if ( GetKey( SDLK_ESCAPE ) )
    {
        // Go to menu
    }
    else if ( GetKey( SDLK_F5 ) )
    {
        // Toggle fullscreen, and pause the game
    }

    else if ( GetKey( SDLK_F4) )
    {
        // Temporary quick-quit
        GameSys::GetInst().State( EXIT );
    }
}

void Input::GetPlayerCommands()
{
    // UP
    if ( GetKey( SDLK_w ) || GetKey( SDLK_UP ) )
    {
        CharacterManager::GetInst().player.Move( UP );
    }
    // DOWN
    else if ( GetKey( SDLK_s ) || GetKey( SDLK_DOWN ) )
    {
        CharacterManager::GetInst().player.Move( DOWN );
    }

    // LEFT
    if ( GetKey( SDLK_a ) || GetKey( SDLK_LEFT ) )
    {
        CharacterManager::GetInst().player.Move( LEFT );
    }
    // RIGHT
    else if ( GetKey( SDLK_d ) || GetKey( SDLK_RIGHT ) )
    {
        CharacterManager::GetInst().player.Move( RIGHT );
    }

    // FIRE
    if ( GetKey( SDLK_SPACE ) )
    {
        CharacterManager::GetInst().player.Shoot();
    }

    // TEMPORARY

//    if ( GetKey( SDLK_RETURN ) )
//    {
//        GameSys::GetInst().State( INGAME );
//        SoundManager::GetInst().PlayMusic( "Melody" );
//    }

    if ( GetKey( SDLK_1 ) )
    {
        CharacterManager::GetInst().player.GetItem( iGEM );
    }
    else if ( GetKey( SDLK_2 ) )
    {
        CharacterManager::GetInst().player.GetItem( iHEART );
    }
    else if ( GetKey( SDLK_3 ) )
    {
        CharacterManager::GetInst().player.GetItem( iFLOWER );
    }

    else if ( GetKey( SDLK_5 ) )
    {
        SoundManager::GetInst().PlaySound( "MachineGun" );
    }


    if ( GetKey( SDLK_r ) )
    {
        cout<<endl<<endl;
        cout<<"Player x: "<<CharacterManager::GetInst().player.X()<<endl;
        cout<<"Abs Scroll: "<<ImageManager::GetInst().AbsScroll()<<endl;
        cout<<"Warp at: "<<LevelManager::GetInst().WarpPoint()<<endl;
    }
}











