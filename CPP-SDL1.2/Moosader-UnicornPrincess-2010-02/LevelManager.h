/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

LevelManager.h
*/

#pragma once

#include <iostream>
#include <fstream>

#include "_ErrorCodes.h"
#include "_Global.h"

#include "CharacterManager.h"

using namespace std;

class LevelManager
{
    private:
        // Singleton instance
        static LevelManager* inst;
        LevelManager();

        // Class variables
        int warpPoint;
    public:
        // Singleton functions
        static LevelManager& GetInst();
        static LevelManager* InstPtr();

        // Initialization / Constructors / Destructors
        void Init( string& lvlDir );

        // Get / Set & other accessor functions
        void WarpPoint( int val );
        int WarpPoint();

        // Misc functions
        void LoadLevel( string& lvlDir, int lvl );
        void CheckIfWarpReached();
};
