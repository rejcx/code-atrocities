/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

Enemy.h
*/


#pragma once

#include <iostream>

#include "_ErrorCodes.h"
#include "_Global.h"

#include "BulletManager.h"
#include "GameSys.h"
#include "ImageManager.h"
#include "ItemManager.h"

#include "Character.h"
#include "Player.h"

using namespace std;

class Enemy : public Character
{
    private:
        EnemyType enemyType;            // Enemy type (See: _Global)
        MovementType movementType;      // Movement behavior (See: _Global)
        float sightDistance;            // How far it can see the player
        float defSpeed;                 // "Default speed". What speed resets to.
    public:
        Enemy();
        void Init( string& type, int x, int y );
        void Update( Player& player );
        void DecidePath( SDL_Rect& playerCoord );
        void DecideShoot( Player& player );
        void Die();
};
