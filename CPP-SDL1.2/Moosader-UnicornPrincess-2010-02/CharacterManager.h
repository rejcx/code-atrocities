/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

CharacterManager.h
*/

#pragma once

#include <iostream>
#include <fstream>

#include "_ErrorCodes.h"
#include "_Global.h"

#include "ImageManager.h"

#include "Enemy.h"
#include "Player.h"

using namespace std;

class CharacterManager
{
    private:
        // Singleton instance
        static CharacterManager* inst;
        CharacterManager();

        // Class variables
        int enemyCount;
    public:
        // Singleton functions
        static CharacterManager& GetInst();
        static CharacterManager* InstPtr();

        // Initialization / Constructors / Destructors
        void Init();

        // Get / Set & other accessor functions
        int EnemyCount();
        void EnemyCount( int val );

        // Misc functions
        void CreateEnemy( string& type, int x, int y );
        void DrawCharacters();
        void UpdateCharacters();
        void AddOneToEnemyArray();

        // Class variables
        Enemy* enemy;       // Dynamic array
        Player player;
};
