/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

Image.h
*/

#pragma once
#include <iostream>
#include <fstream>
#include <string>

#include "_ErrorCodes.h"
#include "_Global.h"

using namespace std;

class Image
{
    private:
        string szID;        // String ID
        int iID;            // Integer ID (index)
        bool scrolling;     // Scrolls in background?
    public:
        // Initialization / Constructors / Destructors
        ~Image();
        void Init( int index, string& gfxDir );
        void LoadImage( string& gfxDir );

        // Get / Set & other accessor functions
        string StrID();
        int IntID();
        int W();
        int H();
        bool Scrolling();
        void Scrolling( bool val );

        // Class variables
        SDL_Surface *img;
};


