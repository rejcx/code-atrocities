/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

Player.h
*/

#pragma once

#include "_ErrorCodes.h"
#include "_Global.h"

#include "ImageManager.h"

#include "Character.h"

using namespace std;

class Player : public Character
{
    private:
        float deathCooldownTime;        // Max. amount of time to wait for player to ressurrect
        float deathCooldownLeft;        // Current amount of time left until player respawns (if dead)
    public:
        Player();
        void Init();
        void ReInit();
        void Die();
        void Update();
        void GetItem( ItemType item );
        void Move( Direction direction );
        void IncrementFrame();
};
