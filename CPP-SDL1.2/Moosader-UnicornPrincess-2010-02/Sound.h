/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

Sound.h
*/

#pragma once
#include <iostream>
#include <fstream>
#include <string>

#include "SDL/SDL.h"
#include "SDL/SDL_mixer.h"

#include "_ErrorCodes.h"
#include "_Global.h"

using namespace std;

class Sound
{
    private:
        string szID;        // String ID
        int iID;            // Integer ID (index)

    public:
        // Initialization / Constructors / Destructors
        ~Sound();
        void Init( int index, string& sndDir );
        void LoadSound( string& sndDir );

        // Get / Set & other accessor functions
        string StrID();
        int IntID();

        // Class variables
        Mix_Chunk* snd;
};

