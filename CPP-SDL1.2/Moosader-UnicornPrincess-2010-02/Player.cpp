/*
-----------------
Unicorn Princess
Iteration 1
Rachel J. Morris
www.moosader.com
Dec 2009-Feb 2010
-----------------

Player.cpp
*/

#include "Player.h"

Player::Player()
{
    SetCoordinates( -2, -2, 2, 2 );
    SetColReg( -2, -2, 2, 2 );
}

void Player::Init()
{
    imgId = 4;  // Unicorn princess image
    SetCoordinates( 0, 768/2, 128, 128 );
    SetColReg( 29, 30, 72, 76 );
    active = true;

    speed = 5.0;

    action = FLYING;
    frame = 0;
    dir = RIGHT;
    bulletType = bSPARKLE;
    gunDamage = ENEMY;
    dying = false;

    deathCooldownTime = 50;         // max amt of time to respawn from death
    shootCooldownTime = 20;
}

void Player::ReInit()
{
    imgId = 4;  // Unicorn princess image
    SetCoordinates( 0, 768/2, 128, 128 );
    SetColReg( 29, 30, 72, 76 );
    active = true;

    speed = 5.0;

    action = FLYING;
    frame = 0;
    dir = RIGHT;
    gunDamage = ENEMY;
    dying = false;

    deathCooldownTime = 50;         // max amt of time to respawn from death
    shootCooldownTime = 20;
}

void Player::Die()
{
    if ( active )
    {
        dying = true;
        frame = 0.0;
        action = DYING;
    }
}

void Player::IncrementFrame()
{
    frame += 0.2;
    if ( frame > MAXFRAME )
    {
        frame = 0;

        if ( dying )
        {
            active = false;
            dying = false;
            deathCooldownLeft = deathCooldownTime;
        }

        if ( action == SHOOTING )
        {
            action = FLYING;
        }
    }
}

void Player::Update()
{
    if ( active )
    {
        IncrementFrame();

        if ( shootCooldownLeft > 0 )
        {
            shootCooldownLeft -= 1;
        }
    }

    if ( deathCooldownLeft > 0 )
    {
        deathCooldownLeft -= 1;
    }

    if ( active == false && deathCooldownLeft <= 0 )
    {
        active = true;
        action = FLYING;
        // Reset coordinates
        SetCoordinates( 0, 768/2 );
    }
}

void Player::GetItem( ItemType item )
{
    //        enum BulletType { bSPARKLE=0, bFLOWER=1, bGEM=2, bHEART=3, bBULLET=4, bSMALLBULLET=5 };
    //        enum ItemType { iFLOWER=0, iGEM=1, iHEART=2, iONEUP=3 };
    switch( item )
    {
        case iFLOWER:
            bulletType = bFLOWER;
        break;

        case iGEM:
            bulletType = bGEM;
        break;

        case iHEART:
            bulletType = bHEART;
        break;

        case iONEUP:
        break;
    }
}

void Player::Move( Direction direction )
{
    if ( active && !dying )
    {
        // Move a character in a given direction, based on it's speed
        if ( direction == UP && coord.y - speed >= 0 )
        {
            coord.y = (int)(coord.y - speed);
        }
        else if ( direction == DOWN && coord.y + speed <= GameSys::GetInst().ScreenHeight() + coord.h )
        {
            coord.y = (int)(coord.y + speed);
        }
        else if ( direction == LEFT && coord.x - speed >= 0 )
        {
            coord.x = (int)(coord.x - speed );
        }
        else if ( direction == RIGHT && coord.y + speed <= GameSys::GetInst().ScreenWidth() + coord.w  )
        {
            coord.x = (int)(coord.x + speed );
        }
    }
}

