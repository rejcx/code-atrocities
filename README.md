# Code Atrocities

## This code is useless
This is a warning - *this is not good code to learn from, or to use for your projects*.
The only learning value here would be to see bad ways to code things,
or to learn more about my evolution over the years.

This includes old games, class projects, and C++ thingamabobs.
POSIX multithreading, OpenGL, MIPS, etc.
