#include <iostream>
#include <ctime>
#include <iomanip>
#include <cmath>
#include <fstream>
#include "stopwatch.h"
using namespace std;

/*
PROJECT 1
CS 352 Data Structures and Algorithm Analysis

Rachel J. Morris
CS 352, TR 12:30 - 1:45
Due Feb 21, 2008
*/

void generateRand_1(int[], int);
// in : the array, and the arraySize
// out : the array full of random numbers
void generateRand_2(int[], int);
// in : the array, and the arraySize
// out : the array full of random numbers
void generateRand_3(int[], int);
// in : the array, and the arraySize
// out : the array full of random numbers
void swap(int*, int*);
// in : the address of two elements of the array
// out : the two elements are swapped
void displayStats(int, double, double);
// in : arraySize, time it took to generate, and weird formula thing from table.
// out : outputs to screen a row in the table.

int main()
{
    system("cls");  //clears dos prompt
    int arraySize = 250;
    int i;
    srand((unsigned)time(NULL));
    cout<<"\tALGORITHM #1"<<endl;                                       //Table header
    cout<<"------------------------------------------------"<<endl;
    cout<<"N"<<"\t\t"<<"T"<<"\t\t"<<"T/(N^2*logN)"<<endl;
    cout<<"------------------------------------------------"<<endl;
    for (i=0; i<4; i++)
    {
        int array[arraySize];
        generateRand_1(array, arraySize);
        arraySize *= 2;
    }
    cout<<"\n\tALGORITHM #2"<<endl;                                       //Table header
    cout<<"------------------------------------------------"<<endl;
    cout<<"N"<<"\t\t"<<"T"<<"\t\t"<<"T/(N*logN)"<<endl;
    cout<<"------------------------------------------------"<<endl;
    arraySize = 2500;
    for (i=0; i<6; i++)
    {
        int array[arraySize];
        generateRand_2(array, arraySize);
        arraySize *= 2;
    }
    cout<<"\n\tALGORITHM #3"<<endl;                                       //Table header
    cout<<"------------------------------------------------"<<endl;
    cout<<"N"<<"\t\t"<<"T"<<"\t\t"<<"T/N"<<endl;
    cout<<"------------------------------------------------"<<endl;
    arraySize = 10000;
    for (i=0; i<6; i++) // was i=0 to 8, program crashes when int array[640000] called.  See project report.
    {
        int array[arraySize];
        generateRand_3(array, arraySize);
        arraySize *= 2;
    }

    system("pause");
    return 0;
}

void generateRand_1(int array[], int arraySize)
{
    Stopwatch stopWatch;
    bool bad = false;       //a flag for checking to see if a certain number was already in another element.
    stopWatch.start();
    for (int i=0; i<arraySize; i++)
    {
        do
        {
            array[i] = (int)(rand()*rand() % arraySize);  //Assign a random value to array[i]
            bad = false;                        //set bad flag to false
            for (int j=0; j<i; j++)           //check each element up to current one
            {
                if ((int)array[i] == (int)array[j])       //if there is already one similar
                {
                    bad = true;                 //make bad flag true.
                }
            }
        } while ( bad );                        //keep assigning new numbers until it's not bad.
    }
    stopWatch.stop();
    displayStats(arraySize, stopWatch.user(), stopWatch.user()/(arraySize * arraySize * log10(arraySize)));
}

void generateRand_2(int array[], int arraySize)
{
    Stopwatch stopWatch;
    int i;                          //for the loops
    bool used[arraySize];            //array to hold whether a number has been used
    for (i=0; i<arraySize; i++) { used[i] = false; }     //initialize array

    for (i=0; i<arraySize; i++)
    {
        array[i] = (int)(rand()*rand() % arraySize);               //Assign random value
        if ( i > 0 )
        {
            //Check to see if that number was already used
            while ( used[ array[i] ] == true )
            {
                array[i] = (int)(rand()*rand() % arraySize);
            }
        }
        used[ array[i] ] = true;
    }
    stopWatch.stop();
    displayStats(arraySize, stopWatch.user(), stopWatch.user()/(arraySize * log10(arraySize)));
}

void generateRand_3(int array[], int arraySize)
{
    Stopwatch stopWatch;
    int i;
    for (i=0; i<arraySize; i++)      //fill array first
    {
        array[i] = i+1;
    }
    stopWatch.start();
    for (i=1; i<arraySize; i++)      //now randomly swap stuff
    {
        swap(array[i], array[ array[i] = (int)(rand()*rand() % arraySize) ]);
    }
    stopWatch.stop();
    displayStats(arraySize, stopWatch.user(), stopWatch.user()/arraySize);
}

void swap(int *num1, int *num2)
{
    int temp;           //stores the value of one of the arguments
    temp = *num1;
    *num1 = *num2;
    *num2 = *num1;
}

void displayStats(int N, double T, double ratio)
{
    cout<<setprecision(10);
    cout<<N<<"\t\t"<<T<<"\t\t"<<ratio<<endl;
}
