#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include "Table.h"

using namespace std;

class InOut
{
    private:
        string input;
    public:
        InOut();
        ~InOut();
        string OpenAndGet( string filename );
        string OpenAndGet();
        void OpenAndWrite( string filename, string data );
        void OpenAndWrite( string filename, Table data[], int maxIndex, Table symTable[] );
        void OutputError( string filename );
};
