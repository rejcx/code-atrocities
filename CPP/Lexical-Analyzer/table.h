#pragma once
#include <iostream>
#include <string>
using namespace std;

enum { LETTER = 10, DIGIT = 11, OPERATOR = 12, DELIMITER = 13, NEWLINE = 14, ILLEGAL = 15, IDENTIFIER = 666, ASSIGN = 500, EQUAL = 501, LESS = 502, LESSEQUAL = 503, GREATER = 504, GREATEREQUAL = 505, NOTEQUAL = 506 };

class Table
{
    public:
        string szID;
        int id;
        string value;
        void Set( int type );
        void SetValue( string val );
        void InitBlank();
};