#include "Table.h"

void Table::Set( int type )
{
    if ( type == LETTER )         { szID = "LETTER"; }
    else if ( type == DIGIT )     { szID = "DIGIT"; }
    else if ( type == OPERATOR )  { szID = "OPERATOR"; }
    else if ( type == DELIMITER ) { szID = "DELIMITER"; }
    else if ( type == NEWLINE )   { szID = "NEWLINE"; }
    else if ( type == ILLEGAL )   { szID = "ILLEGAL"; }
    else if ( type == IDENTIFIER ){ szID = "IDENTIFIER"; }
    id = type;
}

void Table::SetValue( string val )
{
    value = val;
}

void Table::InitBlank()
{
    id = -1;
    szID = value = "NULL";
}