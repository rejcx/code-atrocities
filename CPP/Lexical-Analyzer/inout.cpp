#include "InOut.h"

InOut::InOut()
{
}

InOut::~InOut()
{
}

string InOut::OpenAndGet( string filename )
{
    string tempA, tempB;
    ifstream infile;
    infile.open( filename.c_str() );
    if ( infile == NULL )
    {
        cerr<<"InOut.OpenAndGet : Could not open "<<filename<<endl;
        system("pause");
        exit(20);
    }
    while ( infile.good() && infile.eof() == false )
    {
        infile>>tempB;
        tempA += tempB;
        tempA += ' ';
    }
    infile.close();
    return tempA;
}

string InOut::OpenAndGet()
{
    string filename;
    cout<<"Please enter the program filename.  Please remember the .txt at the end!"<<endl;
    cout<<"--> ";
    cin>>filename;
    string temp;
    temp = OpenAndGet( filename );
    return temp;
}

void InOut::OpenAndWrite( string filename, string data )
{
    ofstream outfile;
    outfile.open( filename.c_str() );
    if ( outfile == NULL )
    {
        cerr<<"InOut.OpenAndWrite : Could not open "<<filename<<endl;
        exit(21);
    }
    outfile<<data;
}

void InOut::OpenAndWrite( string filename, Table data[], int maxIndex, Table symTable[] )
{
    ofstream outfile;
    filename += "_output.txt";
    outfile.open( filename.c_str() );
    if ( outfile == NULL )
    {
        cerr<<"InOut.OpenAndWrite : Could not open "<<filename<<endl;
        exit(21);
    }
    for ( int i=0; i<maxIndex; i++ )
    {
        outfile<<"\tType:"<<data[i].szID<<"\n\tCode:"<<data[i].id<<"\n\tValue:"<<data[i].value<<endl<<endl;
    }
    outfile<<endl;
    outfile<<"--------------"<<endl;
    outfile<<" SYMBOL TABLE "<<endl;
    outfile<<"--------------"<<endl;
    outfile<<endl;
    //whee
    for ( int i=0; i<1000; i++ )
    {
        if ( symTable[i].id != -1 )
            outfile<<"++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;
        else
            outfile<<"-------------------------------"<<endl;
        outfile<<"\tINDEX = "<<i<<endl;
        outfile<<"Type:  "<<symTable[i].szID<<endl;
        outfile<<"Value: "<<symTable[i].value<<endl<<endl;
    }
    outfile.close();
}

void InOut::OutputError( string filename )
{
    ofstream outfile;
    filename += "_output.txt";
    outfile.open( filename.c_str() );
    outfile<<"Error creating program output - too many errors."<<endl;
    outfile<<"To read error codes, try compiling again, codes will be"<<endl;
    outfile<<"Displayed to prompt screen"<<endl;
    outfile.close();
}
