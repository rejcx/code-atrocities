#include <iostream>
#include <string>
#include <fstream>
#include "InOut.h"
#include "Table.h"
using namespace std;

/*
Lexical Analyzer v0.3 -
Rachel J. Morris
*/

void PopulateMap( int MAP[] );

//enum { LETTER = 10, DIGIT = 11, OPERATOR = 12, DELIMITER = 13, NEWLINE = 14, ILLEGAL = 15, IDENTIFIER = 666, ASSIGN = 500, EQUAL = 501, LESS = 502, LESSEQUAL = 503, GREATER = 504, GREATEREQUAL = 505, NOTEQUAL = 506 };
//Above enum is in Table.h
enum State { INITIAL_STATE, ID_STATE, NUMBER_STATE };

void HashAndStore( Table baseTable[], Table symTable[], int amt );

int main()
{
    InOut file;
    string szProgram;
    string filename, tempFile;
    int i = 0, lineCount = 0, j = 0;
    int MAP[256];
    PopulateMap( MAP );
    State state = INITIAL_STATE;
    bool programError = false;
    Table symbolTable[ 1000 ];
    Table table[ 1000 ];

    for ( int i=0; i<1000; i++ )
    {
        symbolTable[i].InitBlank();
        table[i].InitBlank();
    }
    int tableIndex = 0;
    filename = "program";

    cout<<"Please enter a program filename, EXCLUDING .txt at the end"<<endl;
    cout<<"--> ";
    cin>>filename;
    tempFile = filename;
    tempFile += ".txt";

    cout<<"Opening and reading in program: "<<tempFile<<endl;
    szProgram = file.OpenAndGet( tempFile.c_str() );  //add a filename parameter to make it automated

    // Program is now stored in the string szProgram, we do not need
    // to keep the file open any longer.

    /* You aren't very clear on what "ID[10]" is in the notes.  the ID array
    Sort of pops up out of nowhere and I have no idea if you're storing characters
    in a big array before we hash it to put it in the symbol table or what.     */

    //Now we want to read each character and identify tokens and such.
    i=0;
    int token;

    cout<<"Parsing program data..."<<endl;

    while ( i < szProgram.size() )          //using strings instead of characters
    {
        token = MAP[ szProgram[i] ];
        switch( token )
        {
            case LETTER:
                if ( state == INITIAL_STATE || state == ID_STATE )
                {
                    state = ID_STATE;
                    //check stuff
                    string tempWord;
                    j = i;
                    tempWord = szProgram[i];
                    while ( MAP[ szProgram[++j] ] == LETTER )
                    {
                        if ( szProgram[j] != ' ' )
                            tempWord += szProgram[j];
                    }
                    j--;        //go back one
                    //identifier spans from i to j
                    if ( j - i > 10 )
                    {
                        programError = true;
                        cerr<<"\nERROR: variable name ";
                        for ( int k = i; k<j; k++ )
                        {
                            cerr<<szProgram[k];
                        }
                        cerr<<" exceeds maximum identifier name\n limit of (10 characters)"<<endl;
                    }
                    i = j;      //shift index forward
                    //store in table
                    table[ tableIndex ].SetValue( tempWord );
                    table[ tableIndex++ ].Set( IDENTIFIER );
                }
                else
                {
                    cerr<<"\nERROR: Unexpected value in program, character #"<<i<<": "<<szProgram[i]<<endl;
                    cerr<<"State is "<<state<<", expected to be INITIAL_STATE("<<INITIAL_STATE<<") or ID_STATE ("<<ID_STATE<<")."<<endl;
                    programError = true;
                }
            break;
            case DIGIT:
                if ( state == INITIAL_STATE || state == NUMBER_STATE )
                {
                    state = NUMBER_STATE;
                    //check stuff
                    string tempWord;
                    j = i;
                    tempWord = szProgram[i];
                    while ( MAP[ szProgram[++j] ] == DIGIT )
                    {
                        tempWord += szProgram[j];
                    }
                    j--;        //go back one
                    //identifier spans from i to j
                    i = j;      //shift index forward
                    //store in table
                    table[ tableIndex ].SetValue( tempWord );
                    table[ tableIndex++ ].Set( DIGIT );
                }
                else
                {
                    cerr<<"\nERROR: Unexpected value in program, character #"<<i<<": "<<szProgram[i]<<endl;
                    cerr<<"State is "<<state<<", expected to be INITIAL_STATE("<<INITIAL_STATE<<") or NUMBER_STATE ("<<NUMBER_STATE<<")."<<endl;
                    programError = true;
                }
            break;
            case NEWLINE:
                lineCount++;
                //no break, fall through...
            case DELIMITER:
                if ( state == ID_STATE )
                {
                    //announce identifier
                    string tempWord;
                    tempWord = szProgram[i];
                    table[ tableIndex ].SetValue( tempWord );
                    table[ tableIndex++ ].Set( IDENTIFIER );
                }
                else if ( state == NUMBER_STATE )
                {
                    //announce number
                    string tempWord;
                    tempWord = szProgram[i];
                    table[ tableIndex ].SetValue( tempWord );
                    table[ tableIndex++ ].Set( DIGIT );
                }
                state = INITIAL_STATE;
            break;
            case ILLEGAL:
                state = INITIAL_STATE;
                cerr<<"\nERROR: Illegal value, character #"<<i<<": "<<szProgram[i]<<endl;
                programError = true;
            break;
            case OPERATOR:
                if ( state == ID_STATE )
                {
                    //announce identifier
                    string tempWord;
                    tempWord = szProgram[i];
                    table[ tableIndex ].SetValue( tempWord );
                    table[ tableIndex++ ].Set( IDENTIFIER );
                }
                else if( state == NUMBER_STATE )
                {
                    //announce number
                    string tempWord;
                    tempWord = szProgram[i];
                    table[ tableIndex ].SetValue( tempWord );
                    table[ tableIndex++ ].Set( DIGIT );
                }
                state = INITIAL_STATE;
                switch( szProgram[i] )
                {
                    case '+':
                        if ( szProgram[i+1] == '+' )    //This is ++
                        {
                            //announce operator
                            string temp;
                            temp = szProgram[i];
                            temp += szProgram[i+1];
                            table[ tableIndex ].SetValue( temp );
                            table[ tableIndex++ ].Set( OPERATOR );
                            i++;    //makes up for not having extra "getchar"
                        }
                        else if ( MAP[szProgram[i+1]] == LETTER ||
                                    MAP[szProgram[i+1]] == DIGIT ||
                                    szProgram[i+1] == ' ' )
                        {
                            // This is +
                            //announce operator
                            string tempWord;
                            tempWord = szProgram[i];
                            table[ tableIndex ].SetValue( tempWord );
                            table[ tableIndex++ ].Set( OPERATOR );
                        }
                        else
                        {
                            //Next character is not valid
                            cerr<<"\nERROR: Unexpected value in program, characters #"<<i<<","<<i+1<<": "<<szProgram[i]<<szProgram[i+1]<<endl;
                            cerr<<"Possible values are ++ and +, or a number,\n whitespace or variable directly afterwards"<<endl;
                            programError = true;
                        }
                    break;
                    case '-':
                        if ( szProgram[i+1] == '-' )    //This is --
                        {
                            //announce operator
                            string temp;
                            temp = szProgram[i];
                            temp += szProgram[i+1];
                            table[ tableIndex ].SetValue( temp );
                            table[ tableIndex++ ].Set( OPERATOR );
                            i++;    //makes up for not having extra "getchar"
                        }
                        else if ( MAP[szProgram[i+1]] == LETTER ||
                                    MAP[szProgram[i+1]] == DIGIT ||
                                    szProgram[i+1] == ' ' )
                        {
                            // This is -
                            //announce operator
                            string tempWord;
                            tempWord = szProgram[i];
                            table[ tableIndex ].SetValue( tempWord );
                            table[ tableIndex++ ].Set( OPERATOR );
                        }
                        else
                        {
                            //Next character is not valid
                            cerr<<"\nERROR: Unexpected value in program, characters #"<<i<<","<<i+1<<": "<<szProgram[i]<<szProgram[i+1]<<endl;
                            cerr<<"Possible values are -- and -, or a number,\n whitespace or variable directly afterwards"<<endl;
                            programError = true;
                        }
                    break;
                    case '=':
                        if ( szProgram[i+1] == '=' )    //This is ==
                        {
                            //announce operator
                            string temp;
                            temp = szProgram[i];
                            temp += szProgram[i+1];
                            table[ tableIndex ].SetValue( temp );
                            table[ tableIndex++ ].Set( OPERATOR );
                            i++;    //makes up for not having extra "getchar"
                        }
                        else if ( MAP[szProgram[i+1]] == LETTER ||
                                    MAP[szProgram[i+1]] == DIGIT ||
                                    szProgram[i+1] == ' ' )
                        {
                            // This is =
                            //announce operator
                            string tempWord;
                            tempWord = szProgram[i];
                            table[ tableIndex ].SetValue( tempWord );
                            table[ tableIndex++ ].Set( OPERATOR );
                        }
                        else
                        {
                            //Next character is not valid
                            cerr<<"\nERROR: Unexpected value in program, characters #"<<i<<","<<i+1<<": "<<szProgram[i]<<szProgram[i+1]<<endl;
                            cerr<<"Possible values are == and =, or a number,\n whitespace or variable directly afterwards"<<endl;
                            programError = true;
                        }
                    break;
                    case '>':
                        if ( szProgram[i+1] == '=' )    //This is >=
                        {
                            //announce operator
                            string temp;
                            temp = szProgram[i];
                            temp += szProgram[i+1];
                            table[ tableIndex ].SetValue( temp );
                            table[ tableIndex++ ].Set( OPERATOR );
                            i++;    //makes up for not having extra "getchar"
                        }
                        else if ( MAP[szProgram[i+1]] == LETTER ||
                                    MAP[szProgram[i+1]] == DIGIT ||
                                    szProgram[i+1] == ' ' )
                        {
                            // only >
                            //announce operator
                            string tempWord;
                            tempWord = szProgram[i];
                            table[ tableIndex ].SetValue( tempWord );
                            table[ tableIndex++ ].Set( OPERATOR );
                        }
                        else    //Error
                        {
                            //Next character is not valid
                            cerr<<"\nERROR: Unexpected value in program, characters #"<<i<<","<<i+1<<": "<<szProgram[i]<<szProgram[i+1]<<endl;
                            cerr<<"Possible values are >= and >, or a number,\n whitespace or variable directly afterwards"<<endl;
                            programError = true;
                        }
                    break;
                    case '<':
                         if ( szProgram[i+1] == '=' )    //This is <=
                        {
                            //announce operator
                            string temp;
                            temp = szProgram[i];
                            temp += szProgram[i+1];
                            table[ tableIndex ].SetValue( temp );
                            table[ tableIndex++ ].Set( OPERATOR );
                            i++;    //makes up for not having extra "getchar"
                        }
                        else if ( MAP[szProgram[i+1]] == LETTER ||
                                    MAP[szProgram[i+1]] == DIGIT ||
                                    szProgram[i+1] == ' ' )
                        {
                            //This is only <
                            //announce operator
                            string temp;
                            temp = szProgram[i];
                            table[ tableIndex ].SetValue( temp );
                            table[ tableIndex++ ].Set( OPERATOR );
                        }
                        else    //This is incorrect
                        {
                            //Next character is not valid
                            cerr<<"\nERROR: Unexpected value in program, characters #"<<i<<","<<i+1<<": "<<szProgram[i]<<szProgram[i+1]<<endl;
                            cerr<<"Possible values are <= and <, or a number,\n whitespace or variable directly afterwards"<<endl;
                            programError = true;
                        }
                    break;
                    case '!':
                        if ( szProgram[i+1] == '=' )    //This is !=
                        {
                            //announce operator
                            string temp;
                            temp = szProgram[i];
                            temp += szProgram[i+1];
                            table[ tableIndex ].SetValue( temp );
                            table[ tableIndex++ ].Set( OPERATOR );
                            i++;    //makes up for not having extra "getchar"
                        }
                        else
                        {
                            //lexical error
                            cerr<<"\nERROR: Illegal value, character #"<<i<<": "<<szProgram[i]<<szProgram[i+1]<<endl;
                            cerr<<"Expected !="<<endl;
                            table[ tableIndex++ ].Set( ILLEGAL );
                            programError = true;
                        }
                    break;
                }
            break;
        }
        i++;
    }

    if ( programError )
    {
        cout<<"\nLexical error!\nCannot create output file with bad program data."<<endl;
        file.OutputError( filename );
        system("pause");
        exit(20);
    }
    else
    {
        cout<<"Populating hash table..."<<endl;
        //Store into hash table
        HashAndStore( table, symbolTable, tableIndex );

        cout<<"Writing output file..."<<endl;
        //Output data aquired
        file.OpenAndWrite( filename, table, tableIndex, symbolTable );
    }

    cout<<endl<<"Program finished"<<endl;

    system("pause");
    return 0;
}

void PopulateMap( int MAP[] )
{
    int i = 0;
    for ( i=0; i<256; i++ )
    {
        MAP[i] = ILLEGAL;   //init
    }
    for ( i=0; i<26; i++ )
    {
        MAP['a'+i] = LETTER;
        MAP['A'+i] = LETTER;
    }
    for ( i=0; i<10; i++ )
    {
        MAP['0'+i] = DIGIT;
    }

    MAP['+'] = OPERATOR;
    MAP['-'] = OPERATOR;
    MAP['='] = OPERATOR;
    MAP['<'] = OPERATOR;
    MAP['>'] = OPERATOR;
    MAP['!'] = OPERATOR;
    MAP[':'] = DELIMITER;
    MAP[';'] = DELIMITER;
    MAP[' '] = DELIMITER;
    MAP['{'] = DELIMITER;
    MAP['}'] = DELIMITER;
    MAP['\n'] = NEWLINE;
}

void HashAndStore( Table baseTable[], Table symTable[], int amt )
{
    int idValue = -1;
    int hash = -1;
    int index = 0;
    for ( int i=0; i<amt; i++ )
    {
        index = 0;
        idValue = -1;
        while ( index <= baseTable[i].value.size() )
        {
            idValue = idValue + baseTable[i].value[index] * 256 + baseTable[i].value[index+1];
            index += 2;
        }
        if ( idValue != -1 )        //store in table
        {
            hash = idValue % 997;
            symTable[ hash ].value = baseTable[i].value;
            symTable[ hash ].id = baseTable[i].id;
            symTable[ hash ].szID = baseTable[i].szID;
        }
    }
}







