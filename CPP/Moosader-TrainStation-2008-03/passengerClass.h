#pragma once
#include <iomanip>
#include <string>
using namespace std;

class rjm_passengerClass
{
    private:
        string first_name;
        string last_name;
        string station_start;
        string station_destination;
        int station_start_index;
        int station_destination_index;
        int time_start;
        int boarding_time;
        int station_end;
        bool on_train;
    public:
        rjm_passengerClass();
        friend istream& operator>>( istream&, rjm_passengerClass& );
        friend ostream& operator<<( ostream&, const rjm_passengerClass& );
        void set_station_start( string value );
        void set_station_destination( string value );
        void set_on_train( bool value ) { on_train = value; }
        bool get_on_train() { return on_train; }
};

rjm_passengerClass::rjm_passengerClass()
{
    on_train = false;
}

istream& operator>>( istream &input, rjm_passengerClass &passenger )
{
    string temp;
    input>>passenger.first_name;
    input>>passenger.last_name;
    cout<<temp<<endl;
    input>>temp;
    passenger.set_station_start( temp );
    input>>temp;
    passenger.time_start = atoi( temp.c_str() );
    input>>temp;
    passenger.set_station_destination( temp );
    return input;
}

ostream& operator<<( ostream &output, const rjm_passengerClass &passenger )
{
    output<<passenger.first_name<<setw(15)<<passenger.last_name;
    output<<endl;
    return output;
}

void rjm_passengerClass::set_station_start( string value )
{
    station_start = value;
    if ( value == "Olathe" )                { station_start_index = 0; }
    else if ( value == "College" )          { station_start_index = 1; }
    else if ( value == "SantaFe" )          { station_start_index = 2; }
    else if ( value == "Merriam" )          { station_start_index = 3; }
    else if ( value == "Argentine" )        { station_start_index = 4; }
    else if ( value == "KCK" )              { station_start_index = 5; }
    else if ( value == "Parkville" )        { station_start_index = 6; }
    else if ( value == "Riverside" )        { station_start_index = 7; }
    else if ( value == "Gladstone" )        { station_start_index = 8; }
    else if ( value == "ZonaRosa" )         { station_start_index = 9; }
    else if ( value == "TiffanySprings" )   { station_start_index = 10; }
    else if ( value == "Airport" )          { station_start_index = 11; }
}

void rjm_passengerClass::set_station_destination( string value )
{
    station_destination = value;
    if ( value == "Olathe" ) { station_destination_index = 0; }
    else if ( value == "College" )          { station_destination_index = 1; }
    else if ( value == "SantaFe" )          { station_destination_index = 2; }
    else if ( value == "Merriam" )          { station_destination_index = 3; }
    else if ( value == "Argentine" )        { station_destination_index = 4; }
    else if ( value == "KCK" )              { station_destination_index = 5; }
    else if ( value == "Parkville" )        { station_destination_index = 6; }
    else if ( value == "Riverside" )        { station_destination_index = 7; }
    else if ( value == "Gladstone" )        { station_destination_index = 8; }
    else if ( value == "ZonaRosa" )         { station_destination_index = 9; }
    else if ( value == "TiffanySprings" )   { station_destination_index = 10; }
    else if ( value == "Airport" )          { station_destination_index = 11; }
}


