#pragma once

class rjm_timeClass
{
    private:
    int time;
    public:
    void increment_time() { time++; }
    int elapsed_time() { return time; }
    rjm_timeClass() { time = 0; }
};
