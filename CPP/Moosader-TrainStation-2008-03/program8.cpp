/*********************************************
Name:       | Rachel J. Morris
Course:     | CS 201
Program:    | Program 8
Due Date:   | 12/09/2007
*********************************************/

#include <iostream>
#include <fstream>
#include "timeClass.h"
#include "listClass.h"
#include "passengerClass.h"
#include "stationClass.h"
#include "trainClass.h"
using namespace std;

const int station_amt = 12;

int main()
{
    listClass <rjm_passengerClass> lst_passenger;
    rjm_passengerClass passenger;
    rjm_stationClass station[station_amt];
    ifstream infileA, infileB;
    infileA.open("passengers.txt");
    infileB.open("stations.txt");
    if ( infileA.fail() || infileB.fail() )
    {
        cerr<<"\n Unable to open input file :( \n";
        exit(1);
    }
    int i=0;
    //Load in passenger info
    while ( infileA >> passenger )
    {
        lst_passenger.addItemToTail( passenger );
    }
    //Load in station info
    for ( i=0; i<station_amt; i++ ) { infileB>>station[i]; }
    infileA.close();
    infileB.close();
	return 0;
};
