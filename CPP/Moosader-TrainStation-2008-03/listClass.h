#pragma once
#include <iostream>
using namespace std;

// This linked-list template is the one I wrote in
// program 6, except with addItemToHead (and tail) instead of addItemInOrder

template <class T>
class listClass;

template <class T>
class nodeClass
{
    public:
    template<class> friend class listClass;
    nodeClass() { next = NULL; }
    private:
    T data;
    nodeClass *next;
};

template <class T>
class listClass
{
    public:
    listClass() { head = tail = NULL; }   //Normal constructor
    listClass( const listClass &source );   //Copy constructor
    ~listClass();  //Destructor
    const listClass &operator=( const listClass &right );
    void addItemToHead( const T &item );
    void addItemToTail( const T &item );
    const T &getFirst() const;
    const T &getLast() const;
    void removeFirst();
    void removeLast();
    bool isFull()const { return false; }
    bool isEmpty()const { return head == NULL; }
    void print( ostream &out ) const;
    void addItemInOrder( T &item );
    T& listClass::operator=( const T& right );

    private:
    void free();
    nodeClass<T> *head, *tail;
};

template <class T>
listClass<T>::listClass( const listClass &source )
{
    head = tail = NULL;
    *this = source;
}

template <class T>
const listClass<T>& listClass<T>::operator= ( const listClass &right )
{
    if ( this == &right ) { return *this; }
    free();
    nodeClass<T> *ptr = right.head;
    while ( ptr != NULL )
    {
        addItemInOrder( ptr -> data );
        ptr = ptr -> next;
    }
    return *this;
}

template <class T>
listClass<T>::~listClass()
{
    free();
}

template <class T>
void listClass<T>::addItemToHead( const T &item )
{
    nodeClass<T> *ptr = new nodeClass<T>;
    ptr->data = item;
    ptr->next = head;
    head = ptr;
    if ( tail == NULL ) { tail = ptr; }
}

template <class T>
void listClass<T>::addItemToTail( const T &item )
{
    if ( isEmpty() )
    {
        addItemToHead( item );
        return;
    }
    nodeClass<T> *ptr = new nodeClass<T>;
    ptr -> data = item;
    tail -> next = ptr;
    tail = ptr;
}

template <class T>
void listClass<T>::removeFirst()
{
    if ( isEmpty() ) { cerr<<"empty list\n"; exit(41); }
    nodeClass<T> *ptr = head;
    head = head -> next;
    delete ptr;
    if ( head == NULL ) { tail = NULL; }
}

template <class T>
void listClass<T>::removeLast()
{
    if ( isEmpty() ) { cerr<<"empty list\n"; exit(41); }
    if ( head == tail ) { removeFirst(); }
    else
    {
        nodeClass<T> *ptr = head;
        while ( ptr -> next -> next != NULL) { ptr = ptr -> next; }
        delete tail;
        tail = ptr;
        tail -> next = NULL;
    }
}

template <class T>
void listClass<T>::free()
{
    while ( !isEmpty() ) { removeFirst(); }
}

template <class T>
void listClass<T>::print( ostream &out )const
{
    int i = 0;
    nodeClass<T> *ptr = head;
    while ( ptr != NULL )
    {
        out << ptr -> data << endl;
        ptr = ptr -> next;
        i++;
    }
}


