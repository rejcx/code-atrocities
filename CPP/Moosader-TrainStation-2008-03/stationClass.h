#pragma once
#include <string>
using namespace std;

class rjm_stationClass
{
    private:
        string station_name;
        float dist_from_last;
    public:
        friend istream& operator>>( istream&, rjm_stationClass& );  //insertion
        friend ostream& operator<<( ostream&, const rjm_stationClass& ); //extraction
};

istream& operator>>( istream &input, rjm_stationClass &station )
{
    string temp;
    input>>temp;
    station.station_name = temp;
    input>>temp;
    station.dist_from_last = atof( temp.c_str() );
    return input;
}

ostream& operator<<( ostream &output, const rjm_stationClass &station )
{
    output<<"station "<<station.station_name;
    return output;
}
