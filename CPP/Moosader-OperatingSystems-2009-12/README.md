CS 431 - Operating Systems 

UMKC Fall, 2009

Rachel J. Morris, [Moosader.com](http://www.moosader.com/)

## ABOUT
This is a bundle of some POSIX Multithreading 
assignments I worked on throughout the Fall
semester of 2009 for my OS class with Cotter.

## CHEATING IS BAD, MMK?
This code has been uploaded to my portfolio
page to display some knowledge of multithreading 
to potential employers.

*If you happen to be struggling with CS431 and
are contemplating copying my code and trying 
to pass it as your own-- don't.* 

For one, teachers have software that stores
assignments and checks turn-ins against things
that have already been turned in, so it's just
a dumb thing to do (c'mon, we're CS, you should
know better.)

*If you're struggling with your class and just
can't figure this stuff out, you should contact
your teacher, ask on some message boards, or
you are even free to contact me to ask for help:
RachelJMorris@gmail.com8

