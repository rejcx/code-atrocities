#pragma once

#define intconst	2
#define	floatconst	3

class Symbol
{
	public:
		int type;
		string value;
		char name;
};

class SymbolTable
{
	public:
		int currentIndex;
		bool isFull;
		Symbol symbol[100];			//It's not a linked list and it's not dynamic just for the heck of it.
		SymbolTable()
		{
			currentIndex = 0;
			isFull = false;
		}
		void setup( string newvalue, char newname, int newtype )		//adds a new symbol to the table
		{
			if ( ! isFull )
			{
				symbol[currentIndex].name = newname;
				symbol[currentIndex].value = newvalue;
				symbol[currentIndex].type= newtype;
				currentIndex++;
				if ( currentIndex == 99 )
					isFull = true;	
			}
		}
		friend ostream& operator << (ostream &out, const SymbolTable &st)		//extraction operator
		{
			for ( int i = st.currentIndex; i > 0; i-- )
			{
				if ( st.symbol[ i ].type == intconst )
					out<<"int";
				else if ( st.symbol[ i ].type == floatconst )
					out<<"float";
				out << "\t" << st.symbol[i].name << "\t=\t" << st.symbol[i].value << endl;
			}
			return out;
		}
		void print ( ostream &out )		//print stuff
		{
			out<<"Symbol Table:"<<endl;
			for (int i=0; i<currentIndex; i++)
			{
				out<<"#"<<i<<")\t";
				if ( symbol[i].type == intconst )
					out<<"int\t";
				else if ( symbol[i].type == floatconst )
					out<<"float\t";
				out<<symbol[i].name<<"  =  "<<symbol[i].value<<endl;
			}
		}
		void updateValue ( char replaceSym, string newvalue )		//update stuff
		{
			//look for symbol with same name
			for (int i=0; i<currentIndex; i++)
			{
				if ( symbol[i].name == replaceSym )
				{
					symbol[i].value = newvalue;
				}
			}
		}
};