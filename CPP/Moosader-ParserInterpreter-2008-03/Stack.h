#pragma once
#include <iostream>
using namespace std;

//Based on a stack class I wrote last semester

class StackClass
{
    private:
        int maxSize;
        int stackTop;
        char *list;
    public:
        StackClass(int size)
        {
            if ( size > 0 )
                maxSize = size;
            else
            {
                cerr<<"\nInvalid stack size!\n";
                exit(10);
            }
            stackTop = 0;
            list = new char[maxSize];
        }
        StackClass()
        {
            //I don't really think having a maxSize is important.
            stackTop = 0;
            maxSize = 1000;
            list = new char[maxSize];
        }
        ~StackClass()
        {
            cout<<"\ndelete stack class\n";
            if ( !isEmpty() )
                delete [] list;
        }
        void push(char value)
        {
            if ( !isFull() )
            {
                list[stackTop] = value;
                stackTop++;
            }
            else
            {
                cerr<<"\nCannot add to full stack!\n";
                exit(30);
            }
        }
        void pop()
        {
            if ( !isEmpty() )
            {
                stackTop--;
            }
            else
            {
                cerr<<"\nCannot remove from empty list!\n";
            }
        }
        char top() const
        {
            if ( stackTop != 0 )
            {
                return list[stackTop - 1];
            }
            else
            {
                cerr<<"\nStack is empty, cannot get top.\n";
            }
        }
        int getItemAmt() const
        {
            return stackTop;
        }
        bool isFull() const
        {
            return stackTop == maxSize;
        }
        bool isEmpty() const
        {
            return stackTop == 0;
        }
        void print(ostream &out)
        {
            char temp;
            int init_stackTop = stackTop;
            while ( stackTop != 0 )
            {
                temp = top();
                out<<temp;
                stackTop--;
                cout<<endl;
            }
            //set back to the way it was
            stackTop = init_stackTop;
        }
};