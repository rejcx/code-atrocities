#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include "ExpressionTree.h"
#include "LinkedList.h"
#include "SymbolTable.h"
using namespace std;

class Parser
{
	public:
		int length;
		string data;
		int ReadFile(string);
		void Parse( ExpressionTree*, SymbolTable* );
};

int Parser::ReadFile(string filename)
{
    string tempData;
    string tempLine;
	int lineCount = 0;
	cout<<"\n\t-----Read file-----\n";
	ifstream infile;
	char *szData;
	infile.open(filename.c_str());
	if ( infile.fail() ) { exit(10); }
	infile.seekg(0, ios::end);		//look at end of file
	length = infile.tellg();		//store length
	infile.seekg(0, ios::beg);		//look at beginning of file
	szData = new char[length];		//create new character array
	infile.read(szData, length);	//read all of the data into a string
	tempData.clear();
	tempData = szData;				//move chars to a string
    int lastSemicolon = 0;			//if it does not find a semicolon after a line, it will ignore it.
	for (int i=0; i< (int)tempData.length(); i++)		//look at each character
	{
		if ( tempData[i] == ';' )					//if it's asemicolon
		{	
		    for (int j=lastSemicolon; j<i; j++)		//store everything up until that semicolons
		    {
		        tempLine += tempData[j];
		        lastSemicolon = i;
		    }
			lineCount++;		//add to the line count, it means there is one more line than there was before.
		}
	}
	tempLine += ';';		//add dat semicolon back
	lineCount++;			//add another line count
	data = tempLine;		//data is now equal to tempLine

	infile.close();			//close the infile, because not doing so would be poor programming practice
	delete [] szData;		//delete the character array, because not doing so would be poor programming practice.
	cout<<">Data loaded:\n"<<data;	//let the prompt know that the data was loaded
	return lineCount;				//return lineCount so ExpressionTree can create some dynamic arrays, foo!
}

void Parser::Parse( ExpressionTree *tree, SymbolTable *symbolTable )
{
	cout<<"\n\t-----Parse data-----\n";
	int lineIndex = 0;
	bool readLine = true;
	for (int i=0; i<length; i++)		//look at each letter (the data is currently all in one string, but we will consider
	{									//semicolons a marker for new line.
		if ( lineIndex == 0 )
		{
			if ( data[i] == 'i' && data[i+1] == 'n' && data[i+2] == 't' )		//reading in a variable declaration-- "int"
			{
				//create int datatype
				int k = i+4;
				symbolTable->setup( "0", data[k], 2 );		//add it to the symbol table
				while ( data[++k] == ',' )		//if there are more variables listed after a , then
				{
				    k++;
				    while ( data[k] == ' ' ) { k++; }
                    symbolTable->setup ("0", data[k], 2);		//add this variable to the symbol table, too.
				}
				readLine = false;
			}//if ( data[i] == 'i' && data[i+1] == 'n' && data[i+2] == 't' )	
			else if ( data[i] == 'f' && data[i+1] == 'l' && data[i+2] == 'o' && data[i+3] == 'a' && data[i+4] == 't' )		//float variable declaration
			{	//see above for comments
				//create float datatype
				int k = i+6;
				symbolTable->setup( "0", data[k], 3 );
				while ( data[++k] == ',' )
				{
				    k++;
				    while ( data[k] == ' ' ) { k++; }
				    symbolTable->setup("0", data[k], 3);
                }
				readLine = false;
			}//else if ( data[i] == 'f' && data[i+1] == 'l' && data[i+2] == 'o' && data[i+3] == 'a' && data[i+4] == 't' )
		}//if ( lineIndex == 0 )
		if ( readLine )	//if readLine is true, then that must mean it wasn't a variable declaration last character
		{
			if ( data[i] != ' ' && data[i] != '\n' && data[i] != '\0' )		//if it's not a space, new line, or \0 character, store it to the infix string.
			{
				//cout<<"((add "<<data[i]<<"))"<<endl;
                tree->addToInfix(data[i]);
			}//if ( data[i] != ' ' && data[i] != '\n' && data[i] != '\0' )
		}//if ( readLine )
		if ( data[i] == ';' && readLine )		//don't store this
		{
		    //tree->addToInfix(data[i]);
		}//if ( data[i] == ';' && readLine )
		else if ( data[i] == '\n' || data[i] == '\0' )		//if the data is a new line then do stuff.
		{
		    if ( readLine ) { tree->nextInfix(); }
			lineIndex = -1;
			readLine = true;
		}//else if ( data[i] == '\n' || data[i] == '\0' )
		lineIndex++;
	}//for (int i=0; i<length; i++)
	cout<<">Infix array:\n";
	tree->printInfix(cout);

	cout<<"\n>Symbol Table:\n";
	symbolTable->print(cout);
}







