#pragma once
#include <iostream>
#include <string>
#include "Stack.h"
#include "ExpressionTree.h"
#include "function.h"
using namespace std;

class AssignmentConverter
{
    public:
        void convertToPostfix(ExpressionTree *tree)
        {
            char temp;
            cout<<"\n\t-----Convert to Postfix-----\n";
			//infixIndex is the amount of lines, excluding
			//the variable declarations.
            for (int i=0; i<tree->infixIndex; i++)			//for each line (one line equals from the start of a line to the ;)
            {
                string postfixString;	
                postfixString += tree->infixArray[i][0];	//the only lines we're messing with are "x = " ones, so we're going
                postfixString += tree->infixArray[i][1];	//to automatically store the "[variable] =" into the postfixString.
                for (int j=2; j<tree->infixArray[i].length(); j++)		//for each character in that line
                {
                    temp = tree->infixArray[i][j];					
                    if ( temp == '+' || temp == '-' || temp == '*' || temp == '/' || temp == '^' || temp == '=' )	//an operation
                    {
                        if ( tree->postfixArray[i].isEmpty() == true )		//if the stack is empty, go ahead and push the operation
                        {
                            tree->postfixArray[i].push(temp);
                        }
                        else		//stack is not empty
                        {
                            if ( getPriority(tree->postfixArray[i].top()) >= getPriority(temp) )		//PEMDAS... if the top of the stack has higher precedence
                            {																			//than the current character, then push the current character
                                tree->postfixArray[i].push(temp);
                            }
                            else if ( getPriority(tree->postfixArray[i].top()) < getPriority(temp) )	//otherwise
                            {
                                while ( (tree->postfixArray[i].isEmpty() == false) && ( getPriority(tree->postfixArray[i].top()) <= getPriority(temp) ) )
                                {
									//until the stack is empty OR the top of the tree has lower precedence...
                                    postfixString += tree->postfixArray[i].top();		//add the top of the stack to the postfixString
                                    tree->postfixArray[i].pop();						//throw away the top of the stack
                                }
								tree->postfixArray[i].push(temp);						//push the current character on to the stack
                            }
                        }						
                    }//if ( temp == '+' || temp == '-' || temp == '*' || temp == '/' || temp == '^' || temp == '=' )
                    else if ( temp == '(' )
                    {
                        tree->postfixArray[i].push(temp);
                    }
                    else if ( temp == ')' )
                    {
                        while ( tree->postfixArray[i].isEmpty() == false && tree->postfixArray[i].top() != '(' )	// until the stack is empty or we find the corresponding beginning parenthesis...
                        {
                            if ( tree->postfixArray[i].top() != '\n' && tree->postfixArray[i].top() != ')' && tree->postfixArray[i].top() != '(' )
							{
                                postfixString += tree->postfixArray[i].top();		//add the top of the stack to the postfixString
								tree->postfixArray[i].pop();						//throw away the top of the stack
							}														//dance around a little bit
                        }				
                    }
                    else if ( temp == ' ' || temp == ';' || temp == '\n' || temp == '\0' )	//we aren't going to store these
                    {
                        //ignore
                    }
                    else    //letter or number
                    {
                        if ( temp != '\n' && temp != '\0' )		//probably redundant.
                        {
							if ( isNumber(temp) )				//current character is a number
							{
								int a = j;
								int tempB = tree->infixArray[i][a];
								//put [ and ] around number
								postfixString += '[';			//surround it with [ and ], so we can tell numbers that are neighbors apart.
								while ( isNumber( tree->infixArray[i][a] ) || tempB == '.' )	//while the next character is also a number
								{
									postfixString += tree->infixArray[i][a];		//add the number to the postfixString
									a++;											//look at next character
								}
								postfixString += ']';	//now we've stored all the numbers next to each other, put ] at the end.
								j += (a-j-1);
							}
							else		//it's a variable, these can be stored immediately to the postfixString
							{
								postfixString += temp; 
							}
                        }
                    }
                }//for (int j=2; j<tree->infixArray[i].length(); j++)
				//now, if the stack isn't empty, pop it's contents into the postfix String
				while ( tree->postfixArray[i].isEmpty() == false )
				{
					if ( tree->postfixArray[i].top() != '(' && tree->postfixArray[i].top() != ')' )
						postfixString += tree->postfixArray[i].top();
					tree->postfixArray[i].pop();
				}
                tree->postfixString[i] = postfixString;
            }	//do a jig
        }		//eat some candy
};				//wash your socks