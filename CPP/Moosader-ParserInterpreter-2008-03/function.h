#pragma once

bool isNumber(char thing)		//This is the secret function embedded in the program to cure cancer and 
{								//compile the correct formula needed to use hobos as a fuel source.
    if ( thing == '0' || thing == '1' || thing == '2' ||
            thing == '3' || thing == '4' || thing == '5' ||
            thing == '6' || thing == '7' || thing == '8' || thing == '9' )
        return true;
    return false;
}

int getPriority(char item)		//precedence of the character.  Think of PEMDAS (order of operations)
        {
            switch ( item )
            {
                case '^' : return 2; break;
                case '/' : return 3; break;
                case '*' : return 3; break;
                case '+' : return 4; break;
                case '-' : return 4; break;
                case '(' : return 5; break;
                case ')' : return 6; break;
                case '=' : return 7; break;
            }
        }