#
# Rachel J. Morris learns Assembly
# 00 - Basic output
# 
#   Reason:
# To start off super-simple with Assembly.  First, we're not
# going to worry about input, we're just going to output different
# types of things.
# 
#   Cross Reference:
# $t0: a constant number
# $t1: a constant number
# 
# $s0: stores $t0 + $t1
# $s1: stores $t0 - $t1
# $s2: is equal to $s1

		.data
Intro:	.asciiz		"\nWelcome to the program!"
Out1:	.asciiz		"\n $t0 + $t1 = "
Out2:	.asciiz		"\n $t0 - $t1 = "
Out3:	.asciiz		"\n $s2 = "
NewLn:	.asciiz		"\n"
End:	.asciiz		"\nEnd of program!"
#ArbStr:	

		.globl		main

		.text		

main:

		##########  INTRODUCTION  ##########
		# "Welcome to the program!"
		li $v0, 4			# load syscall; address of string (set up for string output)
		la $a0, Intro		# load address of string into parameter $a0
		syscall				# display

		# load values into "temp" registers
		li $t0, 2			# $t0 = 2
		li $t1, 5			# $t1 = 5
		

		##########  BASIC ARITHMETIC  ##########

		# add unsigned integers
		addu $s0, $t0, $t1	# $s0 = $t0 + $t1
	
		# subtract integers
		sub $s1, $t0, $t1

		# set something equal to something else
		move $s2, $s1		# $s2 = $s1

		##########  OUTPUT TO PROMPT  ##########

		# display new line
		li $v0, 4			# prepare to output string
		la $a0, NewLn		# load NewLn as the parameter
		syscall				# display

			### (A) DISPLAY ADDITION

		# display "$t0 + $t1 = "
		li $v0, 4			# prepare to output string
		la $a0, Out1		# load parameter
		syscall				# display

		# display value of $s0
		li $v0, 1			# prepare to output number
		move $a0, $s0		# load parameter
		syscall				# display
		
		# display new line
		li $v0, 4			# prepare to output string
		la $a0, NewLn		# load NewLn as the parameter
		syscall				# display

			### (B) DISPLAY SUBTRACTION

		# display "$t0 - $t1 = "
		li $v0, 4			# prepare to output string
		la $a0, Out2		# load parameter
		syscall				# display

		# display value of $s1
		li $v0, 1			# prepare to output number
		move $a0, $s1		# load parameter
		syscall				# display
		
		# display new line
		li $v0, 4			# prepare to output string
		la $a0, NewLn		# load NewLn as the parameter
		syscall				# display

			### (C) DISPLAY $S0

		# display "$s2 = "
		li $v0, 4			# prepare to output string
		la $a0, Out3		# load parameter
		syscall				# display

		# display value of $s1
		li $v0, 1			# prepare to output number
		move $a0, $s2		# load parameter
		syscall				# display
		
		# display new line
		li $v0, 4			# prepare to output string
		la $a0, NewLn		# load NewLn as the parameter
		syscall				# display

		
		##########  END OF PROGRAM, SAY GOODBYE THEN QUIT  ##########


		# "End of program!"
		li $v0, 4			# set up for string output
		la $a0, End			# load address of string into parameter
		syscall				# display


		# End the program
		li $v0, 10			# set up for ending program
		syscall				# end








