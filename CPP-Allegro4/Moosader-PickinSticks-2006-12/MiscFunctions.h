#pragma once
#include <allegro.h>

volatile long speed_counter = 0;

void increment_speed_counter()
{
    speed_counter++;
}
END_OF_FUNCTION(increment_speed_counter);

void initalizeCrap();

void initializeCrap()
{
  allegro_init();
  install_keyboard();
  install_timer();
  install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, 0)

  LOCK_VARIABLE(speed_counter);
  LOCK_FUNCTION(increment_speed_counter);
  install_int_ex(increment_speed_counter,BPS_TO_TIMER(120));

  set_color_depth(16);
  //set_gfx_mode(GFX_AUTODETECT_WINDOWED, 640, 480, 0, 0);
  set_gfx_mode(GFX_AUTODETECT, 640, 480, 0, 0);

  srand((unsigned)time(NULL));   //(unsigned)time(NULL)
}
END_OF_FUNCTION(initializeCrap);

bool thereIsCollision(Alec player, Stick stick)
{
  //Bounding Boxes First!
  if (!(        (player.Y()+56 >    stick.Y()+2 ) &&
                (player.Y()+35 <    stick.Y()+28 ) &&
                (player.X()+43 >    stick.X()+2 ) &&
                (player.X()+20 <    stick.X()+28 )))
  {
    return false;
  }
  else { return true; }
}
END_OF_FUNCTION (thereIsCollision);
