#pragma once
#include <string>
#include <allegro.h>
#include "rjm_gridClass.h"
#include "common.h"
using namespace std;

class rjm_mapClass
{
    public:
        rjm_gridClass bottom_layer[max_x][max_y];
        rjm_gridClass top_layer[max_x][max_y];
        rjm_gridClass animated_layer[max_x][max_y];
        void draw(BITMAP *buffer, BITMAP *tileset, float);
        void draw_top(BITMAP *buffer, BITMAP *tileset, float);
        void load_map(string name);
        //warp areas
        int warp_x1[4];
        int warp_x2[4];
        int warp_y1[4];
        int warp_y2[4];
        int go_to[4];
        //entrance coordinates
        int enter_x[6];
        int enter_y[6];
};

void rjm_mapClass::load_map(string name)
{
    string filename;
    if (name == "mall_top")
    {
        filename = "shopping_malltop.map";
        //Clothes exit
        warp_x1[0] = 3 * 32;
        warp_x2[0] = 6 * 32;
        warp_y1[0] = 0 * 32 + 96;
        warp_y2[0] = 2 * 32 + 96;
        go_to[0] = 2;
        //Hairdo exit
        warp_x1[1] = 612;
        warp_x2[1] = 22 * 32;
        warp_y1[1] = 0 * 32 + 96;
        warp_y2[1] = 2 * 32 + 96;
        go_to[1] = 3;
        //Bottom exit
        warp_x1[2] = 10 * 32;
        warp_x2[2] = 14 * 32;
        warp_y1[2] = 17 * 32;
        warp_y2[2] = 18 * 32;
        go_to[2] = 0;
        //Excess
        warp_x1[3] = -1;
        warp_x2[3] = -1;
        warp_y1[3] = -1;
        warp_y2[3] = -1;
        go_to[3] = -2;
        enter_x[3] = -1;
        enter_y[3] = -1;
        //Entrances - from clothes, hair, or bottom of mall entrances
        //From bottom
        enter_x[0] = 326;
        enter_y[0] = 432;
        //from top
        enter_x[1] = -1;
        enter_y[1] = -1;
        //from clothes
        enter_x[2] = 92;
        enter_y[2] = 146;
        //from hair
        enter_x[3] = 600;
        enter_y[3] = 146;
        //from makeup
        enter_x[4] = -1;
        enter_y[4] = -1;
        //from shoes
        enter_x[5] = -1;
        enter_y[5] = -1;
    }
    else if (name == "mall_bottom")
    {
        filename = "shopping_mallbottom.map";
        //Top exit
        warp_x1[0] = 10 * 32;
        warp_x2[0] = 14 * 32;
        warp_y1[0] = 0;
        warp_y2[0] = 64;
        go_to[0] = 1;
        //Shoes exit
        warp_x1[1] = 4 * 32;
        warp_x2[1] = 7 * 32;
        warp_y1[1] = 4 * 32 + 96;
        warp_y2[1] = 6 * 32 + 96;
        go_to[1] = 5;
        //Makeup exit
        warp_x1[2] = 18 * 32;
        warp_x2[2] = 21 * 32;
        warp_y1[2] = 4 * 32 + 96;
        warp_y2[2] = 6 * 32 + 96;
        go_to[2] = 4;
        //Exit exit
        warp_x1[3] = 10 * 32;
        warp_x2[3] = 14 * 32;
        warp_y1[3] = 14 * 32 + 96;
        warp_y2[3] = 17 * 32 + 96;
        go_to[3] = -1;
        //Entrances
        //from bottom
        enter_x[0] = 10*32;
        enter_y[0] = 13*32;
        //from mall top
        enter_x[1] = 321;
        enter_y[1] = 32;
        //from clothes
        enter_x[2] = -1;
        enter_y[2] = -1;
        //from hair
        enter_x[3] = -1;
        enter_y[3] = -1;
        //From makeup
        enter_x[4] = 575;
        enter_y[4] = 279;
        //From shoes
        enter_x[5] = 125;
        enter_y[5] = 279;
    }
    else if (name == "clothes")
    {
        filename = "shopping_clothes.map";
        warp_x1[0] = 10 * 32;
        warp_x2[0] = 14 * 32;
//        warp_y1[0] = 14 * 32 + 96;
//        warp_y2[0] = 17 * 32 + 96;
        warp_y1[0] = 550;
        warp_y2[0] = 600;
        go_to[0] = 1;

        warp_x1[1] = warp_x1[2] = warp_x1[3] = -1;
        warp_x2[1] = warp_x2[2] = warp_x2[3] = -1;
        warp_y1[1] = warp_y1[2] = warp_y1[3] = -1;
        warp_y2[1] = warp_y2[2] = warp_y2[3] = -1;
        go_to[1] = go_to[2] = go_to[3] = -1;

        //Entrances
        //from bottom
        enter_x[0] = -1;
        enter_y[0] = -1;
        //from mall top
        enter_x[1] = 320;
        enter_y[1] = 400;
        //from clothes
        enter_x[2] = -1;
        enter_y[2] = -1;
        //from hair
        enter_x[3] = -1;
        enter_y[3] = -1;
        //From makeup
        enter_x[4] = -1;
        enter_y[4] = -1;
        //From shoes
        enter_x[5] = -1;
        enter_y[5] = -1;
    }
    else if (name == "hair")
    {
        filename = "shopping_hair.map";
        //Bottom exit
        warp_x1[0] = 544;
        warp_x2[0] = 544+128;
//        warp_y1[0] = 14 * 32 + 96;
//        warp_y2[0] = 17 * 32 + 96;
        warp_y1[0] = 550;
        warp_y2[0] = 600;
        go_to[0] = 1;

        warp_x1[1] = warp_x1[2] = warp_x1[3] = -1;
        warp_x2[1] = warp_x2[2] = warp_x2[3] = -1;
        warp_y1[1] = warp_y1[2] = warp_y1[3] = -1;
        warp_y2[1] = warp_y2[2] = warp_y2[3] = -1;
        go_to[1] = go_to[2] = go_to[3] = -1;
        //Entrances
        //from bottom
        enter_x[0] = -1;
        enter_y[0] = -1;
        //from mall top
        enter_x[1] = 540;
        enter_y[1] = 450;
        //from clothes
        enter_x[2] = -1;
        enter_y[2] = -1;
        //from hair
        enter_x[3] = -1;
        enter_y[3] = -1;
        //From makeup
        enter_x[4] = -1;
        enter_y[4] = -1;
        //From shoes
        enter_x[5] = -1;
        enter_y[5] = -1;
    }
    else if (name == "shoes")
    {
        filename = "shopping_shoes.map";
        //Bottom exit
        warp_x1[0] = 320;
        warp_x2[0] = 320+160;
        warp_y1[0] = 14 * 32 + 96;
        warp_y2[0] = 17 * 32 + 96;
        go_to[0] = 0;

        warp_x1[1] = warp_x1[2] = warp_x1[3] = -1;
        warp_x2[1] = warp_x2[2] = warp_x2[3] = -1;
        warp_y1[1] = warp_y1[2] = warp_y1[3] = -1;
        warp_y2[1] = warp_y2[2] = warp_y2[3] = -1;
        go_to[1] = go_to[2] = go_to[3] = -1;
        //Entrances
        //from bottom
        enter_x[0] = 320;
        enter_y[0] = 428;
        //from mall tops
        enter_x[1] = -1;
        enter_y[1] = -1;
        //from clothes
        enter_x[2] = -1;
        enter_y[2] = -1;
        //from hair
        enter_x[3] = -1;
        enter_y[3] = -1;
        //From makeup
        enter_x[4] = -1;
        enter_y[4] = -1;
        //From shoes
        enter_x[5] = -1;
        enter_y[5] = -1;
    }
    else if (name == "makeup")
    {
        filename = "shopping_makeup.map";
        //Bottom exit
        warp_x1[0] = 96;
        warp_x2[0] = 96+128;
        warp_y1[0] = 14 * 32 + 96;
        warp_y2[0] = 17 * 32 + 96;
        go_to[0] = 0;

        warp_x1[1] = warp_x1[2] = warp_x1[3] = -1;
        warp_x2[1] = warp_x2[2] = warp_x2[3] = -1;
        warp_y1[1] = warp_y1[2] = warp_y1[3] = -1;
        warp_y2[1] = warp_y2[2] = warp_y2[3] = -1;
        go_to[1] = go_to[2] = go_to[3] = -1;
        //Entrances
        //from bottom
        enter_x[0] = 96;
        enter_y[0] = 422;
        //from mall top
        enter_x[1] = -1;
        enter_y[1] = -1;
        //from clothes
        enter_x[2] = -1;
        enter_y[2] = -1;
        //from hair
        enter_x[3] = -1;
        enter_y[3] = -1;
        //From makeup
        enter_x[4] = -1;
        enter_y[4] = -1;
        //From shoes
        enter_x[5] = -1;
        enter_y[5] = -1;
    }
    cout<<"loading map "<<filename<<endl;
    ifstream infile;
    infile.open(filename.c_str());
    for (int j=0; j<max_y; j++)
    {
        for (int i=0; i<max_x; i++)
        {
            int temp;
            infile>>temp;
            bottom_layer[i][j].codex = temp;
            bottom_layer[i][j].x = i*32;
            bottom_layer[i][j].y = j*32;
            bottom_layer[i][j].w = 32;
            bottom_layer[i][j].h = 32;
        }
    }
    //TOP LAYER
    for (int j=0; j<max_y; j++)
    {
        for (int i=0; i<max_x; i++)
        {
            int temp;
            infile>>temp;
            top_layer[i][j].codex = temp;
            top_layer[i][j].x = i*32;
            top_layer[i][j].y = j*32;
            top_layer[i][j].w = 32;
            top_layer[i][j].h = 32;
        }
    }
    //ANIMATED LAYER
    for (int j=0; j<max_y; j++)
    {
        for (int i=0; i<max_x; i++)
        {
            int temp;
            infile>>temp;
            animated_layer[i][j].codex = temp;
            animated_layer[i][j].x = i*32;
            animated_layer[i][j].y = j*32;
            animated_layer[i][j].w = 32;
            animated_layer[i][j].h = 32;
        }
    }
    infile.close();
}

void rjm_mapClass::draw(BITMAP *buffer, BITMAP *tileset, float game_anim)
{
    //need to have it only draw what's on the screen
    for (int j=0; j<max_y; j++)
    {
        for (int i=0 ;i<max_x; i++)
        {
                masked_blit(tileset, buffer, bottom_layer[i][j].codex, bottom_layer[i][j].codey, bottom_layer[i][j].x,
                    bottom_layer[i][j].y, bottom_layer[i][j].w, bottom_layer[i][j].h);
                if ( top_layer[i][j].codex != 0 )      //if there is one
                {
                    masked_blit(tileset, buffer, top_layer[i][j].codex, top_layer[i][j].codey, top_layer[i][j].x,
                        top_layer[i][j].y, top_layer[i][j].w, top_layer[i][j].h);
                }
                if ( animated_layer[i][j].codex != 0 && (int)game_anim == 1 )      //if there is one and at right frame
                {
                    masked_blit(tileset, buffer, animated_layer[i][j].codex, animated_layer[i][j].codey, animated_layer[i][j].x,
                        animated_layer[i][j].y, animated_layer[i][j].w, animated_layer[i][j].h);
                }
        }
    }
}

void rjm_mapClass::draw_top(BITMAP *buffer, BITMAP *tileset, float game_anim)
{
    for (int j=0; j<max_y; j++)
    {
        for (int i=0 ;i<max_x; i++)
        {
            if ( top_layer[i][j].codex != 0 &&
                    top_layer[i][j].codex == 95*32 || top_layer[i][j].codex == 96*32 ||
                    top_layer[i][j].codex == 97*32 )
            {
                masked_blit(tileset, buffer, top_layer[i][j].codex, top_layer[i][j].codey, top_layer[i][j].x,
                    top_layer[i][j].y, top_layer[i][j].w, top_layer[i][j].h);
            }
            if ( animated_layer[i][j].codex != 0 && (int)game_anim == 1 )      //if there is one and at right frame
            {
                masked_blit(tileset, buffer, animated_layer[i][j].codex, animated_layer[i][j].codey, animated_layer[i][j].x,
                    animated_layer[i][j].y, animated_layer[i][j].w, animated_layer[i][j].h);
            }
        }
    }
}
