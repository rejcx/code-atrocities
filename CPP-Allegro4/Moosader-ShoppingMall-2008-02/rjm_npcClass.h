#pragma once
#include "common.h"

class rjm_npcClass
{
    public:
    int x, y, w, h, talkx, talky, map;
    SAMPLE *speak[2];
    int codex;
    void draw(BITMAP*, BITMAP*);
    bool talking;
    float talk_counter;
    rjm_npcClass() { talking = false; talk_counter = 0.0; w = 48; h = 96; }
    void increment_counter();
};

void rjm_npcClass::draw(BITMAP *buffer, BITMAP *image)
{
    masked_blit(image, buffer, codex, 0, x, y, w, h);
}

void rjm_npcClass::increment_counter()
{
    talk_counter += 1;
    if ( talk_counter >= 500 )
    {
        talk_counter = 0.0;
        talking = false;
    }
}