#pragma once
#include "rjm_bodyPartClass.h"
#include "common.h"

class rjm_playerClass
{
    public:
        rjm_bodyPartClass body;
        rjm_bodyPartClass hair;
        rjm_bodyPartClass face;
        rjm_bodyPartClass clothes;
        rjm_bodyPartClass shoes;
        int x;
        int y;
        int w;
        int h;
        float frame;
        string direction;
        rjm_playerClass();
        void draw(BITMAP *buffer);
        void increment_frame();
        void move(string);
        void init_p2();
        BITMAP *temp;
        float speed;
        void force_update();
        void swap(char);
};

void rjm_playerClass::move(string dir)
{
    direction = dir;
    if ( dir == "up" && (y > 0))
    {
        y -= speed;
        body.y -= speed;
        clothes.y -= speed;
        face.y -= speed;
        shoes.y -= speed;
        hair.y -= speed;
    }
    else if ( dir == "down" && (y + h < 600) )
    {
        y += speed+.5;
        body.y += speed+.5;
        clothes.y += speed+.5;
        face.y += speed+.5;
        shoes.y += speed+.5;
        hair.y += speed+.5;
    }
    else if ( dir == "left" && (x > 0) )
    {
        x -= speed;
        body.x -= speed;
        clothes.x -= speed;
        face.x -= speed;
        shoes.x -= speed;
        hair.x -= speed;
    }
    else if ( dir == "right" && (x + w < 800) )
    {
        x += speed+.5;
        body.x += speed+.5;
        clothes.x += speed+.5;
        face.x += speed+.5;
        shoes.x += speed+.5;
        hair.x += speed+.5;
    }
}

void rjm_playerClass::increment_frame()
{
    frame += 0.08;
    if ( frame >= 2 ) { frame = 0; }
}

void rjm_playerClass::draw(BITMAP *buffer)
{
    int frame_i = (int)frame;
    if ( direction == "left" )
    {
        masked_blit(body.image, buffer, body.codex+(frame_i*body.w), body.codey+body.h, body.x, body.y, body.w, body.h);
        masked_blit(clothes.image, buffer, clothes.codex, clothes.codey+(clothes.h*(frame_i+2)), clothes.x, clothes.y, clothes.w, clothes.h);
        masked_blit(face.image, buffer, face.codex, face.codey+(face.h*(frame_i+2)), face.x+1, face.y, face.w, face.h);
        masked_blit(shoes.image, buffer, shoes.codex, shoes.codey+(shoes.h*(frame_i+2)), shoes.x, shoes.y, shoes.w, shoes.h);
        masked_blit(hair.image, buffer, hair.codex, hair.codey+(hair.h*(frame_i+2)), hair.x, hair.y, hair.w, hair.h);
    }
    else if ( direction == "up" )
    {
        masked_blit(body.image, buffer, body.codex+(frame_i*body.w), body.codey+body.h*2, body.x, body.y, body.w, body.h);
        masked_blit(clothes.image, buffer, clothes.codex, clothes.codey+(clothes.h*4)+(frame_i*clothes.h), clothes.x, clothes.y, clothes.w, clothes.h);
        masked_blit(hair.image, buffer, hair.codex, hair.codey+(hair.h*4)+(frame_i*hair.h), hair.x, hair.y, hair.w, hair.h);
        masked_blit(shoes.image, buffer, shoes.codex, shoes.codey+(shoes.h*4)+(frame_i*shoes.h), shoes.x, shoes.y, shoes.w, shoes.h);
    }
    else if ( direction == "right" )
    {
        temp = create_bitmap(48, 96);
        rectfill(temp, 0, 0, 48, 96, makecol(255, 0, 255));
        masked_blit(body.image, temp, body.codex+(frame_i*body.w), body.codey+body.h, body.x-body.x, body.y-body.y, body.w, body.h);
        masked_blit(clothes.image, temp, clothes.codex, clothes.codey+(clothes.h*(frame_i+2)), clothes.x-body.x, clothes.y-body.y, clothes.w, clothes.h);
        masked_blit(face.image, temp, face.codex, face.codey+(face.h*(frame_i+2)), face.x+1-body.x, face.y-body.y, face.w, face.h);
        masked_blit(shoes.image, temp, shoes.codex, shoes.codey+(shoes.h*(frame_i+2)), shoes.x-body.x, shoes.y-body.y, shoes.w, shoes.h);
        masked_blit(hair.image, temp, hair.codex, hair.codey+(hair.h*(frame_i+2)), hair.x-body.x, hair.y-body.y, hair.w, hair.h);
        draw_sprite_h_flip(buffer, temp, body.x, body.y);
    }
    else if ( direction == "down" )
    {
        masked_blit(body.image, buffer, body.codex+(frame_i*body.w), body.codey, body.x, body.y, body.w, body.h);
        masked_blit(clothes.image, buffer, clothes.codex, clothes.codey+(frame_i*clothes.h), clothes.x, clothes.y, clothes.w, clothes.h);
        masked_blit(face.image, buffer, face.codex, face.codey+(frame_i*face.h), face.x+1, face.y, face.w, face.h);
        masked_blit(hair.image, buffer, hair.codex, hair.codey+(frame_i*hair.h), hair.x, hair.y, hair.w, hair.h);
        masked_blit(shoes.image, buffer, shoes.codex, shoes.codey+(frame_i*shoes.h), shoes.x, shoes.y, shoes.w, shoes.h);
    }
}

rjm_playerClass::rjm_playerClass()
{
    speed = 1.5;
    x = 9*32;
    y = 12*32;
    w = 48;
    h = 96;
    direction = "down";
    body.x = x;
    body.y = y;
    body.w = w;
    body.h = h;
    body.codex = 0;
    body.codey = 0;
    body.image = load_bitmap("shopping_sprite_body.bmp", NULL);

    hair.x = x;
    hair.y = y;
    hair.w = 48;
    hair.h = 38;
    hair.codex = 2*48;
    hair.codey = 0;
    hair.type = 2;
    hair.image = load_bitmap("shopping_sprite_hair.bmp", NULL);

    face.x = x;
    face.y = y+13;
    face.w = 48;
    face.h = 20;
    face.codex = 48*3;
    face.codey = 0;
    face.type = 3;
    face.image = load_bitmap("shopping_sprite_makeup.bmp", NULL);

    clothes.x = x;
    clothes.y = y+33;
    clothes.w = 48;
    clothes.h = 52;
    clothes.codex = 2*48;
    clothes.codey = 0;
    clothes.type = 2;
    clothes.image = load_bitmap("shopping_sprite_clothes.bmp", NULL);

    shoes.x = x;
    shoes.y = y+74;
    shoes.w = 48;
    shoes.h = 18;
    shoes.codex = 5*48;
    shoes.codey = 0;
    shoes.type = 5;
    shoes.image = load_bitmap("shopping_sprite_shoes.bmp", NULL);
}

void rjm_playerClass::init_p2()
{
    speed = 1.5;
    x = 13*32;
    y = 12*32;
    w = 48;
    h = 96;
    direction = "down";
    body.x = x;
    body.y = y;
    body.w = w;
    body.h = h;
    body.codex = 96;
    body.codey = 0;
    body.image = load_bitmap("shopping_sprite_body.bmp", NULL);

    hair.x = x;
    hair.y = y;
    hair.w = 48;
    hair.h = 38;
    hair.codex = 9*48;
    hair.codey = 0;
    hair.type = 9;
    hair.image = load_bitmap("shopping_sprite_hair.bmp", NULL);

    face.x = x;
    face.y = y+13;
    face.w = 48;
    face.h = 20;
    face.codex = 48*3;
    face.codey = 0;
    face.type = 3;
    face.image = load_bitmap("shopping_sprite_makeup.bmp", NULL);

    clothes.x = x;
    clothes.y = y+33;
    clothes.w = 48;
    clothes.h = 52;
    clothes.codex = 3*48;
    clothes.codey = 0;
    clothes.type = 3;
    clothes.image = load_bitmap("shopping_sprite_clothes.bmp", NULL);

    shoes.x = x;
    shoes.y = y+74;
    shoes.w = 48;
    shoes.h = 18;
    shoes.codex = 0;
    shoes.codey = 0;
    shoes.type = 0;
    shoes.image = load_bitmap("shopping_sprite_shoes.bmp", NULL);
}

void rjm_playerClass::force_update()
{
    body.x = x;
    body.y = y;
    body.w = w;
    body.h = h;

    hair.x = x;
    hair.y = y;
    hair.w = 48;
    hair.h = 38;

    face.x = x;
    face.y = y+13;
    face.w = 48;
    face.h = 20;

    clothes.x = x;
    clothes.y = y+33;
    clothes.w = 48;
    clothes.h = 52;

    shoes.x = x;
    shoes.y = y+74;
    shoes.w = 48;
    shoes.h = 18;
}

void rjm_playerClass::swap(char type)
{
    if ( type == 'h' )          //hair
    {
        hair.codex += 48;
        if ( hair.codex >= 1152 ) { hair.codex = 0; }
    }
    else if ( type == 's' )     //shoes
    {
        shoes.codex += 48;
        if ( shoes.codex >= 288 ) { shoes.codex = 0; }
    }
    else if ( type == 'c' )     //clothes
    {
        clothes.codex += 48;
        if ( clothes.codex >= 288 ) { clothes.codex = 0; }
    }
    else if ( type == 'm' )     //makeup
    {
        face.codex += 48;
        if ( face.codex >= 288 ) { face.codex = 0; }
    }
}







