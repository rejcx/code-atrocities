#pragma once
#include "common.h"

class rjm_gridClass
{
    public:
        int x;      //screen x
        int y;
        int w;
        int h;
        int codex;  //tileset x
        int codey;  //tileset y
};
