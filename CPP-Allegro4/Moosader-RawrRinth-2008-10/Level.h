#pragma once
#include "Tile.h"
#include "allegro.h"
#include <string>
#include <iostream>
#include <fstream>
#include "Rect.h"
using namespace std;

const int TILEX = 80;
const int TILEY = 60;
const int TILEWH = 32;		//tiles are square, w = h = 32

class Level
{
	private:
        int enemyCount, itemCount;
        int solidTiles[];
	public:
		Tile tile[3][TILEX][TILEY];
		void LoadMap( string filename );
		void DrawTopLayer( BITMAP *buffer, BITMAP *tileset, float, float );
		void DrawBottomLayer( BITMAP *buffer, BITMAP *tileset, float, float );
		int EnemyCount() { return enemyCount; }
		int ItemCount() { return itemCount; }
		Rect CollisionRegion( int layer, int i, int j )
		{
			return tile[layer][i][j].region;
		}
};
