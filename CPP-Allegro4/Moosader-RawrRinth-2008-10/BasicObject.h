#pragma once

class BasicObject
{
    protected:
        int x, y, w, h, FRAMEMAX;
        int originalX, originalY, originalW, originalH;

        Rect region;	//collision region
        float frame;
        bool exists;
        float hp;
        float originalHP;

    public:
        int index;

        float deadTimer;
        void Reset()
        {
            x = originalX;
            y = originalY;
            hp = originalHP;
            exists = true;
        }

        void Draw( BITMAP *destination, BITMAP *source, int offsetX, int offsetY )
        {
            if ( exists )
            {
                //masked_blit( source, destination, (int)frame * w, 0, x - offsetX, y - offsetY, w, h );
                masked_blit( source, destination, 0, 0, x - offsetX, y - offsetY, w, h );
            }
        }

        void Update();
        void IncrementFrame()
        {
            frame += 0.15f;
            if ( frame >= FRAMEMAX )
                frame = 0.0f;
        }
        int X() { return x; }
        int Y() { return y; }
        int W() { return w; }
        int H() { return h; }
        int X2() { return x + w; }
        int Y2() { return y + h; }
        int RX() { return region.x; }
        int RY() { return region.y; }
        int RW() { return region.w; }
        int RH() { return region.h; }

        bool Exists() { return exists; }
        void Exists( bool val ) { exists = val; }

        Rect CollisionRegion() { return region; }
        float HP() { return hp; }

        void AddHP( float amt )
        {
            hp += amt;
            if ( hp > 100 )
            {
                hp = 100;
            }
        }
};

