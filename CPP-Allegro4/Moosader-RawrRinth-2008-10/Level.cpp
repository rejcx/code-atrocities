#include "Level.h"

void Level::LoadMap( string filename )
{
	ifstream infile;
	infile.open( filename.c_str() );

	enemyCount = 20;
	itemCount = 30; //was 13

	if ( infile.bad() )
	{
		cerr<<"Error loading game map "<<filename<<endl;
	}
	string temp;
	infile>>temp;       //x
    infile>>temp;       //160
    infile>>temp;       //y
    infile>>temp;       //60
    infile>>temp;       //bgimage
    infile>>temp;       //background, none in this game.

    //BOTTOM LAYER
    for ( int j=0; j<TILEY; j++ )
    {
		for ( int i=0; i<TILEX; i++ )
		{
			infile>>temp;
			//Basic filmstrip coordinates, world coordinates
			tile[0][i][j].fx = atoi( temp.c_str() ) * TILEWH;
			tile[0][i][j].fy = 0;
			tile[0][i][j].x = i * TILEWH;
			tile[0][i][j].y = j * TILEWH;
			tile[0][i][j].region.X( 0 );
			tile[0][i][j].region.Y( 0 );
			tile[0][i][j].region.W( 32 );
			tile[0][i][j].region.H( 32 );
			tile[0][i][j].solid = false;
			//Collision? (rects, yay!)
			int t = tile[0][i][j].fx / TILEWH;
			if ( (t >= 1 && t <= 51) || (t >= 59 && t <= 80) || (t >= 87 && t <= 100) ||
                    (t >= 117 && t <= 127) || (t >= 135 && t <= 155) || (t >= 157 && t <= 159) || (t >= 187 && t <= 195) )
			{
				tile[0][i][j].solid = true;
			}
			else
			{
			    tile[0][i][j].solid = false;
			}
		}
	}
	//MIDDLE LAYER
    for ( int j=0; j<TILEY; j++ )
    {
		for ( int i=0; i<TILEX; i++ )
		{
			infile>>temp;
			//Basic filmstrip coordinates, world coordinates
			tile[1][i][j].fx = atoi( temp.c_str() ) * TILEWH;
			tile[1][i][j].fy = 0;
			tile[1][i][j].x = i * TILEWH;
			tile[1][i][j].y = j * TILEWH;
			tile[1][i][j].region.X( 0 );
			tile[1][i][j].region.Y( 0 );
			tile[1][i][j].region.W( 32 );
			tile[1][i][j].region.H( 32 );
			tile[1][i][j].solid = true;
			//Collision?
			int t = tile[1][i][j].fx / TILEWH;
			if ( (t >= 1 && t <= 51) || (t >= 59 && t <= 80) || (t >= 87 && t <= 100) ||
                    (t >= 117 && t <= 127) || (t >= 135 && t <= 155) || (t >= 157 && t <= 159) || (t >= 187 && t <= 195) )
			{
				tile[1][i][j].solid = true;
			}
			else
			{
			    tile[1][i][j].solid = false;
			}
		}
	}
	//TOP LAYER
    for ( int j=0; j<TILEY; j++ )
    {
		for ( int i=0; i<TILEX; i++ )
		{
			infile>>temp;
			//Basic filmstrip coordinates, world coordinates
			tile[2][i][j].fx = atoi( temp.c_str() ) * TILEWH;
			tile[2][i][j].fy = 0;
			tile[2][i][j].x = i * TILEWH;
			tile[2][i][j].y = j * TILEWH;
			//No collision for top layer
		}
	}
	infile.close();
}

void Level::DrawBottomLayer( BITMAP *buffer, BITMAP *tileset, float offsetX, float offsetY )
{
    int tx1, ty1, tx2, ty2;
	for ( int i=0; i<TILEX; i++ )
	{
		for ( int j=0; j<TILEY; j++ )
		{
			if ( tile[0][i][j].fx != 0 )
			{
				//Draw
				masked_blit( tileset, buffer, tile[0][i][j].fx, tile[0][i][j].fy, tile[0][i][j].x - offsetX, tile[0][i][j].y - offsetY, tile[0][i][j].w, tile[0][i][j].h );

				//temp - draw bounding box
//				if ( tile[0][i][j].solid )
//				{
//				    tx1 = tile[0][i][j].x - offsetX;
//				    ty1 = tile[0][i][j].y - offsetY;
//				    tx2 = tile[0][i][j].x + tile[0][i][j].w - offsetX;
//				    ty2 = tile[0][i][j].y + tile[0][i][j].h - offsetY;
//				    rect( buffer, tx1, ty1, tx2, ty2, makecol( 255, 255, 0 ) );
//				}
			}
			if ( tile[1][i][j].fx != 0 )
			{
			    masked_blit( tileset, buffer, tile[1][i][j].fx, tile[1][i][j].fy, tile[1][i][j].x - offsetX, tile[1][i][j].y - offsetY, tile[1][i][j].w, tile[1][i][j].h );

			    //temp - draw bounding box
//			    if ( tile[1][i][j].solid )
//				{
//				    tx1 = tile[1][i][j].x - offsetX;
//				    ty1 = tile[1][i][j].y - offsetY;
//				    tx2 = tile[1][i][j].x + tile[1][i][j].w - offsetX;
//				    ty2 = tile[1][i][j].y + tile[1][i][j].h - offsetY;
//				    rect( buffer, tx1, ty1, tx2, ty2, makecol( 255, 0, 0 ) );
//				}
			}
		}
	}
}

void Level::DrawTopLayer( BITMAP *buffer, BITMAP *tileset, float offsetX, float offsetY )
{
	for ( int i=0; i<TILEX; i++ )
	{
		for ( int j=0; j<TILEY; j++ )
		{
			if ( tile[2][i][j].fx != 0 )
			{
				//Draw
				masked_blit( tileset, buffer, tile[2][i][j].fx, tile[2][i][j].fy, tile[2][i][j].x - offsetX, tile[2][i][j].y - offsetY, tile[2][i][j].w, tile[2][i][j].h );
			}
		}
	}
}









