Rachel J. Morris [Moosader.com](http://www.moosader.com/)

## ABOUT
This is a map editor I had originally written in 2007
and kept updating for a while.

It is badly written, I didn't understand OOP very well and
wrote the UI elements myself.
This is all C++ and Allegro4.

*I do not support MusuGo - I will not fix bugs or help with things*

These days, I use Tiled.
