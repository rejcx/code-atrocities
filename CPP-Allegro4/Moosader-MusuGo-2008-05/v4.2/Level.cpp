#include "Level.h"

void Level::Setup()
{
    xOff = -64;
    yOff = -96;
    confirmSound = load_sample( "confirm.wav" );
    if ( !confirmSound ) { cerr<<"error loading confirm.wav"<<endl; }
}

void Level::Setup( int MAX_X, int MAX_Y )
{
    confirmSound = load_sample( "confirm.wav" );
    if ( !confirmSound ) { cerr<<"error loading confirm.wav"<<endl; }
    xOff = -64;
    yOff = -96;
    //xOff = yOff = 0;
    grid = true;
    for ( int i=0; i<MAX_X; i++ )
    {
        for ( int j=0; j<MAX_Y; j++ )
        {
            //x, y, w, h, filmstrip x, solidity
            tile[0][i][j].Setup( i*32, j*32, 32, 32, 0, false );
            tile[1][i][j].Setup( i*32, j*32, 32, 32, 0, false );
            tile[2][i][j].Setup( i*32, j*32, 32, 32, 0, false );
        }
    }
}

void Level::ToggleGrid()
{
    if ( grid )
        grid = false;
    else
        grid = true;
}

void Level::Draw( BITMAP *buffer, BITMAP *tileset, int MAX_X, int MAX_Y )
{
    for ( int i=0; i<MAX_X; i++ )
    {
        for ( int j=0; j<MAX_Y; j++ )
        {
            if ( tile[0][i][j].X() >= xOff-32 && tile[0][i][j].X() <= xOff+1024 &&
                    tile[0][i][j].Y() >= yOff-32 && tile[0][i][j].Y() <= yOff+768 )
            {
                //x, y, w, h, filmstrip x, solidity
                tile[0][i][j].Draw( buffer, tileset, xOff, yOff );
                tile[1][i][j].Draw( buffer, tileset, xOff, yOff );
                tile[2][i][j].Draw( buffer, tileset, xOff, yOff );
                if ( grid )
                    rect( buffer, i*32-xOff, j*32-yOff, i*32+32-xOff, j*32+32-yOff, makecol( 100, 100, 100 ) );

                textprintf_centre_ex( buffer, font, i*32+16-xOff, 64, makecol( 255, 255, 255 ), -1, "%i", i );
                textprintf_ex( buffer, font, 48, 16+j*32-yOff, makecol( 255, 255, 255 ), -1, "%i", j );
            }
        }
    }
}

void Level::CheckClick( Brush *mouse, int click )
{
    //See if a tile was clicked
    int mx = ( mouse_x + xOff ) / 32;
    int my = ( mouse_y + yOff ) / 32;

    if ( click == 1 )
        tile[ mouse->Layer() ][ mx ][ my ].FX ( mouse->Fx() );
    else
        tile[ mouse->Layer() ][ mx ][ my ].FX ( 0 );

    if ( mouse->Size() == 3 )
    {
        if ( click == 1 )
        {
            tile[ mouse->Layer() ][ mx-1 ][ my ].FX ( mouse->Fx() );
            tile[ mouse->Layer() ][ mx+1 ][ my ].FX ( mouse->Fx() );
            tile[ mouse->Layer() ][ mx ][ my-1 ].FX ( mouse->Fx() );
            tile[ mouse->Layer() ][ mx ][ my+1 ].FX ( mouse->Fx() );
            tile[ mouse->Layer() ][ mx-1 ][ my-1 ].FX ( mouse->Fx() );
            tile[ mouse->Layer() ][ mx-1 ][ my+1 ].FX ( mouse->Fx() );
            tile[ mouse->Layer() ][ mx+1 ][ my-1 ].FX ( mouse->Fx() );
            tile[ mouse->Layer() ][ mx+1 ][ my+1 ].FX ( mouse->Fx() );
        }
        else
        {
            tile[ mouse->Layer() ][ mx-1 ][ my ].FX ( 0 );
            tile[ mouse->Layer() ][ mx+1 ][ my ].FX ( 0 );
            tile[ mouse->Layer() ][ mx ][ my-1 ].FX ( 0 );
            tile[ mouse->Layer() ][ mx ][ my+1 ].FX ( 0 );
            tile[ mouse->Layer() ][ mx-1 ][ my-1 ].FX ( 0 );
            tile[ mouse->Layer() ][ mx-1 ][ my+1 ].FX ( 0 );
            tile[ mouse->Layer() ][ mx+1 ][ my-1 ].FX ( 0 );
            tile[ mouse->Layer() ][ mx+1 ][ my+1 ].FX ( 0 );
        }
    }
}

void Level::FillLayer( int theLayer, int theTile, int MAX_X, int MAX_Y )
{
    for ( int i=0; i<MAX_X; i++ )
    {
        for ( int j=0; j<MAX_Y; j++ )
        {
            tile[ theLayer ][i][j].FX( theTile );
        }
    }
}

void Level::ClearLayer( int theLayer, int MAX_X, int MAX_Y )
{
    for ( int i=0; i<MAX_X; i++ )
    {
        for ( int j=0; j<MAX_Y; j++ )
        {
            tile[ theLayer ][i][j].FX( 0 );
        }
    }
}

void Level::SaveMap( string filename, int MAX_X, int MAX_Y )
{
    cout<<"Saving to "<<filename<<"..."<<endl;
    ofstream outfile;
    outfile.open( filename.c_str() );
    outfile<<"x "<<MAX_X<<"\ty "<<MAX_Y<<"\nbgimage none"<<endl;
    for ( int k=0; k<3; k++ )               //layer
    {
        for ( int j=0; j<MAX_Y; j++ )       //y
        {
            for ( int i=0; i<MAX_X; i++ )   //x
            {
                if ( tile[k][i][j].Fx()/32 < 10 )
                {
                    outfile<<"000"<<tile[k][i][j].Fx()/32<<" ";
                }
                else if ( tile[k][i][j].Fx()/32 < 100 )
                {
                    outfile<<"00"<<tile[k][i][j].Fx()/32<<" ";
                }
                else if ( tile[k][i][j].Fx()/32 < 1000 )
                {
                    outfile<<"0"<<tile[k][i][j].Fx()/32<<" ";
                }
                else
                {
                    outfile<<tile[k][i][j].Fx()/32<<" ";
                }
            }
            outfile<<endl;
        }
        outfile<<endl<<endl;
    }
    outfile.close();
    cout<<"Save complete"<<endl;
    //if ( confirmSound != NULL )
        play_sample( confirmSound, 255, 128, 1000, false );
}

void Level::LoadMap( string filename, int MAX_X, int MAX_Y )
{
    cout<<"Loading from "<<filename<<"..."<<endl;
    ifstream infile;
    infile.open( filename.c_str() );
    string temp;

    infile>>temp>>temp>>temp>>temp>>temp;
    infile>>temp; //bg image

    for ( int k=0; k<3; k++ )
    {
        for ( int j=0; j<MAX_Y; j++ )
        {
            for ( int i=0; i<MAX_X; i++ )
            {
                int temp;
                infile>>temp;
                tile[k][i][j].FX( temp*32 );
            }
        }
    }
    infile.close();
    cout<<"Load complete"<<endl;
    //if ( confirmSound != NULL )
        play_sample( confirmSound, 255, 128, 1000, false );
}
