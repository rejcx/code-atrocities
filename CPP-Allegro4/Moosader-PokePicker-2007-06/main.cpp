#include <allegro.h>
#include <cstdlib>

// got sprites from http://www.spriters-resource.com

volatile long speed_counter = 0;

void increment_speed_counter()
{
    speed_counter++;
}
END_OF_FUNCTION(increment_speed_counter);

class obj_pokemon {
    public:
    int x, y, frame, width, height;
    BITMAP *img;    
    char direction;
    bool carrying; // for player, means you're carrying.  for pokemon, means you're being carried
    bool baking;
    bool dead;
    
    obj_pokemon() {
        carrying = false;  
        baking = false;   
        dead = false;
    }
};

bool thereIsCollision(obj_pokemon player, obj_pokemon pokemon)
{
  //Bounding Boxes First!
  if (!(        (player.y+35 >    pokemon.y ) &&
                (player.y <       pokemon.y+pokemon.height ) &&
                (player.x+35 >    pokemon.x ) &&
                (player.x <       pokemon.x+pokemon.width )))
  {
    return false;
  }
  else { return true; }
}
END_OF_FUNCTION (thereIsCollision);


int main(int argc, char *argv[])
{
    allegro_init();
    install_keyboard();
    install_timer();

    LOCK_VARIABLE(speed_counter);
    LOCK_FUNCTION(increment_speed_counter);
    install_int_ex(increment_speed_counter,BPS_TO_TIMER(90));
    install_joystick(JOY_TYPE_AUTODETECT);
    install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, 0);
    set_color_depth(16);
    set_gfx_mode(GFX_AUTODETECT_WINDOWED, 640, 480, 0, 0);
    
    srand ( 2 ); //gras coordinates
    int grassX[300], grassY[300];
    for (int i=0; i<300; i++) {
        grassX[i] = ( (int)rand() % 20 + 1 ) * 32;
        grassY[i] = ( (int)rand() % 15 + 1 ) * 32;   
    }
    
    float gameCounter = 0.0;    
    MIDI *bgsong = load_midi("pokemon.mid");
    SAMPLE *wavfire = load_sample("fire1.wav");
    bool fullscreen = false;

    BITMAP *buffer = create_bitmap(640,480);
   
    obj_pokemon pkmnClefairy;
    obj_pokemon pkmnOddish;
    obj_pokemon pkmnSlowbro;
    obj_pokemon fire;
    
    obj_pokemon player;
    player.x = 640/2;
    player.y = 128;
    player.img = load_bitmap("sprite_cream.bmp",NULL);
    player.width = 35;
    player.height = 35;
    player.direction = 'l';
    player.carrying = false;
    int yFrameBegin = 35;
    
    pkmnClefairy.x = 32;
    pkmnClefairy.y = 240;
    pkmnClefairy.img = load_bitmap("sprite_clefairy.bmp",NULL);
    pkmnClefairy.width = 32;
    pkmnClefairy.height = 32;
    pkmnOddish.x = 640-32;
    pkmnOddish.y = 240;
    pkmnOddish.width = 17;
    pkmnOddish.height = 22;
    pkmnOddish.img = load_bitmap("sprite_oddish.bmp",NULL);
    pkmnSlowbro.x = 640/2;
    pkmnSlowbro.y = 240;
    pkmnSlowbro.width = 32;
    pkmnSlowbro.height = 25;
    pkmnSlowbro.img = load_bitmap("sprite_slowbro.bmp",NULL);
    fire.img = load_bitmap("sprite_fire.bmp",NULL);
    fire.x = 640/2;
    fire.y = 360;
    fire.width = 25;
    fire.height = 32;

    play_midi(bgsong, true);

    while (!key[KEY_ESC])
    {
        while (speed_counter > 0)
        {
            poll_joystick();
            if(key[KEY_O]) {
                for (int i=0; i<300; i++) {
                    grassX[i] = ( (int)rand() % 20 + 1 ) * 32;
                    grassY[i] = ( (int)rand() % 15 + 1 ) * 32;   
                    
                    pkmnClefairy.x = ( (int)rand() % 19 + 1 ) * 32;
                    pkmnOddish.x = ( (int)rand() % 19 + 1 ) * 32;
                    pkmnSlowbro.x = ( (int)rand() % 19 + 1 ) * 32;
                    pkmnClefairy.y = ( (int)rand() % 14 + 1 ) * 32; 
                    pkmnOddish.y = ( (int)rand() % 14 + 1 ) * 32; 
                    pkmnSlowbro.x = ( (int)rand() % 14 + 1 ) * 32; 
                    
                    pkmnClefairy.dead = false;
                    pkmnOddish.dead = false;
                    pkmnSlowbro.dead = false;
                }    
            }
            if (key[KEY_P]) {
                if (fullscreen) {
                    set_gfx_mode(GFX_AUTODETECT_WINDOWED, 640, 480, 0, 0);
                    fullscreen = false;
                }
                else {
                    set_gfx_mode(GFX_AUTODETECT, 640, 480, 0, 0);  
                    fullscreen = true;
                }                
            } 
            
            //player movement
            if (key[KEY_W] || key[KEY_UP] || joy[0].stick[0].axis[1].d1) {
                //move up    
                if (player.y > 0) {
                    if (!player.carrying) { 
                        player.y -= 2;
                    }
                    else {
                        player.y -= 1;
                    }                    
                }
            }
            else if (key[KEY_S] || key[KEY_DOWN] || joy[0].stick[0].axis[1].d2) {
                //move down  
                if (player.y < 480-35) {
                    if (!player.carrying) {
                        player.y += 2;
                    }
                    else {
                        player.y += 1;   
                    }
                }  
            }
            if (key[KEY_A] || key[KEY_LEFT] || joy[0].stick[0].axis[0].d1) {
                //move left   
                if (player.x > 0) {
                    if (!player.carrying) {
                        player.x -= 2;
                    }
                    else
                    {
                        player.x -= 1;
                    }
                }
                player.direction = 'l';
            }
            else if (key[KEY_D] || key[KEY_RIGHT] || joy[0].stick[0].axis[0].d2) {
                //move right   
                if (player.x < 640-35) { 
                    if (!player.carrying) {
                        player.x += 2;
                    }
                    else {
                        player.x += 1;    
                    }
                }
                player.direction = 'r';
            }
            
            if (key[KEY_SPACE] || joy[0].button[2].b) {
                player.carrying = true;
            }
            else {
                player.carrying = false;   
            }
            
            //end player movement 
            
            pkmnClefairy.frame = (int)gameCounter % 4;
            pkmnOddish.frame = (int)gameCounter % 4;
            pkmnSlowbro.frame = (int)gameCounter % 4;
            fire.frame = (int)gameCounter % 4;
            player.frame = (int)gameCounter % 7;
            speed_counter--;
            gameCounter += 0.1;
            
            //Check to see if cream is carrying anything
            if (player.carrying) {
                if (thereIsCollision(player, pkmnClefairy)) {
                    pkmnClefairy.carrying = true;
                }
                else {
                    pkmnClefairy.carrying = false;    
                }
                
                if (thereIsCollision(player, pkmnOddish)) {
                    pkmnOddish.carrying = true;
                }
                else {
                    pkmnOddish.carrying = false;    
                }
                
                if (thereIsCollision(player, pkmnSlowbro)) {
                    pkmnSlowbro.carrying = true;
                }
                else {
                    pkmnSlowbro.carrying = false;    
                }
                
            }
            else {
                pkmnClefairy.carrying = false;  
                pkmnOddish.carrying = false;  
                pkmnSlowbro.carrying = false;  
            }
            
            //if a pokemon is in the fire
            if (thereIsCollision(fire, pkmnClefairy)) {
                play_sample(wavfire, 255, 128, 1000, false);
                pkmnClefairy.baking = false;
                pkmnClefairy.dead = true;
            }
            else {
                pkmnClefairy.baking = true;
            }
            if (thereIsCollision(fire, pkmnOddish)) {
                play_sample(wavfire, 255, 128, 1000, false);
                pkmnOddish.baking = true;
                pkmnOddish.dead = true;
            }
            else {
                pkmnOddish.baking = false;
            }
            if (thereIsCollision(fire, pkmnSlowbro)) {
                play_sample(wavfire, 255, 128, 1000, false);
                pkmnSlowbro.baking = true;
                pkmnSlowbro.dead = true;
            }
            else {
                pkmnSlowbro.baking = false;
            }
            
        }//while (speed_counter > 0)
        
        //Draw screen
        rectfill (buffer, 0, 0, 640, 480, makecol(0, 200, 50));
        for (int i=0; i<300; i++) {
            //draw grass details
            
            line(buffer, grassX[i], grassY[i], grassX[i], grassY[i]-6, makecol(0, 150, 0));
            line(buffer, grassX[i], grassY[i], grassX[i]+4, grassY[i]-5, makecol(0, 150, 0));
            line(buffer, grassX[i], grassY[i], grassX[i]-4, grassY[i]-5, makecol(0, 150, 0));
        }
        
        if (pkmnClefairy.frame == 3) {
            pkmnClefairy.frame = 1;
            pkmnOddish.frame = 1;
            pkmnSlowbro.frame = 1;
            fire.frame = 1;
        }
        
        if (pkmnClefairy.carrying == false && pkmnClefairy.dead == false) {
            if (pkmnClefairy.y < player.y) {
                masked_blit(pkmnClefairy.img, buffer, pkmnClefairy.frame*pkmnClefairy.width, 0, pkmnClefairy.x, pkmnClefairy.y, pkmnClefairy.width, pkmnClefairy.height);
            }
        }
        if (pkmnOddish.carrying == false && pkmnOddish.dead == false) {
            if (pkmnOddish.y < player.y) {
                masked_blit(pkmnOddish.img, buffer, pkmnOddish.frame*pkmnOddish.width, 0, pkmnOddish.x, pkmnOddish.y, pkmnOddish.width, pkmnOddish.height);
            }   
        }
        
        if (pkmnSlowbro.carrying == false && pkmnSlowbro.dead == false) {
            if (pkmnSlowbro.y < player.y) {
                masked_blit(pkmnSlowbro.img, buffer, pkmnSlowbro.frame*pkmnSlowbro.width, 0, pkmnSlowbro.x, pkmnSlowbro.y, pkmnSlowbro.width, pkmnSlowbro.height);        
            }
        }
        
        if (player.carrying) { yFrameBegin = 0; }
        else { yFrameBegin = 35; }
        
        if (player.direction == 'l') {
            
            masked_blit(player.img, buffer, player.frame*player.width, yFrameBegin, player.x, player.y, player.width, player.height);   
        }
        else {
            BITMAP *temp = load_bitmap("sprite_invisible.bmp",NULL);
            masked_blit(player.img, temp, player.frame*player.width, yFrameBegin, 0, 0, player.width, player.height);
            draw_sprite_h_flip(buffer, temp, player.x, player.y);
        }   
        
        //CLEFAIRY
        
        if (pkmnClefairy.carrying == false && pkmnClefairy.dead == false) {   //PLAYER IS NOT CARRYING CLEFAIRY
            if (pkmnClefairy.y >= player.y) {
                masked_blit(pkmnClefairy.img, buffer, pkmnClefairy.frame*pkmnClefairy.width, 0, pkmnClefairy.x, pkmnClefairy.y, pkmnClefairy.width, pkmnClefairy.height);
            }
        }
        else if (pkmnClefairy.carrying == true && pkmnClefairy.dead == false) {          // PLAYER IS CARRYING CLEFAIRY
            if (player.direction == 'l') { pkmnClefairy.x = player.x - (pkmnClefairy.width / 2); }
            else { pkmnClefairy.x = player.x + (pkmnClefairy.width / 2); }
            pkmnClefairy.y = player.y + 5;
            masked_blit(pkmnClefairy.img, buffer, pkmnClefairy.width, 0, pkmnClefairy.x, pkmnClefairy.y, pkmnClefairy.width, pkmnClefairy.height);
        }
        else if (pkmnClefairy.dead) {       //CLEFAIRY IS DEAD
            masked_blit(pkmnClefairy.img, buffer, pkmnClefairy.width * 3, 0, fire.x, fire.y, pkmnClefairy.width, pkmnClefairy.height);
        }
        
        //ODDISH
        
        if (pkmnOddish.carrying == false && pkmnOddish.dead == false) {     //PLAYER IS NOT CARRYING ODDISH
            if (pkmnOddish.y >= player.y) {     //DRAW IT AT IT'S X
                masked_blit(pkmnOddish.img, buffer, pkmnOddish.frame*pkmnOddish.width, 0, pkmnOddish.x, pkmnOddish.y, pkmnOddish.width, pkmnOddish.height);
            }
        }
        else if (pkmnOddish.carrying == true && pkmnOddish.dead == false) {      // PLAYER IS CARRYING ODDISH
            if (player.direction == 'l') { pkmnOddish.x = player.x - (pkmnOddish.width / 2); }
            else { pkmnOddish.x = player.x  + (pkmnOddish.width / 2); }
            pkmnOddish.y = player.y + 5;
            masked_blit(pkmnOddish.img, buffer, pkmnOddish.width, 0, pkmnOddish.x, pkmnOddish.y, pkmnOddish.width, pkmnOddish.height);
        }
        else if (pkmnOddish.dead) {     //ODDISH IS DEAD
            masked_blit(pkmnOddish.img, buffer, pkmnOddish.width * 3, 0, fire.x, fire.y, pkmnOddish.width, pkmnOddish.height);
        }
        
        //SLOWBRO
        
        if (pkmnSlowbro.carrying == false && pkmnSlowbro.dead == false) {    //PLAYER IS NOT CARRYING SLOWBRO
            if (pkmnSlowbro.y >= player.y) {
                masked_blit(pkmnSlowbro.img, buffer, pkmnSlowbro.frame*pkmnSlowbro.width, 0, pkmnSlowbro.x, pkmnSlowbro.y, pkmnSlowbro.width, pkmnSlowbro.height);        
            }
        }
        else if (pkmnSlowbro.carrying == true && pkmnSlowbro.dead == false) {      // PLAYER IS CARRYING SLOWBRO
            if (player.direction == 'l') { pkmnSlowbro.x = player.x - (pkmnSlowbro.width / 2); }
            else { pkmnSlowbro.x = player.x + (pkmnSlowbro.width / 2); }
            pkmnSlowbro.y = player.y + 5;
            masked_blit(pkmnSlowbro.img, buffer, pkmnSlowbro.width, 0, pkmnSlowbro.x, pkmnSlowbro.y, pkmnSlowbro.width, pkmnSlowbro.height);
        }
        else if (pkmnSlowbro.dead) {        //SLOWBRO IS DEAD
            masked_blit(pkmnSlowbro.img, buffer, pkmnSlowbro.width*3, 0, fire.x, fire.y, pkmnSlowbro.width, pkmnSlowbro.height);
        }
        
        masked_blit(fire.img, buffer, fire.frame * fire.width, 0, fire.x, fire.y, fire.width, fire.height);
        
        text_mode(-1);
        //textprintf(buffer,font,0,0,makecol(255,255,255), "PokePicker");
        textout_centre(buffer, font, "P - Toggle Fullscreen mode, O - Generate new map", 320, 5, makecol(255,255,255));
        textout_centre(buffer, font, "Use WASD, Arrow Keys, or Gamepad to move", 320, 15, makecol(255,255,255));
        textout_centre(buffer, font, "Press SPACE or Button 2 to carry a pokemon", 320, 25, makecol(255,255,255));
        
        acquire_screen();
        blit(buffer, screen, 0, 0, 0, 0, 640, 480);
        clear_bitmap(buffer);
        release_screen();
    }//while (!key[KEY_ESC])
    
    destroy_midi(bgsong);
    destroy_bitmap(pkmnClefairy.img);
    destroy_bitmap(pkmnOddish.img);
    destroy_bitmap(pkmnSlowbro.img);
    destroy_bitmap(buffer);
    destroy_bitmap(fire.img);
    destroy_sample(wavfire);

    return 0;
}
END_OF_MAIN();
