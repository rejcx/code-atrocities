#pragma once

class rjm_cowClass
{
    private:
    int x;
    int y;
    bool tipped;
    bool exists;
    bool clicked;
    public:
    rjm_cowClass() { tipped = false; exists = false; clicked = false; }
    void draw( BITMAP *buffer, BITMAP *graphics );
    bool check_click( int mouse_x, int mouse_y );
    void set_x( int value ) { x = value; }
    int get_x() const { return x; }
    void set_y( int value ) { y = value; }
    int get_y() const { return y; }
    void set_exists( bool value ) { exists = value; }
    bool get_exists() const { return exists; }
    void set_clicked( bool value ) { clicked = value; }
    bool get_clicked() const { return clicked; }
    void set_tipped( bool value ) { tipped = value; }
    bool get_tipped() const { return tipped; }
};

void rjm_cowClass::draw( BITMAP *buffer, BITMAP *graphics )
{
    if ( exists )
    {
        if ( !tipped ) { masked_blit( graphics, buffer, 870, 512, x, y, 38, 38 ); }
        else { masked_blit( graphics, buffer, 870+38, 512, x, y, 38, 38 ); }
    }
}

bool rjm_cowClass::check_click( int mouse_x, int mouse_y )
{
    int mouse_w_h = 10;
    int cow_w_h = 40;

    if (    ( mouse_x+mouse_w_h >   x ) &&
            ( mouse_x           <   x+cow_w_h ) &&
            ( mouse_y+mouse_w_h >   y ) &&
            ( mouse_y           <   y+cow_w_h ) )
    {
        //collision
        return true;
    }
    return false;
}




