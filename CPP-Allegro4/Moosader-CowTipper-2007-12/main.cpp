#include <cstdlib>
#include <string>
#include <allegro.h>
#include "rjm_cowClass.h"
#include "rjm_buttonClass.h"
#include "rjm_graphicsClass.h"
/*
V 1.1 update -- fixed bug where program thinks the mouse button
stays down after the game is won. (Now behaves normally) =P

COW TIPPING - START 11-24-07, FINISH 12-01-07 (coding on and off)
The source isn't really ideal-- the classes don't really make
much sense, but this isn't a huge project so I didn't organize it that much. :(
--Rachel J. Morris!
*/

using namespace std;

const int scr_w = 800;
const int scr_h = 600;

volatile long speed_counter = 0;

void increment_speed_counter()
{
    speed_counter++;
}
END_OF_FUNCTION(increment_speed_counter);

void init_allegro();
void draw_screen( BITMAP *buffer, BITMAP *graphics, rjm_cowClass Cow[15][15], int turn, int score[2], int cows_left, float counter, rjm_buttonClass buttons, rjm_graphicsClass game_graphics );
void handle_click( rjm_cowClass Cow[15][15], int mouse_x, int mouse_y, int score[2], int *cows_left, int *turn, bool *click, SAMPLE *moo, int *amt_tipped, int *tip_x, int *tip_y );
void init_game( rjm_cowClass[15][15], int *cows_left );
void restart_game( rjm_cowClass[15][15], int *cows_left, int score[2], int *turn );

enum WinnerEnum { NOWINNER, RED, BLUE, TIE };

int main( int argc, char *argv[] )
{
    init_allegro();
    WinnerEnum winner;
    winner = NOWINNER;

    BITMAP *buffer = create_bitmap(scr_w,scr_h);
    BITMAP *graphics = load_bitmap( "cowgraphics.bmp", NULL );
    SAMPLE *moo = load_sample( "cow.wav" );
    SAMPLE *restart = load_sample( "cowjingle2.wav" );
    MIDI *song = load_midi( "cowthief.mid" );
    MIDI *win = load_midi( "cowjoyful_song.mid" );
    bool fullscreen = true;   bool click = false;     bool done = false;  int turn = 0;   bool game_over = false;
    int cows_left = 0;          float counter = 0.0;    int score[2] = { 0, 0 };
    int amt_tipped = 0;
    int tip_counter = 5;
    int tip_x;
    int tip_y;

    rjm_cowClass Cow[15][15];
    rjm_buttonClass buttons;
    rjm_graphicsClass game_graphics;

    restart_game( Cow, &cows_left, score, &turn );
    play_midi( song, true );
    while ( !done )
    {
        while (speed_counter > 0)
        {
            if (key[KEY_F5])        //FULLSCREEN TOGGLE!
            {
                if ( fullscreen ) { set_gfx_mode(GFX_AUTODETECT_WINDOWED, scr_w, scr_h, 0, 0); fullscreen = false; }
                else { set_gfx_mode(GFX_AUTODETECT, scr_w, scr_h, 0, 0); fullscreen = true; }
            }
            if (key[KEY_F4]) { done = true; }   //SECRET QUIT KEY!
            if ( mouse_b )
            {
                click = true;
            }
            else
            {
                if ( click == true )    //only process button clicks when the mouse button is released, which is the norm
                {
                    if ( winner == NOWINNER )
                    {
                        tip_counter = 20;
                        amt_tipped = 0;
                        handle_click( Cow, mouse_x, mouse_y, score, &cows_left, &turn, &click, moo, &amt_tipped, &tip_x, &tip_y );
                    }
                    if ( buttons.button_clicked( mouse_x, mouse_y ) == "quit" )
                    {
                        done = true;
                    }
                    else if ( buttons.button_clicked( mouse_x, mouse_y ) == "reset" )
                    {
                        restart_game( Cow, &cows_left, score, &turn );
                        play_sample( restart, 255, 128, 2000, false );
                        play_midi( song, true );
                        winner = NOWINNER;
                        click = false;
                    }
                } //Otherwise, ignore
            }
            if ( cows_left <= 0 && winner == NOWINNER )
            {
                //GAME ENDS
                if ( game_over == false ) { play_midi( win, true ); }
                if ( score[0] > score[1] ) //player 1 wins
                {
                    winner = RED;
                    play_midi( win, true );
                }
                else if ( score[0] < score[1] ) //player 2 wins
                {
                    winner = BLUE;
                    play_midi( win, true );
                }
                else //tie
                {
                    winner = TIE;
                    play_midi( win, true );
                }
            }
            counter += 0.20;
            speed_counter--;
        }//while (speed_counter > 0)
        vsync();
        draw_screen( buffer, graphics, Cow, turn, score, cows_left, counter, buttons, game_graphics );
        if ( tip_counter > 0 )
        {
            int color;
            int shadow = makecol(255, 255, 255);
            if ( turn == 1 ) { color = makecol(255, 0, 0); }
            else { color = makecol(0, 0, 255); }
            //shadow
            textprintf_centre( buffer, font, tip_x+1, tip_y + tip_counter, shadow, "%i", amt_tipped );
            textprintf_centre( buffer, font, tip_x-1, tip_y + tip_counter, shadow, "%i", amt_tipped );

            textprintf_centre( buffer, font, tip_x, tip_y+1 + tip_counter, shadow, "%i", amt_tipped );
            textprintf_centre( buffer, font, tip_x, tip_y-1 + tip_counter, shadow, "%i", amt_tipped );

            textprintf_centre( buffer, font, tip_x+1, tip_y+1 + tip_counter, shadow, "%i", amt_tipped );
            textprintf_centre( buffer, font, tip_x-1, tip_y+1 + tip_counter, shadow, "%i", amt_tipped );

            textprintf_centre( buffer, font, tip_x+1, tip_y-1 + tip_counter, shadow, "%i", amt_tipped );
            textprintf_centre( buffer, font, tip_x-1, tip_y-1 + tip_counter, shadow, "%i", amt_tipped );
            //text
            textprintf_centre( buffer, font, tip_x, tip_y + tip_counter, color, "%i", amt_tipped );
            tip_counter--;
        }
        if ( winner != NOWINNER )
        {
            if ( game_over == false ) { game_over = true; }
            if ( winner == RED ) //red
            {
                masked_blit( graphics, buffer, 880, 348, 600/2 - 55, scr_h/2 - 25 +((int)counter % 4 * 2), 40, 30 );
                masked_blit( graphics, buffer, 800, 400, 600/2 , scr_h/2 - 25 +((int)counter % 4 * 2), 133, 36 );
            }
            else if ( winner == BLUE ) // blue
            {
                masked_blit( graphics, buffer, 880, 320, 600/2 - 55, scr_h/2 - 25 +((int)counter % 4 * 2), 54, 29 );
                masked_blit( graphics, buffer, 800, 400, 600/2 , scr_h/2 - 25 +((int)counter % 4 * 2), 133, 36 );
            }
            else if ( winner == TIE ) //tie
            {
                masked_blit( graphics, buffer, 850, 551, 600/2 - 124/2, 600/2 - 46/2 +((int)counter % 4 * 2), 124, 47 );
            }
        }
        acquire_screen();
        blit(buffer, screen, 0, 0, 0, 0, scr_w, scr_h);
        clear_bitmap(buffer);
        release_screen();
    }//while (!key[KEY_ESC])
    return 0;
}
END_OF_MAIN();

void init_allegro()
{
    allegro_init();
    install_keyboard();
    install_timer();
    install_mouse();
    install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, 0);
    LOCK_VARIABLE(speed_counter);
    LOCK_FUNCTION(increment_speed_counter);
    install_int_ex(increment_speed_counter,BPS_TO_TIMER(90));
    set_color_depth(16);
    set_gfx_mode(GFX_AUTODETECT, scr_w, scr_h, 0, 0);
    text_mode(-1);
}

void draw_screen( BITMAP *buffer, BITMAP *graphics, rjm_cowClass Cow[15][15], int turn, int score[2], int cows_left, float counter, rjm_buttonClass buttons, rjm_graphicsClass game_graphics )
{
    //Draw frame
    masked_blit( graphics, buffer, 0, 0, 0, 0, scr_w, scr_h );
    masked_blit( graphics, buffer, 800, 437, 605, 35, 175, 75 );
    //Draw grass
    for ( int i=0; i<15; i++ )
    {
        for ( int j=0; j<15; j++ )
        {
            masked_blit( graphics, buffer, 800, 512, 17+i*38, 14+j*38, 38, 38 );
        }
    }
    //Draw cows
    for ( int i=0; i<15; i++ )
    {
        for ( int j=0; j<15; j++ )
        {
            Cow[i][j].draw( buffer, graphics );
        }
    }
    //Text
    if ( turn == 0 )
    {
        int x = 700;
        int y = 14;
        int color = makecol(255, 175, 190);
        textprintf_centre( buffer, font, x-1, y-1, color, "Red Player's turn!" );
        textprintf_centre( buffer, font, x-1, y, color, "Red Player's turn!" );
        textprintf_centre( buffer, font, x-1, y+1, color, "Red Player's turn!" );
        textprintf_centre( buffer, font, x+1, y-1, color, "Red Player's turn!" );
        textprintf_centre( buffer, font, x+1, y, color, "Red Player's turn!" );
        textprintf_centre( buffer, font, x+1, y+1, color, "Red Player's turn!" );
        textprintf_centre( buffer, font, x, y, makecol(255, 0, 0), "Red Player's turn!" );
    }
    else
    {
        int x = 700;
        int y = 14;
        int color = makecol(0, 255, 255);
        textprintf_centre( buffer, font, x-1, y-1, color, "Blue Player's turn!" );
        textprintf_centre( buffer, font, x-1, y, color, "Blue Player's turn!" );
        textprintf_centre( buffer, font, x-1, y+1, color, "Blue Player's turn!" );
        textprintf_centre( buffer, font, x+1, y-1, color, "Blue Player's turn!" );
        textprintf_centre( buffer, font, x+1, y, color, "Blue Player's turn!" );
        textprintf_centre( buffer, font, x+1, y+1, color, "Blue Player's turn!" );
        textprintf_centre( buffer, font, x, y, makecol(0, 0, 255), "Blue Player's turn!" );
    }
    textprintf_centre( buffer, font, 699, 566, makecol(100, 0, 0), "Red Player Score: %i", score[0] );
    textprintf_centre( buffer, font, 700, 565, makecol(255, 0, 0), "Red Player Score: %i", score[0] );
    textprintf_centre( buffer, font, 699, 576, makecol(0, 0, 100), "Blue Player Score: %i", score[1] );
    textprintf_centre( buffer, font, 700, 575, makecol(0, 0, 255), "Blue Player Score: %i", score[1] );
    textprintf_centre( buffer, font, 700, 555, makecol(0, 0, 0), "Cows Left: %i", cows_left );
    //draw buttons
    buttons.draw( buffer, graphics );
    game_graphics.draw( buffer, graphics, turn, counter );
    game_graphics.draw_cursor( buffer, graphics, mouse_x, mouse_y, turn, counter );
}

void handle_click( rjm_cowClass Cow[15][15], int mouse_x, int mouse_y, int score[2], int *cows_left, int *turn, bool *click, SAMPLE *moo, int *amt_tipped, int *tip_x, int *tip_y )
{
    for ( int i=0; i<15; i++ )
    {
        for ( int j=0; j<15; j++ )
        {
            if ( Cow[i][j].check_click( mouse_x, mouse_y ) )
            {
                if ( Cow[i][j].get_exists() == true && Cow[i][j].get_tipped() == false )
                {
                    //This cow was clicked
                    Cow[i][j].set_tipped( true );
                    score[(*turn)]++;
                    (*amt_tipped)++;
                    (*cows_left)--;
                    //Tip surrounding cows
                    int temp_i, temp_j;
                    for ( int k=0; k<8; k++)
                    {
                        // Explode any and all cows surrounding the one clicked
                        if ( k == 0 ) { temp_i = i+1; temp_j = j-1; }
                        else if ( k == 1 ) { temp_i = i+1; temp_j = j; }
                        else if ( k == 2 ) { temp_i = i+1; temp_j = j+1; }
                        else if ( k == 3 ) { temp_i = i; temp_j = j-1; }
                        else if ( k == 4 ) { temp_i = i; temp_j = j+1; }
                        else if ( k == 5 ) { temp_i = i-1; temp_j = j-1; }
                        else if ( k == 6 ) { temp_i = i-1; temp_j = j; }
                        else if ( k == 7 ) { temp_i = i-1; temp_j = j+1; }
                        if ( temp_i < 0 || temp_i > 15 || temp_j < 0 || temp_j > 15 ) { ; } // do nothing, one or both of the indices are invalid
                        else
                        {
                            if ( Cow[temp_i][temp_j].get_exists() == true && Cow[temp_i][temp_j].get_tipped() == false )
                            {
                                Cow[temp_i][temp_j].set_tipped( true );
                                score[(*turn)]++;
                                (*cows_left)--;
                                (*amt_tipped)++;
                            }
                        }
                    }
                    if ( (*turn) == 0 ) { (*turn) = 1; }
                    else { (*turn) = 0; }
                    Cow[i][j].set_clicked( false );
                    (*tip_x) = Cow[i][j].get_x()+19;
                    (*tip_y) = Cow[i][j].get_y()+2;
                    //play_sample(sample, volume, pan, frequency, loop)
                    play_sample( moo, 255, 128, 1500, false );
                } //if ( Cow[i][j].get_tipped() == false )
                else
                {
                }
            } //if ( Cow[i][j].check_click( mouse_x, mouse_y ) )
        } //for ( int j=0; j<15; j++ )
    } //for ( int i=0; i<15; i++ )
    (*click) = false;
}

void init_game( rjm_cowClass Cow[15][15], int *cows_left )
{
    srand((unsigned)time(NULL));
    for ( int i=0; i<15; i++ )
    {
        for ( int j=0; j<15; j++ )
        {
            Cow[i][j].set_exists( (int)rand() % 2 );
            if ( Cow[i][j].get_exists() ) { (*cows_left)++; }
            Cow[i][j].set_tipped( false );
        }
    }
}

void restart_game( rjm_cowClass Cow[15][15], int *cows_left, int score[2], int *turn )
{
    score[0] = 0;
    score[1] = 0;
    (*turn) = 0;
    for ( int i=0; i<15; i++ )
    {
        for ( int j=0; j<15; j++ )
        {
            Cow[i][j].set_x ( 17+i*38 );
            Cow[i][j].set_y ( 14+j*38 );
        }
    }
    (*cows_left) = 0;
    init_game( Cow, cows_left );
}