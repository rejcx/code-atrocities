DDD	 AA	PPPP	H  H	N   N	EEEE		SSSS	PPPP	 AA	CCCC	EEEE	!!
D  D	A  A	P  P	H  H	NN  N	E		S	P  P	A  A	C	E	!!
D  D	AAAA	PPPP	HHHH	N N N	EEE	IN	SSSS	PPPP	AAAA	C	EEEE	!!
D  D	A  A	P	H  H	N  NN	E		   S	P	A  A	C	E	
DDD	A  A	P	H  H	N   N	EEEE		SSSS	P	A  A	CCCC	EEEE  	!!

Programming & Sprites by Rachel J. Morris

Background image and sound effects from RPG Maker 2003

Music from Ogre Battle


Website: http://www.eccentrix.com/members/moosader

DeviantArt: http://kadotuksen-lusikka.deviantart.com/

Allegro: http://www.allegro.cc/members/RachelMorris

Made Dec 18th (in one night)

## CONTROLS

* WASD or arrow keys to move
* F or Enter to use bullet-shield
* Spacebar to shoot
* Esc to pause
* F4 to quit
* F5 to toggle fullscreen


## GIFTINGS

* To Daphne (da - FlyingMarshmallows) 
for no real reason except that I was bored


## MISSING ASSETS
I deleted assets that I did not own.
Some of my old projects used RPG Maker music/sounds/sprites,
as well as stuff from other places.
