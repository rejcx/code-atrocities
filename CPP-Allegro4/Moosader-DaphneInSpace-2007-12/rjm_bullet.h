#pragma once
#include <allegro.h>
#include "global.h"

class RJM_BULLET
{
    public:
    float x;
    float y;
    float speed;
    int codex;
    int codey;
    BITMAP *image;
    int w;
    int h;
    bool active;

    RJM_BULLET()
    {
        x = 0;
        y = 0;
        w = 16;
        h = 16;
        image = load_bitmap("content/bullets.bmp", NULL);
        speed = 5.0;
        codex = 0;
        codey = 0;
        active = false;
    }
    void draw(BITMAP *buffer, float counter)
    {
        masked_blit (image, buffer, (int)counter*w + codex, codey, x, y, w, h);
    }
};
