#pragma once
#include <allegro.h>
#include "rjm_bullet.h"
#include "global.h"

class RJM_CHARACTER
{
    public:
    float x;
    float y;
    int codex;
    float speed;
    bool player;
    bool active;
    int shooting;
    int w;
    int h;
    BITMAP *image;
    int baddie_type;
    RJM_BULLET b_bullet[5];         //baddie bullets
    RJM_BULLET p_bullet[20];        //player bullets
    int die_time;

    RJM_CHARACTER()
    {
        die_time = 0;
        shooting = -1;
        w = 64.0;
        h = 48.0;
        x = 0.0;
        y = scr_h/2 - h/2;
        speed = 1.0;
    }

    void dostuff()
    {
        int temp;
        if ( baddie_type == 0 ) { temp = (int)rand() % 25; }
        else if ( baddie_type == 1 ) { temp = (int)rand() % 20; }
        else if ( baddie_type == 2 ) { temp = (int)rand() % 15; }
        if ( temp == 1 )
        {
            for (int i=0; i<max_enemy_bullets; i++)
            {
                if ( b_bullet[i].active == false )
                {
                    b_bullet[i].active = true;
                    b_bullet[i].x = x - w;
                    b_bullet[i].y = y + w/2;
                    break;
                }
            }
        }
    }
    void draw(BITMAP *buffer, float counter)
    {
        if ( player )
        {
            if ( shooting == -1 )
                masked_blit(image, buffer, (int)counter*64, 0, x, y, w, h);
            else
            {
                masked_blit(image, buffer, 128, 0, x, y, w, h);
            }
        }
        else
        {
            masked_blit(image, buffer, codex, 0, x, y, w, h);
        }
    }
    void randomize()
    {
        image = load_bitmap("content/baddies.bmp", NULL);
        player = false;
        active = false;
        x = 800;
        y = ( (int)rand() % 11 + 1 ) * 48;
        int temp;
        temp = (int)rand() % 3;
        if (temp == 0)
        {
            baddie_type = 0;
            codex = 0;
            speed = 0.5;
            for (int i=0; i<max_enemy_bullets; i++)
            {
                b_bullet[i].codex = 32;
                b_bullet[i].codey = 0;
                b_bullet[i].speed = 0.7;
            }
        }
        else if (temp == 1)
        {
            baddie_type = 1;
            codex = 64;
            speed = 2.0;
            for (int i=0; i<max_enemy_bullets; i++)
            {
                b_bullet[i].codex = 32;
                b_bullet[i].codey = 16;
                b_bullet[i].speed = 2.5;
            }
        }
        else if (temp == 2)
        {
            baddie_type = 2;
            codex = 128;
            speed = 3.0;
            for (int i=0; i<max_enemy_bullets; i++)
            {
                b_bullet[i].codex = 0;
                b_bullet[i].codey = 16;
                b_bullet[i].speed = 3.5;
            }
        }
    }
};
