#include <allegro.h>
#include <cstdlib>      //For random number generator
#include <ctime>        //To seed the random number generator
#include "quixGameObject.h"
#include "quixPlayObject.h"

/*
Version 1.1 Update --
- Added original backgorund music by YOURS TRUELY!
- Fixed Fullscreen bug
*/

//yay, the source code is still crap!
//Rachel J. Morris
//December 2006 / January 2007
// I really wouldn't advise using my sourcecode to learn programming
// XD  I didn't do any planning before-hand so it's a mess.
// It's hard to plan a game when you haven't really made one before. o_o;;

volatile long speed_counter = 0;

void increment_speed_counter()
{
    speed_counter++;
}
END_OF_FUNCTION(increment_speed_counter);

void changeControlScheme(int, PlayerObject*, MenuObject*, int);
void drawMenuStuff(BITMAP*, MenuObject, BITMAP*, BITMAP*, LevelObject);
void mouseDownClick(MenuObject*, int[]);
void mouseUp(MenuObject*, int[], PlayerObject*, LevelObject*, MIDI *);
void initializeCrap();
void generateBackground(BITMAP*, LevelObject);
void drawCharacters(PlayerObject[], LevelObject, BITMAP*);
void drawScoreBar(BITMAP*, PlayerObject[], float, float, LevelObject);
void drawLaser(LaserObject, BITMAP*, LevelObject, int);
void shootLaser(LaserObject*, PlayerObject);
void checkCollision(LaserObject*, PlayerObject[], int, LevelObject);
void moveCharacter(LevelObject, PlayerObject*, int, char);

int main()
{
  initializeCrap();
  install_int_ex(increment_speed_counter,BPS_TO_TIMER(120));
  int screenMode = 0; //0 for windowed, 1 for fullscreen
  srand((unsigned)time(NULL));

  BITMAP *buffer = create_bitmap(800,600);
  BITMAP *titleimg = load_bitmap("quiximgTitle.bmp",NULL);
  BITMAP *imgPlayers = load_bitmap("quiximgPlayerSet.bmp",NULL);
  MIDI *song = load_midi("quixsong.mid");
  PlayerObject poPlayer[4];
  LaserObject loLaser[4];
  LevelObject Level;
  MenuObject Menu;
  float scrollerX = -150.0, scrollerY = 5.0;
  int winner;

  poPlayer[0].setControlScheme(0);
  poPlayer[1].setControlScheme(1);
  poPlayer[2].setControlScheme(2);
  poPlayer[3].setControlScheme(3);
  poPlayer[0].samSound = load_sample("quixp1Death.wav");
  poPlayer[1].samSound = load_sample("quixp2Death.wav");
  poPlayer[2].samSound = load_sample("quixp3Death.wav");
  poPlayer[3].samSound = load_sample("quixp4Death.wav");
  poPlayer[0].setX(32);
  poPlayer[0].setY(32);
  poPlayer[0].setDir('l');
  poPlayer[1].setX(32);
  poPlayer[1].setY(544);
  poPlayer[1].setDir('l');
  poPlayer[2].setX(736);
  poPlayer[2].setY(544);
  poPlayer[2].setDir('r');
  poPlayer[3].setX(736);
  poPlayer[3].setY(32);
  poPlayer[3].setDir('r');

  int mouseDown[21];    //Kind of a crappy array --- Each button is a #
                        //and it's set to true if that button was clicked.
  for (int i = 0; i < 20; i++) { mouseDown[i] = false; }

  while ( !Menu.isDone() )
  {
    while (speed_counter > 0)
    {
      /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
      /* ~~~~~~~~~~~~~~~~~~~~~ MENU SCREEN ~~~~~~~~~~~~~~~~~~~~~ */
      /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
      if ( Menu.getEnabled() == true ) {
        if (key[KEY_Z]) {
          if (Menu.getFullscreen() == true) { Menu.changeScreenSize(0); Menu.setFullscreen(false); }
          else { Menu.changeScreenSize(1); Menu.setFullscreen(true); }
        }

        if (mouse_b & 1)
        {
          mouseDownClick(&Menu, mouseDown);
        }
        else {
          mouseUp(&Menu, mouseDown, poPlayer, &Level, song);
        }
      }
      /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
      /* ~~~~~~~~~~~~~~~~~~~~~~~ IN-GAME ~~~~~~~~~~~~~~~~~~~~~~~ */
      /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */
      else {

        //Player Input1
        if (key[KEY_ESC]) {
            play_midi(NULL, false);
          if (Menu.fragLimitHit == true) {
            Menu.fragLimitHit == false;

            for (int i=0; i<4; i++) {
              poPlayer[i].resetScore();
              poPlayer[i].resetDeaths();
            }

            poPlayer[0].setX(32);
            poPlayer[0].setY(32);
            poPlayer[0].setDir('l');
            poPlayer[1].setX(32);
            poPlayer[1].setY(544);
            poPlayer[1].setDir('l');
            poPlayer[2].setX(736);
            poPlayer[2].setY(544);
            poPlayer[2].setDir('r');
            poPlayer[3].setX(736);
            poPlayer[3].setY(32);
            poPlayer[3].setDir('r');

            Menu.fragLimitHit = false;

            Menu.resetPressed = false;
            mouseDown[19] = false;

          }
          Level.setEnabled(false); Menu.setEnabled(true);
        }
        for (int i=0; i<4; i++) {
          if (poPlayer[i].getControlScheme() == 0) {
            if (key[KEY_W]) { moveCharacter(Level, &poPlayer[i], i, 'u'); }
            if (key[KEY_A]) { moveCharacter(Level, &poPlayer[i], i, 'l'); }
            if (key[KEY_S]) { moveCharacter(Level, &poPlayer[i], i, 'd'); }
            if (key[KEY_D]) { moveCharacter(Level, &poPlayer[i], i, 'r'); }
            if (key[KEY_F]) {
              if ( loLaser[i].getEnabled() == false ) { shootLaser( &loLaser[i], poPlayer[i]); }
            }
          }
          else if (poPlayer[i].getControlScheme() == 1) {
            if (key[KEY_I]) { moveCharacter(Level, &poPlayer[i], i, 'u'); }
            if (key[KEY_J]) { moveCharacter(Level, &poPlayer[i], i, 'l'); }
            if (key[KEY_K]) { moveCharacter(Level, &poPlayer[i], i, 'd'); }
            if (key[KEY_L]) { moveCharacter(Level, &poPlayer[i], i, 'r'); }
            if (key[KEY_H]) { if ( loLaser[i].getEnabled() == false ) { shootLaser( &loLaser[i], poPlayer[i]); } }
          }
          else if (poPlayer[i].getControlScheme() == 2) {
            if (key[KEY_UP]) { moveCharacter(Level, &poPlayer[i], i, 'u'); }
            if (key[KEY_LEFT]) { moveCharacter(Level, &poPlayer[i], i, 'l'); }
            if (key[KEY_DOWN]) { moveCharacter(Level, &poPlayer[i], i, 'd'); }
            if (key[KEY_RIGHT]) { moveCharacter(Level, &poPlayer[i], i, 'r'); }
            if (key[KEY_RCONTROL]) { if ( loLaser[i].getEnabled() == false ) { shootLaser( &loLaser[i], poPlayer[i]); } }
          }
          else if (poPlayer[i].getControlScheme() == 3) {
            if (key[KEY_8_PAD]) { moveCharacter(Level, &poPlayer[i], i, 'u'); }
            if (key[KEY_4_PAD]) { moveCharacter(Level, &poPlayer[i], i, 'l'); }
            if (key[KEY_2_PAD]) { moveCharacter(Level, &poPlayer[i], i, 'd'); }
            if (key[KEY_6_PAD]) { moveCharacter(Level, &poPlayer[i], i, 'r'); }
            if (key[KEY_PLUS_PAD]) { if ( loLaser[i].getEnabled() == false ) { shootLaser( &loLaser[i], poPlayer[i]); } }
          }
          else if(poPlayer[i].getControlScheme() == 4) {
            //Joystick #1
            poll_joystick();
            if (joy[0].stick[0].axis[1].d1) { moveCharacter(Level, &poPlayer[i], i, 'u'); } //Up
            if (joy[0].stick[0].axis[0].d1) { moveCharacter(Level, &poPlayer[i], i, 'l'); } //Left
            if (joy[0].stick[0].axis[1].d2) { moveCharacter(Level, &poPlayer[i], i, 'd'); } //Down
            if (joy[0].stick[0].axis[0].d2) { moveCharacter(Level, &poPlayer[i], i, 'r'); } //Right
            if (joy[0].button[0].b || joy[0].button[1].b || joy[0].button[2].b) { if ( loLaser[i].getEnabled() == false ) { shootLaser( &loLaser[i], poPlayer[i]); } }
          }
          else if(poPlayer[i].getControlScheme() == 5) {
            //Joystick #2
            poll_joystick();
            if (joy[1].stick[0].axis[1].d1) { moveCharacter(Level, &poPlayer[i], i, 'u'); } //Up
            if (joy[1].stick[0].axis[0].d1) { moveCharacter(Level, &poPlayer[i], i, 'l'); } //Left
            if (joy[1].stick[0].axis[1].d2) { moveCharacter(Level, &poPlayer[i], i, 'd'); } //Down
            if (joy[1].stick[0].axis[0].d2) {moveCharacter(Level, &poPlayer[i], i, 'r'); } //Right
            if (joy[1].button[0].b || joy[0].button[1].b || joy[0].button[2].b) { if ( loLaser[i].getEnabled() == false ) { shootLaser( &loLaser[i], poPlayer[i]); } }
          }
          else if(poPlayer[i].getControlScheme() == 6) {
            //Joystick #3
            poll_joystick();
            if (joy[2].stick[0].axis[1].d1) { moveCharacter(Level, &poPlayer[i], i, 'u'); } //Up
            if (joy[2].stick[0].axis[0].d1) { moveCharacter(Level, &poPlayer[i], i, 'l'); } //Left
            if (joy[2].stick[0].axis[1].d2) {moveCharacter(Level, &poPlayer[i], i, 'd'); } //Down
            if (joy[2].stick[0].axis[0].d2) { moveCharacter(Level, &poPlayer[i], i, 'r'); } //Right
            if (joy[2].button[0].b || joy[0].button[1].b || joy[0].button[2].b) { if ( loLaser[i].getEnabled() == false ) { shootLaser( &loLaser[i], poPlayer[i]); } }
          }
          else if(poPlayer[i].getControlScheme() == 7) {
            //Joystick #4
            poll_joystick();
            if (joy[3].stick[0].axis[1].d1) { moveCharacter(Level, &poPlayer[i], i, 'u'); } //Up
            if (joy[3].stick[0].axis[0].d1) { moveCharacter(Level, &poPlayer[i], i, 'l'); } //Left
            if (joy[3].stick[0].axis[1].d2) { moveCharacter(Level, &poPlayer[i], i, 'd'); } //Down
            if (joy[3].stick[0].axis[0].d2) { moveCharacter(Level, &poPlayer[i], i, 'r'); } //Right
            if (joy[3].button[0].b || joy[0].button[1].b || joy[0].button[2].b) { if ( loLaser[i].getEnabled() == false ) { shootLaser( &loLaser[i], poPlayer[i]); } }
          }
        }
        if (Level.timeForAPowerupTimer >= 60) { Level.timeForAPowerupTimer = 0; }
        else { Level.timeForAPowerupTimer++; }
      }
      if ( Level.getFrame() < 32 ) { Level.setFrame( Level.getFrame() + 1 ); }
      else { Level.setFrame(0); }

    //Score Bar
      if (Level.getFragLimit() != 0 && Level.getEnabled() == true) {
        scrollerX += 0.5;
        if (scrollerX >= 1200.0) { scrollerX = -150.0; }
      }
      for (int i=0; i<4; i++) {
        //Res circle
        if (poPlayer[i].getRespawnCounter() >= 0) {
          poPlayer[i].setRespawnCounter( poPlayer[i].getRespawnCounter() + 1);
        }
        //"Laser" movement
        if (loLaser[i].getEnabled() == true) {
          if (loLaser[i].getDir() == 'u') { loLaser[i].moveUp('l'); }
          else if (loLaser[i].getDir() == 'd') { loLaser[i].moveDown('l'); }
          else if (loLaser[i].getDir() == 'l') { loLaser[i].moveLeft('l'); }
          else if (loLaser[i].getDir() == 'r') { loLaser[i].moveRight('l'); }
          checkCollision(&loLaser[i], poPlayer, i, Level);
        }
        if ((poPlayer[i].returnScore() == Level.getFragLimit()) && (Level.getFragLimit() > 0)) {
          //Frag Limit hit
          Menu.fragLimitHit = true;
          winner = i;
        }
      }

      speed_counter--;
    }//while (speed_counter > 0)

    if ( Menu.getEnabled() == true ) {
      drawMenuStuff(buffer, Menu, titleimg, imgPlayers, Level);
    }
    else if (Menu.fragLimitHit == true) {
      //Show win window!
      floodfill  (buffer, 0, 0, makecol(0,0,0));
      textprintf(buffer,font,348,203,makecol(100,0,0), "Player %i Wins!", winner+1 );
      textprintf(buffer,font,347,202,makecol(150,0,0), "Player %i Wins!", winner+1 );
      textprintf(buffer,font,346,201,makecol(200,0,0), "Player %i Wins!", winner+1 );
      textprintf(buffer,font,345,200,makecol(255,0,0), "Player %i Wins!", winner+1 );

      textprintf(buffer,font,330,250,makecol(255,0,0), "Player 1 score: %i", poPlayer[0].returnScore() );
      textprintf(buffer,font,330,260,makecol(255,0,0), "Player 2 score: %i", poPlayer[1].returnScore() );
      textprintf(buffer,font,330,270,makecol(255,0,0), "Player 3 score: %i", poPlayer[2].returnScore() );
      textprintf(buffer,font,330,280,makecol(255,0,0), "Player 4 score: %i", poPlayer[3].returnScore() );

      textprintf(buffer,font,330,249,makecol(255,255,255), "Player 1 score: %i", poPlayer[0].returnScore() );
      textprintf(buffer,font,330,259,makecol(255,255,255), "Player 2 score: %i", poPlayer[1].returnScore() );
      textprintf(buffer,font,330,269,makecol(255,255,255), "Player 3 score: %i", poPlayer[2].returnScore() );
      textprintf(buffer,font,330,279,makecol(255,255,255), "Player 4 score: %i", poPlayer[3].returnScore() );

      textout_centre(buffer, font, "Hit esc to continue", 400, 300, makecol(255,255,255));
    }
    else {
      generateBackground(buffer, Level);
      drawCharacters(poPlayer, Level, buffer);
      drawScoreBar(buffer, poPlayer, scrollerX, scrollerY, Level);
      if (loLaser[0].getEnabled() == true) { drawLaser(loLaser[0], buffer, Level, 0); }
      if (loLaser[1].getEnabled() == true) { drawLaser(loLaser[1], buffer, Level, 1); }
      if (loLaser[2].getEnabled() == true) { drawLaser(loLaser[2], buffer, Level, 2); }
      if (loLaser[3].getEnabled() == true) { drawLaser(loLaser[3], buffer, Level, 3); }
    }


    acquire_screen();
    blit(buffer, screen, 0, 0, 0, 0, 800, 600);
    clear_bitmap(buffer);
    release_screen();
  }//while ( !Menu.isDone() )

    return 0;

  return 0;
}
END_OF_MAIN();
