#include <allegro.h>
#include <cstdlib>      //For random number generator
#include <ctime>        //To seed the random number generator
#include "quixGameObject.h"
#include "quixPlayObject.h"

void respawnPlayer(PlayerObject*, LevelObject);
void setBarrierType(LevelObject*);

void changeControlScheme(int direction, PlayerObject *player, MenuObject *menu, int index) {

  int temp = player->getControlScheme() + direction;
  if (temp < 0) { temp = 7; }
  else if (temp > 7) { temp = 0; }

  player->setControlScheme(temp);
  menu->setControlScheme(index, temp);
}

void drawCharacters(PlayerObject poPlayer[], LevelObject Level, BITMAP *buffer) {
  //
  for (int i=0; i<4; i++) {
    int direction;
    if ( poPlayer[i].getHorizDir() == 'l' ) { direction=1; }
    else if ( poPlayer[i].getHorizDir() == 'r' ) { direction=0; }
    if ( Level.getFrame() >=0 && Level.getFrame() < 8 ) {
      //Frame 1
      masked_blit(Level.bmpPlayerFilmstrip, buffer, (direction * 105) + (35*0), i*35, poPlayer[i].getX(), poPlayer[i].getY(), 35, 35);
      //masked_blit(Level.bmpPlayerFilmstrip, buffer, (35*0), i*35, poPlayer[i].getX(), poPlayer[i].getY(), 35, 35);
    }
    else if ( Level.getFrame() >= 8 && Level.getFrame() < 16 ) {
      //Frame 2
      masked_blit(Level.bmpPlayerFilmstrip, buffer, (direction * 105) + (35*1), i*35, poPlayer[i].getX(), poPlayer[i].getY(), 35, 35);
      //masked_blit(Level.bmpPlayerFilmstrip, buffer,  (35*1), i*35, poPlayer[i].getX(), poPlayer[i].getY(), 35, 35);
    }
    else if ( Level.getFrame() >= 16 && Level.getFrame() < 24) {
      //Frame 3
      masked_blit(Level.bmpPlayerFilmstrip, buffer, (direction * 105) + (35*2), i*35, poPlayer[i].getX(), poPlayer[i].getY(), 35, 35);
      //masked_blit(Level.bmpPlayerFilmstrip, buffer, (35*2), i*35, poPlayer[i].getX(), poPlayer[i].getY(), 35, 35);
    }
    else if ( Level.getFrame() >= 24 && Level.getFrame() < 33 ) {
      //Frame 2
      masked_blit(Level.bmpPlayerFilmstrip, buffer, (direction * 105) + (35*1), i*35, poPlayer[i].getX(), poPlayer[i].getY(), 35, 35);
      //masked_blit(Level.bmpPlayerFilmstrip, buffer,  (35*1), i*35, poPlayer[i].getX(), poPlayer[i].getY(), 35, 35);
    }
    //textout_centre(buffer, font, "Frags:", poPlayer[i].getX() / 2, poPlayer[i].getY() / 2, makecol(200,0,0));
    int colorA, colorB;
    if ( i == 0 ) { colorA = makecol(100,255,255); colorB = makecol(0, 200, 200); }
    else if ( i == 1 ) { colorA = makecol(192,192,192); colorB = makecol(32, 32, 32); }
    else if ( i == 2 ) { colorA = makecol(255,200,150); colorB = makecol(255, 125, 15); }
    else if ( i == 3 ) { colorA = makecol(240,165,165); colorB = makecol(224, 80, 80); }
    textprintf(buffer,font,poPlayer[i].getX() - 17, poPlayer[i].getY() - 11 ,makecol(0,0,0), "Frags: %i", poPlayer[i].returnScore() );
    textprintf(buffer,font,poPlayer[i].getX() - 16, poPlayer[i].getY() - 10 ,colorA, "Frags: %i", poPlayer[i].returnScore() );

    if (poPlayer[i].getRespawnCounter() >= 0) {
      circle(buffer, poPlayer[i].getX() + 15, poPlayer[i].getY() + 15, 101 - poPlayer[i].getRespawnCounter(), colorB);
      circle(buffer, poPlayer[i].getX() + 15, poPlayer[i].getY() + 15, 100 - poPlayer[i].getRespawnCounter(), colorA);
      circle(buffer, poPlayer[i].getX() + 15, poPlayer[i].getY() + 15, 99 - poPlayer[i].getRespawnCounter(), colorB);
      if (poPlayer[i].getRespawnCounter() >= 99) {
      poPlayer[i].setRespawnCounter(-1);
    }
    }
    //Draw facing dir arrow
    if (poPlayer[i].getDir() == 'l') {
      line (buffer, poPlayer[i].getX() - 10, poPlayer[i].getY() + 19, poPlayer[i].getX() - 20, poPlayer[i].getY() + 19, makecol(0,0,0));
      line (buffer, poPlayer[i].getX() - 15, poPlayer[i].getY() + 24, poPlayer[i].getX() - 20, poPlayer[i].getY() + 19, makecol(0,0,0));
      line (buffer, poPlayer[i].getX() - 15, poPlayer[i].getY() + 14, poPlayer[i].getX() - 20, poPlayer[i].getY() + 19, makecol(0,0,0));

      line (buffer, poPlayer[i].getX() - 10, poPlayer[i].getY() + 17, poPlayer[i].getX() - 20, poPlayer[i].getY() + 17, makecol(0,0,0));
      line (buffer, poPlayer[i].getX() - 15, poPlayer[i].getY() + 22, poPlayer[i].getX() - 20, poPlayer[i].getY() + 17, makecol(0,0,0));
      line (buffer, poPlayer[i].getX() - 15, poPlayer[i].getY() + 12, poPlayer[i].getX() - 20, poPlayer[i].getY() + 17, makecol(0,0,0));

      line (buffer, poPlayer[i].getX() - 10, poPlayer[i].getY() + 18, poPlayer[i].getX() - 20, poPlayer[i].getY() + 18, colorA);
      line (buffer, poPlayer[i].getX() - 15, poPlayer[i].getY() + 23, poPlayer[i].getX() - 20, poPlayer[i].getY() + 18, colorA);
      line (buffer, poPlayer[i].getX() - 15, poPlayer[i].getY() + 13, poPlayer[i].getX() - 20, poPlayer[i].getY() + 18, colorA);
    }
    else if (poPlayer[i].getDir() == 'r') {
      line (buffer, poPlayer[i].getX() + 32 + 10, poPlayer[i].getY() + 19, poPlayer[i].getX() + 32 + 20, poPlayer[i].getY() + 19, makecol(0,0,0));
      line (buffer, poPlayer[i].getX() + 32 + 15, poPlayer[i].getY() + 24, poPlayer[i].getX() + 32 + 20, poPlayer[i].getY() + 19, makecol(0,0,0));
      line (buffer, poPlayer[i].getX() + 32 + 15, poPlayer[i].getY() + 14, poPlayer[i].getX() + 32 + 20, poPlayer[i].getY() + 19, makecol(0,0,0));

      line (buffer, poPlayer[i].getX() + 32 + 10, poPlayer[i].getY() + 17, poPlayer[i].getX() + 32 + 20, poPlayer[i].getY() + 17, makecol(0,0,0));
      line (buffer, poPlayer[i].getX() + 32 + 15, poPlayer[i].getY() + 22, poPlayer[i].getX() + 32 + 20, poPlayer[i].getY() + 17, makecol(0,0,0));
      line (buffer, poPlayer[i].getX() + 32 + 15, poPlayer[i].getY() + 12, poPlayer[i].getX() + 32 + 20, poPlayer[i].getY() + 17, makecol(0,0,0));

      line (buffer, poPlayer[i].getX() + 32 + 10, poPlayer[i].getY() + 18, poPlayer[i].getX() + 32 + 20, poPlayer[i].getY() + 18, colorA);
      line (buffer, poPlayer[i].getX() + 32 + 15, poPlayer[i].getY() + 23, poPlayer[i].getX() + 32 + 20, poPlayer[i].getY() + 18, colorA);
      line (buffer, poPlayer[i].getX() + 32 + 15, poPlayer[i].getY() + 13, poPlayer[i].getX() + 32 + 20, poPlayer[i].getY() + 18, colorA);
    }
    else if (poPlayer[i].getDir() == 'u') {
      line (buffer, poPlayer[i].getX() +19, poPlayer[i].getY() - 15, poPlayer[i].getX() +19, poPlayer[i].getY() -25, makecol(0,0,0));
      line (buffer, poPlayer[i].getX() +24, poPlayer[i].getY() - 20, poPlayer[i].getX() +19, poPlayer[i].getY() -25, makecol(0,0,0));
      line (buffer, poPlayer[i].getX() +14, poPlayer[i].getY() - 20, poPlayer[i].getX() +19, poPlayer[i].getY() -25, makecol(0,0,0));

      line (buffer, poPlayer[i].getX() +17, poPlayer[i].getY() - 15, poPlayer[i].getX() +17, poPlayer[i].getY() -25, makecol(0,0,0));
      line (buffer, poPlayer[i].getX() +22, poPlayer[i].getY() - 20, poPlayer[i].getX() +17, poPlayer[i].getY() -25, makecol(0,0,0));
      line (buffer, poPlayer[i].getX() +12, poPlayer[i].getY() - 20, poPlayer[i].getX() +17, poPlayer[i].getY() -25, makecol(0,0,0));

      line (buffer, poPlayer[i].getX() +18, poPlayer[i].getY() - 15, poPlayer[i].getX() +18, poPlayer[i].getY() -25, colorA);
      line (buffer, poPlayer[i].getX() +23, poPlayer[i].getY() - 20, poPlayer[i].getX() +18, poPlayer[i].getY() -25, colorA);
      line (buffer, poPlayer[i].getX() +13, poPlayer[i].getY() - 20, poPlayer[i].getX() +18, poPlayer[i].getY() -25, colorA);
    }
    else if (poPlayer[i].getDir() == 'd') {
      line (buffer, poPlayer[i].getX() + 19, poPlayer[i].getY() + 32+10, poPlayer[i].getX() + 19, poPlayer[i].getY() + 32+20, makecol(0,0,0));
      line (buffer, poPlayer[i].getX() + 24, poPlayer[i].getY() + 32+15, poPlayer[i].getX() + 19, poPlayer[i].getY() + 32+20, makecol(0,0,0));
      line (buffer, poPlayer[i].getX() + 14, poPlayer[i].getY() + 32+15, poPlayer[i].getX() + 19, poPlayer[i].getY() + 32+20, makecol(0,0,0));

      line (buffer, poPlayer[i].getX() + 17, poPlayer[i].getY() + 32+10, poPlayer[i].getX() + 17, poPlayer[i].getY() + 32+20, makecol(0,0,0));
      line (buffer, poPlayer[i].getX() + 22, poPlayer[i].getY() + 32+15, poPlayer[i].getX() + 17, poPlayer[i].getY() + 32+20, makecol(0,0,0));
      line (buffer, poPlayer[i].getX() + 12, poPlayer[i].getY() + 32+15, poPlayer[i].getX() + 17, poPlayer[i].getY() + 32+20, makecol(0,0,0));

      line (buffer, poPlayer[i].getX() + 18, poPlayer[i].getY() + 32+10, poPlayer[i].getX() + 18, poPlayer[i].getY() + 32+20, colorA);
      line (buffer, poPlayer[i].getX() + 23, poPlayer[i].getY() + 32+15, poPlayer[i].getX() + 18, poPlayer[i].getY() + 32+20, colorA);
      line (buffer, poPlayer[i].getX() + 13, poPlayer[i].getY() + 32+15, poPlayer[i].getX() + 18, poPlayer[i].getY() + 32+20, colorA);
    }
  }
}

void drawMenuStuff(BITMAP *buffer, MenuObject Menu, BITMAP *titleimg, BITMAP *imgPlayers, LevelObject Level) {
  draw_sprite(buffer, Menu.bmpBackground, 0,0);
  draw_sprite(buffer, Menu.bmpWindow, 800/2 - 500/2,600/2 - 400/2);

  draw_sprite(buffer, titleimg, 800/2 - 280/2, 110);
  if (!Menu.PlayPressed) { draw_sprite(buffer, Menu.bmpPlay, Menu.xPlay, Menu.yPlay); }
  else { draw_sprite(buffer, Menu.bmpPlayDepress, Menu.xPlay, Menu.yPlay); }
  if (!Menu.ExitPressed) { draw_sprite(buffer, Menu.bmpExit, Menu.xExit, Menu.yExit); }
  else { draw_sprite(buffer, Menu.bmpExitDepress, Menu.xExit, Menu.yExit); }

  textout_centre(buffer, font, "Version Deux!  By Rachel J. Morris", 399, 155, makecol(255,0,0));
  textout_centre(buffer, font, "Version Deux!  By Rachel J. Morris", 400, 156, makecol(200,0,0));

  textout_centre(buffer, font, "Player 1", 200, 200, makecol(200,0,0));
  textout_centre(buffer, font, "Player 2", 325, 200, makecol(200,0,0));
  textout_centre(buffer, font, "Player 3", 475, 200, makecol(200,0,0));
  textout_centre(buffer, font, "Player 4", 600, 200, makecol(200,0,0));

  masked_blit(Menu.bmpControlScheme, buffer, 0, 31 * Menu.returnControlScheme(0), 175, 225, 53, 31);  //player 1
  masked_blit(imgPlayers, buffer, 0, 0, 175+10, 275, 35, 35);
  masked_blit(Menu.bmpArrows, buffer, 0, 0, 175-20, 225, 15, 30);
  masked_blit(Menu.bmpArrows, buffer, 15, 0, 175+58, 225, 15, 30);
  masked_blit(Menu.bmpControlScheme, buffer, 0, 31 * Menu.returnControlScheme(1), 300, 225, 53, 31);  //player 2
  masked_blit(imgPlayers, buffer, 0, 35, 300+10, 275, 35, 35);
  masked_blit(Menu.bmpArrows, buffer, 0, 0, 300-20, 225, 15, 30);
  masked_blit(Menu.bmpArrows, buffer, 15, 0, 300+58, 225, 15, 30);
  masked_blit(Menu.bmpControlScheme, buffer, 0, 31 * Menu.returnControlScheme(2), 450, 225, 53, 31);  //player 3
  masked_blit(imgPlayers, buffer, 0, 70, 450+10, 275, 35, 35);
  masked_blit(Menu.bmpArrows, buffer, 0, 0, 450-20, 225, 15, 30);
  masked_blit(Menu.bmpArrows, buffer, 15, 0, 450+58, 225, 15, 30);
  masked_blit(Menu.bmpControlScheme, buffer, 0, 31 * Menu.returnControlScheme(3), 575, 225, 53, 31);  //player 4
  masked_blit(imgPlayers, buffer, 0, 105, 575+10, 275, 35, 35);
  masked_blit(Menu.bmpArrows, buffer, 0, 0, 575-20, 225, 15, 30);
  masked_blit(Menu.bmpArrows, buffer, 15, 0, 575+55, 225, 15, 30);

  textprintf(buffer,font,185,350,makecol(255,0,0), "Frag Limit: %i", Level.getFragLimit() );
  masked_blit(Menu.bmpArrows, buffer, 0, 0, 166, 340, 15, 30);
  masked_blit(Menu.bmpArrows, buffer, 15, 0, 300, 340, 15, 30);

  textprintf(buffer,font,185,380,makecol(255,0,0), "Barrier Style:");
  masked_blit(Menu.bmpArrows, buffer, 0, 0, 166, 370, 15, 30);
  masked_blit(Menu.bmpArrows, buffer, 15, 0, 300, 370, 15, 30);
  if (Level.getBarrierScheme() == 0 )      { textprintf(buffer,font,185,389,makecol(255,0,0), "None"); }
  else if ( Level.getBarrierScheme() == 1 ) { textprintf(buffer,font,185,389,makecol(255,0,0), "Random"); }
  else if ( Level.getBarrierScheme() == 2 ) { textprintf(buffer,font,185,389,makecol(255,0,0), "4 Bunkers"); }
  else if ( Level.getBarrierScheme() == 3 ) { textprintf(buffer,font,185,389,makecol(255,0,0), "2 Bunkers"); }
  else if ( Level.getBarrierScheme() == 4 ) { textprintf(buffer,font,185,389,makecol(255,0,0), "Center bunker"); }
  else { Level.setBarrierScheme(0); textprintf(buffer,font,185,389,makecol(255,0,0), "None"); }

  if (Menu.getFullscreen() == false) { masked_blit( Menu.bmpScreenOpts, buffer, 0, 0, 350, 340, 90, 30); }
  else { masked_blit( Menu.bmpScreenOpts, buffer, 0, 30, 350, 340, 90, 30); }

  if (!Menu.resetPressed) { masked_blit( Menu.bmpScreenOpts, buffer, 0, 60, 350, 371, 90, 30); }
  else { masked_blit( Menu.bmpScreenOpts, buffer, 0, 90, 350, 371, 90, 30); }

  if ( Menu.mouseDown == true ) { masked_blit(Menu.bmpCursor, buffer, 24, 0, mouse_x, mouse_y, 24, 24); }
  else { masked_blit(Menu.bmpCursor, buffer, 0, 0, mouse_x, mouse_y, 24, 24); }
}

void initializeCrap() {
  srand((unsigned)time(NULL));

  allegro_init();
  install_keyboard();
  install_timer();
  install_mouse();
  install_joystick(JOY_TYPE_AUTODETECT);
  install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, 0)

  LOCK_VARIABLE(speed_counter);
  LOCK_FUNCTION(increment_speed_counter);

  set_color_depth(16);
  set_gfx_mode(GFX_AUTODETECT, 800, 600, 0, 0);
  text_mode(-1);

  srand((unsigned)time(NULL));
}

void mouseDownClick (MenuObject *Menu, int mouseDown[]) {
  Menu->mouseDown = true;
  //Hit Play
  if (Menu->CheckClick(mouse_x, mouse_y) == 0) { Menu->PlayPressed = true; mouseDown[12] = true; }
  //Hit Exit
  else if (Menu->CheckClick(mouse_x, mouse_y) == 1) { Menu->ExitPressed = true; mouseDown[11] = true; }
  //Player 1's Left Arrow
  else if (Menu->CheckClick(mouse_x, mouse_y) == 2) { mouseDown[2] = true; }
  //Player 1's Right Arrow
  else if (Menu->CheckClick(mouse_x, mouse_y) == 3) { mouseDown[3] = true; }

  //Player 2's Left Arrow
  else if (Menu->CheckClick(mouse_x, mouse_y) == 4) { mouseDown[4] = true; }
  //Player 2's Right Arrow
  else if (Menu->CheckClick(mouse_x, mouse_y) == 5) { mouseDown[5] = true; }

  //Player 3's Left Arrow
  else if (Menu->CheckClick(mouse_x, mouse_y) == 6) { mouseDown[6] = true; }
  //Player 3's Right Arrow
  else if (Menu->CheckClick(mouse_x, mouse_y) == 7) { mouseDown[7] = true; }

  //Player 4's Left Arrow
  else if (Menu->CheckClick(mouse_x, mouse_y) == 8) { mouseDown[8] = true; }
  //Player 4's Right Arrow
  else if (Menu->CheckClick(mouse_x, mouse_y) == 9) { mouseDown[9] = true; }

  //Fraglimit Left Arrow
  else if (Menu->CheckClick(mouse_x, mouse_y) == 14) { mouseDown[14] = true; }
  //limit Right Arrow
  else if (Menu->CheckClick(mouse_x, mouse_y) == 15) { mouseDown[15] = true; }

  //Barriers Left Arrow
  else if (Menu->CheckClick(mouse_x, mouse_y) == 16) { mouseDown[16] = true; }
  //Barriers Right Arrow
  else if (Menu->CheckClick(mouse_x, mouse_y) == 17) { mouseDown[17] = true; }

  //Fullscreen Button
  else if (Menu->CheckClick(mouse_x, mouse_y) == 18) { mouseDown[18] = true; }

  //Reset Button
  else if (Menu->CheckClick(mouse_x, mouse_y) == 19) { mouseDown[19] = true; Menu->resetPressed = true; }

}

void mouseUp(MenuObject *Menu, int mouseDown[], PlayerObject *poPlayer, LevelObject *Level, MIDI *song) {
  Menu->mouseDown = false;
  Menu->PlayPressed = false;
  Menu->ExitPressed = false;
  for (int i=2; i<10; i++) {
    if (mouseDown[i] == true) {
      int temp = (i / 2) - 1;
      int dir;
      if ( (i%2) == 1 ) { dir = 1; }
      else if  ( (i%2) == 0 ) { dir = -1; }
      changeControlScheme(dir, &poPlayer[temp], Menu, temp);
      mouseDown[i] = false;
    }
  }
  if (mouseDown[11] == true) { Menu->quit(); mouseDown[11] = false; }
  if (mouseDown[12] == true) {
    for (int i=0; i<4; i++) {
      poPlayer[i].setControlScheme( Menu->returnControlScheme(i) );
    }
    setBarrierType(Level);
    Level->setEnabled(true);
    Menu->setEnabled(false);
    play_midi(song, true);
    //Level->setFragLimit(Level->getFragLimit() );

    mouseDown[12] = false;
  } //then play
  if (mouseDown[14] == true) {
    int temp = Level->getFragLimit();
    if (temp > 0) { temp--; } else { temp = 20; }
    Level->setFragLimit( temp );

    //Reset gamestuffs
    mouseDown[19] = true; Menu->resetPressed = true;

    mouseDown[14] = false;
  }
  if (mouseDown[15] == true) {
    int temp = Level->getFragLimit();
    if (temp < 20) { temp++; } else { temp = 0; }
    Level->setFragLimit(temp);

    //Reset gamestuffs
    mouseDown[19] = true; Menu->resetPressed = true;

    mouseDown[15] = false;
  }

  if (mouseDown[16] == true) {
    int temp = Level->getBarrierScheme();
    if (temp > 0) { temp--; } else { temp = 4; }
    Level->setBarrierScheme( temp );

    //Reset gamestuffs
    mouseDown[19] = true; Menu->resetPressed = true;

    mouseDown[16] = false;
  }
  if (mouseDown[17] == true) {
    int temp = Level->getBarrierScheme();
    if (temp < 4) { temp++; } else { temp = 0; }
    Level->setBarrierScheme(temp);

    //Reset gamestuffs
    mouseDown[19] = true; Menu->resetPressed = true;

    mouseDown[17] = false;
  }
  //Fullscreen toggle
  if (mouseDown[18] == true) {
    if (Menu->getFullscreen() == true) {
      set_gfx_mode(GFX_AUTODETECT_WINDOWED, 800, 600, 0, 0);
      Menu->setFullscreen(false);
    }
    else if (Menu->getFullscreen() == false) {
      set_gfx_mode(GFX_AUTODETECT, 800, 600, 0, 0);
      Menu->setFullscreen(true);
    }
    mouseDown[18] = false;
  }

  if (mouseDown[19] == true) {
    //Reset game stats
    for (int i=0; i<4; i++) {
      poPlayer[i].resetScore();
      poPlayer[i].resetDeaths();
    }

    poPlayer[0].setX(32);
    poPlayer[0].setY(32);
    poPlayer[0].setDir('l');
    poPlayer[1].setX(32);
    poPlayer[1].setY(544);
    poPlayer[1].setDir('l');
    poPlayer[2].setX(736);
    poPlayer[2].setY(544);
    poPlayer[2].setDir('r');
    poPlayer[3].setX(736);
    poPlayer[3].setY(32);
    poPlayer[3].setDir('r');

    Menu->fragLimitHit = false;

    Menu->resetPressed = false;
    mouseDown[19] = false;
  }


}

void generateBackground(BITMAP *buffer, LevelObject Level) {
  for (int i=0; i<25; i++) {
    for (int j=0; j<19; j++) {
      masked_blit(Level.bmpBackground, buffer, 0, 0, i*32, j*32, 32, 32);
    }
  }
  //Barriers
  if ( Level.getBarrierScheme() == 1 ) {  // Random Barriers
    for (int i = 0; i < Level.getBarrierAmount(); i++) {
      masked_blit(Level.bmpBackground, buffer, 32, 0, Level.getBarrierX(i), Level.getBarrierY(i), 32, 32);
    }
  }
  else if ( Level.getBarrierScheme() == 2 ) { // Four Bunkers
    for (int i = 0; i < Level.getBarrierAmount(); i++) {
      //
      masked_blit(Level.bmpBackground, buffer, 32, 0, Level.getBarrierX(i), Level.getBarrierY(i), 32, 32);
    }
  }
  else if ( Level.getBarrierScheme() == 3 ) { // Two Bunkers
    for (int i = 0; i < Level.getBarrierAmount(); i++) {
      //
      masked_blit(Level.bmpBackground, buffer, 32, 0, Level.getBarrierX(i), Level.getBarrierY(i), 32, 32);
    }
  }
  else if ( Level.getBarrierScheme() == 4 ) { // Center Bunker
    for (int i = 0; i < Level.getBarrierAmount(); i++) {
      //
      masked_blit(Level.bmpBackground, buffer, 32, 0, Level.getBarrierX(i), Level.getBarrierY(i), 32, 32);
    }
  }
}

void drawScoreBar(BITMAP *buffer, PlayerObject poPlayer[], float scrollerX, float scrollerY, LevelObject Level){
  int tempMaxFrags = poPlayer[0].returnScore();
  int tempLeader = 0;
  if ( poPlayer[1].returnScore() > tempMaxFrags ) { tempMaxFrags = poPlayer[1].returnScore(); tempLeader = 1; }
  if ( poPlayer[2].returnScore() > tempMaxFrags ) { tempMaxFrags = poPlayer[2].returnScore(); tempLeader = 2; }
  if ( poPlayer[3].returnScore() > tempMaxFrags ) { tempMaxFrags = poPlayer[3].returnScore(); tempLeader = 3;}

  textprintf(buffer,font,scrollerX, scrollerY ,makecol(0,0,0), "Frag Limit %i / %i", tempMaxFrags, Level.getFragLimit() );
  textprintf(buffer,font,scrollerX - 400, scrollerY ,makecol(0,0,0), "Currently in lead: Player %i", tempLeader+1 );
  textprintf(buffer,font,scrollerX - 1, scrollerY + 1 ,makecol(255,255,255), "Frag Limit %i / %i", tempMaxFrags, Level.getFragLimit() );
  textprintf(buffer,font,scrollerX - 401, scrollerY + 1 ,makecol(255,255,255), "Currently in lead: Player %i", tempLeader+1 );
}

void drawLaser(LaserObject loLaser, BITMAP *buffer, LevelObject Level, int index) {
  masked_blit(Level.bmpLaserFilmstrip, buffer, index * 10, 0, loLaser.getX(), loLaser.getY(), 10, 10);
}

void shootLaser(LaserObject *loLaser, PlayerObject poPlayer) {
  if (poPlayer.getDir() == 'r') {
    loLaser->setX( poPlayer.getX() + 35 + 10 );
    loLaser->setY( poPlayer.getY() + 15);
    loLaser->setDir('r');
  }
  else if (poPlayer.getDir() == 'l') {
    loLaser->setX( poPlayer.getX() - 10 );
    loLaser->setY( poPlayer.getY() + 15);
    loLaser->setDir('l');
  }
  else if (poPlayer.getDir() == 'u') {
    loLaser->setX( poPlayer.getX() + 15);
    loLaser->setY( poPlayer.getY() - 10 );
    loLaser->setDir('u');
  }
  else if (poPlayer.getDir() == 'd') {
    loLaser->setX( poPlayer.getX() + 15);
    loLaser->setY( poPlayer.getY() + 35 + 10 );
    loLaser->setDir('d');
  }
  loLaser->setEnabled(true);
}

void checkCollision(LaserObject *Laser, PlayerObject poPlayer[], int index, LevelObject Level) {
  //RACH, QUIT EDITING THIS.  THIS IS THE LASER COLLISION, NOT THE BUNKER COLLISION.

  //LASER-BARRIER COLLISION
  int left1, left2, right1, right2, top1, top2, bottom1, bottom2;

  left1 = Laser->getX();   //Not actual sprite size, "region"
  top1 = Laser->getY();    // of 32x32 inside the sprite's size
  right1 = left1 + 10;
  bottom1 = top1 + 10;
  for (int i = 0; i < Level.getBarrierAmount(); i++) {
    //WHEE
    left2 = Level.getBarrierX(i);
    top2 = Level.getBarrierY(i);
    right2 = left2 + 32;
    bottom2 = top2 + 32;

    if (    (bottom2 > top1) && (top2 < bottom1)    &&
            (right2 > left1) && (left2 < right1))
    {
      Laser->setEnabled(false);
    }
  }

  //LASER-PLAYER COLLISION
  for (int i=0; i<4; i++) {
    if (  (   (Laser->getY()+10 > poPlayer[i].getY() ) &&
              (Laser->getY() < poPlayer[i].getY() + 35) &&
              (Laser->getX() + 10 > poPlayer[i].getX() ) &&
              (Laser->getX() < poPlayer[i].getX() + 35) ) ) {
      //collision
      if (poPlayer[i].getRespawnCounter() == -1) {
        //No res barrier
        //play sound
        play_sample(poPlayer[i].samSound, 255, 128, 1000, 0);

        poPlayer[i].addToDeaths();
        poPlayer[index].addToScore();
        Laser->setEnabled(false);
        respawnPlayer(&poPlayer[i], Level);
      }
    }
  }
}

void respawnPlayer(PlayerObject *poPlayer, LevelObject Level) {
  int tempRand;
  tempRand = ( (int)rand() % 22 ) * 35;
  poPlayer->setX(tempRand);
  tempRand = ( (int)rand() % 17 ) * 35;
  poPlayer->setY(tempRand);
  poPlayer->setRespawnCounter(0);
}

void setBarrierType(LevelObject *Level) {
  if (Level->getBarrierScheme() == 0) { }
  else if (Level->getBarrierScheme() == 1) {
    // Random
    Level->setBarrierAmount(15);
    int tempX, tempY, i = 0;

    for (i=0; i < 15; i++) {
      tempX = ( (int)rand() % 19 + 3 ) * 32;  //X should be between 96 and 704.
      tempY = ( (int)rand() % 12 + 3 ) * 32;  //Y should be between 96 and 504.
      Level->setBarrierCoordinates(i, tempX, tempY);  //Index, X, Y
    }
  }
  else if (Level->getBarrierScheme() == 2) {
    // Four Bunkers

    //1st bunker
    Level->setBarrierCoordinates(0, 64, 96);  //Index, X, Y
    Level->setBarrierCoordinates(1, 96, 96);  //Index, X, Y
    Level->setBarrierCoordinates(2, 96, 64);  //Index, X, Y

    //2nd bunker
    Level->setBarrierCoordinates(3, 64, 480);  //Index, X, Y
    Level->setBarrierCoordinates(4, 96, 480);  //Index, X, Y
    Level->setBarrierCoordinates(5, 96, 512);  //Index, X, Y

    //3rd bunker
    Level->setBarrierCoordinates(6, 704, 96);  //Index, X, Y
    Level->setBarrierCoordinates(7, 672, 96);  //Index, X, Y
    Level->setBarrierCoordinates(8, 672, 64);  //Index, X, Y

    //4th bunker
    Level->setBarrierCoordinates(9, 672, 512);  //Index, X, Y
    Level->setBarrierCoordinates(10, 672, 480);  //Index, X, Y
    Level->setBarrierCoordinates(11, 704, 480);  //Index, X, Y

    Level->setBarrierAmount(12);

  }
  else if (Level->getBarrierScheme() == 3) {
    // Two Bunkers

    //1st bunker
    Level->setBarrierCoordinates(0, 288, 64);  //Index, X, Y
    Level->setBarrierCoordinates(1, 288, 96);  //Index, X, Y
    Level->setBarrierCoordinates(2, 320, 96);  //Index, X, Y
    Level->setBarrierCoordinates(3, 352, 96);  //Index, X, Y
    Level->setBarrierCoordinates(4, 384, 96);  //Index, X, Y
    Level->setBarrierCoordinates(5, 416, 96);  //Index, X, Y
    Level->setBarrierCoordinates(6, 448, 96);  //Index, X, Y
    Level->setBarrierCoordinates(7, 480, 96);  //Index, X, Y
    Level->setBarrierCoordinates(8, 480, 64);  //Index, X, Y

    //2nd bunker
    Level->setBarrierCoordinates(9, 288, 480);  //Index, X, Y
    Level->setBarrierCoordinates(10, 288, 448);  //Index, X, Y
    Level->setBarrierCoordinates(11, 320, 448);  //Index, X, Y
    Level->setBarrierCoordinates(12, 352, 448);  //Index, X, Y
    Level->setBarrierCoordinates(13, 384, 448);  //Index, X, Y
    Level->setBarrierCoordinates(14, 416, 448);  //Index, X, Y
    Level->setBarrierCoordinates(15, 448, 448);  //Index, X, Y
    Level->setBarrierCoordinates(16, 480, 448);  //Index, X, Y
    Level->setBarrierCoordinates(17, 480, 480);  //Index, X, Y

    Level->setBarrierAmount(18);
  }
  else if (Level->getBarrierScheme() == 4) {
    // Center Bunker

    //1st Wall
    Level->setBarrierCoordinates(0, 320, 128);  //Index, X, Y
    Level->setBarrierCoordinates(1, 352, 128);  //Index, X, Y
    Level->setBarrierCoordinates(2, 384, 128);  //Index, X, Y
    Level->setBarrierCoordinates(3, 416, 128);  //Index, X, Y
    Level->setBarrierCoordinates(4, 448, 128);  //Index, X, Y

    //2nd Wall
    Level->setBarrierCoordinates(5, 320, 416);  //Index, X, Y
    Level->setBarrierCoordinates(6, 352, 416);  //Index, X, Y
    Level->setBarrierCoordinates(7, 384, 416);  //Index, X, Y
    Level->setBarrierCoordinates(8, 416, 416);  //Index, X, Y
    Level->setBarrierCoordinates(9, 448, 416);  //Index, X, Y

    //3rd Wall
    Level->setBarrierCoordinates(10, 256, 224);  //Index, X, Y
    Level->setBarrierCoordinates(11, 256, 256);  //Index, X, Y
    Level->setBarrierCoordinates(12, 256, 288);  //Index, X, Y
    Level->setBarrierCoordinates(13, 256, 320);  //Index, X, Y

    //4th Wall
    Level->setBarrierCoordinates(14, 512, 224);  //Index, X, Y
    Level->setBarrierCoordinates(15, 512, 256);  //Index, X, Y
    Level->setBarrierCoordinates(16, 512, 288);  //Index, X, Y
    Level->setBarrierCoordinates(17, 512, 320);  //Index, X, Y

    Level->setBarrierAmount(18);
  }
}

void moveCharacter(LevelObject Level, PlayerObject *poPlayer, int index, char direction) {
  //to check barrier collision
  bool Collision = false;
  poPlayer->setDir(direction);

  int left1, left2, right1, right2, top1, top2, bottom1, bottom2;

  left1 = poPlayer->getX() + 2;   //Not actual sprite size, "region"
  top1 = poPlayer->getY() + 2;    // of 32x32 inside the sprite's size
  right1 = left1 + 32;
  bottom1 = top1 + 32;

  if (direction == 'd')   //Down
  {
    bottom1++;
    top1++;
  }
  else if (direction == 'l') //Left
  {
    left1--;
    right1--;
  }
  else if (direction == 'r') //Right
  {
    right1++;
    left1++;
  }
  else if (direction == 'u') // Up
  {
    top1--;
    bottom1--;
  }

  for (int i = 0; i < Level.getBarrierAmount(); i++) {
    //WHEE
    left2 = Level.getBarrierX(i);
    top2 = Level.getBarrierY(i);
    right2 = left2 + 32;
    bottom2 = top2 + 32;

    if (    (bottom2 > top1) && (top2 < bottom1)    &&
            (right2 > left1) && (left2 < right1))
    {
      Collision = true;
    }
  }
  if (Level.getBarrierAmount() == 0) { Collision = false; }

  if (!Collision) {
    //Move!
    if (direction == 'd')   //Down
    {
      poPlayer->moveDown('p');
    }
    else if (direction == 'l') //Left
    {
      poPlayer->moveLeft('p');
    }
    else if (direction == 'r') //Right
    {
      poPlayer->moveRight('p');
    }
    else if (direction == 'u') // Up
    {
      poPlayer->moveUp('p');
    }
  }
}
