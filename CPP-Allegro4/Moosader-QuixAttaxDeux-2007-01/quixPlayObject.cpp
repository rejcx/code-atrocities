#include <allegro.h>
#include "quixPlayObject.h"

quixPlayObject::quixPlayObject() {
  //
}

void quixPlayObject::setX(int value) { x = value; }
int quixPlayObject::getX() { return x; }

void quixPlayObject::setY(int value) { y = value; }
int quixPlayObject::getY() { return y; }

void quixPlayObject::setDir(char value) {
  if (value == 'u' || value == 'd') { vertDir = value; }
  else { horizDir = value; }
  direction = value;
}
char quixPlayObject::getDir() { return direction; }
char quixPlayObject::getVertDir() { return vertDir; }
char quixPlayObject::getHorizDir() { return horizDir; }

void quixPlayObject::setSpriteCode(int value) { spriteCode = value; }
int quixPlayObject::getSpriteCode() {return spriteCode; }

void quixPlayObject::setEnabled(bool value) { enabled = value; }
bool quixPlayObject::getEnabled() { return enabled; }

void quixPlayObject::moveUp(char type) {
  if (type == 'p') { //player
    if (y > 0) { y--; }
  }
  else if (type == 'l') { //laser
    y -= 3;
    if (y <= -10) { enabled = false; }
  }
}
void quixPlayObject::moveDown(char type) {
  if (type == 'p') { //player
    if (y < 565) { y++; }
  }
  else if (type == 'l') { //laser
    y += 3;
    if (y >= 565) { enabled = false; }
  }
}
void quixPlayObject::moveLeft(char type) {
  if (type == 'p') { //player
    if (x > 0) { x--; }
  }
  else if (type == 'l') { //laser
    x -= 3;
    if (x <= -10) { enabled = false; }
  }
}
void quixPlayObject::moveRight(char type) {
  if (type == 'p') { //player
    if (x < 765) { x++; }
  }
  else if (type == 'l') { //laser
    x += 3;
    if (x >= 765) { enabled = false; }
  }
}
void quixPlayObject::attack() {
  //
}

PlayerObject::PlayerObject() {
  iScore = 0;
  iDeaths = 0;
  isAI = false;
  setEnabled(true);
  setDir('r');
  respawnCounter = -1;
}

void PlayerObject::resetScore() { iScore = 0; }
void PlayerObject::addToScore() { iScore++; }
int PlayerObject::returnScore() { return iScore; }

void PlayerObject::resetDeaths() { iDeaths = 0; }
void PlayerObject::addToDeaths() { iDeaths++; }
int PlayerObject::returnDeaths() { return iDeaths; }

void PlayerObject::setControlScheme(int value) { iControlScheme = value;}
int PlayerObject::getControlScheme() { return iControlScheme; }

void PlayerObject::setRespawnCounter(int value) { respawnCounter = value; }
int PlayerObject::getRespawnCounter() { return respawnCounter; }

LaserObject::LaserObject() {
  setEnabled(false);
}

