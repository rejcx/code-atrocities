#include <allegro.h>

class quixPlayObject {
  private:
  int x, y;
  char direction;
  char vertDir;
  char horizDir;
  bool enabled;
  int spriteCode;
  public:
  quixPlayObject();

  void setX(int);
  int getX();

  void setY(int);
  int getY();

  void setDir(char);
  char getVertDir();
  char getHorizDir();
  char getDir();

  void setSpriteCode(int);
  int getSpriteCode();

  void setEnabled(bool);
  bool getEnabled();

  void moveUp(char);
  void moveDown(char);
  void moveLeft(char);
  void moveRight(char);
  void attack();
};

class PlayerObject: public quixPlayObject {
  private:
  int iScore, iDeaths;
  int iControlScheme;
  bool isAI;
  int respawnCounter;
  public:
  SAMPLE *samSound;
  PlayerObject();

  void resetScore();
  void addToScore();
  int returnScore();

  void resetDeaths();
  void addToDeaths();
  int returnDeaths();

  int getControlScheme();
  void setControlScheme(int);

  int getRespawnCounter();
  void setRespawnCounter(int);

  //~PlayerObject();
};

class LaserObject: public quixPlayObject {
  public:
  LaserObject();
  //~LaserObject();
};



