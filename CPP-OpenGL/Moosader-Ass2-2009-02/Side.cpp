#include "Side.h"

float Side::X( int index )
{
    if ( index >= 0 && index < 3)
        return x[index];
    else
        return -1;
}

float Side::Y( int index )
{
    if ( index >= 0 && index < 3)
        return y[index];
    else
        return -1;
}

float Side::Z( int index )
{
    if ( index >= 0 && index < 3)
        return z[index];
    else
        return -1;
}

void Side::SetX( int index, float val )
{
    if ( index >= 0 && index < 3)
        x[index] = val;
}

void Side::SetY( int index, float val )
{
    if ( index >= 0 && index < 3)
        y[index] = val;
}

void Side::SetZ( int index, float val )
{
    if ( index >= 0 && index < 3)
        z[index] = val;
}

void Side::SetColor( float new_r, float new_g, float new_b )
{
    r = new_r;
    g = new_g;
    b = new_b;
}

void Side::SetSides( float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3, float x4, float y4, float z4 )
{
    x[0] = x1;
    y[0] = y1;
    z[0] = z1;
    x[1] = x2;
    y[1] = y2;
    z[1] = z2;
    x[2] = x3;
    y[2] = y3;
    z[2] = z3;
    x[3] = x4;
    y[3] = y4;
    z[3] = z4;
}

void Side::Draw()
{
    glBegin( GL_TRIANGLES );
        glColor3f( r, g, b );
        //Polygon 1
        glVertex3f( x[0], y[0], z[0] );
        glVertex3f( x[1], y[1], z[1] );
        glVertex3f( x[2], y[2], z[2] );
        //Polygon 2
        glVertex3f( x[2], y[2], z[2] );
        glVertex3f( x[3], y[3], z[3] );
        glVertex3f( x[0], y[0], z[0] );
    glEnd();
}

void Side::Draw( float cX, float cY, float cZ )
{
    glBegin( GL_TRIANGLES );
        glColor3f( r, g, b );
        //Polygon 1
        glVertex3f( x[0]+cX, y[0]+cY, z[0]+cZ );
        glVertex3f( x[1]+cX, y[1]+cY, z[1]+cZ );
        glVertex3f( x[2]+cX, y[2]+cY, z[2]+cZ );
        //Polygon 2
        glVertex3f( x[2]+cX, y[2]+cY, z[2]+cZ );
        glVertex3f( x[3]+cX, y[3]+cY, z[3]+cZ );
        glVertex3f( x[0]+cX, y[0]+cY, z[0]+cZ );
    glEnd();
}

void Side::Stats()
{
    //for debugging, nothing special.
    cout<<endl;
    for ( int i=0; i<4; i++ ) 
    {
        cout<<"  x "<<i<<"\t"<<x[i]<<endl;
        cout<<"  y "<<i<<"\t"<<y[i]<<endl;
        cout<<"  z "<<i<<"\t"<<z[i]<<endl;
        cout<<"  r\t"<<r<<endl;
        cout<<"  g\t"<<g<<endl;
        cout<<"  b\t"<<b<<endl<<endl;
    }
}





