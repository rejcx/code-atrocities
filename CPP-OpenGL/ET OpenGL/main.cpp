#include <iostream>
#include <fstream>
#include "SDL/SDL.h"
#include "SDL/SDL_opengl.h"
#include "Display.h"
#include "Input.h"
#include "MapGrid.h"
#include "Timer.h"
#include "Camera.h"
#include "Player.h"
using namespace std;

void LoadMap(MapGrid map[]);

const int FPS = 60;

void CheckArea( MapGrid*, Player*, Camera* );

int main(int argc, char *argv[])
{
	bool done = false;
	Timer frameRate;
	Display display;
	Input input;
	MapGrid map[6];
	Camera camera;
	Player player;
	//init maps
	for (int i=0; i<6; i++)
	{
		map[i].setup(i);
	}
	display.Init();

	while ( !done )
	{
		frameRate.Start();
		//Display stuffs

		if ( input.GetKey(27) ) { done = true; }

		//Camera controls
		if ( input.GetKey( SDLK_EQUALS ) )
		{
			camera.IncreaseAngle();
		}
		else if ( input.GetKey( SDLK_MINUS ) )
		{
			camera.DecreaseAngle();
		}
		//Player controls
		if ( input.GetKey( SDLK_UP ) )
		{
			player.Move('n');
			camera.Move('s');
			CheckArea( &map[ camera.CurrentMap() ], &player, &camera );
		}
		else if ( input.GetKey( SDLK_DOWN ) )
		{
			player.Move('s');
			camera.Move('n');
			CheckArea( &map[ camera.CurrentMap() ], &player, &camera );
		}
		if ( input.GetKey( SDLK_LEFT ) )
		{
			player.Move('e');
			camera.Move('w');
			CheckArea( &map[ camera.CurrentMap() ], &player, &camera );
		}
		else if ( input.GetKey( SDLK_RIGHT ) )
		{
			player.Move('w');
			camera.Move('e');
			CheckArea( &map[ camera.CurrentMap() ], &player, &camera );
		}
		if ( input.GetKey( SDLK_RSHIFT ) )
		{
			player.Move('u');
		}
		else if ( input.GetKey( SDLK_RCTRL ) )
		{
			player.Move('d');
		}

		display.Draw( map, player, camera );
		input.Refresh();

		if ( frameRate.GetTicks() < 1000 / FPS )
		{
			SDL_Delay( (1000/FPS) - frameRate.GetTicks() );
		}
	}

	SDL_Quit();

	return 0;
}

void CheckArea( MapGrid *map, Player *player, Camera *camera )
{
	if ( player->X() < 0 )
	{
		player->SetMap( map->adjacentLeft );
		camera->SetMap( map->adjacentLeft );
		player->SetX( 74 );
		camera->SetX( -74 );
	}
	if ( player->X() > 75 )
	{
		player->SetMap( map->adjacentRight );
		camera->SetMap( map->adjacentRight );
		player->SetX( 0 );
		camera->SetX( 0 );
	}

	if ( player->Y() > 50 )
	{
		player->SetMap( map->adjacentUp );
		camera->SetMap( map->adjacentUp );
		player->SetY( 0 );
		camera->SetY( 49 );
	}
	if ( player->Y() < 0 )
	{
		player->SetMap( map->adjacentDown );
		camera->SetMap( map->adjacentDown );
		player->SetY( 49 );
		camera->SetY( 0 );
	}
}



