Rachel J. Morris [Moosader.com](http://www.moosader.com/)

![screenshot](screenshot.png)

## ABOUT

Was working on a clone of the Atari 2600 E.T. game, but in 3D
with OpenGL.

Project from May 2008?
