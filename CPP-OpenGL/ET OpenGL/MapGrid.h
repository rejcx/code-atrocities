#pragma once
#include <iostream>
#include <string>
#include <math.h>
using namespace std;

class PitClass
{
public:
	float cx, cy, w, h;
	int type;
	int hasItem;
	bool exists;
	void setup(float x, float y, float tw, float th, int ttype, int thasItem, bool texists);
};

class MapGrid
{
public:
	MapGrid(int index) { setup(index); }
	MapGrid() { adjacentLeft = adjacentRight = adjacentUp = adjacentDown = 5; }
	void setup(int);
	int adjacentLeft;
	int adjacentRight;
	int adjacentUp;
	int adjacentDown;
	string name;

	int pitAmt;
	PitClass pit[8];	//max of 8 pits per "grid"
	void Draw( float, float );
};

