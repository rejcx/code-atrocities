#pragma once
#include "SDL/SDL.h"
#include "SDL/SDL_opengl.h"
#include "MapGrid.h"
#include "Camera.h"
#include "Player.h"


class Display 
{
	private:
		int screenWidth, screenHeight, screenBPP;
	public:
		Display();
		~Display();
		void Init();
		void ResizeWindow( int, int, bool );
		void SetWindowCaption( char* );
		void Refresh();
		void Draw( MapGrid[], Player, Camera );
		int ScrW() { return screenWidth; }
		int ScrH() { return screenHeight; }
};
