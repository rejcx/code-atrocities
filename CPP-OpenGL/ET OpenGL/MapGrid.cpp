#include "MapGrid.h"
#include "SDL/SDL.h"
#include "SDL/SDL_opengl.h"

#define FOREST 0
#define SYMHOLES 1
#define HELLHOLES 2
#define ARROWHOLES 3
#define FOURHOLES 4
#define TOWN 5
//pit things
#define NOTHING -1
#define PHONE1 6
#define PHONE2 7
#define PHONE3 8
#define FLOWER 9
//pit shapes
#define DIAMOND 10
#define ARROW1 11
#define ARROW2 12

void DrawHouse( float tx, float ty );
void DrawBuilding( float tx, float ty );
void DrawPillar( float tx, float ty );
void DrawTree( float tx, float ty );

void MapGrid::setup( int index )
{
	switch (index) 
	{
	case FOREST:
		adjacentLeft = SYMHOLES;
		adjacentRight = HELLHOLES;
		adjacentUp = FOURHOLES;
		adjacentDown = ARROWHOLES;
		pitAmt = 0;
		for (int i=0; i<8; i++)
			pit[i].setup(0, 0, 0, 0, 0, 0, false);
		name = "forest";
		break;
	case SYMHOLES:
		adjacentLeft = HELLHOLES;
		adjacentRight = ARROWHOLES;
		adjacentUp = FOREST;
		adjacentDown = TOWN;
		pitAmt = 4;
		pit[0].setup(18, 12.5, 10, 2.5, DIAMOND, NOTHING, true);
		pit[1].setup(56, 12.5, 10, 2.5, DIAMOND, NOTHING, true);
		pit[2].setup(18, 37.5, 10, 2.5, DIAMOND, NOTHING, true);
		pit[3].setup(56, 37.5, 10, 2.5, DIAMOND, NOTHING, true);
		for (int i=4; i<8; i++)
			pit[i].setup(0, 0, 0, 0, 0, 0, false);
		name = "symmetric";
		break;
	case HELLHOLES:
		adjacentLeft = ARROWHOLES;
		adjacentRight = FOURHOLES;
		adjacentUp = FOREST;
		adjacentDown = TOWN;
		pitAmt = 8;
		pit[0].setup(0, 12.5, 10, 2.5, DIAMOND, NOTHING, true);
		pit[1].setup(37.5, 12.5, 10, 2.5, DIAMOND, NOTHING, true);
		pit[2].setup(75, 12.5, 10, 2.5, DIAMOND, NOTHING, true);
		pit[3].setup(20, 25, 10, 4, DIAMOND, NOTHING, true);
		pit[4].setup(55, 25, 10, 4, DIAMOND, NOTHING, true);
		pit[5].setup(0, 37.5, 10, 2.5, DIAMOND, NOTHING, true);
		pit[6].setup(37.5, 37.5, 10, 2.5, DIAMOND, NOTHING, true);
		pit[7].setup(75, 37.5, 10, 2.5, DIAMOND, NOTHING, true);
		name = "hellhole";
		break;
	case ARROWHOLES:
		adjacentLeft = SYMHOLES;
		adjacentRight = HELLHOLES;
		adjacentUp = FOREST;
		adjacentDown = TOWN;
		pitAmt = 4;
		pit[0].setup(29, 37.5, 20, 10, ARROW1, NOTHING, true);
		pit[1].setup(46, 37.5, 20, 10, ARROW2, NOTHING, true);
		pit[2].setup(18, 12.5, 10, 1, DIAMOND, NOTHING, true);
		pit[3].setup(56, 12.5, 10, 1, DIAMOND, NOTHING, true);
		for (int i=4; i<8; i++)
			pit[i].setup(0, 0, 0, 0, 0, 0, false);
		name = "arrowholes";
		break;
	case FOURHOLES:
		adjacentLeft = HELLHOLES;
		adjacentRight = SYMHOLES;
		adjacentUp = FOREST;
		adjacentDown = TOWN;
		pitAmt = 4;
		pit[0].setup(12.5, 25, 5, 15, DIAMOND, NOTHING, true);
		pit[1].setup(37.5, 15, 10, 5, DIAMOND, NOTHING, true);
		pit[2].setup(37.5, 35, 10, 5, DIAMOND, NOTHING, true);
		pit[3].setup(62.5, 25, 5, 15, DIAMOND, NOTHING, true);
		name = "fourholes";
		for (int i=4; i<8; i++)
			pit[i].setup(0, 0, 0, 0, 0, 0, false);
		break;
	case TOWN:
		adjacentLeft = HELLHOLES;
		adjacentRight = FOURHOLES;
		adjacentUp = ARROWHOLES;
		adjacentDown = ARROWHOLES;
		pitAmt = 0;
		for (int i=0; i<8; i++)
			pit[i].setup(0, 0, 0, 0, 0, 0, false);
		name = "town";
		break;
 	}
}

void PitClass::setup( float x, float y, float tw, float th, int ttype, int thasItem, bool texists )
{
	cx = x;
	cy = y;
	w = tw;
	h = th;
	type = ttype;
	hasItem = thasItem;
	exists = texists;
}


void MapGrid::Draw( float x, float y )
{
	glBegin( GL_QUADS );
		if ( name == "town" )
			glColor3f( 0.30f, 0.34f, 0.88f );
		else if ( name == "forest")
			glColor3f( 0.0f, 0.26f, 0.0f );
		else
			glColor3f( 0.3f, 0.5f, 0.2f );
		glVertex3f( x+0, y+0, 0 );

		if ( name == "town" )
			glColor3f( 0.4f, 0.2f, 0.9f );
		else if ( name == "forest")
			glColor3f( 0.2f, 0.28f, 0.2f );
		else
			glColor3f( 0.5f, 0.7f, 0.4f );
		glVertex3f( x+0, y+50, 0 );

		if ( name == "town" )
			glColor3f( 0.4f, 0.2f, 0.7f );
		else if ( name == "forest")
			glColor3f( 0.0f, 0.26f, 0.0f );
		else
			glColor3f( 0.3f, 0.5f, 0.2f );
		glVertex3f( x+75, y+50, 0 );

		if ( name == "town" )
			glColor3f( 0.30f, 0.34f, 0.88f );
		else if ( name == "forest")
			glColor3f( 0.0f, 0.26f, 0.0f );
		else
			glColor3f( 0.3f, 0.5f, 0.2f );
		glVertex3f( x+75, y+0, 0 );
	glEnd();

	for (int i=0; i<pitAmt; i++)
	{
		if ( pit[i].exists )
		{
			glBegin( GL_QUADS );
			if ( pit[i].type == DIAMOND )
			{
				glColor3f(0.1, 0.35, 0.1);
				glVertex3f( x+pit[i].cx-pit[i].w,		y+pit[i].cy, 1 );
				glVertex3f( x+pit[i].cx,				y+pit[i].cy-pit[i].h, 1 );
				glVertex3f( x+pit[i].cx+pit[i].w,		y+pit[i].cy, 1 );
				glVertex3f( x+pit[i].cx,				y+pit[i].cy+pit[i].h, 1 );		
			}
			else if ( pit[i].type == ARROW1 )
			{
			
				glColor3f(0.1, 0.35, 0.1);
				glVertex3f( x+pit[i].cx ,			y+pit[i].cy, 1 );
				glVertex3f( x+pit[i].cx-pit[i].w/2,	y+pit[i].cy, 1 );
				glVertex3f( x+pit[i].cx-pit[i].w,	y+pit[i].cy-pit[i].h/2, 1 );
				glVertex3f( x+pit[i].cx-pit[i].w/2,	y+pit[i].cy-pit[i].h/2, 1 );

				glVertex3f( x+pit[i].cx-pit[i].w,	y+pit[i].cy-pit[i].h/2, 1 );
				glVertex3f( x+pit[i].cx-pit[i].w/2,	y+pit[i].cy-pit[i].h/2, 1 );
				glVertex3f( x+pit[i].cx,			y+pit[i].cy-pit[i].h, 1 );
				glVertex3f( x+pit[i].cx-pit[i].w/2,	y+pit[i].cy-pit[i].h, 1 );
			}
			else if ( pit[i].type == ARROW2 )
			{
				glColor3f(0.1, 0.35, 0.1);
				glVertex3f( x+pit[i].cx ,			y+pit[i].cy, 1 );
				glVertex3f( x+pit[i].cx+pit[i].w/2,	y+pit[i].cy, 1 );
				glVertex3f( x+pit[i].cx+pit[i].w,	y+pit[i].cy-pit[i].h/2, 1 );
				glVertex3f( x+pit[i].cx+pit[i].w/2,	y+pit[i].cy-pit[i].h/2, 1 );

				glVertex3f( x+pit[i].cx+pit[i].w,	y+pit[i].cy-pit[i].h/2, 1 );
				glVertex3f( x+pit[i].cx+pit[i].w/2,	y+pit[i].cy-pit[i].h/2, 1 );
				glVertex3f( x+pit[i].cx,			y+pit[i].cy-pit[i].h, 1 );
				glVertex3f( x+pit[i].cx+pit[i].w/2,	y+pit[i].cy-pit[i].h, 1 );
			}
			glEnd();
		}
	}	

	if ( name == "town" )		//buildings
	{
		DrawBuilding( x+15, y+35 );
		DrawBuilding( x+60, y+35 );
		DrawHouse( x+37.5, y+10 );
	}
	else if ( name == "forest" )
	{
		DrawTree( x+15, y+35 );
		DrawTree( x+45, y+10 );
		DrawTree( x+60, y+35 );
		DrawTree( x+25, y+25 );
		DrawTree( x+10, y+10 );
		DrawTree( x+70, y+10 );
	}
}

void DrawHouse( float tx, float ty )
{
	glBegin( GL_QUADS );
		//Back face
		glColor3f(0.5, 0.5, 0.5);
		glVertex3f(tx-10, ty+5, 0.0);
		glVertex3f(tx+10, ty+5, 0.0);
		glColor3f(0.7, 0.7, 0.7);
		glVertex3f(tx+10, ty+5, 10.0);
		glVertex3f(tx-10, ty+5, 10.0);

		//Left face
		glColor3f(0.5, 0.5, 0.5);
		glVertex3f(tx-10, ty-5, 0.0);
		glVertex3f(tx-10, ty+5, 0.0);
		glColor3f(0.7, 0.7, 0.7);
		glVertex3f(tx-10, ty+5, 10.0);
		glVertex3f(tx-10, ty-5, 10.0);

		//Right face
		glColor3f(0.5, 0.5, 0.5);
		glVertex3f(tx+10, ty-5, 0.0);
		glVertex3f(tx+10, ty+5, 0.0);
		glColor3f(0.7, 0.7, 0.7);
		glVertex3f(tx+10, ty+5, 10.0);
		glVertex3f(tx+10, ty-5, 10.0);

		//Front face
		glColor3f(0.5, 0.5, 0.5);
		glVertex3f(tx-10, ty-5, 0.0);
		glVertex3f(tx+10, ty-5, 0.0);
		glColor3f(0.7, 0.7, 0.7);
		glVertex3f(tx+10, ty-5, 10.0);
		glVertex3f(tx-10, ty-5, 10.0);

		//Windows
		glColor3f(0.30f, 0.34f, 0.88f);
		glVertex3f(tx-5, ty-5, 3.0);
		glVertex3f(tx-2, ty-5, 3.0);
		glVertex3f(tx-2, ty-5, 5.0);
		glVertex3f(tx-5, ty-5, 5.0);

		glVertex3f(tx-5, ty-5, 6.0);
		glVertex3f(tx-2, ty-5, 6.0);
		glVertex3f(tx-2, ty-5, 8.0);
		glVertex3f(tx-5, ty-5, 8.0);

		glVertex3f(tx+5, ty-5, 3.0);
		glVertex3f(tx+2, ty-5, 3.0);
		glVertex3f(tx+2, ty-5, 5.0);
		glVertex3f(tx+5, ty-5, 5.0);

		glVertex3f(tx+5, ty-5, 6.0);
		glVertex3f(tx+2, ty-5, 6.0);
		glVertex3f(tx+2, ty-5, 8.0);
		glVertex3f(tx+5, ty-5, 8.0);

		//Roof
		glColor3f(0.5, 0.2, 0.2);
		glVertex3f(tx-12, ty-5, 9.0);
		glVertex3f(tx-12, ty+5, 9.0);
		glColor3f(0.7, 0.5, 0.5);
		glVertex3f(tx+0, ty+5, 15.0);
		glVertex3f(tx+0, ty-5, 15.0);
		
		glColor3f(0.5, 0.2, 0.2);
		glVertex3f(tx+12, ty-5, 9.0);
		glVertex3f(tx+12, ty+5, 9.0);
		glColor3f(0.7, 0.5, 0.5);
		glVertex3f(tx+0, ty+5, 15.0);
		glVertex3f(tx+0, ty-5, 15.0);
	glEnd();

	glBegin ( GL_TRIANGLES );
		glColor3f(0.7, 0.5, 0.5);
		glVertex3f(tx+0, ty-5, 15.0);
		glColor3f(0.5, 0.2, 0.2);
		glVertex3f(tx-10, ty-5, 10.0);
		glVertex3f(tx+10, ty-5, 10.0);
	glEnd();
}

void DrawBuilding( float tx, float ty )
{
	glBegin( GL_QUADS );
		//Base
		glColor3f(0.5, 0.5, 0.5);
		glVertex3f(tx-15, ty-5, 0.0);
		glVertex3f(tx+15, ty-5, 0.0);
		glVertex3f(tx+15, ty+5, 0.0);
		glVertex3f(tx-15, ty+5, 0.0);

		//Left face
		glColor3f(0.5, 0.5, 0.5);
		glVertex3f(tx-15, ty-5, 0.0);
		glVertex3f(tx-15, ty+5, 0.0);
		glColor3f(0.7, 0.7, 0.7);
		glVertex3f(tx-10, ty+5, 2.0);
		glVertex3f(tx-10, ty-5, 2.0);

		//Right face
		glColor3f(0.5, 0.5, 0.5);
		glVertex3f(tx+15, ty-5, 0.0);
		glVertex3f(tx+15, ty+5, 0.0);
		glColor3f(0.7, 0.7, 0.7);
		glVertex3f(tx+10, ty+5, 2.0);
		glVertex3f(tx+10, ty-5, 2.0);

		//front face of base
		glColor3f(0.5, 0.5, 0.5);
		glVertex3f(tx-15, ty-5, 0.0);
		glVertex3f(tx+15, ty-5, 0.0);
		glColor3f(0.7, 0.7, 0.7);
		glVertex3f(tx+10, ty-5, 2.0);
		glVertex3f(tx-10, ty-5, 2.0);

		//Base2
		glColor3f(0.7, 0.7, 0.7);
		glVertex3f(tx-10, ty-5, 2.0);
		glVertex3f(tx+10, ty-5, 2.0);
		glVertex3f(tx+10, ty+5, 2.0);
		glVertex3f(tx-10, ty+5, 2.0);

		//pillars
		DrawPillar(tx, ty);
		DrawPillar(tx+7.5, ty);
		DrawPillar(tx+15, ty);	

		//roof
		//Left face
		glColor3f(0.5, 0.5, 0.5);
		glVertex3f(tx-15, ty-5, 12.0);
		glVertex3f(tx-15, ty+5, 12.0);
		glColor3f(0.7, 0.7, 0.7);
		glVertex3f(tx-15, ty+5, 15.0);
		glVertex3f(tx-15, ty-5, 15.0);

		//Right face
		glColor3f(0.5, 0.5, 0.5);
		glVertex3f(tx+15, ty-5, 12.0);
		glVertex3f(tx+15, ty+5, 12.0);
		glColor3f(0.7, 0.7, 0.7);
		glVertex3f(tx+15, ty+5, 15.0);
		glVertex3f(tx+15, ty-5, 15.0);

		//Front face
		glColor3f(0.5, 0.5, 0.5);
		glVertex3f(tx-15, ty-5, 12.0);
		glVertex3f(tx+15, ty-5, 12.0);
		glColor3f(0.7, 0.7, 0.7);
		glVertex3f(tx+15, ty-5, 15.0);
		glVertex3f(tx-15, ty-5, 15.0);

		//top
		glColor3f(0.9, 0.9, 0.9);
		glVertex3f(tx-15, ty-5, 15.0);
		glVertex3f(tx+15, ty-5, 15.0);
		glVertex3f(tx+15, ty+5, 15.0);
		glVertex3f(tx-15, ty+5, 15.0);

	glEnd();
}

void DrawPillar( float tx, float ty )
{
	//Left face
		glColor3f(0.5, 0.5, 0.5);
		glVertex3f(tx-10, ty-5, 2.0);
		glVertex3f(tx-10, ty+5, 2.0);
		glColor3f(0.7, 0.7, 0.7);
		glVertex3f(tx-10, ty+5, 12.0);
		glVertex3f(tx-10, ty-5, 12.0);

		//Right face
		glColor3f(0.5, 0.5, 0.5);
		glVertex3f(tx-5, ty-5, 2.0);
		glVertex3f(tx-5, ty+5, 2.0);
		glColor3f(0.7, 0.7, 0.7);
		glVertex3f(tx-5, ty+5, 12.0);
		glVertex3f(tx-5, ty-5, 12.0);

		//front face of base
		glColor3f(0.5, 0.5, 0.5);
		glVertex3f(tx-10, ty-5, 2.0);
		glVertex3f(tx-5, ty-5, 2.0);
		glColor3f(0.7, 0.7, 0.7);
		glVertex3f(tx-5, ty-5, 12.0);
		glVertex3f(tx-10, ty-5, 12.0);
}

void DrawTree( float tx, float ty )
{
	glBegin( GL_QUADS );
	//TRUNK
	//Left face
	glColor3f(0.3, 0.2, 0.0);
	glVertex3f(tx-2, ty-2, 2.0);
	glVertex3f(tx-2, ty+2, 2.0);
	glColor3f(0.5, 0.3, 0.0);
	glVertex3f(tx-2, ty+2, 12.0);
	glVertex3f(tx-2, ty-2, 12.0);

	//Right face
	glColor3f(0.3, 0.2, 0.0);
	glVertex3f(tx+2, ty-2, 2.0);
	glVertex3f(tx+2, ty+2, 2.0);
	glColor3f(0.5, 0.3, 0.0);
	glVertex3f(tx+2, ty+2, 12.0);
	glVertex3f(tx+2, ty-2, 12.0);

	//front face of base
	glColor3f(0.3, 0.2, 0.0);
	glVertex3f(tx-2, ty-2, 2.0);
	glVertex3f(tx+2, ty-2, 2.0);
	glColor3f(0.5, 0.3, 0.0);
	glVertex3f(tx+2, ty-2, 12.0);
	glVertex3f(tx-2, ty-2, 12.0);

	//greenery
	//Base1
	glColor3f(0.0, 0.5, 0.0);
	glVertex3f(tx-5, ty-5, 12.0);
	glVertex3f(tx-5, ty+5, 12.0);
	glColor3f(0.0, 0.8, 0.0);
	glVertex3f(tx+5, ty+5, 12.0);
	glVertex3f(tx+5, ty-5, 12.0);

	
	//left face
	glColor3f(0.0, 0.5, 0.0);
	glVertex3f(tx-5, ty-5, 12.0);
	glVertex3f(tx-5, ty+5, 12.0);
	glColor3f(0.0, 0.8, 0.0);
	glVertex3f(tx-3, ty+3, 16.0);
	glVertex3f(tx-3, ty-3, 16.0);

	//right face
	glColor3f(0.0, 0.5, 0.0);
	glVertex3f(tx+5, ty-5, 12.0);
	glVertex3f(tx+5, ty+5, 12.0);
	glColor3f(0.0, 0.8, 0.0);
	glVertex3f(tx+3, ty+3, 16.0);
	glVertex3f(tx+3, ty-3, 16.0);

	//front face
	glColor3f(0.0, 0.5, 0.0);
	glVertex3f(tx-5, ty-5, 12.0);
	glVertex3f(tx+5, ty-5, 12.0);
	glColor3f(0.0, 0.8, 0.0);
	glVertex3f(tx+3, ty-3, 16.0);
	glVertex3f(tx-3, ty-3, 16.0);

	//Base2
	glColor3f(0.0, 0.5, 0.0);
	glVertex3f(tx-3, ty-3, 16.0);
	glVertex3f(tx-3, ty+3, 16.0);
	glColor3f(0.0, 0.8, 0.0);
	glVertex3f(tx+3, ty+3, 16.0);
	glVertex3f(tx+3, ty-3, 16.0);

	//left face
	glColor3f(0.0, 0.5, 0.0);
	glVertex3f(tx-4, ty-4, 16);
	glVertex3f(tx-4, ty+4, 16);
	glColor3f(0.0, 0.8, 0.0);
	glVertex3f(tx-1, ty+1, 25);
	glVertex3f(tx-1, ty-1, 25);

	//right face
	glColor3f(0.0, 0.5, 0.0);
	glVertex3f(tx+4, ty-4, 16);
	glVertex3f(tx+4, ty+4, 16);
	glColor3f(0.0, 0.8, 0.0);
	glVertex3f(tx+1, ty+1, 25);
	glVertex3f(tx+1, ty-1, 25);

	//front face
	glColor3f(0.0, 0.5, 0.0);
	glVertex3f(tx-4, ty-4, 16);
	glVertex3f(tx+4, ty-4, 16);
	glColor3f(0.0, 0.8, 0.0);
	glVertex3f(tx+1, ty-1, 25);
	glVertex3f(tx-1, ty-1, 25);

	//Base3
	glColor3f(0.0, 0.5, 0.0);
	glVertex3f(tx-1, ty-1, 25);
	glVertex3f(tx-1, ty+1, 25);
	glColor3f(0.0, 0.8, 0.0);
	glVertex3f(tx+1, ty+1, 25);
	glVertex3f(tx+1, ty-1, 25);

	glEnd();
}