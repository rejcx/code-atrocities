#include "SDL/SDL.h"
#include "SDL/SDL_opengl.h"
#include "Input.h"

Input::Input()
{
	keys = NULL;
	Refresh();
}

Input::~Input()
{
}

void Input::Refresh()
{
	SDL_PumpEvents();
	keys = SDL_GetKeyState(NULL);
	SDL_GetMouseState(&mousex, &mousey);
	for (int i=0; i<BTNAMT; i++)
	{
		SDL_GetMouseState(NULL, NULL) &SDL_BUTTON(i);
	}
}

bool Input::GetKey(int key)
{
	return keys[key];
}

int Input::GetMouseX()
{
	return mousex;
}

int Input::GetMouseY()
{
	return mousey;
}

bool Input::GetMouseButton(int button)
{
	if (button > BTNAMT)
	{
		return false;
	}
	else
	{
		return mouseButtons[button];
	}
}