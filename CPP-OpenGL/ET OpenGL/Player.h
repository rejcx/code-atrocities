#pragma once

class Player
{
	private:
		float x, y, z, w, h;
		int currentMap;
	public:
		Player();
		float X() { return x; }
		float Y() { return y; }
		float Z() { return z; }
		float W() { return w; }
		float H() { return h; }
		void SetX( float val ) { x = val; }
		void SetY( float val ) { y = val; }
		int CurrentMap() { return currentMap; }
		void SetMap( int val ) { currentMap = val; }
		void Move( char direction );
		void Draw();
};