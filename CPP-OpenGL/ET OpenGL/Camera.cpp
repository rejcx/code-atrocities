#include "Camera.h"
#include <iostream>
using namespace std;

Camera::Camera()
{
	cameraX = -37.5;
	cameraY = 35;
	cameraZ = -50;
	angle = -45;
	currentMap = 3;
}

void Camera::IncreaseAngle()
{
	if ( angle - 1 >= -60 )
	{
		angle -= 1.0;
		cameraY += 1.3;
		cameraZ += 0.5;
	}
}

void Camera::DecreaseAngle()
{
	if ( angle + 1 <= 0 )
	{
		angle += 1.0;
		cameraY -= 1.3;
		cameraZ -= 0.5;	
	}
}

void Camera::Move( char direction )
{
	if ( direction == 'n' )
	{
		cameraY++;
	}
	else if ( direction == 's' )
	{
		cameraY--;
	}
	else if ( direction == 'e' )
	{
		cameraX--;
	}
	else if ( direction == 'w' )
	{
		cameraX++;
	}
	else if ( direction == 'u' )
	{
		cameraZ--;
	}
	else if ( direction == 'd' )
	{
		cameraZ++;
	}
}