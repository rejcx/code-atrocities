#pragma once

#define BTNAMT 6

class Input
{
	private:
		Uint8 *keys;
		int mousex, mousey;
		bool mouseButtons[BTNAMT];
	public:
		Input();
		~Input();
		void Refresh();
		int GetMouseX();
		int GetMouseY();
		bool GetKey(int);
		bool GetMouseButton(int);
};