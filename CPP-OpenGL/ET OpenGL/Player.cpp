#include "Player.h"
#include "SDL/SDL.h"
#include "SDL/SDL_opengl.h"
#include <iostream>
using namespace std;

Player::Player()
{
	x = 34;
	y = 13;
	z = 1.2;
	w = 5;
	h = 10;
	currentMap = 3;
}

void Player::Move( char direction )
{
	if ( direction == 'n' )
	{
		y++;
	}
	else if ( direction == 's' )
	{
		y--;
	}
	else if ( direction == 'e' )
	{
		x--;
	}
	else if ( direction == 'w' )
	{
		x++;
	}
	else if ( direction == 'u' )
	{
		z++;
	}
	else if ( direction == 'd' )
	{
		z--;
	}
}

void Player::Draw()
{
	glBegin( GL_QUADS );
		glColor3f( 1.0f, 0.0f, 0.0f );
		glVertex3f( x, y, z );

		glColor3f( 0.0f, 1.0f, 0.0f );
		glVertex3f( x+w, y, z );

		glColor3f( 0.0f, 0.0f, 1.0f );
		glVertex3f( x+w, y, z+h );

		glColor3f( 1.0f, 1.0f, 0.0f );
		glVertex3f( x, y, z+h );
	glEnd();
}