#pragma once

class Camera
{
	private:
		float cameraX, cameraY, cameraZ, angle;
		int currentMap;
	public:
		Camera();
		void IncreaseAngle();
		void DecreaseAngle();
		float CameraX() { return cameraX; }
		float CameraY() { return cameraY; }
		float CameraZ() { return cameraZ; }
		void SetX( float val ) { cameraX = val; }
		void SetY( float val ) { cameraY = val; }
		float Angle() { return angle; }
		int CurrentMap() { return currentMap; }
		void SetMap( int val ) { currentMap = val; }
		void Move( char direction );
};