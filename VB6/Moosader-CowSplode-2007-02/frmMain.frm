VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.OCX"
Begin VB.Form frmMain 
   BackColor       =   &H0000C000&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Cow Splode"
   ClientHeight    =   10620
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7440
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   10620
   ScaleWidth      =   7440
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer tmrFrame 
      Enabled         =   0   'False
      Interval        =   750
      Left            =   7200
      Top             =   120
   End
   Begin VB.Timer tmrQuit 
      Enabled         =   0   'False
      Interval        =   10
      Left            =   7200
      Top             =   600
   End
   Begin VB.CommandButton cmdDisconnect 
      Caption         =   "Disconnect"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   18
      Top             =   10200
      Width           =   7215
   End
   Begin VB.CommandButton cmdSendMessage 
      Caption         =   "Transmit Message Of Evil"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   4
      Top             =   9840
      Width           =   7215
   End
   Begin VB.TextBox txtMessage 
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   120
      TabIndex        =   3
      Top             =   9480
      Width           =   7215
   End
   Begin MSWinsockLib.Winsock Winsock1 
      Left            =   7200
      Top             =   1080
      _ExtentX        =   741
      _ExtentY        =   741
   End
   Begin VB.Frame fraChat 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0FFFF&
      ForeColor       =   &H0000FF00&
      Height          =   1575
      Left            =   120
      TabIndex        =   0
      Top             =   7800
      Visible         =   0   'False
      Width           =   7215
      Begin VB.Label lblClientScore 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Score: 0"
         BeginProperty Font 
            Name            =   "Comic Sans MS"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   3840
         TabIndex        =   17
         Top             =   1200
         Width           =   3135
      End
      Begin VB.Label lblServerScore 
         BackStyle       =   0  'Transparent
         Caption         =   "Score: 0"
         BeginProperty Font 
            Name            =   "Comic Sans MS"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   1200
         Width           =   3135
      End
      Begin VB.Label lblClientTalk 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "I am KLIENT the EVIL ALIEN FARMER.  I own farming, totally."
         BeginProperty Font 
            Name            =   "Comic Sans MS"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   3840
         TabIndex        =   2
         Top             =   240
         Width           =   1935
         WordWrap        =   -1  'True
      End
      Begin VB.Label lblServerTalk 
         Alignment       =   2  'Center
         BackStyle       =   0  'Transparent
         Caption         =   "I am SIR VERR, I'm at war with farmer KLIENT because he's dumb."
         BeginProperty Font 
            Name            =   "Comic Sans MS"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   1320
         TabIndex        =   1
         Top             =   240
         Width           =   1935
         WordWrap        =   -1  'True
      End
      Begin VB.Image Image4 
         Height          =   960
         Left            =   3720
         Picture         =   "frmMain.frx":08CA
         Top             =   120
         Width           =   2400
      End
      Begin VB.Image Image3 
         Height          =   960
         Left            =   960
         Picture         =   "frmMain.frx":0D99
         Top             =   120
         Width           =   2400
      End
      Begin VB.Image Image2 
         Height          =   960
         Left            =   6480
         Picture         =   "frmMain.frx":1264
         Top             =   120
         Width           =   480
      End
      Begin VB.Image Image1 
         Height          =   960
         Left            =   240
         Picture         =   "frmMain.frx":1788
         Top             =   120
         Width           =   480
      End
   End
   Begin VB.Frame fraOnline 
      Caption         =   "HOST GAME, OR CONNECT TO GAME?"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1575
      Left            =   120
      TabIndex        =   5
      Top             =   7800
      Width           =   7215
      Begin VB.TextBox txtIP 
         BeginProperty Font 
            Name            =   "Comic Sans MS"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   2160
         TabIndex        =   12
         Text            =   "127.0.0.1"
         Top             =   720
         Width           =   1215
      End
      Begin VB.TextBox txtPort 
         BeginProperty Font 
            Name            =   "Comic Sans MS"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   2160
         TabIndex        =   11
         Text            =   "666"
         Top             =   360
         Width           =   1215
      End
      Begin VB.OptionButton optClient 
         Caption         =   "Client"
         BeginProperty Font 
            Name            =   "Comic Sans MS"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   9
         Top             =   720
         Value           =   -1  'True
         Width           =   1215
      End
      Begin VB.OptionButton optServer 
         Caption         =   "Server"
         BeginProperty Font 
            Name            =   "Comic Sans MS"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   8
         Top             =   360
         Width           =   1215
      End
      Begin VB.CommandButton cmdStartConnection 
         Caption         =   "OK!"
         BeginProperty Font 
            Name            =   "Comic Sans MS"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   1200
         Width           =   1575
      End
      Begin VB.Label Label35 
         BackStyle       =   0  'Transparent
         Caption         =   "Each turn cows may move around.  Blowing up a cow will blow up all other cows in a one-tile radius."
         BeginProperty Font 
            Name            =   "Comic Sans MS"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   735
         Left            =   3600
         TabIndex        =   15
         Top             =   840
         Width           =   3495
      End
      Begin VB.Label Label34 
         BackStyle       =   0  'Transparent
         Caption         =   "You and a fellow farmer will take turns blowing up cows."
         BeginProperty Font 
            Name            =   "Comic Sans MS"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   4320
         TabIndex        =   14
         Top             =   360
         Width           =   2775
      End
      Begin VB.Label Label33 
         Caption         =   "Rules:"
         BeginProperty Font 
            Name            =   "Comic Sans MS"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3600
         TabIndex        =   13
         Top             =   360
         Width           =   615
      End
      Begin VB.Line Line1 
         X1              =   3480
         X2              =   3480
         Y1              =   360
         Y2              =   1440
      End
      Begin VB.Label Label32 
         Alignment       =   1  'Right Justify
         Caption         =   "IP:"
         BeginProperty Font 
            Name            =   "Comic Sans MS"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1200
         TabIndex        =   10
         Top             =   720
         Width           =   855
      End
      Begin VB.Label Label31 
         Alignment       =   1  'Right Justify
         Caption         =   "PORT:"
         BeginProperty Font 
            Name            =   "Comic Sans MS"
            Size            =   12
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1080
         TabIndex        =   7
         Top             =   360
         Width           =   975
      End
   End
   Begin VB.Label lblStatus 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00008000&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "COW SPLODE by RACHEL J. MORRIS"
      BeginProperty Font 
         Name            =   "Comic Sans MS"
         Size            =   12.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FFFFFF&
      Height          =   375
      Left            =   120
      TabIndex        =   19
      Top             =   0
      Width           =   7215
   End
   Begin VB.Image imgBlood 
      Height          =   480
      Left            =   -360
      Picture         =   "frmMain.frx":1D0D
      Top             =   9600
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   224
      Left            =   6840
      Picture         =   "frmMain.frx":20F2
      Top             =   7200
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   223
      Left            =   6360
      Picture         =   "frmMain.frx":2483
      Top             =   7200
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   222
      Left            =   5880
      Picture         =   "frmMain.frx":2814
      Top             =   7200
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   221
      Left            =   5400
      Picture         =   "frmMain.frx":2BA5
      Top             =   7200
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   220
      Left            =   4920
      Picture         =   "frmMain.frx":2F36
      Top             =   7200
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   219
      Left            =   4440
      Picture         =   "frmMain.frx":32C7
      Top             =   7200
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   218
      Left            =   3960
      Picture         =   "frmMain.frx":3658
      Top             =   7200
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   217
      Left            =   3480
      Picture         =   "frmMain.frx":39E9
      Top             =   7200
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   216
      Left            =   3000
      Picture         =   "frmMain.frx":3D7A
      Top             =   7200
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   215
      Left            =   2520
      Picture         =   "frmMain.frx":410B
      Top             =   7200
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   214
      Left            =   2040
      Picture         =   "frmMain.frx":449C
      Top             =   7200
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   213
      Left            =   1560
      Picture         =   "frmMain.frx":482D
      Top             =   7200
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   212
      Left            =   1080
      Picture         =   "frmMain.frx":4BBE
      Top             =   7200
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   211
      Left            =   600
      Picture         =   "frmMain.frx":4F4F
      Top             =   7200
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   210
      Left            =   120
      Picture         =   "frmMain.frx":52E0
      Top             =   7200
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   209
      Left            =   6840
      Picture         =   "frmMain.frx":5671
      Top             =   6720
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   208
      Left            =   6360
      Picture         =   "frmMain.frx":5A02
      Top             =   6720
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   207
      Left            =   5880
      Picture         =   "frmMain.frx":5D93
      Top             =   6720
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   206
      Left            =   5400
      Picture         =   "frmMain.frx":6124
      Top             =   6720
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   205
      Left            =   4920
      Picture         =   "frmMain.frx":64B5
      Top             =   6720
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   204
      Left            =   4440
      Picture         =   "frmMain.frx":6846
      Top             =   6720
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   203
      Left            =   3960
      Picture         =   "frmMain.frx":6BD7
      Top             =   6720
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   202
      Left            =   3480
      Picture         =   "frmMain.frx":6F68
      Top             =   6720
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   201
      Left            =   3000
      Picture         =   "frmMain.frx":72F9
      Top             =   6720
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   200
      Left            =   2520
      Picture         =   "frmMain.frx":768A
      Top             =   6720
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   199
      Left            =   2040
      Picture         =   "frmMain.frx":7A1B
      Top             =   6720
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   198
      Left            =   1560
      Picture         =   "frmMain.frx":7DAC
      Top             =   6720
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   197
      Left            =   1080
      Picture         =   "frmMain.frx":813D
      Top             =   6720
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   196
      Left            =   600
      Picture         =   "frmMain.frx":84CE
      Top             =   6720
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   195
      Left            =   120
      Picture         =   "frmMain.frx":885F
      Top             =   6720
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   194
      Left            =   6840
      Picture         =   "frmMain.frx":8BF0
      Top             =   6240
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   193
      Left            =   6360
      Picture         =   "frmMain.frx":8F81
      Top             =   6240
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   192
      Left            =   5880
      Picture         =   "frmMain.frx":9312
      Top             =   6240
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   191
      Left            =   5400
      Picture         =   "frmMain.frx":96A3
      Top             =   6240
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   190
      Left            =   4920
      Picture         =   "frmMain.frx":9A34
      Top             =   6240
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   189
      Left            =   4440
      Picture         =   "frmMain.frx":9DC5
      Top             =   6240
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   188
      Left            =   3960
      Picture         =   "frmMain.frx":A156
      Top             =   6240
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   187
      Left            =   3480
      Picture         =   "frmMain.frx":A4E7
      Top             =   6240
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   186
      Left            =   3000
      Picture         =   "frmMain.frx":A878
      Top             =   6240
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   185
      Left            =   2520
      Picture         =   "frmMain.frx":AC09
      Top             =   6240
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   184
      Left            =   2040
      Picture         =   "frmMain.frx":AF9A
      Top             =   6240
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   183
      Left            =   1560
      Picture         =   "frmMain.frx":B32B
      Top             =   6240
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   182
      Left            =   1080
      Picture         =   "frmMain.frx":B6BC
      Top             =   6240
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   181
      Left            =   600
      Picture         =   "frmMain.frx":BA4D
      Top             =   6240
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   180
      Left            =   120
      Picture         =   "frmMain.frx":BDDE
      Top             =   6240
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   179
      Left            =   6840
      Picture         =   "frmMain.frx":C16F
      Top             =   5760
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   178
      Left            =   6360
      Picture         =   "frmMain.frx":C500
      Top             =   5760
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   177
      Left            =   5880
      Picture         =   "frmMain.frx":C891
      Top             =   5760
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   176
      Left            =   5400
      Picture         =   "frmMain.frx":CC22
      Top             =   5760
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   175
      Left            =   4920
      Picture         =   "frmMain.frx":CFB3
      Top             =   5760
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   174
      Left            =   4440
      Picture         =   "frmMain.frx":D344
      Top             =   5760
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   173
      Left            =   3960
      Picture         =   "frmMain.frx":D6D5
      Top             =   5760
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   172
      Left            =   3480
      Picture         =   "frmMain.frx":DA66
      Top             =   5760
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   171
      Left            =   3000
      Picture         =   "frmMain.frx":DDF7
      Top             =   5760
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   170
      Left            =   2520
      Picture         =   "frmMain.frx":E188
      Top             =   5760
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   169
      Left            =   2040
      Picture         =   "frmMain.frx":E519
      Top             =   5760
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   168
      Left            =   1560
      Picture         =   "frmMain.frx":E8AA
      Top             =   5760
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   167
      Left            =   1080
      Picture         =   "frmMain.frx":EC3B
      Top             =   5760
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   166
      Left            =   600
      Picture         =   "frmMain.frx":EFCC
      Top             =   5760
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   165
      Left            =   120
      Picture         =   "frmMain.frx":F35D
      Top             =   5760
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   164
      Left            =   6840
      Picture         =   "frmMain.frx":F6EE
      Top             =   5280
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   163
      Left            =   6360
      Picture         =   "frmMain.frx":FA7F
      Top             =   5280
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   162
      Left            =   5880
      Picture         =   "frmMain.frx":FE10
      Top             =   5280
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   161
      Left            =   5400
      Picture         =   "frmMain.frx":101A1
      Top             =   5280
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   160
      Left            =   4920
      Picture         =   "frmMain.frx":10532
      Top             =   5280
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   159
      Left            =   4440
      Picture         =   "frmMain.frx":108C3
      Top             =   5280
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   158
      Left            =   3960
      Picture         =   "frmMain.frx":10C54
      Top             =   5280
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   157
      Left            =   3480
      Picture         =   "frmMain.frx":10FE5
      Top             =   5280
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   156
      Left            =   3000
      Picture         =   "frmMain.frx":11376
      Top             =   5280
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   155
      Left            =   2520
      Picture         =   "frmMain.frx":11707
      Top             =   5280
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   154
      Left            =   2040
      Picture         =   "frmMain.frx":11A98
      Top             =   5280
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   153
      Left            =   1560
      Picture         =   "frmMain.frx":11E29
      Top             =   5280
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   152
      Left            =   1080
      Picture         =   "frmMain.frx":121BA
      Top             =   5280
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   151
      Left            =   600
      Picture         =   "frmMain.frx":1254B
      Top             =   5280
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   150
      Left            =   120
      Picture         =   "frmMain.frx":128DC
      Top             =   5280
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   149
      Left            =   6840
      Picture         =   "frmMain.frx":12C6D
      Top             =   4800
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   148
      Left            =   6360
      Picture         =   "frmMain.frx":12FFE
      Top             =   4800
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   147
      Left            =   5880
      Picture         =   "frmMain.frx":1338F
      Top             =   4800
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   146
      Left            =   5400
      Picture         =   "frmMain.frx":13720
      Top             =   4800
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   145
      Left            =   4920
      Picture         =   "frmMain.frx":13AB1
      Top             =   4800
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   144
      Left            =   4440
      Picture         =   "frmMain.frx":13E42
      Top             =   4800
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   143
      Left            =   3960
      Picture         =   "frmMain.frx":141D3
      Top             =   4800
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   142
      Left            =   3480
      Picture         =   "frmMain.frx":14564
      Top             =   4800
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   141
      Left            =   3000
      Picture         =   "frmMain.frx":148F5
      Top             =   4800
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   140
      Left            =   2520
      Picture         =   "frmMain.frx":14C86
      Top             =   4800
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   139
      Left            =   2040
      Picture         =   "frmMain.frx":15017
      Top             =   4800
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   138
      Left            =   1560
      Picture         =   "frmMain.frx":153A8
      Top             =   4800
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   137
      Left            =   1080
      Picture         =   "frmMain.frx":15739
      Top             =   4800
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   136
      Left            =   600
      Picture         =   "frmMain.frx":15ACA
      Top             =   4800
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   135
      Left            =   120
      Picture         =   "frmMain.frx":15E5B
      Top             =   4800
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   134
      Left            =   6840
      Picture         =   "frmMain.frx":161EC
      Top             =   4320
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   133
      Left            =   6360
      Picture         =   "frmMain.frx":1657D
      Top             =   4320
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   132
      Left            =   5880
      Picture         =   "frmMain.frx":1690E
      Top             =   4320
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   131
      Left            =   5400
      Picture         =   "frmMain.frx":16C9F
      Top             =   4320
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   130
      Left            =   4920
      Picture         =   "frmMain.frx":17030
      Top             =   4320
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   129
      Left            =   4440
      Picture         =   "frmMain.frx":173C1
      Top             =   4320
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   128
      Left            =   3960
      Picture         =   "frmMain.frx":17752
      Top             =   4320
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   127
      Left            =   3480
      Picture         =   "frmMain.frx":17AE3
      Top             =   4320
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   126
      Left            =   3000
      Picture         =   "frmMain.frx":17E74
      Top             =   4320
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   125
      Left            =   2520
      Picture         =   "frmMain.frx":18205
      Top             =   4320
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   124
      Left            =   2040
      Picture         =   "frmMain.frx":18596
      Top             =   4320
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   123
      Left            =   1560
      Picture         =   "frmMain.frx":18927
      Top             =   4320
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   122
      Left            =   1080
      Picture         =   "frmMain.frx":18CB8
      Top             =   4320
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   121
      Left            =   600
      Picture         =   "frmMain.frx":19049
      Top             =   4320
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   120
      Left            =   120
      Picture         =   "frmMain.frx":193DA
      Top             =   4320
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   119
      Left            =   6840
      Picture         =   "frmMain.frx":1976B
      Top             =   3840
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   118
      Left            =   6360
      Picture         =   "frmMain.frx":19AFC
      Top             =   3840
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   117
      Left            =   5880
      Picture         =   "frmMain.frx":19E8D
      Top             =   3840
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   116
      Left            =   5400
      Picture         =   "frmMain.frx":1A21E
      Top             =   3840
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   115
      Left            =   4920
      Picture         =   "frmMain.frx":1A5AF
      Top             =   3840
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   114
      Left            =   4440
      Picture         =   "frmMain.frx":1A940
      Top             =   3840
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   113
      Left            =   3960
      Picture         =   "frmMain.frx":1ACD1
      Top             =   3840
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   112
      Left            =   3480
      Picture         =   "frmMain.frx":1B062
      Top             =   3840
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   111
      Left            =   3000
      Picture         =   "frmMain.frx":1B3F3
      Top             =   3840
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   110
      Left            =   2520
      Picture         =   "frmMain.frx":1B784
      Top             =   3840
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   109
      Left            =   2040
      Picture         =   "frmMain.frx":1BB15
      Top             =   3840
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   108
      Left            =   1560
      Picture         =   "frmMain.frx":1BEA6
      Top             =   3840
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   107
      Left            =   1080
      Picture         =   "frmMain.frx":1C237
      Top             =   3840
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   106
      Left            =   600
      Picture         =   "frmMain.frx":1C5C8
      Top             =   3840
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   105
      Left            =   120
      Picture         =   "frmMain.frx":1C959
      Top             =   3840
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   104
      Left            =   6840
      Picture         =   "frmMain.frx":1CCEA
      Top             =   3360
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   103
      Left            =   6360
      Picture         =   "frmMain.frx":1D07B
      Top             =   3360
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   102
      Left            =   5880
      Picture         =   "frmMain.frx":1D40C
      Top             =   3360
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   101
      Left            =   5400
      Picture         =   "frmMain.frx":1D79D
      Top             =   3360
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   100
      Left            =   4920
      Picture         =   "frmMain.frx":1DB2E
      Top             =   3360
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   99
      Left            =   4440
      Picture         =   "frmMain.frx":1DEBF
      Top             =   3360
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   98
      Left            =   3960
      Picture         =   "frmMain.frx":1E250
      Top             =   3360
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   97
      Left            =   3480
      Picture         =   "frmMain.frx":1E5E1
      Top             =   3360
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   96
      Left            =   3000
      Picture         =   "frmMain.frx":1E972
      Top             =   3360
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   95
      Left            =   2520
      Picture         =   "frmMain.frx":1ED03
      Top             =   3360
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   94
      Left            =   2040
      Picture         =   "frmMain.frx":1F094
      Top             =   3360
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   93
      Left            =   1560
      Picture         =   "frmMain.frx":1F425
      Top             =   3360
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   92
      Left            =   1080
      Picture         =   "frmMain.frx":1F7B6
      Top             =   3360
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   91
      Left            =   600
      Picture         =   "frmMain.frx":1FB47
      Top             =   3360
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   90
      Left            =   120
      Picture         =   "frmMain.frx":1FED8
      Top             =   3360
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   89
      Left            =   6840
      Picture         =   "frmMain.frx":20269
      Top             =   2880
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   88
      Left            =   6360
      Picture         =   "frmMain.frx":205FA
      Top             =   2880
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   87
      Left            =   5880
      Picture         =   "frmMain.frx":2098B
      Top             =   2880
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   86
      Left            =   5400
      Picture         =   "frmMain.frx":20D1C
      Top             =   2880
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   85
      Left            =   4920
      Picture         =   "frmMain.frx":210AD
      Top             =   2880
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   84
      Left            =   4440
      Picture         =   "frmMain.frx":2143E
      Top             =   2880
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   83
      Left            =   3960
      Picture         =   "frmMain.frx":217CF
      Top             =   2880
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   82
      Left            =   3480
      Picture         =   "frmMain.frx":21B60
      Top             =   2880
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   81
      Left            =   3000
      Picture         =   "frmMain.frx":21EF1
      Top             =   2880
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   80
      Left            =   2520
      Picture         =   "frmMain.frx":22282
      Top             =   2880
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   79
      Left            =   2040
      Picture         =   "frmMain.frx":22613
      Top             =   2880
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   78
      Left            =   1560
      Picture         =   "frmMain.frx":229A4
      Top             =   2880
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   77
      Left            =   1080
      Picture         =   "frmMain.frx":22D35
      Top             =   2880
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   76
      Left            =   600
      Picture         =   "frmMain.frx":230C6
      Top             =   2880
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   75
      Left            =   120
      Picture         =   "frmMain.frx":23457
      Top             =   2880
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   74
      Left            =   6840
      Picture         =   "frmMain.frx":237E8
      Top             =   2400
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   73
      Left            =   6360
      Picture         =   "frmMain.frx":23B79
      Top             =   2400
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   72
      Left            =   5880
      Picture         =   "frmMain.frx":23F0A
      Top             =   2400
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   71
      Left            =   5400
      Picture         =   "frmMain.frx":2429B
      Top             =   2400
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   70
      Left            =   4920
      Picture         =   "frmMain.frx":2462C
      Top             =   2400
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   69
      Left            =   4440
      Picture         =   "frmMain.frx":249BD
      Top             =   2400
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   68
      Left            =   3960
      Picture         =   "frmMain.frx":24D4E
      Top             =   2400
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   67
      Left            =   3480
      Picture         =   "frmMain.frx":250DF
      Top             =   2400
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   66
      Left            =   3000
      Picture         =   "frmMain.frx":25470
      Top             =   2400
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   65
      Left            =   2520
      Picture         =   "frmMain.frx":25801
      Top             =   2400
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   64
      Left            =   2040
      Picture         =   "frmMain.frx":25B92
      Top             =   2400
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   63
      Left            =   1560
      Picture         =   "frmMain.frx":25F23
      Top             =   2400
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   62
      Left            =   1080
      Picture         =   "frmMain.frx":262B4
      Top             =   2400
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   61
      Left            =   600
      Picture         =   "frmMain.frx":26645
      Top             =   2400
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   60
      Left            =   120
      Picture         =   "frmMain.frx":269D6
      Top             =   2400
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   59
      Left            =   6840
      Picture         =   "frmMain.frx":26D67
      Top             =   1920
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   58
      Left            =   6360
      Picture         =   "frmMain.frx":270F8
      Top             =   1920
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   57
      Left            =   5880
      Picture         =   "frmMain.frx":27489
      Top             =   1920
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   56
      Left            =   5400
      Picture         =   "frmMain.frx":2781A
      Top             =   1920
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   55
      Left            =   4920
      Picture         =   "frmMain.frx":27BAB
      Top             =   1920
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   54
      Left            =   4440
      Picture         =   "frmMain.frx":27F3C
      Top             =   1920
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   53
      Left            =   3960
      Picture         =   "frmMain.frx":282CD
      Top             =   1920
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   52
      Left            =   3480
      Picture         =   "frmMain.frx":2865E
      Top             =   1920
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   51
      Left            =   3000
      Picture         =   "frmMain.frx":289EF
      Top             =   1920
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   50
      Left            =   2520
      Picture         =   "frmMain.frx":28D80
      Top             =   1920
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   49
      Left            =   2040
      Picture         =   "frmMain.frx":29111
      Top             =   1920
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   48
      Left            =   1560
      Picture         =   "frmMain.frx":294A2
      Top             =   1920
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   47
      Left            =   1080
      Picture         =   "frmMain.frx":29833
      Top             =   1920
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   46
      Left            =   600
      Picture         =   "frmMain.frx":29BC4
      Top             =   1920
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   45
      Left            =   120
      Picture         =   "frmMain.frx":29F55
      Top             =   1920
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   44
      Left            =   6840
      Picture         =   "frmMain.frx":2A2E6
      Top             =   1440
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   43
      Left            =   6360
      Picture         =   "frmMain.frx":2A677
      Top             =   1440
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   42
      Left            =   5880
      Picture         =   "frmMain.frx":2AA08
      Top             =   1440
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   41
      Left            =   5400
      Picture         =   "frmMain.frx":2AD99
      Top             =   1440
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   40
      Left            =   4920
      Picture         =   "frmMain.frx":2B12A
      Top             =   1440
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   39
      Left            =   4440
      Picture         =   "frmMain.frx":2B4BB
      Top             =   1440
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   38
      Left            =   3960
      Picture         =   "frmMain.frx":2B84C
      Top             =   1440
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   37
      Left            =   3480
      Picture         =   "frmMain.frx":2BBDD
      Top             =   1440
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   36
      Left            =   3000
      Picture         =   "frmMain.frx":2BF6E
      Top             =   1440
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   35
      Left            =   2520
      Picture         =   "frmMain.frx":2C2FF
      Top             =   1440
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   34
      Left            =   2040
      Picture         =   "frmMain.frx":2C690
      Top             =   1440
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   33
      Left            =   1560
      Picture         =   "frmMain.frx":2CA21
      Top             =   1440
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   32
      Left            =   1080
      Picture         =   "frmMain.frx":2CDB2
      Top             =   1440
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   31
      Left            =   600
      Picture         =   "frmMain.frx":2D143
      Top             =   1440
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   30
      Left            =   120
      Picture         =   "frmMain.frx":2D4D4
      Top             =   1440
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   29
      Left            =   6840
      Picture         =   "frmMain.frx":2D865
      Top             =   960
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   28
      Left            =   6360
      Picture         =   "frmMain.frx":2DBF6
      Top             =   960
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   27
      Left            =   5880
      Picture         =   "frmMain.frx":2DF87
      Top             =   960
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   26
      Left            =   5400
      Picture         =   "frmMain.frx":2E318
      Top             =   960
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   25
      Left            =   4920
      Picture         =   "frmMain.frx":2E6A9
      Top             =   960
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   24
      Left            =   4440
      Picture         =   "frmMain.frx":2EA3A
      Top             =   960
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   23
      Left            =   3960
      Picture         =   "frmMain.frx":2EDCB
      Top             =   960
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   22
      Left            =   3480
      Picture         =   "frmMain.frx":2F15C
      Top             =   960
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   21
      Left            =   3000
      Picture         =   "frmMain.frx":2F4ED
      Top             =   960
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   20
      Left            =   2520
      Picture         =   "frmMain.frx":2F87E
      Top             =   960
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   19
      Left            =   2040
      Picture         =   "frmMain.frx":2FC0F
      Top             =   960
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   18
      Left            =   1560
      Picture         =   "frmMain.frx":2FFA0
      Top             =   960
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   17
      Left            =   1080
      Picture         =   "frmMain.frx":30331
      Top             =   960
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   16
      Left            =   600
      Picture         =   "frmMain.frx":306C2
      Top             =   960
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   15
      Left            =   120
      Picture         =   "frmMain.frx":30A53
      Top             =   960
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   14
      Left            =   6840
      Picture         =   "frmMain.frx":30DE4
      Top             =   480
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   13
      Left            =   6360
      Picture         =   "frmMain.frx":31175
      Top             =   480
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   12
      Left            =   5880
      Picture         =   "frmMain.frx":31506
      Top             =   480
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   11
      Left            =   5400
      Picture         =   "frmMain.frx":31897
      Top             =   480
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   10
      Left            =   4920
      Picture         =   "frmMain.frx":31C28
      Top             =   480
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   9
      Left            =   4440
      Picture         =   "frmMain.frx":31FB9
      Top             =   480
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   8
      Left            =   3960
      Picture         =   "frmMain.frx":3234A
      Top             =   480
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   7
      Left            =   3480
      Picture         =   "frmMain.frx":326DB
      Top             =   480
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   6
      Left            =   3000
      Picture         =   "frmMain.frx":32A6C
      Top             =   480
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   5
      Left            =   2520
      Picture         =   "frmMain.frx":32DFD
      Top             =   480
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   4
      Left            =   2040
      Picture         =   "frmMain.frx":3318E
      Top             =   480
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   3
      Left            =   1560
      Picture         =   "frmMain.frx":3351F
      Top             =   480
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   2
      Left            =   1080
      Picture         =   "frmMain.frx":338B0
      Top             =   480
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   1
      Left            =   600
      Picture         =   "frmMain.frx":33C41
      Top             =   480
      Width           =   480
   End
   Begin VB.Image grid 
      Height          =   480
      Index           =   0
      Left            =   120
      Picture         =   "frmMain.frx":33FD2
      Top             =   480
      Width           =   480
   End
   Begin VB.Image imgTile 
      Height          =   480
      Left            =   -360
      Picture         =   "frmMain.frx":34363
      Top             =   8160
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgCowB 
      Height          =   480
      Left            =   -360
      Picture         =   "frmMain.frx":346F4
      Top             =   9120
      Visible         =   0   'False
      Width           =   480
   End
   Begin VB.Image imgCowA 
      Height          =   480
      Left            =   -360
      Picture         =   "frmMain.frx":34B64
      Top             =   8640
      Visible         =   0   'False
      Width           =   480
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'Cow Sploder by Rachel J. Morris, February 2007~

'Global Variables!
'Because I'm too lazy to re-learn how to do classes with VB.
Dim isServer As Boolean
Dim iScoreServer As Integer
Dim iScoreClient As Integer
Dim notConnected As Boolean
Dim iFrame As Integer
Dim iMove As Integer
Dim yourTurn As Boolean
Dim iCowCount As Integer
Dim boolConnected As Boolean

Dim iGridCode(-1 To 225) As Integer
Dim cowX(1 To 20) As Integer
Dim cowY(1 To 20) As Integer


Private Sub cmdDisconnect_Click()
If boolConnected = True Then
    Dim sTempMessage As String
    sTempMessage = "(COMP) PLAYER HAS LEFT GAME"
    Winsock1.SendData sTempMessage
    tmrQuit.Enabled = True
Else
    End
End If
End Sub

Private Sub cmdSendMessage_Click()
If boolConnected = False Then
    MsgBox ("you are not connected")
Else
    'send message
    Dim sTempMessage As String
    sTempMessage = txtMessage.Text
    Winsock1.SendData sTempMessage
    
    'display in your chatbox
    If isServer = True Then
        lblServerTalk.Caption = sTempMessage
    Else
        lblClientTalk.Caption = sTempMessage
    End If
    
    'reset chatbox
    txtMessage.Text = ""
    txtMessage.SetFocus
End If
End Sub

Private Sub cmdStartConnection_Click()
If optServer.Value = True Then 'player is server
    isServer = True
    Winsock1.LocalPort = txtPort.Text
    Winsock1.Listen
    fraOnline.Visible = False
    fraChat.Visible = True
    frmMain.Caption = "Cow Splode     -     SERVER: LISTENING FOR CLIENT"
Else 'player is client
    isServer = False
    fraOnline.Visible = False
    fraChat.Visible = True
    Winsock1.RemoteHost = txtIP.Text
    Winsock1.RemotePort = txtPort.Text
    Winsock1.Connect
    frmMain.Caption = "Cow Splode     -     CLIENT: CONNECTING TO HOST"
End If  'If optServer.Value = True Then
End Sub



Private Sub Form_Load()
yourTurn = False    'YOU CAN'T MOVE, because you're not connected o.o
iCowCount = 0
Randomize

iScoreServer = 0
iScoreClient = 0

iFrame = 0

Dim index As Integer
index = 0

Do While index < 225
    iGridCode(index) = 0
    
    index = index + 1
Loop

Call generateBeginningCows
Call drawTiles
End Sub

Private Sub grid_Click(index As Integer)
If yourTurn = True And boolConnected = True Then
    If iGridCode(index) = 0 Or iGridCode(index) = 2 Then
        lblStatus.Caption = "YOU HAVE TO CLICK A COW"
        lblStatus.BackColor = vbBlue
    Else
        Dim sTempMessage As String
        sTempMessage = "C" & index
        Winsock1.SendData sTempMessage
        yourTurn = False
        
        Dim iCowsExploded As Integer
        iCowsExploded = 1
        
        iGridCode(index) = 2
        
        'check surrounding tiles to see if they were killed.
        'first check to make sure they're not edge places
        
        '-16,   -15,    -14
        ' -1,     i,    +1
        '+14,   +15,    +16
        
        'ROW ABOVE CLICKED
        If index > 14 Then
            If (index - 16) >= 0 Then
                If iGridCode(index - 16) = 1 And (index Mod 15) <> 0 Then
                    iGridCode(index - 16) = 2
                    iCowsExploded = iCowsExploded + 1
                End If
            End If
            If iGridCode(index - 15) = 1 And (index > 14) Then
                iGridCode(index - 15) = 2
                iCowsExploded = iCowsExploded + 1
            End If
            If iGridCode(index - 14) = 1 And ((index + 1) Mod 15) <> 0 Then
                iGridCode(index - 14) = 2
                iCowsExploded = iCowsExploded + 1
            End If
            
        End If
        
        'ROW BELOW CLICKED
        If index < 210 Then
            If index + 16 < 224 Then
                If iGridCode(index + 16) = 1 And ((index + 1) Mod 15) <> 0 Then
                    iGridCode(index + 16) = 2
                    iCowsExploded = iCowsExploded + 1
                End If
            End If
            If iGridCode(index + 15) = 1 And (index < 210) Then
                iGridCode(index + 15) = 2
                iCowsExploded = iCowsExploded + 1
            End If
            If iGridCode(index + 14) = 1 And ((index + 1) Mod 15) <> 0 Then
                iGridCode(index + 14) = 2
                iCowsExploded = iCowsExploded + 1
            End If
        End If
        
        'TO LEFT AND RIGHT OF CLICKED TILE
         If iGridCode(index + 1) = 1 And ((index + 1) Mod 15) <> 0 Then
            iGridCode(index + 1) = 2
            iCowsExploded = iCowsExploded + 1
        End If
         If iGridCode(index - 1) = 1 And (index Mod 15) <> 0 Then
            iGridCode(index - 1) = 2
            iCowsExploded = iCowsExploded + 1
        End If
        
        If isServer = True Then
            iScoreServer = iScoreServer + iCowsExploded
            lblServerScore.Caption = iScoreServer
        Else
            iScoreClient = iScoreClient + iCowsExploded
            lblClientScore.Caption = iScoreClient
        End If
        
        iCowCount = iCowCount - iCowsExploded
        
        If iCowCount <= 0 Then
            'win
            Call drawTiles
            If isServer = True Then
                If iScoreServer > iScoreClient Then
                    lblStatus.Caption = "YOU WIN!"
                    lblStatus.BackColor = vbYellow
                    lblStatus.ForeColor = vbBlack
                Else
                    lblStatus.Caption = "YOU LOSE!"
                    lblStatus.BackColor = vbYellow
                    lblStatus.ForeColor = vbBlack
                End If
            Else
                If iScoreServer < iScoreClient Then
                    lblStatus.Caption = "YOU WIN!"
                    lblStatus.BackColor = vbYellow
                    lblStatus.ForeColor = vbBlack
                Else
                    lblStatus.Caption = "YOU LOSE!"
                    lblStatus.BackColor = vbYellow
                    lblStatus.ForeColor = vbBlack
                End If
            End If
        Else
            Call drawTiles
            Call itIsNotYourTurn
        End If
        
    End If
Else
    lblStatus.Caption = "IT IS NOT YOUR TURN"
    lblStatus.BackColor = vbBlue
End If
End Sub

Private Sub optClient_Click()
txtIP.Enabled = True
End Sub

Private Sub optServer_Click()
txtIP.Enabled = False
txtIP.Text = Winsock1.LocalIP
End Sub

Private Sub tmrQuit_Timer()
End
End Sub

Private Sub txtMessage_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyReturn And boolConnected = True Then
    'send message
    Dim sTempMessage As String
    sTempMessage = txtMessage.Text
    Winsock1.SendData sTempMessage
    
    'display in your chatbox
    If isServer = True Then
        lblServerTalk.Caption = sTempMessage
    Else
        lblClientTalk.Caption = sTempMessage
    End If
    
    'reset chatbox
    txtMessage.Text = ""
    txtMessage.SetFocus
ElseIf KeyCode = vbKeyReturn And boolConnected = False Then
    MsgBox ("you are not connected")
End If
End Sub

Private Sub Winsock1_Connect()
frmMain.Caption = "Cow Splode     -     CLIENT: CONNECTED TO IP " & txtIP.Text & " / PORT " & txtPort.Text
End Sub

Private Sub Winsock1_ConnectionRequest(ByVal requestID As Long)
If Winsock1.State <> sckClosed Then Winsock1.Close  'make sure not already running

Winsock1.Accept requestID

frmMain.Caption = "Cow Splode     -     SERVER: CLIENT CONNECTED FROM " & requestID

'send client the cow map
Dim sCowMap As String
sCowMap = ""
Dim index As Integer
index = 0
Do While (index < 225)
    If (index = 0) Then
        sCowMap = "M" & iGridCode(index)
    Else
        sCowMap = sCowMap & iGridCode(index)
    End If
    index = index + 1
Loop

boolConnected = True

Winsock1.SendData sCowMap

yourTurn = True
Call itIsYourTurnNow
End Sub

Private Sub Winsock1_DataArrival(ByVal bytesTotal As Long)
Dim sMessageBuffer As String
Winsock1.GetData sMessageBuffer

Dim sMapUpdate As String
sMapUpdate = "M*"
Dim sDisconnect As String
sDisconnect = "(COMP)*"
Dim sOpponantHasGone As String
sOpponantHasGone = "C*"

If sMessageBuffer Like sMapUpdate Then  'updating map
    boolConnected = True
    Dim index As Integer
    index = 0
    Do While (index < 225)
        'MAP         012345678
        'FILE       M010010010
        'CHAR       1234567890
        
        'index - 2 is the start point of the string/chararray
        '1 is the length
        'The map's code actually starts on character 2
        iGridCode(index) = Mid(sMessageBuffer, index + 2, 1)
        index = index + 1
    Loop
    drawTiles
ElseIf sMessageBuffer Like sDisconnect Then 'opponant disconnected
notConnected = True
boolConnected = False

ElseIf sMessageBuffer Like sOpponantHasGone Then    'opponant went
    Dim xindex As Integer
    xindex = Right(sMessageBuffer, Len(sMessageBuffer) - 1)  'Take everything from the very right of the message down to the second to last character
    yourTurn = True
    
    'check what the person hit
    Dim iCowsExploded As Integer
    iCowsExploded = 1
    
    'check surrounding tiles to see if they were killed.
    'first check to make sure they're not edge places
    
    iGridCode(xindex) = 2
    
    'ROW ABOVE CLICKED
    If xindex > 14 Then
        If iGridCode(xindex - 16) = 1 And (xindex Mod 15) <> 0 Then
            iGridCode(xindex - 16) = 2
            iCowsExploded = iCowsExploded + 1
        End If
        If iGridCode(xindex - 15) = 1 And (xindex > 14) Then
            iGridCode(xindex - 15) = 2
            iCowsExploded = iCowsExploded + 1
        End If
        If iGridCode(xindex - 14) = 1 And ((xindex + 1) Mod 15) <> 0 Then
            iGridCode(xindex - 14) = 2
            iCowsExploded = iCowsExploded + 1
        End If
    End If
    
    'ROW BELOW CLICKED
    If xindex < 210 Then
        If iGridCode(xindex + 16) = 1 And ((xindex + 1) Mod 15) <> 0 Then
            iGridCode(xindex + 16) = 2
            iCowsExploded = iCowsExploded + 1
        End If
        If iGridCode(xindex + 15) = 1 And (xindex < 210) Then
            iGridCode(xindex + 15) = 2
            iCowsExploded = iCowsExploded + 1
        End If
        If iGridCode(xindex + 14) = 1 And (xindex Mod 15) <> 0 Then
            iGridCode(xindex + 14) = 2
            iCowsExploded = iCowsExploded + 1
        End If
    End If
    
    'TO LEFT AND RIGHT OF CLICKED TILE
     If iGridCode(xindex + 1) = 1 And ((xindex + 1) Mod 15) <> 0 Then
        iGridCode(xindex + 1) = 2
        iCowsExploded = iCowsExploded + 1
    End If
     If iGridCode(xindex - 1) = 1 And (xindex Mod 15) <> 0 Then
        iGridCode(xindex - 1) = 2
        iCowsExploded = iCowsExploded + 1
    End If
    
    If isServer = True Then
        iScoreClient = iScoreClient + iCowsExploded
        lblClientScore.Caption = iScoreClient
    Else
        iScoreServer = iScoreServer + iCowsExploded
        lblServerScore.Caption = iScoreServer
    End If
    
    iCowCount = iCowCount - iCowsExploded
    
    If iCowCount <= 0 Then
        'win
        Call drawTiles
        If isServer = True Then
            If iScoreServer > iScoreClient Then
                lblStatus.Caption = "YOU WIN!"
                lblStatus.BackColor = vbYellow
                lblStatus.ForeColor = vbBlack
            Else
                lblStatus.Caption = "YOU LOSE!"
                lblStatus.BackColor = vbYellow
                lblStatus.ForeColor = vbBlack
            End If
        Else
            If iScoreServer < iScoreClient Then
                lblStatus.Caption = "YOU WIN!"
                lblStatus.BackColor = vbYellow
                lblStatus.ForeColor = vbBlack
            Else
                lblStatus.Caption = "YOU LOSE!"
                lblStatus.BackColor = vbYellow
                lblStatus.ForeColor = vbBlack
            End If
        End If
    Else
        Call drawTiles
        Call itIsNotYourTurn
    End If
    
    
    Call drawTiles
    
    Call itIsYourTurnNow

Else 'talking
    If isServer = True Then     'client talking
        lblClientTalk.Caption = sMessageBuffer
    Else                        'server talking
        lblServerTalk.Caption = sMessageBuffer
    End If
End If

End Sub

Private Sub Winsock1_Error(ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
    MsgBox "Error..." & vbCrLf & Number & vbCrLf & Description
End Sub

Private Sub generateBeginningCows()
Dim iRand As Integer
Dim index As Integer
index = 0

Do While (index < 225)
    iRand = Rnd(2) 'generate a random x coordinate for cow [index]
    If iRand = 1 Then
        iGridCode(index) = 1
        iCowCount = iCowCount + 1
    End If
    index = index + 1
Loop
End Sub

Private Sub drawTiles()
Dim index As Integer
index = 0

Do While (index < 225)
    If iGridCode(index) = 0 Then
        grid(index).Picture = imgTile.Picture
    ElseIf iGridCode(index) = 1 Then
        grid(index).Picture = imgCowA.Picture
    ElseIf iGridCode(index) = 2 Then
        grid(index).Picture = imgBlood.Picture
    End If
    
    index = index + 1
Loop
End Sub

Private Sub itIsYourTurnNow()
lblStatus.Caption = "YOUR TURN! -- click a cow to splode it"
lblStatus.BackColor = vbRed
End Sub

Private Sub itIsNotYourTurn()
lblStatus.Caption = "ENEMY'S TURN!"
lblStatus.BackColor = vbBlack
End Sub
