VERSION 5.00
Object = "{248DD890-BB45-11CF-9ABC-0080C7E7B78D}#1.0#0"; "MSWINSCK.ocx"
Begin VB.Form frmMain 
   Caption         =   "ChatChien - Multi client chat by Rachel J. Morris"
   ClientHeight    =   3990
   ClientLeft      =   165
   ClientTop       =   855
   ClientWidth     =   5415
   LinkTopic       =   "Form1"
   ScaleHeight     =   3990
   ScaleWidth      =   5415
   StartUpPosition =   3  'Windows Default
   Begin VB.ListBox lstStatus 
      Height          =   1815
      Left            =   120
      TabIndex        =   16
      Top             =   120
      Width           =   2055
   End
   Begin VB.CommandButton cmdSend 
      Caption         =   "Send"
      Height          =   375
      Left            =   4440
      TabIndex        =   11
      Top             =   2400
      Width           =   855
   End
   Begin VB.TextBox txtMessage 
      Height          =   405
      Left            =   120
      TabIndex        =   10
      Top             =   2400
      Width           =   4215
   End
   Begin VB.Frame fraOptions 
      Caption         =   "Options"
      Height          =   2175
      Left            =   2280
      TabIndex        =   1
      Top             =   120
      Width           =   3015
      Begin VB.CommandButton cmdExit 
         Caption         =   "E&xit"
         Height          =   255
         Left            =   1560
         TabIndex        =   13
         Top             =   1800
         Width           =   1335
      End
      Begin VB.CommandButton cmdConnect 
         Caption         =   "Connect!"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   1800
         Width           =   1335
      End
      Begin VB.TextBox txtPort 
         Height          =   285
         Left            =   720
         TabIndex        =   9
         Text            =   "2000"
         Top             =   1440
         Width           =   2175
      End
      Begin VB.TextBox txtIP 
         Height          =   285
         Left            =   720
         TabIndex        =   7
         Text            =   "127.0.0.1"
         Top             =   1080
         Width           =   2175
      End
      Begin VB.TextBox txtUsername 
         Height          =   285
         Left            =   720
         TabIndex        =   5
         Text            =   "user"
         Top             =   600
         Width           =   2175
      End
      Begin VB.OptionButton optClient 
         Caption         =   "Client"
         Height          =   255
         Left            =   1440
         TabIndex        =   3
         Top             =   240
         Value           =   -1  'True
         Width           =   1455
      End
      Begin VB.OptionButton optServer 
         Caption         =   "Server"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label Label3 
         Caption         =   "Port"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   1440
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "IP"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Name"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   600
         Width           =   855
      End
   End
   Begin VB.ListBox lstConversation 
      Height          =   1035
      Left            =   120
      TabIndex        =   0
      Top             =   2880
      Width           =   5175
   End
   Begin MSWinsockLib.Winsock wnsckConnect 
      Left            =   6360
      Top             =   1320
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
      RemotePort      =   2000
   End
   Begin MSWinsockLib.Winsock wnsckClient 
      Index           =   0
      Left            =   6360
      Top             =   120
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
      RemotePort      =   2000
      LocalPort       =   2000
   End
   Begin MSWinsockLib.Winsock wnsckClient 
      Index           =   1
      Left            =   6840
      Top             =   120
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
      RemotePort      =   2000
      LocalPort       =   2000
   End
   Begin MSWinsockLib.Winsock wnsckClient 
      Index           =   2
      Left            =   7320
      Top             =   120
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
      RemotePort      =   2000
      LocalPort       =   2000
   End
   Begin MSWinsockLib.Winsock wnsckClient 
      Index           =   3
      Left            =   7800
      Top             =   120
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
      RemotePort      =   2000
      LocalPort       =   2000
   End
   Begin MSWinsockLib.Winsock wnsckClient 
      Index           =   4
      Left            =   6360
      Top             =   600
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
      RemotePort      =   2000
      LocalPort       =   2000
   End
   Begin MSWinsockLib.Winsock wnsckClient 
      Index           =   5
      Left            =   6840
      Top             =   600
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
      RemotePort      =   2000
      LocalPort       =   2000
   End
   Begin MSWinsockLib.Winsock wnsckClient 
      Index           =   6
      Left            =   7320
      Top             =   600
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
      RemotePort      =   2000
      LocalPort       =   2000
   End
   Begin MSWinsockLib.Winsock wnsckClient 
      Index           =   7
      Left            =   7800
      Top             =   600
      _ExtentX        =   741
      _ExtentY        =   741
      _Version        =   393216
      RemotePort      =   2000
      LocalPort       =   2000
   End
   Begin VB.Label lblStatus 
      Caption         =   "Disconnected"
      Height          =   255
      Left            =   840
      TabIndex        =   15
      Top             =   2040
      Width           =   1335
   End
   Begin VB.Label Label4 
      Caption         =   "Status:"
      Height          =   255
      Left            =   120
      TabIndex        =   14
      Top             =   2040
      Width           =   1335
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuExit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuAbout 
         Caption         =   "&About"
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
' VISUAL BASIC 6.0 CHAT PROGRAM (multiple client support)
' Coded by Rachel J. Morris (c) 2008
Option Explicit
Dim strUsername As String
Dim blnConnected As Boolean
Dim blnIsServer As Boolean
Dim blnClientConnected(7) As Boolean

Private Sub cmdConnect_Click()
If optServer.Value = True Then      'HOST CODE
    Dim i As Integer
    i = 0
    For i = 0 To 7
        wnsckClient(i).LocalPort = txtPort.Text + i
        wnsckClient(i).Listen
        lstStatus.AddItem "Listening on port " & txtPort.Text + i
    Next i
    blnConnected = True
    lblStatus.Caption = "Connected"
    cmdConnect.Enabled = False
    blnIsServer = True
Else                                'CLIENT CODE
    wnsckConnect.RemoteHost = txtIP.Text
    wnsckConnect.RemotePort = txtPort.Text
    wnsckConnect.Connect
    blnConnected = True
    cmdConnect.Enabled = False
End If
End Sub

Private Sub cmdExit_Click()
End
End Sub

Private Sub cmdSend_Click()
Dim tempMessage As String
Dim i As Integer
tempMessage = txtUsername.Text & ": " & txtMessage.Text
If optServer.Value = True Then      'SERVER
    For i = 0 To 7
        If blnClientConnected(i) = True Then
            wnsckClient(i).SendData tempMessage
        End If
    Next i
    lstConversation.AddItem tempMessage, 0
Else                                'CLIENT
    wnsckConnect.SendData tempMessage
End If
txtMessage.Text = ""
txtMessage.SetFocus
End Sub

Private Sub Form_Load()
'initialize
Dim i As Integer
For i = 0 To 7
    blnClientConnected(i) = False
Next i
End Sub

Private Sub mnuAbout_Click()
MsgBox ("ChatChien - Multi-client chat program made in Visual Basic 6.0" & vbCrLf & "By Rachel J. Morris, 2008" & vbCrLf & "Feel free to use in any way, no credit necessary :)")
End Sub

Private Sub mnuExit_Click()
End
End Sub

Private Sub optClient_Click()
txtIP.Enabled = True
cmdConnect.Caption = "Connect!"
End Sub

Private Sub optServer_Click()
cmdConnect.Caption = "Host!"
txtIP.Enabled = False
End Sub

Private Sub wnsckClient_ConnectionRequest(Index As Integer, ByVal requestID As Long)
'SERVER CODE
If wnsckClient(Index).State <> sckClosed Then
    wnsckClient(Index).Close
End If
wnsckClient(Index).Accept requestID
lstConversation.AddItem "user connected"
blnClientConnected(Index) = True
'Now send this to the other clients
Dim i As Integer
For i = 0 To 7
    If blnClientConnected(i) = True Then
        wnsckClient(i).SendData "user connected"
    End If
Next i
End Sub

Private Sub wnsckClient_DataArrival(Index As Integer, ByVal bytesTotal As Long)
'SERVER CODE
Dim receivedData As String
wnsckClient(Index).GetData receivedData, vbString
lstConversation.AddItem receivedData, 0
'Now send this to the other clients
Dim i As Integer
For i = 0 To 7
    If blnClientConnected(i) = True Then
        wnsckClient(i).SendData receivedData
    End If
Next i
End Sub

Private Sub wnsckClient_Error(Index As Integer, ByVal Number As Integer, Description As String, ByVal Scode As Long, ByVal Source As String, ByVal HelpFile As String, ByVal HelpContext As Long, CancelDisplay As Boolean)
'SERVER CODE
blnConnected = False
lblStatus.Caption = "Error!"
Exit Sub
End Sub

Private Sub wnsckConnect_Connect()
lblStatus.Caption = "Connected"
End Sub

Private Sub wnsckConnect_ConnectionRequest(ByVal requestID As Long)
'CLIENT CODE
If wnsckConnect.State <> sckClosed Then
    wnsckConnect.Close
End If
wnsckConnect.Accept requestID
End Sub

Private Sub wnsckConnect_DataArrival(ByVal bytesTotal As Long)
'CLIENT CODE
Dim receivedData As String
wnsckConnect.GetData receivedData, vbString
lstConversation.AddItem receivedData, 0
End Sub

